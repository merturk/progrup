﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using Global;


namespace GABI_CRM
{
    public partial class frmTekSonucu : DevExpress.XtraEditors.XtraForm
    {
        public frmTekSonucu()
        {
            InitializeComponent();
        }

        const string GP_TYPE = "TSnc";
        const string FTITLE  = "Teklif Sonuçları";

        SqlDataAdapter SqlDap;
        SqlCommandBuilder SqlScb;
        DataTable dtGenel;

        private void GetList()
        {
            this.Text = FTITLE;
            Cursor.Current = Cursors.WaitCursor;
            
            string Sql =string.Format(@"SELECT  * FROM  GEN_PARAMS Where GP_TYPE='{0}'", GP_TYPE);
                SqlDap = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenel = new DataTable();
                SqlScb = new SqlCommandBuilder(SqlDap);
                SqlScb.DataAdapter.Fill(dtGenel);
            grd.DataSource = dtGenel;


            Cursor.Current = Cursors.Default;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textEdit1.Focus();
            DataView _dv = new DataView(dtGenel);
            for (int i = 0; i < _dv.Count; i++)
            {
                string Ulke = _dv[i]["GP_VALUE_STR1"].ToString();
                if (string.IsNullOrEmpty(Ulke))
                {
                    MessageBox.Show("Kod tanımını boş geçemezsiniz.");
                    return;
                }
               
            }

            if (Glob.ControlDoubleRows(_dv, "GP_VALUE_STR1") == true) { return; }
            
            SqlScb.DataAdapter.Update(dtGenel);
            MessageBox.Show("Veriler güncellendi.");

        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grd.ShowPrintPreview();
        }

        private void grdV_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {

         

            }

            if (e.KeyCode == Keys.Insert)
            {
                     
          

            }
          
        }

        private void frmUlkeler_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void btnEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textEdit1.Focus();
            grdV.AddNewRow();
            grdV.SetRowCellValue(grdV.FocusedRowHandle, grdV.Columns["GP_TYPE"], GP_TYPE);
            grdV.FocusedRowHandle = grdV.GetVisibleRowHandle(grdV.FocusedRowHandle);
           // grd.RefreshDataSource();
        }

        private void btnSil_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            grdV.DeleteRow(grdV.FocusedRowHandle);
        //    grd.RefreshDataSource();
        }


    }
}