﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;
using System.Collections;
using DevExpress.XtraReports.UI;
using BTI;

namespace GABI_CRM
{
    public partial class frmMusteriListesi : DevExpress.XtraEditors.XtraForm
    {
        public frmMusteriListesi()
        {
            InitializeComponent();
        }
        const string FTITLE = "Müşteri Listesi";

        private static string SelectedNo, SelectedEmail, SelectedCari;

        private void GetSelectedRows()
        {
            SelectedNo = "";
            SelectedEmail = "";
            SelectedCari = "";
            ArrayList rows = new ArrayList();
            // Add the selected rows to the list.
            for (int i = 0; i < grdV.SelectedRowsCount; i++)
            {
                if (grdV.GetSelectedRows()[i] >= 0)
                    rows.Add(grdV.GetDataRow(grdV.GetSelectedRows()[i]));
            }
            try
            {
                grdV.BeginUpdate();
                for (int i = 0; i < rows.Count; i++)
                {
                    DataRow row = rows[i] as DataRow;
                    if (!string.IsNullOrEmpty(row["EmpID"].ToString()))
                        SelectedNo = i == 0 ? row["EmpID"].ToString() : SelectedNo + "," + row["EmpID"].ToString();
                    if (row["Email"].ToString().Length>=5)                   
                        SelectedEmail =row["Email"].ToString()+","+ SelectedEmail ;
                    if (row["Adi"].ToString().Length >= 5)
                        SelectedCari = row["Adi"].ToString() + "," + SelectedCari;
                }

            }
            finally
            {
                grdV.EndUpdate();
            }

        }

        private void GetList()
        {
            this.Text = FTITLE;
            var dt = new DataTable();
       
            dt = DbConnSql.SelectSQL(@"Select * From  dbo.MusteriListesi Where Type=1");
            grd.DataSource = dt;

        }

        private void frmMusteriListesi_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grd.ShowPrintPreview();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            
            
            GetSelectedRows();
            RepMusteriEtiket.EmpId = SelectedNo;
            var report = new RepMusteriEtiket();
            report.ShowPreview();

        }

        private void btnAttach_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PubObjects.RowHandle(grdV);
            string MusKodu = grdV.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();
            if (string.IsNullOrEmpty(MusKodu))
            {
                MessageBox.Show("Müşteri kodu seçilmeden kayıt ekleme yapamazsınız.");
                return;
            }
            Attach_File.MusteriNo = MusKodu;
            var frm = new Attach_File();
            frm.ShowDialog();
            PubObjects.SetBookmark(grdV);
            
        }

        private void btnEmail_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            try
            {


                string MusKodu = grdV.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();

                if (string.IsNullOrEmpty(MusKodu))
                {
                    MessageBox.Show("Müşteri kodu seçilmeden kayıt ekleme yapamazsınız.");
                    return;
                }

                GetSelectedRows();
                
                SendMail.SelectedEmailList = SelectedEmail;
                SendMail.SelectedCariList  = SelectedCari;
                var frm = new SendMail();
                frm.ShowDialog();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
      

        }

    }
}