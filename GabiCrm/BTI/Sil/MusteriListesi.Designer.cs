﻿namespace GABI_CRM
{
    partial class frmMusteriListesi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMusteriListesi));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.grd = new DevExpress.XtraGrid.GridControl();
            this.grdV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cMusteriKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cAdi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSektoru = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cIlce = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSehirKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cUlkeKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cYetkili = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cYetkiliPozisyonu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cTelefon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repLupUlke = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repLupSehir = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repLupIlce = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repSektor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.RepMemNereden = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repZiyNedeni = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutView2 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn11 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colID1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn12 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colMusteriKodu2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn13 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colAdi1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn14 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colSektoru1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn15 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colIlce1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn16 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colSehirKodu1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn17 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colUlkeKodu1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn18 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colAdres1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn19 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colVergiDairesi1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colMusteriKodu1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cAdi = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cSektoru = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cIlce = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cSehirKodu = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn8 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cUlkeKodu = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn9 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cAdres = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn10 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cVergiDairesi = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnAttach = new DevExpress.XtraBars.BarButtonItem();
            this.btnEmail = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupUlke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupSehir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupIlce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSektor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepMemNereden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repZiyNedeni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdi1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSektoru1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIlce1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSehirKodu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colUlkeKodu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdres1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colVergiDairesi1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSektoru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cIlce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSehirKodu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cUlkeKodu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cVergiDairesi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            this.SmallImage.Images.SetKeyName(43, "attach.png");
            this.SmallImage.Images.SetKeyName(44, "paper_clip.png");
            this.SmallImage.Images.SetKeyName(45, "note2.png");
            this.SmallImage.Images.SetKeyName(46, "barcode_arrow_up.png");
            this.SmallImage.Images.SetKeyName(47, "Document2.png");
            this.SmallImage.Images.SetKeyName(48, "Document2.png");
            this.SmallImage.Images.SetKeyName(49, "email.png");
            // 
            // grd
            // 
            this.grd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd.Location = new System.Drawing.Point(0, 25);
            this.grd.MainView = this.grdV;
            this.grd.Name = "grd";
            this.grd.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemPictureEdit1,
            this.repositoryItemImageEdit1,
            this.repositoryItemPictureEdit2,
            this.repLupUlke,
            this.repLupSehir,
            this.repLupIlce,
            this.repSektor,
            this.RepMemNereden,
            this.repZiyNedeni});
            this.grd.Size = new System.Drawing.Size(1255, 589);
            this.grd.TabIndex = 16;
            this.grd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdV,
            this.layoutView2,
            this.layoutView1});
            // 
            // grdV
            // 
            this.grdV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cID,
            this.cMusteriKodu,
            this.cAdi,
            this.cSektoru,
            this.cIlce,
            this.cSehirKodu,
            this.cUlkeKodu,
            this.cYetkili,
            this.cYetkiliPozisyonu,
            this.cTelefon,
            this.cEmail});
            this.grdV.GridControl = this.grd;
            this.grdV.Name = "grdV";
            this.grdV.OptionsBehavior.Editable = false;
            this.grdV.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdV.OptionsNavigation.AutoFocusNewRow = true;
            this.grdV.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdV.OptionsSelection.MultiSelect = true;
            this.grdV.OptionsView.ColumnAutoWidth = false;
            this.grdV.OptionsView.RowAutoHeight = true;
            this.grdV.OptionsView.ShowAutoFilterRow = true;
            this.grdV.OptionsView.ShowFooter = true;
            this.grdV.OptionsView.ShowGroupPanel = false;
            // 
            // cID
            // 
            this.cID.Caption = "ID";
            this.cID.FieldName = "ID";
            this.cID.Name = "cID";
            // 
            // cMusteriKodu
            // 
            this.cMusteriKodu.Caption = "Kodu";
            this.cMusteriKodu.FieldName = "MusteriKodu";
            this.cMusteriKodu.Name = "cMusteriKodu";
            this.cMusteriKodu.OptionsColumn.AllowEdit = false;
            this.cMusteriKodu.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cMusteriKodu.Visible = true;
            this.cMusteriKodu.VisibleIndex = 0;
            this.cMusteriKodu.Width = 84;
            // 
            // cAdi
            // 
            this.cAdi.Caption = "Firma Adı";
            this.cAdi.FieldName = "Adi";
            this.cAdi.Name = "cAdi";
            this.cAdi.OptionsColumn.AllowEdit = false;
            this.cAdi.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cAdi.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.cAdi.Visible = true;
            this.cAdi.VisibleIndex = 1;
            this.cAdi.Width = 118;
            // 
            // cSektoru
            // 
            this.cSektoru.Caption = "Sektorü";
            this.cSektoru.FieldName = "Sektoru";
            this.cSektoru.Name = "cSektoru";
            this.cSektoru.OptionsColumn.AllowEdit = false;
            this.cSektoru.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cSektoru.Visible = true;
            this.cSektoru.VisibleIndex = 4;
            this.cSektoru.Width = 133;
            // 
            // cIlce
            // 
            this.cIlce.Caption = "Ilçe";
            this.cIlce.FieldName = "Ilce";
            this.cIlce.Name = "cIlce";
            this.cIlce.OptionsColumn.AllowEdit = false;
            this.cIlce.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cIlce.Visible = true;
            this.cIlce.VisibleIndex = 5;
            this.cIlce.Width = 101;
            // 
            // cSehirKodu
            // 
            this.cSehirKodu.Caption = "Şehir";
            this.cSehirKodu.FieldName = "Sehir";
            this.cSehirKodu.Name = "cSehirKodu";
            this.cSehirKodu.OptionsColumn.AllowEdit = false;
            this.cSehirKodu.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cSehirKodu.Visible = true;
            this.cSehirKodu.VisibleIndex = 6;
            this.cSehirKodu.Width = 96;
            // 
            // cUlkeKodu
            // 
            this.cUlkeKodu.Caption = "Ülke";
            this.cUlkeKodu.FieldName = "Ulke";
            this.cUlkeKodu.Name = "cUlkeKodu";
            this.cUlkeKodu.OptionsColumn.AllowEdit = false;
            this.cUlkeKodu.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cUlkeKodu.Visible = true;
            this.cUlkeKodu.VisibleIndex = 7;
            this.cUlkeKodu.Width = 104;
            // 
            // cYetkili
            // 
            this.cYetkili.Caption = "Yetkili";
            this.cYetkili.FieldName = "Yetkili";
            this.cYetkili.Name = "cYetkili";
            this.cYetkili.OptionsColumn.AllowEdit = false;
            this.cYetkili.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cYetkili.Visible = true;
            this.cYetkili.VisibleIndex = 2;
            // 
            // cYetkiliPozisyonu
            // 
            this.cYetkiliPozisyonu.Caption = "Yetkili Pozisyonu";
            this.cYetkiliPozisyonu.FieldName = "YetkiliPozisyonu";
            this.cYetkiliPozisyonu.Name = "cYetkiliPozisyonu";
            this.cYetkiliPozisyonu.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cYetkiliPozisyonu.Visible = true;
            this.cYetkiliPozisyonu.VisibleIndex = 3;
            this.cYetkiliPozisyonu.Width = 89;
            // 
            // cTelefon
            // 
            this.cTelefon.Caption = "Telefon";
            this.cTelefon.FieldName = "Telefon";
            this.cTelefon.Name = "cTelefon";
            this.cTelefon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cTelefon.ToolTip = "Yetkili kişi telefonu";
            this.cTelefon.Visible = true;
            this.cTelefon.VisibleIndex = 8;
            this.cTelefon.Width = 106;
            // 
            // cEmail
            // 
            this.cEmail.Caption = "Email";
            this.cEmail.FieldName = "Email";
            this.cEmail.Name = "cEmail";
            this.cEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cEmail.Visible = true;
            this.cEmail.VisibleIndex = 9;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.NullText = "";
            this.repositoryItemLookUpEdit2.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            // 
            // repLupUlke
            // 
            this.repLupUlke.AutoHeight = false;
            this.repLupUlke.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupUlke.DisplayMember = "Tanim";
            this.repLupUlke.Name = "repLupUlke";
            this.repLupUlke.NullText = "";
            this.repLupUlke.ValueMember = "Kod";
            // 
            // repLupSehir
            // 
            this.repLupSehir.AutoHeight = false;
            this.repLupSehir.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupSehir.DisplayMember = "Tanim";
            this.repLupSehir.Name = "repLupSehir";
            this.repLupSehir.NullText = "";
            this.repLupSehir.ValueMember = "Kod";
            // 
            // repLupIlce
            // 
            this.repLupIlce.AutoHeight = false;
            this.repLupIlce.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupIlce.DisplayMember = "Tanim";
            this.repLupIlce.Name = "repLupIlce";
            this.repLupIlce.NullText = "";
            this.repLupIlce.ValueMember = "Tanim";
            // 
            // repSektor
            // 
            this.repSektor.AutoHeight = false;
            this.repSektor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSektor.DisplayMember = "Tanim";
            this.repSektor.DropDownRows = 20;
            this.repSektor.Name = "repSektor";
            this.repSektor.NullText = "";
            this.repSektor.PopupWidth = 600;
            this.repSektor.ValueMember = "Kod";
            // 
            // RepMemNereden
            // 
            this.RepMemNereden.Appearance.Options.UseTextOptions = true;
            this.RepMemNereden.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.AppearanceDisabled.Options.UseTextOptions = true;
            this.RepMemNereden.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.AppearanceFocused.Options.UseTextOptions = true;
            this.RepMemNereden.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.AppearanceReadOnly.Options.UseTextOptions = true;
            this.RepMemNereden.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.Name = "RepMemNereden";
            // 
            // repZiyNedeni
            // 
            this.repZiyNedeni.AutoHeight = false;
            this.repZiyNedeni.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repZiyNedeni.DisplayMember = "Tanim";
            this.repZiyNedeni.Name = "repZiyNedeni";
            this.repZiyNedeni.NullText = "";
            this.repZiyNedeni.ValueMember = "Kod";
            // 
            // layoutView2
            // 
            this.layoutView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn11,
            this.layoutViewColumn12,
            this.layoutViewColumn13,
            this.layoutViewColumn14,
            this.layoutViewColumn15,
            this.layoutViewColumn16,
            this.layoutViewColumn17,
            this.layoutViewColumn18,
            this.layoutViewColumn19});
            this.layoutView2.GridControl = this.grd;
            this.layoutView2.Name = "layoutView2";
            this.layoutView2.OptionsSelection.MultiSelect = true;
            this.layoutView2.TemplateCard = null;
            // 
            // layoutViewColumn11
            // 
            this.layoutViewColumn11.Caption = "ID";
            this.layoutViewColumn11.FieldName = "ID";
            this.layoutViewColumn11.LayoutViewField = this.layoutViewField_colID1;
            this.layoutViewColumn11.Name = "layoutViewColumn11";
            // 
            // layoutViewField_colID1
            // 
            this.layoutViewField_colID1.EditorPreferredWidth = 129;
            this.layoutViewField_colID1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colID1.Name = "layoutViewField_colID1";
            this.layoutViewField_colID1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colID1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colID1.TextToControlDistance = 5;
            // 
            // layoutViewColumn12
            // 
            this.layoutViewColumn12.Caption = "Müşteri Kodu";
            this.layoutViewColumn12.FieldName = "MusteriKodu";
            this.layoutViewColumn12.LayoutViewField = this.layoutViewField_colMusteriKodu2;
            this.layoutViewColumn12.Name = "layoutViewColumn12";
            this.layoutViewColumn12.Width = 84;
            // 
            // layoutViewField_colMusteriKodu2
            // 
            this.layoutViewField_colMusteriKodu2.EditorPreferredWidth = 129;
            this.layoutViewField_colMusteriKodu2.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField_colMusteriKodu2.Name = "layoutViewField_colMusteriKodu2";
            this.layoutViewField_colMusteriKodu2.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colMusteriKodu2.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colMusteriKodu2.TextToControlDistance = 5;
            // 
            // layoutViewColumn13
            // 
            this.layoutViewColumn13.Caption = "Adı";
            this.layoutViewColumn13.FieldName = "Adi";
            this.layoutViewColumn13.LayoutViewField = this.layoutViewField_colAdi1;
            this.layoutViewColumn13.Name = "layoutViewColumn13";
            this.layoutViewColumn13.Width = 118;
            // 
            // layoutViewField_colAdi1
            // 
            this.layoutViewField_colAdi1.EditorPreferredWidth = 129;
            this.layoutViewField_colAdi1.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField_colAdi1.Name = "layoutViewField_colAdi1";
            this.layoutViewField_colAdi1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colAdi1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colAdi1.TextToControlDistance = 5;
            // 
            // layoutViewColumn14
            // 
            this.layoutViewColumn14.Caption = "Sektorü";
            this.layoutViewColumn14.FieldName = "Sektoru";
            this.layoutViewColumn14.LayoutViewField = this.layoutViewField_colSektoru1;
            this.layoutViewColumn14.Name = "layoutViewColumn14";
            this.layoutViewColumn14.Width = 133;
            // 
            // layoutViewField_colSektoru1
            // 
            this.layoutViewField_colSektoru1.EditorPreferredWidth = 129;
            this.layoutViewField_colSektoru1.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField_colSektoru1.Name = "layoutViewField_colSektoru1";
            this.layoutViewField_colSektoru1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colSektoru1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colSektoru1.TextToControlDistance = 5;
            // 
            // layoutViewColumn15
            // 
            this.layoutViewColumn15.Caption = "Ilçe";
            this.layoutViewColumn15.ColumnEdit = this.repLupIlce;
            this.layoutViewColumn15.FieldName = "Ilce";
            this.layoutViewColumn15.LayoutViewField = this.layoutViewField_colIlce1;
            this.layoutViewColumn15.Name = "layoutViewColumn15";
            this.layoutViewColumn15.Width = 101;
            // 
            // layoutViewField_colIlce1
            // 
            this.layoutViewField_colIlce1.EditorPreferredWidth = 129;
            this.layoutViewField_colIlce1.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField_colIlce1.Name = "layoutViewField_colIlce1";
            this.layoutViewField_colIlce1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colIlce1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colIlce1.TextToControlDistance = 5;
            // 
            // layoutViewColumn16
            // 
            this.layoutViewColumn16.Caption = "Şehir";
            this.layoutViewColumn16.ColumnEdit = this.repLupSehir;
            this.layoutViewColumn16.FieldName = "SehirKodu";
            this.layoutViewColumn16.LayoutViewField = this.layoutViewField_colSehirKodu1;
            this.layoutViewColumn16.Name = "layoutViewColumn16";
            this.layoutViewColumn16.Width = 96;
            // 
            // layoutViewField_colSehirKodu1
            // 
            this.layoutViewField_colSehirKodu1.EditorPreferredWidth = 129;
            this.layoutViewField_colSehirKodu1.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField_colSehirKodu1.Name = "layoutViewField_colSehirKodu1";
            this.layoutViewField_colSehirKodu1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colSehirKodu1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colSehirKodu1.TextToControlDistance = 5;
            // 
            // layoutViewColumn17
            // 
            this.layoutViewColumn17.Caption = "Ülke";
            this.layoutViewColumn17.ColumnEdit = this.repLupUlke;
            this.layoutViewColumn17.FieldName = "UlkeKodu";
            this.layoutViewColumn17.LayoutViewField = this.layoutViewField_colUlkeKodu1;
            this.layoutViewColumn17.Name = "layoutViewColumn17";
            this.layoutViewColumn17.Width = 104;
            // 
            // layoutViewField_colUlkeKodu1
            // 
            this.layoutViewField_colUlkeKodu1.EditorPreferredWidth = 129;
            this.layoutViewField_colUlkeKodu1.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField_colUlkeKodu1.Name = "layoutViewField_colUlkeKodu1";
            this.layoutViewField_colUlkeKodu1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colUlkeKodu1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colUlkeKodu1.TextToControlDistance = 5;
            // 
            // layoutViewColumn18
            // 
            this.layoutViewColumn18.Caption = "Adres";
            this.layoutViewColumn18.FieldName = "Adres";
            this.layoutViewColumn18.LayoutViewField = this.layoutViewField_colAdres1;
            this.layoutViewColumn18.Name = "layoutViewColumn18";
            this.layoutViewColumn18.Width = 165;
            // 
            // layoutViewField_colAdres1
            // 
            this.layoutViewField_colAdres1.EditorPreferredWidth = 129;
            this.layoutViewField_colAdres1.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField_colAdres1.Name = "layoutViewField_colAdres1";
            this.layoutViewField_colAdres1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colAdres1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colAdres1.TextToControlDistance = 5;
            // 
            // layoutViewColumn19
            // 
            this.layoutViewColumn19.Caption = "Vergi Dairesi";
            this.layoutViewColumn19.FieldName = "VergiDairesi";
            this.layoutViewColumn19.LayoutViewField = this.layoutViewField_colVergiDairesi1;
            this.layoutViewColumn19.Name = "layoutViewColumn19";
            this.layoutViewColumn19.Width = 87;
            // 
            // layoutViewField_colVergiDairesi1
            // 
            this.layoutViewField_colVergiDairesi1.EditorPreferredWidth = 129;
            this.layoutViewField_colVergiDairesi1.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField_colVergiDairesi1.Name = "layoutViewField_colVergiDairesi1";
            this.layoutViewField_colVergiDairesi1.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField_colVergiDairesi1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colVergiDairesi1.TextToControlDistance = 5;
            // 
            // layoutView1
            // 
            this.layoutView1.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn2,
            this.layoutViewColumn3,
            this.layoutViewColumn4,
            this.layoutViewColumn5,
            this.layoutViewColumn6,
            this.layoutViewColumn7,
            this.layoutViewColumn8,
            this.layoutViewColumn9,
            this.layoutViewColumn10});
            this.layoutView1.GridControl = this.grd;
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsSelection.MultiSelect = true;
            this.layoutView1.OptionsView.ShowCardCaption = false;
            this.layoutView1.TemplateCard = null;
            // 
            // layoutViewColumn2
            // 
            this.layoutViewColumn2.Caption = "ID";
            this.layoutViewColumn2.FieldName = "ID";
            this.layoutViewColumn2.LayoutViewField = this.layoutViewField_cID;
            this.layoutViewColumn2.Name = "layoutViewColumn2";
            // 
            // layoutViewField_cID
            // 
            this.layoutViewField_cID.EditorPreferredWidth = 253;
            this.layoutViewField_cID.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_cID.Name = "layoutViewField_cID";
            this.layoutViewField_cID.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cID.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cID.TextToControlDistance = 5;
            // 
            // layoutViewColumn3
            // 
            this.layoutViewColumn3.Caption = "Müşteri Kodu";
            this.layoutViewColumn3.FieldName = "MusteriKodu";
            this.layoutViewColumn3.LayoutViewField = this.layoutViewField_colMusteriKodu1;
            this.layoutViewColumn3.Name = "layoutViewColumn3";
            this.layoutViewColumn3.Width = 84;
            // 
            // layoutViewField_colMusteriKodu1
            // 
            this.layoutViewField_colMusteriKodu1.EditorPreferredWidth = 253;
            this.layoutViewField_colMusteriKodu1.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField_colMusteriKodu1.Name = "layoutViewField_colMusteriKodu1";
            this.layoutViewField_colMusteriKodu1.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_colMusteriKodu1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colMusteriKodu1.TextToControlDistance = 5;
            // 
            // layoutViewColumn4
            // 
            this.layoutViewColumn4.Caption = "Adı";
            this.layoutViewColumn4.FieldName = "Adi";
            this.layoutViewColumn4.LayoutViewField = this.layoutViewField_cAdi;
            this.layoutViewColumn4.Name = "layoutViewColumn4";
            this.layoutViewColumn4.Width = 118;
            // 
            // layoutViewField_cAdi
            // 
            this.layoutViewField_cAdi.EditorPreferredWidth = 253;
            this.layoutViewField_cAdi.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField_cAdi.Name = "layoutViewField_cAdi";
            this.layoutViewField_cAdi.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cAdi.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cAdi.TextToControlDistance = 5;
            // 
            // layoutViewColumn5
            // 
            this.layoutViewColumn5.Caption = "Sektorü";
            this.layoutViewColumn5.FieldName = "Sektoru";
            this.layoutViewColumn5.LayoutViewField = this.layoutViewField_cSektoru;
            this.layoutViewColumn5.Name = "layoutViewColumn5";
            this.layoutViewColumn5.Width = 133;
            // 
            // layoutViewField_cSektoru
            // 
            this.layoutViewField_cSektoru.EditorPreferredWidth = 253;
            this.layoutViewField_cSektoru.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField_cSektoru.Name = "layoutViewField_cSektoru";
            this.layoutViewField_cSektoru.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cSektoru.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cSektoru.TextToControlDistance = 5;
            // 
            // layoutViewColumn6
            // 
            this.layoutViewColumn6.Caption = "Ilçe";
            this.layoutViewColumn6.ColumnEdit = this.repLupIlce;
            this.layoutViewColumn6.FieldName = "Ilce";
            this.layoutViewColumn6.LayoutViewField = this.layoutViewField_cIlce;
            this.layoutViewColumn6.Name = "layoutViewColumn6";
            this.layoutViewColumn6.Width = 101;
            // 
            // layoutViewField_cIlce
            // 
            this.layoutViewField_cIlce.EditorPreferredWidth = 253;
            this.layoutViewField_cIlce.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField_cIlce.Name = "layoutViewField_cIlce";
            this.layoutViewField_cIlce.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cIlce.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cIlce.TextToControlDistance = 5;
            // 
            // layoutViewColumn7
            // 
            this.layoutViewColumn7.Caption = "Şehir";
            this.layoutViewColumn7.ColumnEdit = this.repLupSehir;
            this.layoutViewColumn7.FieldName = "SehirKodu";
            this.layoutViewColumn7.LayoutViewField = this.layoutViewField_cSehirKodu;
            this.layoutViewColumn7.Name = "layoutViewColumn7";
            this.layoutViewColumn7.Width = 96;
            // 
            // layoutViewField_cSehirKodu
            // 
            this.layoutViewField_cSehirKodu.EditorPreferredWidth = 253;
            this.layoutViewField_cSehirKodu.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField_cSehirKodu.Name = "layoutViewField_cSehirKodu";
            this.layoutViewField_cSehirKodu.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cSehirKodu.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cSehirKodu.TextToControlDistance = 5;
            // 
            // layoutViewColumn8
            // 
            this.layoutViewColumn8.Caption = "Ülke";
            this.layoutViewColumn8.ColumnEdit = this.repLupUlke;
            this.layoutViewColumn8.FieldName = "UlkeKodu";
            this.layoutViewColumn8.LayoutViewField = this.layoutViewField_cUlkeKodu;
            this.layoutViewColumn8.Name = "layoutViewColumn8";
            this.layoutViewColumn8.Width = 104;
            // 
            // layoutViewField_cUlkeKodu
            // 
            this.layoutViewField_cUlkeKodu.EditorPreferredWidth = 253;
            this.layoutViewField_cUlkeKodu.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField_cUlkeKodu.Name = "layoutViewField_cUlkeKodu";
            this.layoutViewField_cUlkeKodu.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cUlkeKodu.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cUlkeKodu.TextToControlDistance = 5;
            // 
            // layoutViewColumn9
            // 
            this.layoutViewColumn9.Caption = "Adres";
            this.layoutViewColumn9.FieldName = "Adres";
            this.layoutViewColumn9.LayoutViewField = this.layoutViewField_cAdres;
            this.layoutViewColumn9.Name = "layoutViewColumn9";
            this.layoutViewColumn9.Width = 165;
            // 
            // layoutViewField_cAdres
            // 
            this.layoutViewField_cAdres.EditorPreferredWidth = 253;
            this.layoutViewField_cAdres.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField_cAdres.Name = "layoutViewField_cAdres";
            this.layoutViewField_cAdres.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cAdres.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cAdres.TextToControlDistance = 5;
            // 
            // layoutViewColumn10
            // 
            this.layoutViewColumn10.Caption = "Vergi Dairesi";
            this.layoutViewColumn10.FieldName = "VergiDairesi";
            this.layoutViewColumn10.LayoutViewField = this.layoutViewField_cVergiDairesi;
            this.layoutViewColumn10.Name = "layoutViewColumn10";
            this.layoutViewColumn10.Width = 87;
            // 
            // layoutViewField_cVergiDairesi
            // 
            this.layoutViewField_cVergiDairesi.EditorPreferredWidth = 253;
            this.layoutViewField_cVergiDairesi.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField_cVergiDairesi.Name = "layoutViewField_cVergiDairesi";
            this.layoutViewField_cVergiDairesi.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField_cVergiDairesi.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cVergiDairesi.TextToControlDistance = 5;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.SmallImage;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.btnAttach,
            this.btnEmail});
            this.barManager1.MaxItemId = 6;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar1.Text = "Tools";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Yenile";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.ImageIndex = 41;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 2;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAttach),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEmail, true)});
            this.bar2.Offset = 136;
            this.bar2.Text = "Custom 3";
            // 
            // btnAttach
            // 
            this.btnAttach.Caption = "Attach";
            this.btnAttach.Id = 4;
            this.btnAttach.ImageIndex = 48;
            this.btnAttach.Name = "btnAttach";
            toolTipTitleItem1.Text = "Bilgi";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Dosya Ekle";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnAttach.SuperTip = superToolTip1;
            this.btnAttach.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAttach_ItemClick);
            // 
            // btnEmail
            // 
            this.btnEmail.Caption = "Email";
            this.btnEmail.Id = 5;
            this.btnEmail.ImageIndex = 49;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEmail_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 4";
            this.bar3.DockCol = 1;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3, true)});
            this.bar3.Offset = 54;
            this.bar3.Text = "Custom 4";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Yazdir";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageIndex = 9;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Etiket";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.ImageIndex = 46;
            this.barButtonItem3.Name = "barButtonItem3";
            toolTipTitleItem2.Text = "Bilgi";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Seçili olan kayıtların etiket çıktılarını verir.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.barButtonItem3.SuperTip = superToolTip2;
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1255, 25);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 614);
            this.barDockControlBottom.Size = new System.Drawing.Size(1255, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 589);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1255, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 589);
            // 
            // frmMusteriListesi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1255, 614);
            this.Controls.Add(this.grd);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMusteriListesi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMusteriListesi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupUlke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupSehir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupIlce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSektor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepMemNereden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repZiyNedeni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdi1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSektoru1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIlce1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSehirKodu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colUlkeKodu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdres1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colVergiDairesi1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSektoru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cIlce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSehirKodu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cUlkeKodu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cVergiDairesi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraGrid.GridControl grd;
        private DevExpress.XtraGrid.Views.Grid.GridView grdV;
        private DevExpress.XtraGrid.Columns.GridColumn cID;
        private DevExpress.XtraGrid.Columns.GridColumn cMusteriKodu;
        private DevExpress.XtraGrid.Columns.GridColumn cAdi;
        private DevExpress.XtraGrid.Columns.GridColumn cSektoru;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repSektor;
        private DevExpress.XtraGrid.Columns.GridColumn cIlce;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupIlce;
        private DevExpress.XtraGrid.Columns.GridColumn cSehirKodu;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupSehir;
        private DevExpress.XtraGrid.Columns.GridColumn cUlkeKodu;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupUlke;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit RepMemNereden;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repZiyNedeni;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn11;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colID1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn12;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colMusteriKodu2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn13;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colAdi1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn14;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colSektoru1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn15;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colIlce1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn16;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colSehirKodu1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn17;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colUlkeKodu1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn18;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colAdres1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn19;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colVergiDairesi1;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cID;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colMusteriKodu1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cAdi;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cSektoru;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn6;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cIlce;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn7;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cSehirKodu;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn8;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cUlkeKodu;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn9;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cAdres;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn10;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cVergiDairesi;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn cYetkili;
        private DevExpress.XtraGrid.Columns.GridColumn cTelefon;
        private DevExpress.XtraGrid.Columns.GridColumn cEmail;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnAttach;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraGrid.Columns.GridColumn cYetkiliPozisyonu;
        private DevExpress.XtraBars.BarButtonItem btnEmail;
    }
}