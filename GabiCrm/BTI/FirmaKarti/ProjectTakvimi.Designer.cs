﻿namespace GABI_CRM
{
    partial class frmGantSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeScaleYear timeScaleYear1 = new DevExpress.XtraScheduler.TimeScaleYear();
            DevExpress.XtraScheduler.TimeScaleQuarter timeScaleQuarter1 = new DevExpress.XtraScheduler.TimeScaleQuarter();
            DevExpress.XtraScheduler.TimeScaleMonth timeScaleMonth1 = new DevExpress.XtraScheduler.TimeScaleMonth();
            DevExpress.XtraScheduler.TimeScaleWeek timeScaleWeek1 = new DevExpress.XtraScheduler.TimeScaleWeek();
            DevExpress.XtraScheduler.TimeScaleDay timeScaleDay1 = new DevExpress.XtraScheduler.TimeScaleDay();
            DevExpress.XtraScheduler.TimeScaleHour timeScaleHour1 = new DevExpress.XtraScheduler.TimeScaleHour();
            DevExpress.XtraScheduler.TimeScale15Minutes timeScale15Minutes1 = new DevExpress.XtraScheduler.TimeScale15Minutes();
            DevExpress.XtraScheduler.TimeScaleYear timeScaleYear2 = new DevExpress.XtraScheduler.TimeScaleYear();
            DevExpress.XtraScheduler.TimeScaleQuarter timeScaleQuarter2 = new DevExpress.XtraScheduler.TimeScaleQuarter();
            DevExpress.XtraScheduler.TimeScaleMonth timeScaleMonth2 = new DevExpress.XtraScheduler.TimeScaleMonth();
            DevExpress.XtraScheduler.TimeScaleWeek timeScaleWeek2 = new DevExpress.XtraScheduler.TimeScaleWeek();
            DevExpress.XtraScheduler.TimeScaleDay timeScaleDay2 = new DevExpress.XtraScheduler.TimeScaleDay();
            DevExpress.XtraScheduler.TimeScaleHour timeScaleHour2 = new DevExpress.XtraScheduler.TimeScaleHour();
            DevExpress.XtraScheduler.TimeScale15Minutes timeScale15Minutes2 = new DevExpress.XtraScheduler.TimeScale15Minutes();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGantSchedule));
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnAnaFaaliyet = new DevExpress.XtraBars.BarButtonItem();
            this.btnResourceDuzelt = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.changeScaleWidthItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.trckScaleWidth = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTrackBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.btnProjeEkle = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.resourcesTree1 = new DevExpress.XtraScheduler.UI.ResourcesTree();
            this.resourceTreeColumn1 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.resourceTreeColumn2 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.resourceTreeColumn4 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.resourceTreeColumn3 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.resourceTreeColumn5 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.resourceTreeColumn6 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.resourceTreeColumn7 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.resourceTreeColumn8 = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.altFaaliyetEkleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
            this.colIdSort = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.colId = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            this.colDescription = new DevExpress.XtraScheduler.Native.ResourceTreeColumn();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesTree1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.AppointmentDependencies.Mappings.DependentId = "DependentId";
            this.schedulerStorage1.AppointmentDependencies.Mappings.ParentId = "ParentId";
            this.schedulerStorage1.AppointmentDependencies.Mappings.Type = "Type";
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.SystemColors.Window, "None", "&None"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(190))))), "Important", "&Important"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(213)))), ((int)(((byte)(255))))), "Business", "&Business"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(244)))), ((int)(((byte)(156))))), "Personal", "&Personal"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(228)))), ((int)(((byte)(199))))), "Vacation", "&Vacation"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(206)))), ((int)(((byte)(147))))), "Must Attend", "Must &Attend"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(244)))), ((int)(((byte)(255))))), "Travel Required", "&Travel Required"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(219)))), ((int)(((byte)(152))))), "Needs Preparation", "&Needs Preparation"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(207)))), ((int)(((byte)(233))))), "Birthday", "&Birthday"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(233)))), ((int)(((byte)(223))))), "Anniversary", "&Anniversary"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.Black, "Phone Call", "Phone &Call"));
            this.schedulerStorage1.Appointments.Mappings.AllDay = "AllDay";
            this.schedulerStorage1.Appointments.Mappings.AppointmentId = "UniqueId";
            this.schedulerStorage1.Appointments.Mappings.Description = "Description";
            this.schedulerStorage1.Appointments.Mappings.End = "EndDate";
            this.schedulerStorage1.Appointments.Mappings.Label = "Label";
            this.schedulerStorage1.Appointments.Mappings.Location = "Location";
            this.schedulerStorage1.Appointments.Mappings.PercentComplete = "PercentComplete";
            this.schedulerStorage1.Appointments.Mappings.RecurrenceInfo = "RecurrenceInfo";
            this.schedulerStorage1.Appointments.Mappings.ReminderInfo = "ReminderInfo";
            this.schedulerStorage1.Appointments.Mappings.ResourceId = "ResourceId";
            this.schedulerStorage1.Appointments.Mappings.Start = "StartDate";
            this.schedulerStorage1.Appointments.Mappings.Status = "Status";
            this.schedulerStorage1.Appointments.Mappings.Subject = "Subject";
            this.schedulerStorage1.Appointments.Mappings.Type = "Type";
            this.schedulerStorage1.Resources.CustomFieldMappings.Add(new DevExpress.XtraScheduler.ResourceCustomFieldMapping("IdSort", "IdSort"));
            this.schedulerStorage1.Resources.Mappings.Caption = "Description";
            this.schedulerStorage1.Resources.Mappings.Color = "Color";
            this.schedulerStorage1.Resources.Mappings.Id = "Id";
            this.schedulerStorage1.Resources.Mappings.Image = "Image";
            this.schedulerStorage1.Resources.Mappings.ParentId = "ParentId";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Images = this.SmallImage;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnRefresh,
            this.changeScaleWidthItem1,
            this.trckScaleWidth,
            this.btnResourceDuzelt,
            this.btnProjeEkle,
            this.barButtonItem2,
            this.barButtonItem3,
            this.btnAnaFaaliyet});
            this.barManager1.MaxItemId = 22;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit2,
            this.repositoryItemTextEdit2,
            this.repositoryItemTrackBar2});
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(318, 169);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAnaFaaliyet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnResourceDuzelt, true)});
            this.bar1.Text = "Tools";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Yenile";
            this.btnRefresh.Id = 8;
            this.btnRefresh.ImageIndex = 41;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            toolTipTitleItem1.Text = "Bilgi";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Yenile";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnRefresh.SuperTip = superToolTip1;
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnAnaFaaliyet
            // 
            this.btnAnaFaaliyet.Caption = "AnaFaaliyet";
            this.btnAnaFaaliyet.Id = 21;
            this.btnAnaFaaliyet.ImageIndex = 42;
            this.btnAnaFaaliyet.Name = "btnAnaFaaliyet";
            this.btnAnaFaaliyet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAnaFaaliyet_ItemClick);
            // 
            // btnResourceDuzelt
            // 
            this.btnResourceDuzelt.Caption = "Düzelt";
            this.btnResourceDuzelt.Id = 16;
            this.btnResourceDuzelt.ImageIndex = 7;
            this.btnResourceDuzelt.Name = "btnResourceDuzelt";
            toolTipTitleItem2.Text = "Bilgi";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Kaydı düzelt";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnResourceDuzelt.SuperTip = superToolTip2;
            this.btnResourceDuzelt.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnResourceDuzelt_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 4";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.changeScaleWidthItem1)});
            this.bar3.Text = "Custom 4";
            // 
            // changeScaleWidthItem1
            // 
            this.changeScaleWidthItem1.Caption = "spin";
            this.changeScaleWidthItem1.Edit = this.repositoryItemSpinEdit2;
            this.changeScaleWidthItem1.EditValue = 11;
            this.changeScaleWidthItem1.Id = 13;
            this.changeScaleWidthItem1.Name = "changeScaleWidthItem1";
            this.changeScaleWidthItem1.EditValueChanged += new System.EventHandler(this.changeScaleWidthItem1_EditValueChanged);
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            this.repositoryItemSpinEdit2.NullText = "25";
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 5";
            this.bar2.DockCol = 2;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.trckScaleWidth),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3, true)});
            this.bar2.Offset = 385;
            this.bar2.Text = "Custom 5";
            // 
            // trckScaleWidth
            // 
            this.trckScaleWidth.Caption = "tr";
            this.trckScaleWidth.Edit = this.repositoryItemTrackBar2;
            this.trckScaleWidth.EditValue = 80;
            this.trckScaleWidth.Id = 15;
            this.trckScaleWidth.Name = "trckScaleWidth";
            this.trckScaleWidth.Width = 100;
            this.trckScaleWidth.EditValueChanged += new System.EventHandler(this.trckScaleWidth_EditValueChanged);
            // 
            // repositoryItemTrackBar2
            // 
            this.repositoryItemTrackBar2.LabelAppearance.Options.UseTextOptions = true;
            this.repositoryItemTrackBar2.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTrackBar2.Maximum = 200;
            this.repositoryItemTrackBar2.Minimum = 20;
            this.repositoryItemTrackBar2.Name = "repositoryItemTrackBar2";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Faaliyet Tarihi Ekle";
            this.barButtonItem2.Id = 19;
            this.barButtonItem2.ImageIndex = 42;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Faaliyeti düzenle";
            this.barButtonItem3.Id = 20;
            this.barButtonItem3.ImageIndex = 7;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1260, 25);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 447);
            this.barDockControl2.Size = new System.Drawing.Size(1260, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 25);
            this.barDockControl3.Size = new System.Drawing.Size(0, 422);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1260, 25);
            this.barDockControl4.Size = new System.Drawing.Size(0, 422);
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            this.SmallImage.Images.SetKeyName(43, "attach.png");
            this.SmallImage.Images.SetKeyName(44, "paper_clip.png");
            this.SmallImage.Images.SetKeyName(45, "note2.png");
            this.SmallImage.Images.SetKeyName(46, "barcode_arrow_up.png");
            this.SmallImage.Images.SetKeyName(47, "Mail4.png");
            this.SmallImage.Images.SetKeyName(48, "Document2.png");
            this.SmallImage.Images.SetKeyName(49, "email.png");
            this.SmallImage.Images.SetKeyName(50, "Forum.png");
            this.SmallImage.Images.SetKeyName(51, "pin_blue.png");
            this.SmallImage.Images.SetKeyName(52, "pin_green.png");
            this.SmallImage.Images.SetKeyName(53, "pin_grey.png");
            this.SmallImage.Images.SetKeyName(54, "pin_orange.png");
            this.SmallImage.Images.SetKeyName(55, "pin_red.png");
            this.SmallImage.Images.SetKeyName(56, "pin_yellow.png");
            this.SmallImage.Images.SetKeyName(57, "calendar_up.png");
            this.SmallImage.Images.SetKeyName(58, "businessman_view.png");
            // 
            // btnProjeEkle
            // 
            this.btnProjeEkle.Caption = "Ekle";
            this.btnProjeEkle.Id = 17;
            this.btnProjeEkle.ImageIndex = 42;
            this.btnProjeEkle.Name = "btnProjeEkle";
            toolTipTitleItem3.Text = "Bilgi";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Ana başlık ekleme\r\n";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.btnProjeEkle.SuperTip = superToolTip3;
            this.btnProjeEkle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnProjeEkle_ItemClick);
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 25);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.resourcesTree1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.schedulerControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1260, 422);
            this.splitContainerControl1.SplitterPosition = 383;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // resourcesTree1
            // 
            this.resourcesTree1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9F);
            this.resourcesTree1.Appearance.Row.Options.UseFont = true;
            this.resourcesTree1.Appearance.TreeLine.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.resourcesTree1.Appearance.TreeLine.Options.UseForeColor = true;
            this.resourcesTree1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.resourceTreeColumn1,
            this.resourceTreeColumn2,
            this.resourceTreeColumn4,
            this.resourceTreeColumn3,
            this.resourceTreeColumn5,
            this.resourceTreeColumn6,
            this.resourceTreeColumn7,
            this.resourceTreeColumn8});
            this.resourcesTree1.ContextMenuStrip = this.contextMenuStrip1;
            this.resourcesTree1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resourcesTree1.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.resourcesTree1.Location = new System.Drawing.Point(0, 0);
            this.resourcesTree1.Name = "resourcesTree1";
            this.resourcesTree1.OptionsBehavior.Editable = false;
            this.resourcesTree1.OptionsView.AutoWidth = false;
            this.resourcesTree1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit2,
            this.repositoryItemDateEdit1});
            this.resourcesTree1.RowHeight = 25;
            this.resourcesTree1.SchedulerControl = this.schedulerControl1;
            this.resourcesTree1.Size = new System.Drawing.Size(383, 422);
            this.resourcesTree1.TabIndex = 2;
            // 
            // resourceTreeColumn1
            // 
            this.resourceTreeColumn1.FieldName = "IdSort";
            this.resourceTreeColumn1.Name = "resourceTreeColumn1";
            this.resourceTreeColumn1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            // 
            // resourceTreeColumn2
            // 
            this.resourceTreeColumn2.FieldName = "Id";
            this.resourceTreeColumn2.Name = "resourceTreeColumn2";
            // 
            // resourceTreeColumn4
            // 
            this.resourceTreeColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.resourceTreeColumn4.AppearanceCell.Options.UseFont = true;
            this.resourceTreeColumn4.Caption = "Sira No";
            this.resourceTreeColumn4.FieldName = "SiraNo";
            this.resourceTreeColumn4.Name = "resourceTreeColumn4";
            this.resourceTreeColumn4.Visible = true;
            this.resourceTreeColumn4.VisibleIndex = 0;
            // 
            // resourceTreeColumn3
            // 
            this.resourceTreeColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.resourceTreeColumn3.AppearanceCell.Options.UseFont = true;
            this.resourceTreeColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.resourceTreeColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.resourceTreeColumn3.Caption = "Proje Detay";
            this.resourceTreeColumn3.ColumnEdit = this.repositoryItemMemoEdit1;
            this.resourceTreeColumn3.FieldName = "Description";
            this.resourceTreeColumn3.Name = "resourceTreeColumn3";
            this.resourceTreeColumn3.Visible = true;
            this.resourceTreeColumn3.VisibleIndex = 1;
            this.resourceTreeColumn3.Width = 250;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // resourceTreeColumn5
            // 
            this.resourceTreeColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.resourceTreeColumn5.AppearanceCell.Options.UseFont = true;
            this.resourceTreeColumn5.Caption = "Progrup Sorumlusu";
            this.resourceTreeColumn5.FieldName = "YerelSorumlu";
            this.resourceTreeColumn5.Name = "resourceTreeColumn5";
            this.resourceTreeColumn5.Visible = true;
            this.resourceTreeColumn5.VisibleIndex = 2;
            this.resourceTreeColumn5.Width = 125;
            // 
            // resourceTreeColumn6
            // 
            this.resourceTreeColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.resourceTreeColumn6.AppearanceCell.Options.UseFont = true;
            this.resourceTreeColumn6.Caption = "Firma Sorumlusu";
            this.resourceTreeColumn6.FieldName = "KarsiTarafSorumlusu";
            this.resourceTreeColumn6.Name = "resourceTreeColumn6";
            this.resourceTreeColumn6.Visible = true;
            this.resourceTreeColumn6.VisibleIndex = 3;
            this.resourceTreeColumn6.Width = 133;
            // 
            // resourceTreeColumn7
            // 
            this.resourceTreeColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.resourceTreeColumn7.AppearanceCell.Options.UseFont = true;
            this.resourceTreeColumn7.Caption = "Proje Başlama";
            this.resourceTreeColumn7.ColumnEdit = this.repositoryItemDateEdit1;
            this.resourceTreeColumn7.FieldName = "ProjeBaslamaTarihi";
            this.resourceTreeColumn7.Name = "resourceTreeColumn7";
            this.resourceTreeColumn7.Visible = true;
            this.resourceTreeColumn7.VisibleIndex = 4;
            this.resourceTreeColumn7.Width = 106;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit1.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // resourceTreeColumn8
            // 
            this.resourceTreeColumn8.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8F);
            this.resourceTreeColumn8.AppearanceCell.Options.UseFont = true;
            this.resourceTreeColumn8.Caption = "Proje Bitiş";
            this.resourceTreeColumn8.ColumnEdit = this.repositoryItemDateEdit1;
            this.resourceTreeColumn8.FieldName = "ProjeBitisTarihi";
            this.resourceTreeColumn8.Name = "resourceTreeColumn8";
            this.resourceTreeColumn8.Visible = true;
            this.resourceTreeColumn8.VisibleIndex = 5;
            this.resourceTreeColumn8.Width = 108;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altFaaliyetEkleToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(157, 26);
            // 
            // altFaaliyetEkleToolStripMenuItem
            // 
            this.altFaaliyetEkleToolStripMenuItem.Name = "altFaaliyetEkleToolStripMenuItem";
            this.altFaaliyetEkleToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.altFaaliyetEkleToolStripMenuItem.Text = "Alt Faaliyet Ekle";
            this.altFaaliyetEkleToolStripMenuItem.Click += new System.EventHandler(this.altFaaliyetEkleToolStripMenuItem_Click);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit1.AppearanceDisabled.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit1.AppearanceFocused.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit1.AppearanceReadOnly.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // schedulerControl1
            // 
            this.schedulerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControl1.Location = new System.Drawing.Point(0, 0);
            this.schedulerControl1.LookAndFeel.SkinName = "Office 2010 Blue";
            this.schedulerControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.schedulerControl1.MenuManager = this.barManager1;
            this.schedulerControl1.Name = "schedulerControl1";
            this.schedulerControl1.OptionsBehavior.RecurrentAppointmentDeleteAction = DevExpress.XtraScheduler.RecurrentAppointmentAction.Cancel;
            this.schedulerControl1.OptionsBehavior.RecurrentAppointmentEditAction = DevExpress.XtraScheduler.RecurrentAppointmentAction.Cancel;
            this.schedulerControl1.Size = new System.Drawing.Size(873, 422);
            this.schedulerControl1.Start = new System.DateTime(2015, 11, 8, 0, 0, 0, 0);
            this.schedulerControl1.Storage = this.schedulerStorage1;
            this.schedulerControl1.TabIndex = 1;
            this.schedulerControl1.Text = "schedulerControl1";
            this.schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler1);
            timeScaleQuarter1.Enabled = false;
            timeScaleHour1.Enabled = false;
            timeScale15Minutes1.Enabled = false;
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScaleYear1);
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScaleQuarter1);
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScaleMonth1);
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScaleWeek1);
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScaleDay1);
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScaleHour1);
            this.schedulerControl1.Views.GanttView.Scales.Add(timeScale15Minutes1);
            timeScaleQuarter2.Enabled = false;
            timeScaleMonth2.Enabled = false;
            timeScaleHour2.Enabled = false;
            timeScale15Minutes2.Enabled = false;
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleYear2);
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleQuarter2);
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleMonth2);
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleWeek2);
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleDay2);
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScaleHour2);
            this.schedulerControl1.Views.TimelineView.Scales.Add(timeScale15Minutes2);
            this.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this.schedulerControl1.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControl1_EditAppointmentFormShowing_1);
            // 
            // colIdSort
            // 
            this.colIdSort.FieldName = "IdSort";
            this.colIdSort.Name = "colIdSort";
            this.colIdSort.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Üretim Tezgahları";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            // 
            // frmGantSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1260, 447);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGantSchedule";
            this.Text = "..";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmGantSchedule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.resourcesTree1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarEditItem changeScaleWidthItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraBars.BarEditItem trckScaleWidth;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn colIdSort;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn colId;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn colDescription;
        private DevExpress.XtraScheduler.UI.ResourcesTree resourcesTree1;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn1;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn2;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn3;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn4;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn5;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn6;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn7;
        private DevExpress.XtraScheduler.Native.ResourceTreeColumn resourceTreeColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarButtonItem btnResourceDuzelt;
        private DevExpress.XtraBars.BarButtonItem btnProjeEkle;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem altFaaliyetEkleToolStripMenuItem;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem btnAnaFaaliyet;

    }
}