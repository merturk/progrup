﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using Global;
using BTI;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;

using DevExpress.XtraReports.UI;
using System.Collections;

namespace GABI_CRM
{
    public partial class Firmalar : DevExpress.XtraEditors.XtraForm
    {
        public Firmalar()
        {
            InitializeComponent();
            //Load += (s, e) => grdProjeler.Focus();
            //grdVProjeler.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
        }

        public static string Type = ""; // 1: Normal Müşteri 2: Karvizit müşterileri
        public static string FTITLE = "";
        const string GP_TYPE = "Firma";
        private static string SelectedNo, SelectedEmail, SelectedCari;
        private Boolean SozlesmeDurum = false;

        private void GetSelectedRows()
        {
            SelectedNo = "";
            SelectedEmail = "";
            SelectedCari = "";
            ArrayList rows = new ArrayList();
            // Add the selected rows to the list.
            for (int i = 0; i < grdVMusteriListesi.SelectedRowsCount; i++)
            {
                if (grdVMusteriListesi.GetSelectedRows()[i] >= 0)
                    rows.Add(grdVMusteriListesi.GetDataRow(grdVMusteriListesi.GetSelectedRows()[i]));
            }
            try
            {
                grdVMusteriListesi.BeginUpdate();
                for (int i = 0; i < rows.Count; i++)
                {
                    DataRow row = rows[i] as DataRow;
                    if (!string.IsNullOrEmpty(row["EmpID"].ToString()))
                        SelectedNo = i == 0 ? row["EmpID"].ToString() : SelectedNo + "," + row["EmpID"].ToString();
                    else
                        SelectedNo = i == 0 ? "0" : SelectedNo + "," + "0";
                    if (row["Email"].ToString().Length >= 5)
                        SelectedEmail = row["Email"].ToString() + "," + SelectedEmail;
                    if (row["Adi"].ToString().Length >= 5)
                        SelectedCari = row["Adi"].ToString() + "," + SelectedCari;

                }

            }
            finally
            {
                grdVMusteriListesi.EndUpdate();
            }

        }

        //Firmalar
        SqlDataAdapter SqlDap;
        SqlCommandBuilder SqlScb;
        DataTable dtGenel;

        //Kişiler
        SqlDataAdapter SqlDapYetkiliKisiler;
        SqlCommandBuilder SqlScbYetkiliKisiler;
        DataTable dtGenelYetkiliKisiler;
        //Ziyaretler
        SqlDataAdapter SqlDapZiyaretler;
        SqlCommandBuilder SqlScbZiyaretler;
        DataTable dtGenelZiyaretler;

        //Teklifler
        SqlDataAdapter SqlDapTeklifler;
        SqlCommandBuilder SqlScbTeklifler;
        DataTable dtGenelTeklifler;

        //Sözleşmeler
        SqlDataAdapter SqlDapSozlesmeler;
        SqlCommandBuilder SqlScbSozlesmeler;
        DataTable dtGenelSozlesmeler;
        //Ödemeler
        SqlDataAdapter SqlDapOdemeler;
        SqlCommandBuilder SqlScbOdemeler;
        DataTable dtGenelOdemeler;

        //Projeler
        SqlDataAdapter SqlDapProjeler;
        SqlCommandBuilder SqlScbProjeler;
        DataTable dtGenelProjeler;

        //Proje detayları
        SqlDataAdapter SqlDapProjeDetay;
        SqlCommandBuilder SqlScbProjeDetay;
        DataTable dtGenelProjeDetay;

        //Sertifikalar
        SqlDataAdapter SqlDapSertifikalar;
        SqlCommandBuilder SqlScbSertifikalar;
        DataTable dtGenelSertifikalar;

        //Yazılımlar
        SqlDataAdapter SqlDapYazilimlar;
        SqlCommandBuilder SqlScbYazilimlar;
        DataTable dtGenelYazilimlar;


        public static string MusteriAdi, MusteriKodu;


        private void GetListProjeler(string FirmKodu)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  PROJELER Where Pro_Musteri_Kodu={0}", FirmKodu);
                SqlDapProjeler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelProjeler = new DataTable();
                SqlScbProjeler = new SqlCommandBuilder(SqlDapProjeler);
                SqlScbProjeler.DataAdapter.Fill(dtGenelProjeler);
                grdProjeler.DataSource = dtGenelProjeler;



                //
                //Sözleşmeler
                var dsL = new DataTable();
                dsL = DbConnSql.SelectSQL(string.Format(@"SELECT  Soz_Id  as Tanim FROM  SOZLESMELER Where Soz_Musteri_Kodu={0}", FirmKodu));
                repChkSozNo.DataSource = null;
                repChkSozNo.DataSource = dsL;


                var dsL10 = new DataTable();
                dsL10 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='Ang'"));
                repAnaGrupProje.DataSource = null;
                repAnaGrupProje.DataSource = dsL10;


                var dsL20 = new DataTable();
                dsL20 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG'   "));
                repAltGrupProje.DataSource = null;
                repAltGrupProje.DataSource = dsL20;

                //alt proje grupları
                var DSv = new DataTable();
                DSv = DbConnSql.SelectSQL(string.Format(@"
                             SELECT   2  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG1' 
            
                "));
                repAnaGrupProje1.DataSource = null;
                repAnaGrupProje1.DataSource = DSv;


                Cursor.Current = Cursors.Default;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetListProjeDetay(string ProjeKodu)
        {

            grdProjeDetay.Enabled = string.IsNullOrEmpty(ProjeKodu) ? false : true;


            if (grdVProjeler.RowCount <= 0) return;

            try
            {
                

                Cursor.Current = Cursors.WaitCursor;
                
                string Sql = string.Format(@"SELECT  * FROM  PROJE_DETAY Where Pdt_ProjeNo='{0}'", ProjeKodu);
                SqlDapProjeDetay = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelProjeDetay = new DataTable();
                SqlScbProjeDetay = new SqlCommandBuilder(SqlDapProjeDetay);
                SqlScbProjeDetay.DataAdapter.Fill(dtGenelProjeDetay);
                grdProjeDetay.DataSource = dtGenelProjeDetay;


          
                
                //Danışman isimleri
                var dsL2 = new DataTable();
                dsL2 = DbConnSql.SelectSQL(@"SELECT  GP_VALUE_STR1 AS Kod, GP_VIEW as Tanim FROM  GEN_PARAMS Where GP_TYPE='Dnsm' Order by Kod asc");
                repChkDanisman.DataSource = null;
                repChkDanisman.DataSource = dsL2;


                Cursor.Current = Cursors.Default;

                pvtKarneNotlari.DataSource = DbConnSql.SelectSQL(string.Format(@"Select * From dbo.ProjeDegerlendirmeNotlari Where ProjeKodu='{0}'", ProjeKodu));


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void GetListOdemeler(string FirmKodu)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  ODEMELER Where Ode_Musteri_Kodu='{0}'", FirmKodu);
                SqlDapOdemeler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelOdemeler = new DataTable();
                SqlScbOdemeler = new SqlCommandBuilder(SqlDapOdemeler);
                SqlScbOdemeler.DataAdapter.Fill(dtGenelOdemeler);
                grdOdemeler.DataSource = dtGenelOdemeler;

                //Sözleşmeler
                var dsL = new DataTable();
                dsL = DbConnSql.SelectSQL(string.Format(@"SELECT  Soz_Id  as Tanim FROM  SOZLESMELER Where Soz_Musteri_Kodu='{0}'", FirmKodu));
                repChkOdemeSozNo.DataSource = null;
                repChkOdemeSozNo.DataSource = dsL;

                //Fatura tipleri
                var dsL2 = new DataTable();
                dsL2 = DbConnSql.SelectSQL(string.Format(@"SELECT  GP_VIEW as Tanim FROM  GEN_PARAMS Where GP_TYPE='TFatTip'", FirmKodu));
                repLFaturaTipi.DataSource = null;
                repLFaturaTipi.DataSource = dsL2;
            

                  //Ödeme tipleri
                var dsL3 = new DataTable();
                dsL3 = DbConnSql.SelectSQL(string.Format(@"SELECT  GP_VIEW as Tanim FROM  GEN_PARAMS Where GP_TYPE='TFatOdeTip'", FirmKodu));
                repLOdmSekli.DataSource = null;
                repLOdmSekli.DataSource = dsL3;



                //gider tipleri
                var dsL4 = new DataTable();
                dsL4 = DbConnSql.SelectSQL(string.Format(@"SELECT  GP_VIEW as Tanim FROM  GEN_PARAMS Where GP_TYPE='TFatGidTip'", FirmKodu));
                repLUlsSekli.DataSource  = null;
                repLKnkSekli.DataSource  = null;
                repYmkSekl.DataSource    = null;
                repLUlsSekli.DataSource  = dsL4;
                repLKnkSekli.DataSource  = dsL4;
                repYmkSekl.DataSource    = dsL4;
                


                //repLUlsSekli

                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void GetListSozlesmeler(string FirmKodu)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  SOZLESMELER Where Soz_Musteri_Kodu='{0}'", FirmKodu);
                SqlDapSozlesmeler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelSozlesmeler = new DataTable();
                SqlScbSozlesmeler = new SqlCommandBuilder(SqlDapSozlesmeler);
                SqlScbSozlesmeler.DataAdapter.Fill(dtGenelSozlesmeler);
                grdSozlesmeler.DataSource = dtGenelSozlesmeler;

                //Teklifler
                var dsL = new DataTable();
                dsL = DbConnSql.SelectSQL(string.Format(@"SELECT  TekID  as Tanim FROM  TEKLIFLER Where TekFirmaKodu='{0}'", FirmKodu));
                repSozlTeklNo.DataSource = null;
                repSozlTeklNo.DataSource = dsL;

                //Satışı yapan
                var dsL3 = new DataTable();
                dsL3 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='THazr' ");
                repChkSatYap.DataSource = null;
                repChkSatYap.DataSource = dsL3;


                var dsL4 = new DataTable();
                dsL4 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='SzIc'"));
                repSozIcerik.DataSource = null;
                repSozIcerik.DataSource = dsL4;


                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetListSertifikalar(string FirmKodu)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  SERTIFIKALAR Where SFirmaKodu='{0}'", FirmKodu);
                SqlDapSertifikalar = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelSertifikalar = new DataTable();
                SqlScbSertifikalar = new SqlCommandBuilder(SqlDapSertifikalar);
                SqlScbSertifikalar.DataAdapter.Fill(dtGenelSertifikalar);
                grdSertikalar.DataSource = dtGenelSertifikalar;


                var dsL40 = new DataTable();
                dsL40 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim  , GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='SerT'"));
                repSertifakalar.DataSource = null;
                repSertifakalar.DataSource = dsL40;


                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetListKullanilanYazilimlar(string FirmKodu)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  YAZILIMLAR Where YFirmaKodu='{0}'", FirmKodu);
                SqlDapYazilimlar = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelYazilimlar = new DataTable();
                SqlScbYazilimlar = new SqlCommandBuilder(SqlDapYazilimlar);
                SqlScbYazilimlar.DataAdapter.Fill(dtGenelYazilimlar);
                grdKullanilanYaz.DataSource = dtGenelYazilimlar;

           

                var dsLn40 = new DataTable();
                dsLn40 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim  , GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='YazGrup'"));
                repYazilimGrup.DataSource = null;
                repYazilimGrup.DataSource = dsLn40;

                var dsLn401 = new DataTable();
                dsLn401 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod   FROM  GEN_PARAMS Where GP_TYPE='YazMarka'"));
                repYazilimMarka.DataSource = null;
                repYazilimMarka.DataSource = dsLn401;

                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        //


        private void GetListTeklifler(string FirmKodu)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  TEKLIFLER Where TekFirmaKodu='{0}'", FirmKodu);
                SqlDapTeklifler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelTeklifler = new DataTable();
                SqlScbTeklifler = new SqlCommandBuilder(SqlDapTeklifler);
                SqlScbTeklifler.DataAdapter.Fill(dtGenelTeklifler);
                grdTeklifler.DataSource = dtGenelTeklifler;


                //Yetkili kişiler
                var dsL = new DataTable();
                dsL= DbConnSql.SelectSQL(string.Format(@"Select EmpAdi+space(1)+EmpSoyadi as Tanim from KISILER Where EmpFirmaKodu='{0}' ", FirmKodu));
                repItemChk1.DataSource = null;
                repItemChk1.DataSource = dsL;
                //Teklif tipleri
                var dsL2 = new DataTable();
                dsL2 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='TTip' ");
                repChkTip.DataSource = null;
                repChkTip.DataSource = dsL2;
                //Teklif hazırlayanlar
                var dsL3 = new DataTable();
                dsL3 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='THazr' ");
                repChkHazirlayan.DataSource = null;
                repChkHazirlayan.DataSource = dsL3;
                 //Teklif sonuç seçimleri
                var dsL4 = new DataTable();
                dsL4 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='TSnc' ");
                repChkLSonuc.DataSource = null;
                repChkLSonuc.DataSource = dsL4;

                //Teklif sonuç nedenleri
                var dsL5 = new DataTable();
                dsL5 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='TKnz' ");
                repChkNedenler.DataSource = null;
                repChkNedenler.DataSource = dsL5;


                var dsL10 = new DataTable();
                dsL10 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='Ang'"));
                repTekChkTeklifAnaGroup.DataSource = null;
                repTekChkTeklifAnaGroup.DataSource = dsL10;


                var dsL20 = new DataTable();
                dsL20 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG'"));
                repTekChkTeklifAltGroup.DataSource = null;
                repTekChkTeklifAltGroup.DataSource = dsL20;
        

                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void GetListZiyaretler(string FirmKodu)
        {




        
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Sql = string.Format(@"SELECT  * FROM  ZIYARETLER Where zFirmaKodu='{0}'", FirmKodu);
                SqlDapZiyaretler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelZiyaretler = new DataTable();
                SqlScbZiyaretler = new SqlCommandBuilder(SqlDapZiyaretler);
                SqlScbZiyaretler.DataAdapter.Fill(dtGenelZiyaretler);
                grdZiyaretler.DataSource = dtGenelZiyaretler;


                var dsZiyaretler = new DataTable();
                dsZiyaretler = DbConnSql.SelectSQL(string.Format(@"Select EmpAdi+space(1)+EmpSoyadi as Tanim from KISILER Where EmpFirmaKodu='{0}' ", FirmKodu));
                repGorusulenKisiler.DataSource = null;
                repGorusulenKisiler.DataSource = dsZiyaretler;

                var dsGorusenKisiler = new DataTable();
                dsGorusenKisiler = DbConnSql.SelectSQL(@"SELECT  Tanim=GP_VIEW,Kod=GP_VALUE_STR1,CREUSER=dbo.GetUserName(GP_CREUSER),CREDATE=GP_CREDATE, MODUSER =dbo.GetUserName(GP_MODUSER), MODDATE= GP_MODDATE FROM  GEN_PARAMS Where GP_TYPE='THazr'   ");
                repGorusenKisiler2.DataSource = null;
                repGorusenKisiler2.DataSource = dsGorusenKisiler;

                


                var dsL1 = new DataTable();
                dsL1 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='Ang'"));
                repAnaGrup.DataSource = null;
                repAnaGrup.DataSource = dsL1;


                var dsL2 = new DataTable();
                dsL2 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG'"));
                repAltGrup.DataSource = null;
                repAltGrup.DataSource = dsL2;


                
                var dsL3 = new DataTable();
                dsL3 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='Zned'"));
                repZiyNedeniM.DataSource = null;
                repZiyNedeniM.DataSource = dsL3;


                var dsL4 = new DataTable();
                dsL4 = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='PTan'"));
                repPromosyon.DataSource = null;
                repPromosyon.DataSource = dsL4;





                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void GetListKisiler(string FirmKodu)
        {
           
         
            try
            {

                Cursor.Current = Cursors.WaitCursor;

                string Sql = string.Format(@"SELECT  * FROM  KISILER Where EmpFirmaKodu='{0}'", FirmKodu);
                SqlDapYetkiliKisiler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelYetkiliKisiler = new DataTable();
                SqlScbYetkiliKisiler = new SqlCommandBuilder(SqlDapYetkiliKisiler);
                SqlScbYetkiliKisiler.DataAdapter.Fill(dtGenelYetkiliKisiler);
                grdYetkiliKisiler.DataSource = dtGenelYetkiliKisiler;

                var dsL6 = new DataTable();
                dsL6 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='YSatutu' ");
                repYetkiliStatu.DataSource = null;
                repYetkiliStatu.DataSource = dsL6; 
                  

                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetList()
        {
            this.Text = FTITLE;

            switch (Type)
            {
                case "2":
                    tbMusList.Text = "Firma Kartı Girişi";
                    tbilaveBilgi.PageVisible  = false;
                    tbOdemeler.PageVisible   = false;
                    tbProjeler.PageVisible   = false;
                    tbSozlesme.PageVisible   = false;
                    tbTeklifler.PageVisible  = false;                  
                    tbZiyaretler.PageVisible = false;
                    //tbYazilimlar.PageVisible = false;    
                    break;
            }            
         
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                    string Sql = string.Format(@"
                                SELECT 
                                   ID
								  ,Type
								  ,MusteriKodu
								  ,Adi
								  ,Sektoru
								  ,Ilce
								  ,SehirKodu
								  ,UlkeKodu
								  ,Adres
								  ,VergiDairesi
								  ,VergiNo
								  ,NeredenUlasildi
								  ,Segmantasyonu
								  ,FirmaTel
								  ,FimaFax
								  ,FirmaWebAdresi
								  ,CalisanKisiSayisi
								  ,BagliDigerFirmalar
								  ,Ciro
								  ,CiroYuzdesi
								  ,AileSirketi	  
								  ,Kategori
                                  ,kis.EmpAdi+ space(1)+kis.EmpSoyadi as Yetkili
                                  ,kis.Emp_YetkiliKisi as YetkiliPozisyonu
                                  ,isnull(kis.EmpTelefon1,'') as Telefon
                                  ,isnull(kis.EmpEmail,'') as Email
                                  ,( Select  Top 1    AnaGrup From   GEN_PARAMS as gg
                                      left join
                                      (Select  GP_VIEW as AnaGrup, GP_VALUE_STR1  as KodAna From GEN_PARAMS Where GP_TYPE='SektAna')as g on g.KodAna = GP_VALUE_STR2
                                      Where   (GP_TYPE = 'NACE')  and gg.GP_VALUE_STR1=f.Sektoru
                                    ) as SektoruAna
                                  , '' as Renk
                                  ,Olusturan = dbo.GetUserName(f.CREUSER)
                                  ,OlusturmaTarihi = f.CREDATE
                                  ,Degistiren = dbo.GetUserName(f.MODUSER)
                                  ,DegistirmeTarihi = f.MODDATE
                                  ,Kategorim = Ktg.Tanim

                                  , EmpID 
                                  , Emp_YetkiliKisi 
                                  , EmpFirmaKodu 
                                  , EmpAdi 
                                  , EmpSoyadi 
                                  , EmpGorevi 
                                  , EmpEmail 
                                  , EmpTelefon1 
                                  , EmpTelefon2                                 
                                  , OdemeYetkiliKisiMi 
                                  , Note 
                                  , UserType                              
                                  , PRINTUSERLABEL 
                                  , PRINTUSERDATE 
                                 FROM  FIRMALAR as f
                                left join
                                (Select * from KISILER  )as kis on kis.EmpFirmaKodu = f.MusteriKodu
                                left join
                                (SELECT Tanim=GP_VIEW,Kod=GP_VALUE_STR1 FROM  GEN_PARAMS Where GP_TYPE='Ktg' AND GP_DURUM='A' )as Ktg on Ktg.Kod=f.Kategori
                                Where f.Type={0} 
                                Order by Adi asc
                               

                            ", Type);
                    SqlDap = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                    dtGenel = new DataTable();
                    SqlScb = new SqlCommandBuilder(SqlDap);
                    SqlScb.DataAdapter.Fill(dtGenel);
                    grdMusteriListesi.DataSource = dtGenel;


                    var dsUlkeler = new DataTable();
                    dsUlkeler = DbConnSql.SelectSQL("SELECT  GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE ='ULKELER' ");
                    repLupUlke.DataSource = null;
                    repLupUlke.DataSource = dsUlkeler;

                    var dsSehirler = new DataTable();
                    dsSehirler = DbConnSql.SelectSQL("SELECT  GP_VIEW as Tanim, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER' ");
                    repLupSehir.DataSource = null;
                    repLupSehir.DataSource = dsSehirler;

                    var dsIlceler = new DataTable();
                    dsIlceler = DbConnSql.SelectSQL("SELECT  GP_ID, GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE ='ILCELER' ");
                    repLupIlce.DataSource = null;
                    repLupIlce.DataSource = dsIlceler;

                    var dsSektor = new DataTable();
                    dsSektor = DbConnSql.SelectSQL("SELECT      GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM            GEN_PARAMS WHERE        (GP_TYPE = 'NACE')");
                    repSektor.DataSource = null;
                    repSektor.DataSource = dsSektor;


                    var dsCiroBas= new DataTable();
                    dsCiroBas = DbConnSql.SelectSQL("SELECT      GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM            GEN_PARAMS WHERE        (GP_TYPE = 'CiroB')");
                    repCiro.DataSource = null;
                    repCiro.DataSource = dsCiroBas;

                    var dsNereden = new DataTable();
                    dsNereden = DbConnSql.SelectSQL("SELECT      GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM            GEN_PARAMS WHERE        (GP_TYPE = 'Nered')");
                    repLupNereden.DataSource = null;
                    repLupNereden.DataSource = dsNereden;


                    var dsSeg = new DataTable();
                    dsSeg = DbConnSql.SelectSQL("SELECT   GP_VALUE_STR1 as Kod ,GP_VIEW as Tanim FROM            GEN_PARAMS WHERE        (GP_TYPE = 'Mseg')");
                    repSegmantas.DataSource = null;
                    repSegmantas.DataSource = dsSeg;


                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            try
            {

              /*  textEdit1.Focus();          
                SqlScb.DataAdapter.Update(dtGenel);          
                MessageBox.Show("Veriler güncellendi.");
                grdMusteri.MainView = grdVMusteriListesi;
               **/
                PubObjects.RowHandle(grdVMusteriListesi);


                EkleMusteriTanim.FTITLE = string.Format("{0}>>Düzeltme :{1}", "Firma Kartı", grdVMusteriListesi.GetFocusedRowCellDisplayText(cAdi).ToString());
                EkleMusteriTanim.Type = Type;
                EkleMusteriTanim.GelisMusteriKodu = grdVMusteriListesi.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();

                var frm = new EkleMusteriTanim();
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                GetList();
                PubObjects.SetBookmark(grdVMusteriListesi);
            
                
            }
            catch (Exception ex )
            {

                MessageBox.Show(ex.Message);
           
            }



        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdMusteriListesi.ShowPrintPreview();
        }

        private void frmUlkeler_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            


        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            if ((e.Column.FieldName != "UlkeKodu")  && (e.Column.FieldName != "SehirKodu")  ) return;

            string UlkeKodu = grdVMusteriListesi.GetRowCellValue(e.RowHandle, grdVMusteriListesi.Columns["UlkeKodu"]).ToString();
            var dsSehirler = new DataTable();
            dsSehirler = DbConnSql.SelectSQL(string.Format(@"
                    Select * From
                    (
                    SELECT 1 AS Id,  GP_VIEW as Tanim, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER'  and  GP_VALUE_STR1='{0}' 
                    UNION ALL
                    SELECT 2  AS Id, GP_VIEW as Tanim, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER' and  GP_VALUE_STR1<>'{0}' 
                    )As Sub1 
                    Order by Id asc
            
            ", UlkeKodu));
            repLupSehir.DataSource = null;
            repLupSehir.DataSource = dsSehirler;


           
            string SehirKodu = grdVMusteriListesi.GetRowCellValue(e.RowHandle, grdVMusteriListesi.Columns["SehirKodu"]).ToString();

            var dsIlceler = new DataTable();
            dsIlceler = DbConnSql.SelectSQL(string.Format(@"
                    Select * From
                    (
                        SELECT   1 AS Id, GP_VIEW as Tanim, GP_VIEW as Kod FROM  GEN_PARAMS Where GP_TYPE ='ILCELER' and GP_VALUE_STR1='{0}'  
                        UNION ALL
                        SELECT   2  AS Id, GP_VIEW as Tanim, GP_VIEW as Kod FROM  GEN_PARAMS Where GP_TYPE ='ILCELER' and GP_VALUE_STR1<>'{0}'  
                    )As Sub1 
                    Order by Id asc
            
                ", SehirKodu));
            repLupIlce.DataSource = null;
            repLupIlce.DataSource = dsIlceler;


        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            grdVMusteriListesi.OptionsSelection.MultiSelect = true;
        }

        private void grdMusteriListesi_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
     
            MusteriKodu = grdVMusteriListesi.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();
            MusteriAdi = grdVMusteriListesi.GetFocusedRowCellDisplayText(cAdi).ToString();

        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            try
            {
                //Sözleşme griddinde değişiklik varsa kayıt kontrolü yapılsıngr
                if (SozlesmeDurum == true) {SaveSozlesmeler(); if (SozlesmeDurum == true) { tbMain.SelectedTabPageIndex = 4; return;}}
            
                switch (tbMain.SelectedTabPageIndex)
                {
                    case 0:
                    
                        break;
                    case 1:
                        grpYetkiliKisiler.Text = string.Format("{0}-{1}",MusteriKodu,MusteriAdi);
                        GetListKisiler(MusteriKodu);
                        break;

                    case 2:
                        GetListKisiler(MusteriKodu);
                        if (dtGenelYetkiliKisiler.Rows.Count <= 0) { MessageBox.Show("Yetkili kişi tanımını yaptıktan sonra yeniden deneyin."); tbMain.SelectedTabPageIndex = 1; return; }

                        grpZiyaret.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListZiyaretler(MusteriKodu);
                        break;
                    case 3:
                        GetListKisiler(MusteriKodu);
                        if (dtGenelYetkiliKisiler.Rows.Count <= 0) { MessageBox.Show("Yetkili kişi tanımını yaptıktan sonra yeniden deneyin."); tbMain.SelectedTabPageIndex = 1; return; }

                        grpTeklifler.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListTeklifler(MusteriKodu);
                        break;

                    case 4:
                        GetListTeklifler(MusteriKodu);
                        if (dtGenelTeklifler.Rows.Count <= 0) { MessageBox.Show("Teklif girişi yaptıktan sonra yeniden deneyin."); tbMain.SelectedTabPageIndex = 3; return; }
                        grpSozlesmeler.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListSozlesmeler(MusteriKodu);
                    

                        break;

                    case 5:
                        GetListSozlesmeler(MusteriKodu);
                        if (dtGenelSozlesmeler.Rows.Count <= 0) { MessageBox.Show("Sözleşme girişi yaptıktan sonra yeniden deneyin."); tbMain.SelectedTabPageIndex = 4; return; }
                        grpOdemeler.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListOdemeler(MusteriKodu);
                        break;

                    case 6:
                        GetListSozlesmeler(MusteriKodu);
                        if (dtGenelSozlesmeler.Rows.Count <= 0) { MessageBox.Show("Sözleşme girişi yaptıktan sonra yeniden deneyin."); tbMain.SelectedTabPageIndex = 4; return; }
                        grpProjeler.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListProjeler(MusteriKodu);
                        DataView _dvp = new DataView(dtGenelProjeler);
                        string ProjeKodu = _dvp.Count <= 0 ? "" : (grdVProjeler.GetFocusedRowCellValue(cPro_Id).ToString());
                         GetListProjeDetay(ProjeKodu);
                        break;

                    case 7:
                        grpSertifika.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListSertifikalar(MusteriKodu);
                        break;

                    case 8:
                        grpKullanilanYazi.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListKullanilanYazilimlar(MusteriKodu);
                        break;
                
                        }
          
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnYetkiliKisiEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            lvYetkiliKisiler.AddNewRow();
             lvYetkiliKisiler.SetRowCellValue(lvYetkiliKisiler.FocusedRowHandle, lvYetkiliKisiler.Columns["EmpFirmaKodu"], MusteriKodu);         
          //  grdYetkiliKisiler.RefreshDataSource();

            
            //  GetListKisiler(MusteriKodu);


        }

        private void barButtonItem4_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnSaveYetkiliKisiler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            textEdit1.Focus();
            try
            {
                DataView _dv = new DataView(dtGenelYetkiliKisiler);
                for (int i = 0; i < _dv.Count; i++)
                {
                    string Ulke = _dv[i]["Emp_YetkiliKisi"].ToString();
                    if (string.IsNullOrEmpty(Ulke))
                    {
                        MessageBox.Show("Statüyü boş geçemezsiniz.");
                        return;
                    }

                }
            
            SqlScbYetkiliKisiler.DataAdapter.Update(dtGenelYetkiliKisiler);
            MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListKisiler(MusteriKodu);
        }

    

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
            
        }

        private void barButtonItem4_ItemClick_2(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void barButtonItem3_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


                EkleMusteriTanim.FTITLE = "Firma Kartı Ekle";
                EkleMusteriTanim.Type = Type;
                EkleMusteriTanim.GelisMusteriKodu = "";
                var frm = new EkleMusteriTanim();
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();
                GetList();

            /*
             
                grdVMusteriListesi.AddNewRow();
                grdVMusteriListesi.SetRowCellValue(grdVMusteriListesi.FocusedRowHandle, grdVMusteriListesi.Columns["MusteriKodu"], MusteriNo);
                grdVMusteriListesi.SetRowCellValue(grdVMusteriListesi.FocusedRowHandle, grdVMusteriListesi.Columns["Type"], Type);
                grdVMusteriListesi.FocusedRowHandle = grdVMusteriListesi.GetVisibleRowHandle(grdVMusteriListesi.FocusedRowHandle);
                grdMusteri.RefreshDataSource();
            */


        }

        private void barButtonItem4_ItemClick_3(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string MusK = grdVMusteriListesi.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();

            DataView _dv = new DataView(dtGenelProjeler);
            DataView _dvOdemeler = new DataView(dtGenelOdemeler);
            DataView _dvSozlesmeler = new DataView(dtGenelSozlesmeler);
            DataView _dvTeklifler = new DataView(dtGenelTeklifler);
            DataView _dvYetkiliKisiler = new DataView(dtGenelYetkiliKisiler);
            DataView _dvZiyaretler = new DataView(dtGenelZiyaretler);
            
            if (_dvZiyaretler.Count > 0) { MessageBox.Show("Ziyaretler kayıtlarını sildikten sonra yeniden deneyin."); return; }
            if (_dvYetkiliKisiler.Count > 0) { MessageBox.Show("Yetkili kişiler kayıtlarını sildikten sonra yeniden deneyin."); return; }
            if (_dvOdemeler.Count > 0) { MessageBox.Show("Ödeme kayıtlarını sildikten sonra yeniden deneyin."); return; }
            if (_dv.Count > 0) { MessageBox.Show("Projeler kayıtlarını sildikten sonra yeniden deneyin."); return; }
            if (_dvSozlesmeler.Count > 0) { MessageBox.Show("Sözleşmeler kayıtlarını sildikten sonra yeniden deneyin."); return; }
            if (_dvTeklifler.Count > 0) { MessageBox.Show("Teklifler kayıtlarını sildikten sonra yeniden deneyin."); return; }

            DbConnSql.CmdDeleteNew(string.Format(@"
                        Delete FIRMALAR  Where MusteriKodu ='{0}'    
                        Delete ZIYARETLER Where zFirmaKodu='{0}'
                        Delete KISILER Where EmpFirmaKodu='{0}'
                        Delete TEKLIFLER Where TekFirmaKodu='{0}'
                        Delete ODEMELER Where Ode_Musteri_Kodu='{0}'
                        Delete SERTIFIKALAR Where SFirmaKodu='{0}'
                        Delete SOZLESMELER Where Soz_Musteri_Kodu='{0}'
                        Delete YAZILIMLAR Where YFirmaKodu='{0}'
                        Delete  PROJELER Where Pro_Musteri_Kodu='{0}'
                        Delete  PROJE_DETAY Where Pdt_ProjeNo= (SELECT Top 1 Pro_Id FROM PROJELER Where Pro_Musteri_Kodu='{0}')
                        Delete  PROJE_DETAY_DEGERLENDIRME Where Pdtd_Pdt_Id=(SELECT Top 1 Pro_Id FROM PROJELER Where Pro_Musteri_Kodu='{0}')
                     ", MusK));

            GetList();
           // grdMusteri.RefreshDataSource();         
        //    SqlScb.DataAdapter.Update(dtGenel); 
           

            

        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
           // DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            lvYetkiliKisiler.DeleteRow(lvYetkiliKisiler.FocusedRowHandle);
           // grdYetkiliKisiler.RefreshDataSource();
         
        }

        private void btnYenileZiyaret_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetListZiyaretler(MusteriKodu);
        }

        private void btnKaydetZiyaret_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            try
            {
                textEdit1.Focus();
                SqlScbZiyaretler.DataAdapter.Update(dtGenelZiyaretler);
                MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


            GetListZiyaretler(MusteriKodu);
        }

        private void btnEkleZiyaret_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVZiyaretler.AddNewRow();
            grdVZiyaretler.SetRowCellValue(grdVZiyaretler.FocusedRowHandle, grdVZiyaretler.Columns["zFirmaKodu"], MusteriKodu);
            //grdZiyaretler.RefreshDataSource();
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            lvYetkiliKisiler.ShowPrintPreview();
        }

        private void btnSilZiyaret_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVZiyaretler.DeleteRow(grdVZiyaretler.FocusedRowHandle);
            //grdZiyaretler.RefreshDataSource();
            
        }

        private void btnYazZiyaret_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVZiyaretler.ShowPrintPreview();
        }

        private void btnEkleTeklif_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVTeklifler.AddNewRow();
            grdVTeklifler.SetRowCellValue(grdVTeklifler.FocusedRowHandle, grdVTeklifler.Columns["TekFirmaKodu"], MusteriKodu);
            //grdTeklifler.RefreshDataSource();
        }

        private void btnYenileTeklif_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetListTeklifler(MusteriKodu);
        }

        private void btnKaydetTeklif_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                textEdit1.Focus();
                SqlScbTeklifler.DataAdapter.Update(dtGenelTeklifler);
                MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListTeklifler(MusteriKodu);
        }

        private void btnYazTeklif_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVTeklifler.ShowPrintPreview();
        }

        private void btnSilTeklif_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVTeklifler.DeleteRow(grdVTeklifler.FocusedRowHandle);
           // grdTeklifler.RefreshDataSource();
        
        }

        private void btnEkleSozlemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVSozlesmeler.AddNewRow();
            grdVSozlesmeler.SetRowCellValue(grdVSozlesmeler.FocusedRowHandle, grdVSozlesmeler.Columns["Soz_Musteri_Kodu"], MusteriKodu);
           // grdSozlesmeler.RefreshDataSource();
        }

        private void btnYenileSozlesmeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetListSozlesmeler(MusteriKodu);
        }


        private void SaveSozlesmeler()
        {
            try
            {
                textEdit1.Focus();

                DataView _dv = new DataView(dtGenelSozlesmeler);
                for (int i = 0; i < _dv.Count; i++)
                {
                    string Ulke = _dv[i]["Soz_TeklifNo"].ToString();
                    if (string.IsNullOrEmpty(Ulke))
                    {
                        MessageBox.Show("Teklif numarası tanımını boş geçemezsiniz.");
                    
                        return;
                    }
                    
                }

                SqlScbSozlesmeler.DataAdapter.Update(dtGenelSozlesmeler);
                MessageBox.Show("Veriler güncellendi.");
                SozlesmeDurum = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListSozlesmeler(MusteriKodu);
        
        }

        private void btnKayderSozlesmeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveSozlesmeler();
        }

        private void btnSilSozlemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVSozlesmeler.DeleteRow(grdVSozlesmeler.FocusedRowHandle);
           // grdSozlesmeler.RefreshDataSource();
           
        }

        private void btnYazSozlemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVSozlesmeler.ShowPrintPreview();
        }

        private void btnKaydetOdemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             try
            {
                textEdit1.Focus();
                DataView _dv = new DataView(dtGenelOdemeler);
                for (int i = 0; i < _dv.Count; i++)
                {
                    string Ulke = _dv[i]["Ode_SozlesmeNo"].ToString();
                    if (string.IsNullOrEmpty(Ulke))
                    {
                        MessageBox.Show("Sözleşme numarası tanımını boş geçemezsiniz.");
                        return;
                    }
                }

                SqlScbOdemeler.DataAdapter.Update(dtGenelOdemeler);
                MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListOdemeler(MusteriKodu);
        }

        private void btnYenileOdemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetListOdemeler(MusteriKodu);
        }

        private void btnEkleOdemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVOdemeler.AddNewRow();
            grdVOdemeler.SetRowCellValue(grdVOdemeler.FocusedRowHandle, grdVOdemeler.Columns["Ode_Musteri_Kodu"], MusteriKodu);
          //  grdOdemeler.RefreshDataSource();
        }

        private void btnSilOdemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVOdemeler.DeleteRow(grdVOdemeler.FocusedRowHandle);
            //grdOdemeler.RefreshDataSource();
         
        }

        private void btnYazdirOdemeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            grdVOdemeler.ShowPrintPreview();
        }

        private void grdVProjeler_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

            
            DataView _dv = new DataView(dtGenelProjeler);
            if (_dv.Count <= 0) return;
            if ( string.IsNullOrEmpty( grdVProjeler.GetFocusedRowCellValue(cPro_Id).ToString() ) ) return;
            
            string ProjeKodu = (grdVProjeler.GetFocusedRowCellValue(cPro_Id).ToString());
            GetListProjeDetay(ProjeKodu);
        }

        private void btnEkleProjeDetay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ProjeKodu = (grdVProjeler.GetFocusedRowCellValue(cPro_Id).ToString());
            if (string.IsNullOrEmpty( ProjeKodu))
            {
                MessageBox.Show("Proje kodu seçilmeden kayıt ekleme yapamazsınız.");
                return;
            }


            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format(@"Select count(1) as Kayit from PROJE_DETAY Where Pdt_ProjeNo={0}", ProjeKodu));

            if (dt.Rows[0]["Kayit"].ToString() == "0")
            {
                grdProjeDetay.Enabled = true;
                grdVProjeDetay.AddNewRow();
                grdVProjeDetay.SetRowCellValue(grdVProjeDetay.FocusedRowHandle, grdVProjeDetay.Columns["Pdt_ProjeNo"], ProjeKodu);                
            }
            else
            {
                //Proje ana numara ile proje kodu aynı olduğu için tek kayıt olmaliki Karne notlarını tabib edebilelime
                MessageBox.Show("Proje detay kısmına 1 kayıttan fazla ekleyemezsiniz.");
                return;
            
            }

            
       //     grdProjeDetay.RefreshDataSource();

           
                



        }

        private void btnEkleProjeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVProjeler.AddNewRow();
            grdVProjeler.SetRowCellValue(grdVProjeler.FocusedRowHandle, grdVProjeler.Columns["Pro_Musteri_Kodu"], MusteriKodu);
            //grdProjeler.RefreshDataSource();
        }

        private void btnYenileProjeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetListProjeler(MusteriKodu);
        }

        private void btnKaydetProjeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataView _dvp = new DataView(dtGenelProjeler);
            DataView _dvp2 = new DataView(dtGenelProjeDetay);
            try
            {
                textEdit1.Focus();


                for (int i = 0; i < _dvp.Count; i++)
                {
                    string t = _dvp[i]["Pro_Sozlesme_No"].ToString();
                    if (string.IsNullOrEmpty(t))
                    {
                        MessageBox.Show("Sözleşme numarası tanımını boş geçemezsiniz.");
                        return;
                    }
                }

                SqlScbProjeler.DataAdapter.Update(dtGenelProjeler);
                SqlScbProjeDetay.DataAdapter.Update(dtGenelProjeDetay);                
                MessageBox.Show("Veriler güncellendi.");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            PubObjects.RowHandle(grdVProjeler);
                GetListProjeler(MusteriKodu);
            PubObjects.SetBookmark(grdVProjeler);
            
            string ProjeKodu = _dvp.Count <= 0 ?"": (grdVProjeler.GetFocusedRowCellValue(cPro_Id).ToString());
            GetListProjeDetay(ProjeKodu);
        }

        private void btnSilProjeler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataView _dv = new DataView(dtGenelProjeDetay);
            

            if (_dv.Count>0 )
            {
                MessageBox.Show("Proje detay kayıtlarını sildikten sonra yeniden deneyin.");
                return;
            }

            grdVProjeler.DeleteRow(grdVProjeler.FocusedRowHandle);
            //grdProjeler.RefreshDataSource();
          
        }

        private void btnSilProjeDetay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVProjeDetay.DeleteRow(grdVProjeDetay.FocusedRowHandle);
            grdProjeDetay.RefreshDataSource();
        }

        private void grdMusteri_Click(object sender, EventArgs e)
        {

        }

        private void grdVZiyaretler_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            if ((e.Column.FieldName != "zAnaGrup") ) return;
            string Kod = grdVZiyaretler.GetRowCellValue(e.RowHandle, grdVZiyaretler.Columns["zAnaGrup"]).ToString();
            var DS = new DataTable();
            DS = DbConnSql.SelectSQL(string.Format(@"
                    Select * From
                    (
                        SELECT   1  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG' and GP_VALUE_STR2='{0}'
                        UNION ALL                        
                        SELECT   2  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG'
                    )As Sub1 
                    Order by Id asc
            
                ", Kod));
            repAltGrup.DataSource = null;
            repAltGrup.DataSource = DS;
        }

        private void grdVTeklifler_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if ((e.Column.FieldName != "zAnaGrup")) return;
            string Kod = grdVTeklifler.GetRowCellValue(e.RowHandle, grdVTeklifler.Columns["zAnaGrup"]).ToString();
            var DS = new DataTable();
            DS = DbConnSql.SelectSQL(string.Format(@"
                    Select * From
                    (
                        SELECT   1  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG' and GP_VALUE_STR2='{0}'
                        UNION ALL                        
                        SELECT   2  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG'
                    )As Sub1 
                    Order by Id asc
            
                ", Kod));
            repAltGrupTeklif.DataSource = null;
            repAltGrupTeklif.DataSource = DS;

        }

        private void grdVProjeDetay_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }

        private void grdVProjeler_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            if ((e.Column.FieldName != "Pro_ProjeTanim")) return;
            string Kod = grdVProjeler.GetRowCellValue(e.RowHandle, grdVProjeler.Columns["Pro_ProjeTanim"]).ToString();
            var DS = new DataTable();
            DS = DbConnSql.SelectSQL(string.Format(@"
                             SELECT   2  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG' and   GP_DURUM='A' and GP_VALUE_STR2='{0}'
            
                ", Kod));
            repAltGrupProje.DataSource = null;
            repAltGrupProje.DataSource = DS;
        }

        private static void DoRowDoubleClick(GridView view, Point pt)
        {
           
        }

        private void grdVProjeDetay_DoubleClick(object sender, EventArgs e)
        {

        }
       
        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
           

            
        }

        private void grdVProjeDetay_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

      
        }

        private void barButtonItem10_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (grdVProjeler.RowCount <= 0) return;

            string ProjeKodu = (grdVProjeler.GetFocusedRowCellValue(cPro_Id).ToString());
            if (string.IsNullOrEmpty(ProjeKodu))
            {
                MessageBox.Show("Proje kodu seçilmeden kayıt ekleme yapamazsınız.");
                return;
            }


            frmProjeKarneGiris.ProjeNo = ProjeKodu;
            var frm = new frmProjeKarneGiris();
            frm.ShowDialog();            
            
            pvtKarneNotlari.DataSource = DbConnSql.SelectSQL(string.Format( @"Select * From dbo.ProjeDegerlendirmeNotlari Where ProjeKodu={0}",ProjeKodu));
        }

        private void pvtKarneNotlari_FieldValueDisplayText(object sender, DevExpress.XtraPivotGrid.PivotFieldDisplayTextEventArgs e)
        {
            if (e.ValueType == DevExpress.XtraPivotGrid.PivotGridValueType.GrandTotal)
                e.DisplayText = "Toplam";
        }

        private void btnAttachFileMusteri_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           PubObjects.RowHandle(grdVMusteriListesi);
           try
           {

           
           string MusKodu = grdVMusteriListesi.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();
           if (string.IsNullOrEmpty(MusKodu))
           {
               MessageBox.Show("Müşteri kodu seçilmeden kayıt ekleme yapamazsınız.");
               return;
           }


           Attach_File.MusteriNo = MusKodu;
           var frm = new Attach_File();
           frm.ShowDialog();

           }
           catch (Exception ex)
           {

               MessageBox.Show(ex.Message);
           }
           PubObjects.SetBookmark(grdVMusteriListesi);


        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            GetSelectedRows();
            RepMusteriEtiket.EmpId = SelectedNo;
            var report = new RepMusteriEtiket();
            report.ShowPreview();
        }

        private void btnSerRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetListSertifikalar(MusteriKodu);
        }

        private void btnEkleSertifka_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVSertikalar.AddNewRow();
            grdVSertikalar.SetRowCellValue(grdVSertikalar.FocusedRowHandle, grdVSertikalar.Columns["SFirmaKodu"], MusteriKodu);
            grdSertikalar.RefreshDataSource();
        }

        private void btnKaydetSertifika_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                textEdit1.Focus();
                SqlScbSertifikalar.DataAdapter.Update(dtGenelSertifikalar);
                MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListSertifikalar(MusteriKodu);
        }

        private void btnYazSertifika_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVSertikalar.ShowPrintPreview();
        }

        private void btnSilSertifika_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVSertikalar.DeleteRow(grdVSertikalar.FocusedRowHandle);
          //  grdSertikalar.RefreshDataSource();
            
        }

        private void btnYazilimYenile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            GetListKullanilanYazilimlar(MusteriKodu);
        }

        private void btnYazilimEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVKullanilanYaz.AddNewRow();
            grdVKullanilanYaz.SetRowCellValue(grdVKullanilanYaz.FocusedRowHandle, grdVKullanilanYaz.Columns["YFirmaKodu"], MusteriKodu);
            grdKullanilanYaz.RefreshDataSource();
        }

        private void btnYazilimKaydet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                textEdit1.Focus();
                SqlScbYazilimlar.DataAdapter.Update(dtGenelYazilimlar);
                MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListKullanilanYazilimlar(MusteriKodu);
        }

        private void btnYazilimYazdir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVKullanilanYaz.ShowPrintPreview();
        }

        private void btnYazilimSil_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVKullanilanYaz.DeleteRow(grdVKullanilanYaz.FocusedRowHandle);
           // grdKullanilanYaz.RefreshDataSource();
            
        }

        private void Firmalar_Activated(object sender, EventArgs e)
        {
            GetList();
        
        }

        private void btnEmailGonderim_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {


                string MusKodu = grdVMusteriListesi.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();
                if (string.IsNullOrEmpty(MusKodu))
                {
                    MessageBox.Show("Müşteri kodu seçilmeden kayıt ekleme yapamazsınız.");
                    return;
                }

                GetSelectedRows();

                SendMail.SelectedEmailList = SelectedEmail;
                SendMail.SelectedCariList  = SelectedCari;
                var frm = new SendMail();
                frm.ShowDialog();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
      
        }

        private void grdVProjeDetay_CellValueChanged_1(object sender, CellValueChangedEventArgs e)
        {

            if ((e.Column.FieldName != "Pdt_ProjeDetayTanim")) return;
            string Kod = grdVProjeDetay.GetRowCellValue(e.RowHandle, grdVProjeDetay.Columns["Pdt_ProjeDetayTanim"]).ToString();
            var DS = new DataTable();
            DS = DbConnSql.SelectSQL(string.Format(@"
                             SELECT   2  AS Id, GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='AltG1' and   GP_DURUM='A' and GP_VALUE_STR2='{0}'
            
                ", Kod));
            repAnaGrupProje1.DataSource = null;
            repAnaGrupProje1.DataSource = DS;


  

        }

        private void xtraTabControl1_SelectedPageChanged_1(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {

            string ProjeKodu = grdVProjeler.GetFocusedRowCellDisplayText(cPro_Id).ToString();
            switch (tbMainProjeler.SelectedTabPageIndex)
            {
                case 1:
                    if (string.IsNullOrEmpty(ProjeKodu))
                    {
                        MessageBox.Show("Proje kodunu kayıt ettikten sonra yeniden deneyin.");
                        tbMainProjeler.SelectedTabPageIndex = 0;
                        return;
                    
                    }


                    break;
             
            }
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
     
        }

    

        private void tbSozlesme_Leave(object sender, EventArgs e)
        {

            if (Glob.DurumKontrol("Yapılan değişiklikler kayıt edilsin mi?", "Değişiklikler kayıt edilmedi.") == true)
                SozlesmeDurum = true;
            else
                SozlesmeDurum = false;
        }

        private void grdVMusteriListesi_RowStyle(object sender, RowStyleEventArgs e)
        {

            /*
            string SourceType;
            SourceType = Convert.ToString(grdVMusteriListesi.GetRowCellValue(e.RowHandle, "Renk"));
            switch (SourceType)
            {
                case "Yellow":
                          e.Appearance.BackColor = Color.Yellow; //Color.FromArgb(192, 192, 255);
                    break;
                case "Orange":
                    e.Appearance.BackColor = Color.Orange; //Color.FromArgb(192, 192, 255);
                    break;
                case "Green":
                    e.Appearance.BackColor = Color.FromArgb(0x93, 0xE5, 0x93); //Color.FromArgb(192, 192, 255);
                    break;
                case "Red":
                    e.Appearance.BackColor = Color.FromArgb(0xFD, 0x70, 0x70); //Color.FromArgb(192, 192, 255);
                    break;
            }
            //if (SourceType == "Yellow") e.Appearance.BackColor = Color.Yellow; //Color.FromArgb(192, 192, 255);       
             */
        }

        private void barButtonItem7_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grdVMusteriListesi.OptionsView.ShowGroupPanel = true;

        }

        private void tbEk_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            try
            {
                //Sözleşme griddinde değişiklik varsa kayıt kontrolü yapılsıngr
                if (SozlesmeDurum == true) { SaveSozlesmeler(); if (SozlesmeDurum == true) { tbMain.SelectedTabPageIndex = 4; return; } }

                switch (tbEk.SelectedTabPageIndex)
                {
                  

                    case 0:
                        grpSertifika.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListSertifikalar(MusteriKodu);
                        break;

                    case 1:
                        grpKullanilanYazi.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListKullanilanYazilimlar(MusteriKodu);
                        break;

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnProjeDetay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            
         


        }



        private void repButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
           
        }

        private void repButtonEdit_ButtonClick_1(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
           

        }

        private void zProjeDetay_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string ProjeId = grdVProjeler.GetFocusedRowCellDisplayText(cPro_Id).ToString();
            int scalewidth = string.IsNullOrEmpty(grdVProjeler.GetFocusedRowCellDisplayText(cPro_ScaleWidth).ToString()) ? 11 : Convert.ToInt32(grdVProjeler.GetFocusedRowCellDisplayText(cPro_ScaleWidth).ToString());

            if (string.IsNullOrEmpty(ProjeId))
                MessageBox.Show("İlgili satırı kayıt ettikten sonra yeniden deneyin...");
            else
            {
                frmGantSchedule.ProjeNo = ProjeId;
                frmGantSchedule.MusteriTanimi = MusteriAdi;
                frmGantSchedule.FirmaKodu = MusteriKodu;
                frmGantSchedule.ScaleWidth = scalewidth;

                var frm = new frmGantSchedule();

                frm.Size = new System.Drawing.Size(1276,486);
           
                frm.WindowState = FormWindowState.Normal;
                
            

                frm.ShowDialog();

                GetListProjeler(MusteriKodu);

            }

        }

    }
}