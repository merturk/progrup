﻿namespace GABI_CRM
{
    partial class Firmalar

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Firmalar));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colMusteriKodu1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cAdi = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cSektoru = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repLupIlce = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_cIlce = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repLupSehir = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_cSehirKodu = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn8 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repLupUlke = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_cUlkeKodu = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn9 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cAdres = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn10 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cVergiDairesi = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.grdMusteriListesi = new DevExpress.XtraGrid.GridControl();
            this.grdVMusteriListesi = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cTypeF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.cRenk = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.cMusteriKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSegmantasyon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repSegmantas = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cKategori = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cAdi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSektoru = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repSektor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cAltSektor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cYetkili = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cYetkiliPozisyonu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cIlce = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSehirKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cUlkeKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cAdres = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cVergiDairesi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cNeredenUlasildi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repLupNereden = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cFirmaTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTel = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cFimaFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cFirmaWebAdresi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cCalisanKisiSayisi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cBagliDigerFirmalar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cCiro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repCiro = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cCiroYuzde = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cAileSirketi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox26 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cEmpID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOlusturan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOlusturmaTarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cDegistiren = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cDegistirmeTarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.RepMemNereden = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repZiyNedeni = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemLookUpEdit36 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutView2 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn11 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colID1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn12 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colMusteriKodu2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn13 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colAdi1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn14 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colSektoru1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn15 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colIlce1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn16 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colSehirKodu1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn17 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colUlkeKodu1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn18 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colAdres1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn19 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colVergiDairesi1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnRefreshYetkiliKisiler = new DevExpress.XtraBars.BarButtonItem();
            this.btnYetkiliKisiEkle = new DevExpress.XtraBars.BarButtonItem();
            this.btnSaveYetkiliKisiler = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.btnYenileZiyaret = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleZiyaret = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetZiyaret = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazZiyaret = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilZiyaret = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl3 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.btnYenileTeklif = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleTeklif = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetTeklif = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazTeklif = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilTeklif = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl4 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.btnYenileSozlesmeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleSozlemeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydet = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazSozlemeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilSozlemeler = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl5 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar6 = new DevExpress.XtraBars.Bar();
            this.btnYenileProjeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleProjeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetProjeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazProjeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilProjeler = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl6 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar7 = new DevExpress.XtraBars.Bar();
            this.btnSerRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleSertifka = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetSertifika = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazSertifika = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilSertifika = new DevExpress.XtraBars.BarButtonItem();
            this.btnYenileEgitimler = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar8 = new DevExpress.XtraBars.Bar();
            this.btnYenileOdemeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleOdemeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetOdemeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazdirOdemeler = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilOdemeler = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl7 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar9 = new DevExpress.XtraBars.Bar();
            this.btnEkleProjeDetay = new DevExpress.XtraBars.BarButtonItem();
            this.btnSilProjeDetay = new DevExpress.XtraBars.BarButtonItem();
            this.btnKarneGiris = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl8 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar10 = new DevExpress.XtraBars.Bar();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl9 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar11 = new DevExpress.XtraBars.Bar();
            this.btnAttachFileMusteri = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.btnEmailGonderim = new DevExpress.XtraBars.BarButtonItem();
            this.bar13 = new DevExpress.XtraBars.Bar();
            this.btnYazilimYenile = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazilimEkle = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazilimKaydet = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazilimYazdir = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazilimSil = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl10 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar12 = new DevExpress.XtraBars.Bar();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.bar14 = new DevExpress.XtraBars.Bar();
            this.btnProjeDetay = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnKayderSozlesmeler = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.tbMain = new DevExpress.XtraTab.XtraTabControl();
            this.tbMusList = new DevExpress.XtraTab.XtraTabPage();
            this.tbYetkiliKisi = new DevExpress.XtraTab.XtraTabPage();
            this.grpYetkiliKisiler = new DevExpress.XtraEditors.GroupControl();
            this.grdYetkiliKisiler = new DevExpress.XtraGrid.GridControl();
            this.lvYetkiliKisiler = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_STATU = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repYetkiliStatu = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_layoutViewColumn110_7 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_ADI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_ADI = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_SOYADI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_SOYADI = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_GOREVI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_GOREVI = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_EMAIL = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_EMAIL = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_TELEFON1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repTel2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutViewField_cEMP_TELEFON1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_TELEFON2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_TELEFON2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPicture = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_cPicture = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cNote = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_cNote = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.tbZiyaretler = new DevExpress.XtraTab.XtraTabPage();
            this.grpZiyaret = new DevExpress.XtraEditors.GroupControl();
            this.grdZiyaretler = new DevExpress.XtraGrid.GridControl();
            this.grdVZiyaretler = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTarih = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repCheckUpTar = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repGorusulenKisiler = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repGorusenKisiler2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repAnaGrup = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repAltGrup = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.czZiyaretNedeni = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repZiyNedeniM = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.czPromosyon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repPromosyon = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.czPromosyonMiktar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemLookUpEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repGorusenKisiler = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutView3 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn20 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn21 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn22 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn23 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn24 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn25 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn26 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField7 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn27 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField8 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn28 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField9 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView4 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn29 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField10 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn30 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField11 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn31 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField12 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn32 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField13 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn33 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField14 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn34 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField15 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn35 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField16 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn36 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField17 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn37 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField18 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.tbTeklifler = new DevExpress.XtraTab.XtraTabPage();
            this.grpTeklifler = new DevExpress.XtraEditors.GroupControl();
            this.grdTeklifler = new DevExpress.XtraGrid.GridControl();
            this.grdVTeklifler = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cTekID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cTekFirmaKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cTekGorusmeTarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.cTekVerilenKisiler = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemChk1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.czAnaGrup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTekChkTeklifAnaGroup = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.czAltGrup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTekChkTeklifAltGroup = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cTekTipi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkTip = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cTekTarih = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.cTekHazirlayan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkHazirlayan = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cTekSonucu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkLSonuc = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cIkincilKisi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cBirincilKisi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cTekKazanmaKaybetmeNedeni = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkNedenler = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cTekKazanmaKaybetmeNotu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox10 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox11 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox12 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemLookUpEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repChkTekSonuc = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repCmbKisi = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repAltGrupTeklif = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repAnaGrupTeklif = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repSegman = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutView5 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn38 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField19 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn39 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField20 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn40 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField21 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn41 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField22 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn42 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField23 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn43 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField24 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn44 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField25 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn45 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField26 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn46 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField27 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView6 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn47 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField28 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn48 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField29 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn49 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField30 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn50 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField31 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn51 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField32 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn52 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField33 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn53 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField34 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn54 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField35 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn55 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField36 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.tbSozlesme = new DevExpress.XtraTab.XtraTabPage();
            this.grpSozlesmeler = new DevExpress.XtraEditors.GroupControl();
            this.grdSozlesmeler = new DevExpress.XtraGrid.GridControl();
            this.grdVSozlesmeler = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_TeklifNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repSozlTeklNo = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cSoz_Musteri_Kodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_NeredenUlasildi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repMem = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.cSoz_Imza_Tar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.cSoz_Icerik = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repSozIcerik = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cSoz_Teklif_Fiyat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_Teklif_Fiyat_Birim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repBr = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cSoz_Imz_Tutar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_Imz_Tutar_Birim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_Satis_Yapan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkSatYap = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cSoz_Performans_Prim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_Performans_Prim_Birim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSoz_Baslangıc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.cSoz_Bitis = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox13 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox14 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox15 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemLookUpEdit20 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit22 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit23 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemLookUpEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemMemoEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox16 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutView7 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn56 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField37 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn57 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField38 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn58 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField39 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn59 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField40 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn60 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField41 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn61 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField42 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn62 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField43 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn63 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField44 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn64 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField45 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView8 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn65 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField46 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn66 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField47 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn67 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField48 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn68 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField49 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn69 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField50 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn70 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField51 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn71 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField52 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn72 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField53 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn73 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField54 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.tbOdemeler = new DevExpress.XtraTab.XtraTabPage();
            this.grpOdemeler = new DevExpress.XtraEditors.GroupControl();
            this.grdOdemeler = new DevExpress.XtraGrid.GridControl();
            this.grdVOdemeler = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cOde_Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Musteri_Kodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_SozlesmeNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkOdemeSozNo = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cOde_Fatura = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repLFaturaTipi = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cOde_Fatura_Fiyat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Fatura_Fiyat_Br = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repFatBr = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cOde_OdemeSekli = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repLOdmSekli = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cOde_OdemeSekli_Fiyat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_OdemeSekli_Br = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Ulasim_Giderleri = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repLUlsSekli = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cOde_Ulasim_Giderleri_Fiyat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Ulasim_Giderleri_Br = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Konaklama_Giderleri = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repLKnkSekli = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cOde_Konaklama_Giderleri_Fiyat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Konaklama_Giderleri_Br = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Yemek_Gideleri = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repYmkSekl = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cOde_Yemek_Giderleri_Fiyat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Yemek_Giderleri_Br = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cOde_Notlar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repNot = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox18 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox19 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox20 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit24 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit25 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemLookUpEdit26 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit27 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit28 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit29 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemCheckedComboBoxEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemMemoEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemDateEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemLookUpEdit30 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemDateEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox21 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox17 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckedComboBoxEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemComboBox22 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.layoutView9 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn74 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField55 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn75 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField56 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn76 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField57 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn77 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField58 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn78 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField59 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn79 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField60 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn80 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField61 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn81 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField62 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn82 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField63 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView10 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn83 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField64 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn84 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField65 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn85 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField66 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn86 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField67 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn87 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField68 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn88 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField69 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn89 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField70 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn90 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField71 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn91 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField72 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.tbProjeler = new DevExpress.XtraTab.XtraTabPage();
            this.grpProjeler = new DevExpress.XtraEditors.GroupControl();
            this.pnlProje = new DevExpress.XtraEditors.PanelControl();
            this.tbMainProjeler = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.grdProjeler = new DevExpress.XtraGrid.GridControl();
            this.grdVProjeler = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cPro_Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cPro_Musteri_Kodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cPro_Sozlesme_No = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkSozNo = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.cPro_ProjeTanim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repAnaGrupProje = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cPro_SunumTarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTar = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.zProjeDetay = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.cPro_Chk_Tarih = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cPro_Chk_Sonucu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repProMem = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.cPro_Analiz_Tarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cPro_Analiz_Sonucu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cPro_Analiz_Rapor_Tarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdPro_ProjeTanim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cPro_ScaleWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutView11 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn92 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField73 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn93 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField74 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn94 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField75 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn95 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField76 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn96 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemLookUpEdit31 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField77 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn97 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemLookUpEdit32 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField78 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn98 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemLookUpEdit33 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField79 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn99 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField80 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn100 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField81 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView12 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn101 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField82 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn102 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField83 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn103 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField84 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn104 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField85 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn105 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField86 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn106 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField87 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn107 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField88 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn108 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField89 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn109 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField90 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.grdProjeDetay = new DevExpress.XtraGrid.GridControl();
            this.grdVProjeDetay = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.cPdt_ProjeNo = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn110 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeDetayTanim = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repAltGrupProje = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_layoutViewColumn110_8 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeLideri = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repChkDanisman = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.layoutViewField_layoutViewColumn110_9 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeSorumlusu = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn110_5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeBaslangicTarihi = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repTarProDet = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.layoutViewField_layoutViewColumn110_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeBitisTarihi = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn110_2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repAnaGrupProje1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_layoutViewColumn110_3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn110_4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn110_6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn111 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn112 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani3 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn113 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn114 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani4 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn115 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn116 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani5 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn117 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn118 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani6 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn119 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn120 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani7 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn121 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi8 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn122 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani9 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn123 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi9 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn124 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.Pdt_ProjeModulDanismani10 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn125 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi10 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn126 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani11 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn127 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulAdi11 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn128 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPdt_ProjeModulDanismani12 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn129 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.repositoryItemComboBox23 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox24 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox25 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit34 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit35 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemMemoEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemCheckedComboBoxEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.pvtKarneNotlari = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pAy = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.tbilaveBilgi = new DevExpress.XtraTab.XtraTabPage();
            this.tbEk = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.grpSertifika = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grdSertikalar = new DevExpress.XtraGrid.GridControl();
            this.grdVSertikalar = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.CSID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CSFirmaKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cSTanim = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repSertifakalar = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cSAktifPasif = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox32 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox28 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox29 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox30 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit38 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit39 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemLookUpEdit40 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit41 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit42 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit43 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemCheckedComboBoxEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemMemoEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemDateEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit20 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemLookUpEdit44 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemDateEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox31 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox27 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckedComboBoxEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemLookUpEdit37 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutView13 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn110 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField91 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn111 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField92 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn112 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField93 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn113 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField94 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn114 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField95 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn115 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField96 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn116 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField97 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn117 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField98 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn118 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField99 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView14 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn119 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField100 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn120 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField101 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn121 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField102 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn122 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField103 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn123 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField104 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn124 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField105 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn125 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField106 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn126 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField107 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn127 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField108 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.grpKullanilanYazi = new DevExpress.XtraEditors.GroupControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.grdKullanilanYaz = new DevExpress.XtraGrid.GridControl();
            this.grdVKullanilanYaz = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cY_Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cY_Tipi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repYazilimGrup = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.ccY_Marka = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repYazilimMarka = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.cY_Adi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cY_KullanimOrani = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cYFirmaKodu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox34 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox35 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox36 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit46 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit47 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemPictureEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemLookUpEdit48 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit49 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit50 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit51 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemCheckedComboBoxEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemMemoEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemDateEdit20 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit22 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit23 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit24 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemCheckedComboBoxEdit25 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemLookUpEdit52 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemDateEdit22 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox37 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox38 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckedComboBoxEdit26 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemDateEdit23 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit24 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckedComboBoxEdit27 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemLookUpEdit53 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemComboBox33 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit45 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutView15 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn128 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField109 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn129 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField110 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn130 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField111 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn131 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField112 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn132 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField113 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn133 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField114 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn134 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField115 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn135 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField116 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn136 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField117 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutView16 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn137 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField118 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn138 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField119 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn139 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField120 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn140 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField121 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn141 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField122 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn142 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField123 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn143 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField124 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn144 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField125 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewColumn145 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField126 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSektoru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupIlce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cIlce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupSehir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSehirKodu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupUlke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cUlkeKodu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cVergiDairesi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMusteriListesi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVMusteriListesi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSegmantas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSektor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupNereden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepMemNereden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repZiyNedeni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdi1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSektoru1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIlce1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSehirKodu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colUlkeKodu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdres1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colVergiDairesi1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMain)).BeginInit();
            this.tbMain.SuspendLayout();
            this.tbMusList.SuspendLayout();
            this.tbYetkiliKisi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpYetkiliKisiler)).BeginInit();
            this.grpYetkiliKisiler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdYetkiliKisiler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvYetkiliKisiler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYetkiliStatu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_ADI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_SOYADI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_GOREVI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_EMAIL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).BeginInit();
            this.tbZiyaretler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpZiyaret)).BeginInit();
            this.grpZiyaret.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdZiyaretler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVZiyaretler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarih)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarih.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckUpTar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckUpTar.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGorusulenKisiler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGorusenKisiler2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAltGrup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repZiyNedeniM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPromosyon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGorusenKisiler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField18)).BeginInit();
            this.tbTeklifler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTeklifler)).BeginInit();
            this.grpTeklifler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTeklifler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVTeklifler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemChk1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTekChkTeklifAnaGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTekChkTeklifAltGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkTip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkHazirlayan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkLSonuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkNedenler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkTekSonuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCmbKisi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAltGrupTeklif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrupTeklif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSegman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField36)).BeginInit();
            this.tbSozlesme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSozlesmeler)).BeginInit();
            this.grpSozlesmeler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSozlesmeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVSozlesmeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSozlTeklNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSozIcerik)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkSatYap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField54)).BeginInit();
            this.tbOdemeler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpOdemeler)).BeginInit();
            this.grpOdemeler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOdemeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVOdemeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkOdemeSozNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLFaturaTipi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFatBr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLOdmSekli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLUlsSekli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLKnkSekli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYmkSekl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit12.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit13.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit9.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit10.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit11.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField72)).BeginInit();
            this.tbProjeler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpProjeler)).BeginInit();
            this.grpProjeler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProje)).BeginInit();
            this.pnlProje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMainProjeler)).BeginInit();
            this.tbMainProjeler.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProjeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVProjeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkSozNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrupProje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTar.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zProjeDetay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repProMem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField90)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProjeDetay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVProjeDetay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAltGrupProje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkDanisman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarProDet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarProDet.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrupProje1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit14.CalendarTimeProperties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pvtKarneNotlari)).BeginInit();
            this.tbilaveBilgi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbEk)).BeginInit();
            this.tbEk.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSertifika)).BeginInit();
            this.grpSertifika.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSertikalar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVSertikalar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSertifakalar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit18.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit19.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit15.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit16.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit17.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField108)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpKullanilanYazi)).BeginInit();
            this.grpKullanilanYazi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdKullanilanYaz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVKullanilanYaz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYazilimGrup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYazilimMarka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit20.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit21.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit22.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit23.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit24.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField126)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutView1
            // 
            this.layoutView1.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn2,
            this.layoutViewColumn3,
            this.layoutViewColumn4,
            this.layoutViewColumn5,
            this.layoutViewColumn6,
            this.layoutViewColumn7,
            this.layoutViewColumn8,
            this.layoutViewColumn9,
            this.layoutViewColumn10});
            this.layoutView1.GridControl = this.grdMusteriListesi;
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsSelection.MultiSelect = true;
            this.layoutView1.OptionsView.ShowCardCaption = false;
            this.layoutView1.TemplateCard = null;
            // 
            // layoutViewColumn2
            // 
            this.layoutViewColumn2.Caption = "ID";
            this.layoutViewColumn2.FieldName = "ID";
            this.layoutViewColumn2.LayoutViewField = this.layoutViewField_cID;
            this.layoutViewColumn2.Name = "layoutViewColumn2";
            // 
            // layoutViewField_cID
            // 
            this.layoutViewField_cID.EditorPreferredWidth = 253;
            this.layoutViewField_cID.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_cID.Name = "layoutViewField_cID";
            this.layoutViewField_cID.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cID.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cID.TextToControlDistance = 5;
            // 
            // layoutViewColumn3
            // 
            this.layoutViewColumn3.Caption = "Müşteri Kodu";
            this.layoutViewColumn3.FieldName = "MusteriKodu";
            this.layoutViewColumn3.LayoutViewField = this.layoutViewField_colMusteriKodu1;
            this.layoutViewColumn3.Name = "layoutViewColumn3";
            this.layoutViewColumn3.Width = 84;
            // 
            // layoutViewField_colMusteriKodu1
            // 
            this.layoutViewField_colMusteriKodu1.EditorPreferredWidth = 253;
            this.layoutViewField_colMusteriKodu1.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField_colMusteriKodu1.Name = "layoutViewField_colMusteriKodu1";
            this.layoutViewField_colMusteriKodu1.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_colMusteriKodu1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colMusteriKodu1.TextToControlDistance = 5;
            // 
            // layoutViewColumn4
            // 
            this.layoutViewColumn4.Caption = "Adı";
            this.layoutViewColumn4.FieldName = "Adi";
            this.layoutViewColumn4.LayoutViewField = this.layoutViewField_cAdi;
            this.layoutViewColumn4.Name = "layoutViewColumn4";
            this.layoutViewColumn4.Width = 118;
            // 
            // layoutViewField_cAdi
            // 
            this.layoutViewField_cAdi.EditorPreferredWidth = 253;
            this.layoutViewField_cAdi.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField_cAdi.Name = "layoutViewField_cAdi";
            this.layoutViewField_cAdi.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cAdi.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cAdi.TextToControlDistance = 5;
            // 
            // layoutViewColumn5
            // 
            this.layoutViewColumn5.Caption = "Sektorü";
            this.layoutViewColumn5.FieldName = "Sektoru";
            this.layoutViewColumn5.LayoutViewField = this.layoutViewField_cSektoru;
            this.layoutViewColumn5.Name = "layoutViewColumn5";
            this.layoutViewColumn5.Width = 133;
            // 
            // layoutViewField_cSektoru
            // 
            this.layoutViewField_cSektoru.EditorPreferredWidth = 253;
            this.layoutViewField_cSektoru.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField_cSektoru.Name = "layoutViewField_cSektoru";
            this.layoutViewField_cSektoru.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cSektoru.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cSektoru.TextToControlDistance = 5;
            // 
            // layoutViewColumn6
            // 
            this.layoutViewColumn6.Caption = "Ilçe";
            this.layoutViewColumn6.ColumnEdit = this.repLupIlce;
            this.layoutViewColumn6.FieldName = "Ilce";
            this.layoutViewColumn6.LayoutViewField = this.layoutViewField_cIlce;
            this.layoutViewColumn6.Name = "layoutViewColumn6";
            this.layoutViewColumn6.Width = 101;
            // 
            // repLupIlce
            // 
            this.repLupIlce.AutoHeight = false;
            this.repLupIlce.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupIlce.DisplayMember = "Tanim";
            this.repLupIlce.Name = "repLupIlce";
            this.repLupIlce.NullText = "";
            this.repLupIlce.ValueMember = "GP_ID";
            // 
            // layoutViewField_cIlce
            // 
            this.layoutViewField_cIlce.EditorPreferredWidth = 253;
            this.layoutViewField_cIlce.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField_cIlce.Name = "layoutViewField_cIlce";
            this.layoutViewField_cIlce.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cIlce.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cIlce.TextToControlDistance = 5;
            // 
            // layoutViewColumn7
            // 
            this.layoutViewColumn7.Caption = "Şehir";
            this.layoutViewColumn7.ColumnEdit = this.repLupSehir;
            this.layoutViewColumn7.FieldName = "SehirKodu";
            this.layoutViewColumn7.LayoutViewField = this.layoutViewField_cSehirKodu;
            this.layoutViewColumn7.Name = "layoutViewColumn7";
            this.layoutViewColumn7.Width = 96;
            // 
            // repLupSehir
            // 
            this.repLupSehir.AutoHeight = false;
            this.repLupSehir.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupSehir.DisplayMember = "Tanim";
            this.repLupSehir.Name = "repLupSehir";
            this.repLupSehir.NullText = "";
            this.repLupSehir.ValueMember = "Kod";
            // 
            // layoutViewField_cSehirKodu
            // 
            this.layoutViewField_cSehirKodu.EditorPreferredWidth = 253;
            this.layoutViewField_cSehirKodu.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField_cSehirKodu.Name = "layoutViewField_cSehirKodu";
            this.layoutViewField_cSehirKodu.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cSehirKodu.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cSehirKodu.TextToControlDistance = 5;
            // 
            // layoutViewColumn8
            // 
            this.layoutViewColumn8.Caption = "Ülke";
            this.layoutViewColumn8.ColumnEdit = this.repLupUlke;
            this.layoutViewColumn8.FieldName = "UlkeKodu";
            this.layoutViewColumn8.LayoutViewField = this.layoutViewField_cUlkeKodu;
            this.layoutViewColumn8.Name = "layoutViewColumn8";
            this.layoutViewColumn8.Width = 104;
            // 
            // repLupUlke
            // 
            this.repLupUlke.AutoHeight = false;
            this.repLupUlke.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupUlke.DisplayMember = "Tanim";
            this.repLupUlke.Name = "repLupUlke";
            this.repLupUlke.NullText = "";
            this.repLupUlke.ValueMember = "Kod";
            // 
            // layoutViewField_cUlkeKodu
            // 
            this.layoutViewField_cUlkeKodu.EditorPreferredWidth = 253;
            this.layoutViewField_cUlkeKodu.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField_cUlkeKodu.Name = "layoutViewField_cUlkeKodu";
            this.layoutViewField_cUlkeKodu.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cUlkeKodu.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cUlkeKodu.TextToControlDistance = 5;
            // 
            // layoutViewColumn9
            // 
            this.layoutViewColumn9.Caption = "Adres";
            this.layoutViewColumn9.FieldName = "Adres";
            this.layoutViewColumn9.LayoutViewField = this.layoutViewField_cAdres;
            this.layoutViewColumn9.Name = "layoutViewColumn9";
            this.layoutViewColumn9.Width = 165;
            // 
            // layoutViewField_cAdres
            // 
            this.layoutViewField_cAdres.EditorPreferredWidth = 253;
            this.layoutViewField_cAdres.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField_cAdres.Name = "layoutViewField_cAdres";
            this.layoutViewField_cAdres.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField_cAdres.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cAdres.TextToControlDistance = 5;
            // 
            // layoutViewColumn10
            // 
            this.layoutViewColumn10.Caption = "Vergi Dairesi";
            this.layoutViewColumn10.FieldName = "VergiDairesi";
            this.layoutViewColumn10.LayoutViewField = this.layoutViewField_cVergiDairesi;
            this.layoutViewColumn10.Name = "layoutViewColumn10";
            this.layoutViewColumn10.Width = 87;
            // 
            // layoutViewField_cVergiDairesi
            // 
            this.layoutViewField_cVergiDairesi.EditorPreferredWidth = 253;
            this.layoutViewField_cVergiDairesi.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField_cVergiDairesi.Name = "layoutViewField_cVergiDairesi";
            this.layoutViewField_cVergiDairesi.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField_cVergiDairesi.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_cVergiDairesi.TextToControlDistance = 5;
            // 
            // grdMusteriListesi
            // 
            this.grdMusteriListesi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMusteriListesi.Location = new System.Drawing.Point(0, 29);
            this.grdMusteriListesi.MainView = this.grdVMusteriListesi;
            this.grdMusteriListesi.Name = "grdMusteriListesi";
            this.grdMusteriListesi.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox3,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemPictureEdit1,
            this.repositoryItemImageEdit1,
            this.repositoryItemPictureEdit2,
            this.repLupUlke,
            this.repLupSehir,
            this.repLupIlce,
            this.repSektor,
            this.RepMemNereden,
            this.repZiyNedeni,
            this.repSegmantas,
            this.repTel,
            this.repositoryItemTextEdit5,
            this.repositoryItemImageComboBox1,
            this.repLupNereden,
            this.repCiro,
            this.repositoryItemTextEdit6,
            this.repositoryItemLookUpEdit36,
            this.repositoryItemComboBox26,
            this.repositoryItemImageComboBox2});
            this.grdMusteriListesi.Size = new System.Drawing.Size(1357, 379);
            this.grdMusteriListesi.TabIndex = 6;
            this.grdMusteriListesi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVMusteriListesi,
            this.layoutView2,
            this.layoutView1});
            this.grdMusteriListesi.Click += new System.EventHandler(this.grdMusteri_Click);
            // 
            // grdVMusteriListesi
            // 
            this.grdVMusteriListesi.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cID,
            this.cTypeF,
            this.cRenk,
            this.cMusteriKodu,
            this.cSegmantasyon,
            this.cKategori,
            this.cAdi,
            this.cSektoru,
            this.cAltSektor,
            this.cYetkili,
            this.cYetkiliPozisyonu,
            this.cEmail,
            this.cIlce,
            this.cSehirKodu,
            this.cUlkeKodu,
            this.cAdres,
            this.cVergiDairesi,
            this.cNeredenUlasildi,
            this.cFirmaTel,
            this.cFimaFax,
            this.cFirmaWebAdresi,
            this.cCalisanKisiSayisi,
            this.cBagliDigerFirmalar,
            this.cType,
            this.cCiro,
            this.cCiroYuzde,
            this.cAileSirketi,
            this.cEmpID,
            this.cOlusturan,
            this.cOlusturmaTarihi,
            this.cDegistiren,
            this.cDegistirmeTarihi});
            this.grdVMusteriListesi.GridControl = this.grdMusteriListesi;
            this.grdVMusteriListesi.Name = "grdVMusteriListesi";
            this.grdVMusteriListesi.OptionsBehavior.Editable = false;
            this.grdVMusteriListesi.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVMusteriListesi.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVMusteriListesi.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVMusteriListesi.OptionsSelection.MultiSelect = true;
            this.grdVMusteriListesi.OptionsView.ColumnAutoWidth = false;
            this.grdVMusteriListesi.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.grdVMusteriListesi.OptionsView.RowAutoHeight = true;
            this.grdVMusteriListesi.OptionsView.ShowAutoFilterRow = true;
            this.grdVMusteriListesi.OptionsView.ShowFooter = true;
            this.grdVMusteriListesi.OptionsView.ShowGroupPanel = false;
            this.grdVMusteriListesi.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdVMusteriListesi_RowStyle);
            this.grdVMusteriListesi.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdMusteriListesi_FocusedRowChanged);
            this.grdVMusteriListesi.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.grdVMusteriListesi.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanging);
            this.grdVMusteriListesi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView1_KeyDown);
            this.grdVMusteriListesi.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // cID
            // 
            this.cID.Caption = "ID";
            this.cID.FieldName = "ID";
            this.cID.Name = "cID";
            // 
            // cTypeF
            // 
            this.cTypeF.Caption = "...";
            this.cTypeF.ColumnEdit = this.repositoryItemImageComboBox1;
            this.cTypeF.FieldName = "Type";
            this.cTypeF.Name = "cTypeF";
            this.cTypeF.ToolTip = "Aktarım";
            this.cTypeF.Width = 26;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Müşteri Listesine Aktar", 1, 31),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Kart Vizit Listesine Aktar", 2, 34)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.SmallImage;
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            this.SmallImage.Images.SetKeyName(43, "attach.png");
            this.SmallImage.Images.SetKeyName(44, "paper_clip.png");
            this.SmallImage.Images.SetKeyName(45, "note2.png");
            this.SmallImage.Images.SetKeyName(46, "barcode_arrow_up.png");
            this.SmallImage.Images.SetKeyName(47, "Mail4.png");
            this.SmallImage.Images.SetKeyName(48, "Document2.png");
            this.SmallImage.Images.SetKeyName(49, "email.png");
            this.SmallImage.Images.SetKeyName(50, "Forum.png");
            this.SmallImage.Images.SetKeyName(51, "pin_blue.png");
            this.SmallImage.Images.SetKeyName(52, "pin_green.png");
            this.SmallImage.Images.SetKeyName(53, "pin_grey.png");
            this.SmallImage.Images.SetKeyName(54, "pin_orange.png");
            this.SmallImage.Images.SetKeyName(55, "pin_red.png");
            this.SmallImage.Images.SetKeyName(56, "pin_yellow.png");
            this.SmallImage.Images.SetKeyName(57, "calendar_up.png");
            this.SmallImage.Images.SetKeyName(58, "businessman_view.png");
            // 
            // cRenk
            // 
            this.cRenk.Caption = "Durum";
            this.cRenk.ColumnEdit = this.repositoryItemImageComboBox2;
            this.cRenk.FieldName = "Renk";
            this.cRenk.Name = "cRenk";
            this.cRenk.Visible = true;
            this.cRenk.VisibleIndex = 0;
            this.cRenk.Width = 28;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Ziyaret edilmişler", "Yellow", 55),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Müşteri ziyaret edildi ve teklif verildi ", "Orange", 54),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Müşteri teklif onaylandı satışı gerçekleşti ", "Green", 52),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Teklif onaylanmadı ", "Red", 55)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            this.repositoryItemImageComboBox2.SmallImages = this.SmallImage;
            // 
            // cMusteriKodu
            // 
            this.cMusteriKodu.Caption = "Kodu";
            this.cMusteriKodu.FieldName = "MusteriKodu";
            this.cMusteriKodu.Name = "cMusteriKodu";
            this.cMusteriKodu.OptionsColumn.AllowEdit = false;
            this.cMusteriKodu.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cMusteriKodu.Visible = true;
            this.cMusteriKodu.VisibleIndex = 1;
            this.cMusteriKodu.Width = 54;
            // 
            // cSegmantasyon
            // 
            this.cSegmantasyon.Caption = "Segmantasyonu";
            this.cSegmantasyon.ColumnEdit = this.repSegmantas;
            this.cSegmantasyon.FieldName = "Segmantasyonu";
            this.cSegmantasyon.Name = "cSegmantasyon";
            this.cSegmantasyon.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cSegmantasyon.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.cSegmantasyon.ToolTip = "Segmantasyonu";
            this.cSegmantasyon.Visible = true;
            this.cSegmantasyon.VisibleIndex = 2;
            this.cSegmantasyon.Width = 64;
            // 
            // repSegmantas
            // 
            this.repSegmantas.AutoHeight = false;
            this.repSegmantas.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSegmantas.DisplayMember = "Kod";
            this.repSegmantas.Name = "repSegmantas";
            this.repSegmantas.NullText = "";
            this.repSegmantas.ValueMember = "Kod";
            // 
            // cKategori
            // 
            this.cKategori.Caption = "Kategori";
            this.cKategori.FieldName = "Kategorim";
            this.cKategori.Name = "cKategori";
            this.cKategori.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cKategori.Visible = true;
            this.cKategori.VisibleIndex = 3;
            // 
            // cAdi
            // 
            this.cAdi.Caption = "Firma Adı";
            this.cAdi.FieldName = "Adi";
            this.cAdi.Name = "cAdi";
            this.cAdi.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cAdi.Visible = true;
            this.cAdi.VisibleIndex = 4;
            this.cAdi.Width = 140;
            // 
            // cSektoru
            // 
            this.cSektoru.Caption = "Alt Sektör";
            this.cSektoru.ColumnEdit = this.repSektor;
            this.cSektoru.FieldName = "Sektoru";
            this.cSektoru.Name = "cSektoru";
            this.cSektoru.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cSektoru.Visible = true;
            this.cSektoru.VisibleIndex = 9;
            this.cSektoru.Width = 133;
            // 
            // repSektor
            // 
            this.repSektor.AutoHeight = false;
            this.repSektor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSektor.DisplayMember = "Tanim";
            this.repSektor.DropDownRows = 20;
            this.repSektor.Name = "repSektor";
            this.repSektor.NullText = "";
            this.repSektor.PopupWidth = 600;
            this.repSektor.ValueMember = "Kod";
            // 
            // cAltSektor
            // 
            this.cAltSektor.Caption = "Ana Sektör";
            this.cAltSektor.FieldName = "SektoruAna";
            this.cAltSektor.Name = "cAltSektor";
            this.cAltSektor.Visible = true;
            this.cAltSektor.VisibleIndex = 8;
            this.cAltSektor.Width = 117;
            // 
            // cYetkili
            // 
            this.cYetkili.Caption = "Yetkili";
            this.cYetkili.FieldName = "Yetkili";
            this.cYetkili.Name = "cYetkili";
            this.cYetkili.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cYetkili.Visible = true;
            this.cYetkili.VisibleIndex = 5;
            this.cYetkili.Width = 84;
            // 
            // cYetkiliPozisyonu
            // 
            this.cYetkiliPozisyonu.Caption = "Yetkili Pozisyonu";
            this.cYetkiliPozisyonu.FieldName = "YetkiliPozisyonu";
            this.cYetkiliPozisyonu.Name = "cYetkiliPozisyonu";
            this.cYetkiliPozisyonu.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cYetkiliPozisyonu.Visible = true;
            this.cYetkiliPozisyonu.VisibleIndex = 6;
            this.cYetkiliPozisyonu.Width = 90;
            // 
            // cEmail
            // 
            this.cEmail.Caption = "Email";
            this.cEmail.FieldName = "Email";
            this.cEmail.Name = "cEmail";
            this.cEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cEmail.Visible = true;
            this.cEmail.VisibleIndex = 7;
            // 
            // cIlce
            // 
            this.cIlce.Caption = "Ilçe";
            this.cIlce.ColumnEdit = this.repLupIlce;
            this.cIlce.FieldName = "Ilce";
            this.cIlce.Name = "cIlce";
            this.cIlce.Visible = true;
            this.cIlce.VisibleIndex = 14;
            this.cIlce.Width = 101;
            // 
            // cSehirKodu
            // 
            this.cSehirKodu.Caption = "Şehir";
            this.cSehirKodu.ColumnEdit = this.repLupSehir;
            this.cSehirKodu.FieldName = "SehirKodu";
            this.cSehirKodu.Name = "cSehirKodu";
            this.cSehirKodu.Visible = true;
            this.cSehirKodu.VisibleIndex = 13;
            this.cSehirKodu.Width = 96;
            // 
            // cUlkeKodu
            // 
            this.cUlkeKodu.Caption = "Ülke";
            this.cUlkeKodu.ColumnEdit = this.repLupUlke;
            this.cUlkeKodu.FieldName = "UlkeKodu";
            this.cUlkeKodu.Name = "cUlkeKodu";
            this.cUlkeKodu.Visible = true;
            this.cUlkeKodu.VisibleIndex = 12;
            this.cUlkeKodu.Width = 104;
            // 
            // cAdres
            // 
            this.cAdres.Caption = "Adres";
            this.cAdres.FieldName = "Adres";
            this.cAdres.Name = "cAdres";
            this.cAdres.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cAdres.Visible = true;
            this.cAdres.VisibleIndex = 10;
            this.cAdres.Width = 165;
            // 
            // cVergiDairesi
            // 
            this.cVergiDairesi.Caption = "Vergi Dairesi";
            this.cVergiDairesi.FieldName = "VergiDairesi";
            this.cVergiDairesi.Name = "cVergiDairesi";
            this.cVergiDairesi.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cVergiDairesi.Visible = true;
            this.cVergiDairesi.VisibleIndex = 11;
            this.cVergiDairesi.Width = 87;
            // 
            // cNeredenUlasildi
            // 
            this.cNeredenUlasildi.Caption = "Ner.Ulasildi";
            this.cNeredenUlasildi.ColumnEdit = this.repLupNereden;
            this.cNeredenUlasildi.FieldName = "NeredenUlasildi";
            this.cNeredenUlasildi.Name = "cNeredenUlasildi";
            this.cNeredenUlasildi.ToolTip = "Nereden ulaşıldı";
            this.cNeredenUlasildi.Visible = true;
            this.cNeredenUlasildi.VisibleIndex = 15;
            this.cNeredenUlasildi.Width = 136;
            // 
            // repLupNereden
            // 
            this.repLupNereden.AutoHeight = false;
            this.repLupNereden.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupNereden.DisplayMember = "Tanim";
            this.repLupNereden.Name = "repLupNereden";
            this.repLupNereden.NullText = "";
            this.repLupNereden.ValueMember = "Kod";
            // 
            // cFirmaTel
            // 
            this.cFirmaTel.Caption = "FirmaTel";
            this.cFirmaTel.ColumnEdit = this.repTel;
            this.cFirmaTel.FieldName = "FirmaTel";
            this.cFirmaTel.Name = "cFirmaTel";
            this.cFirmaTel.Visible = true;
            this.cFirmaTel.VisibleIndex = 16;
            this.cFirmaTel.Width = 101;
            // 
            // repTel
            // 
            this.repTel.AutoHeight = false;
            this.repTel.Mask.EditMask = "(\\d?\\d?\\d?)\\d\\d\\d-\\d\\d\\d\\d";
            this.repTel.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.repTel.Name = "repTel";
            // 
            // cFimaFax
            // 
            this.cFimaFax.Caption = "FimaFax";
            this.cFimaFax.ColumnEdit = this.repTel;
            this.cFimaFax.FieldName = "FimaFax";
            this.cFimaFax.Name = "cFimaFax";
            this.cFimaFax.Visible = true;
            this.cFimaFax.VisibleIndex = 17;
            this.cFimaFax.Width = 96;
            // 
            // cFirmaWebAdresi
            // 
            this.cFirmaWebAdresi.Caption = "FirmaWebAdresi";
            this.cFirmaWebAdresi.FieldName = "FirmaWebAdresi";
            this.cFirmaWebAdresi.Name = "cFirmaWebAdresi";
            this.cFirmaWebAdresi.Visible = true;
            this.cFirmaWebAdresi.VisibleIndex = 18;
            this.cFirmaWebAdresi.Width = 113;
            // 
            // cCalisanKisiSayisi
            // 
            this.cCalisanKisiSayisi.Caption = "CalisanKisiSayisi";
            this.cCalisanKisiSayisi.DisplayFormat.FormatString = "f0";
            this.cCalisanKisiSayisi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cCalisanKisiSayisi.FieldName = "CalisanKisiSayisi";
            this.cCalisanKisiSayisi.Name = "cCalisanKisiSayisi";
            this.cCalisanKisiSayisi.Visible = true;
            this.cCalisanKisiSayisi.VisibleIndex = 19;
            this.cCalisanKisiSayisi.Width = 107;
            // 
            // cBagliDigerFirmalar
            // 
            this.cBagliDigerFirmalar.Caption = "BagliDigerFirmalar";
            this.cBagliDigerFirmalar.FieldName = "BagliDigerFirmalar";
            this.cBagliDigerFirmalar.Name = "cBagliDigerFirmalar";
            this.cBagliDigerFirmalar.Visible = true;
            this.cBagliDigerFirmalar.VisibleIndex = 20;
            this.cBagliDigerFirmalar.Width = 134;
            // 
            // cType
            // 
            this.cType.Caption = "Type";
            this.cType.FieldName = "Type";
            this.cType.Name = "cType";
            // 
            // cCiro
            // 
            this.cCiro.Caption = "Ciro";
            this.cCiro.ColumnEdit = this.repCiro;
            this.cCiro.FieldName = "Ciro";
            this.cCiro.Name = "cCiro";
            this.cCiro.Visible = true;
            this.cCiro.VisibleIndex = 21;
            // 
            // repCiro
            // 
            this.repCiro.AutoHeight = false;
            this.repCiro.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCiro.DisplayMember = "Tanim";
            this.repCiro.Name = "repCiro";
            this.repCiro.NullText = "";
            this.repCiro.ValueMember = "Kod";
            // 
            // cCiroYuzde
            // 
            this.cCiroYuzde.Caption = "Ciro Yüzde";
            this.cCiroYuzde.ColumnEdit = this.repositoryItemTextEdit6;
            this.cCiroYuzde.DisplayFormat.FormatString = "p";
            this.cCiroYuzde.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cCiroYuzde.FieldName = "CiroYuzdesi";
            this.cCiroYuzde.Name = "cCiroYuzde";
            this.cCiroYuzde.Visible = true;
            this.cCiroYuzde.VisibleIndex = 22;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "p";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // cAileSirketi
            // 
            this.cAileSirketi.Caption = "Aile Sirketi Mi";
            this.cAileSirketi.ColumnEdit = this.repositoryItemComboBox26;
            this.cAileSirketi.FieldName = "AileSirketi";
            this.cAileSirketi.Name = "cAileSirketi";
            this.cAileSirketi.Visible = true;
            this.cAileSirketi.VisibleIndex = 23;
            // 
            // repositoryItemComboBox26
            // 
            this.repositoryItemComboBox26.AutoHeight = false;
            this.repositoryItemComboBox26.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox26.Items.AddRange(new object[] {
            "Evet",
            "Hayır"});
            this.repositoryItemComboBox26.Name = "repositoryItemComboBox26";
            this.repositoryItemComboBox26.NullText = "Hayır";
            this.repositoryItemComboBox26.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cEmpID
            // 
            this.cEmpID.Caption = "EmpID";
            this.cEmpID.FieldName = "EmpID";
            this.cEmpID.Name = "cEmpID";
            // 
            // cOlusturan
            // 
            this.cOlusturan.Caption = "Oluşturan";
            this.cOlusturan.FieldName = "Olusturan";
            this.cOlusturan.Name = "cOlusturan";
            this.cOlusturan.Visible = true;
            this.cOlusturan.VisibleIndex = 24;
            // 
            // cOlusturmaTarihi
            // 
            this.cOlusturmaTarihi.Caption = "Oluşturma Tarihi";
            this.cOlusturmaTarihi.FieldName = "OlusturmaTarihi";
            this.cOlusturmaTarihi.Name = "cOlusturmaTarihi";
            this.cOlusturmaTarihi.Visible = true;
            this.cOlusturmaTarihi.VisibleIndex = 25;
            // 
            // cDegistiren
            // 
            this.cDegistiren.Caption = "Değiştiren";
            this.cDegistiren.FieldName = "Degistiren";
            this.cDegistiren.Name = "cDegistiren";
            this.cDegistiren.Visible = true;
            this.cDegistiren.VisibleIndex = 26;
            // 
            // cDegistirmeTarihi
            // 
            this.cDegistirmeTarihi.Caption = "Değiştirme Tarihi";
            this.cDegistirmeTarihi.FieldName = "DegistirmeTarihi";
            this.cDegistirmeTarihi.Name = "cDegistirmeTarihi";
            this.cDegistirmeTarihi.Visible = true;
            this.cDegistirmeTarihi.VisibleIndex = 27;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            this.repositoryItemComboBox2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.NullText = "";
            this.repositoryItemLookUpEdit2.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            // 
            // RepMemNereden
            // 
            this.RepMemNereden.Appearance.Options.UseTextOptions = true;
            this.RepMemNereden.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.AppearanceDisabled.Options.UseTextOptions = true;
            this.RepMemNereden.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.AppearanceFocused.Options.UseTextOptions = true;
            this.RepMemNereden.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.AppearanceReadOnly.Options.UseTextOptions = true;
            this.RepMemNereden.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RepMemNereden.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.RepMemNereden.Name = "RepMemNereden";
            // 
            // repZiyNedeni
            // 
            this.repZiyNedeni.AutoHeight = false;
            this.repZiyNedeni.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repZiyNedeni.DisplayMember = "Tanim";
            this.repZiyNedeni.Name = "repZiyNedeni";
            this.repZiyNedeni.NullText = "";
            this.repZiyNedeni.ValueMember = "Kod";
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemLookUpEdit36
            // 
            this.repositoryItemLookUpEdit36.AutoHeight = false;
            this.repositoryItemLookUpEdit36.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit36.Name = "repositoryItemLookUpEdit36";
            // 
            // layoutView2
            // 
            this.layoutView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn11,
            this.layoutViewColumn12,
            this.layoutViewColumn13,
            this.layoutViewColumn14,
            this.layoutViewColumn15,
            this.layoutViewColumn16,
            this.layoutViewColumn17,
            this.layoutViewColumn18,
            this.layoutViewColumn19});
            this.layoutView2.GridControl = this.grdMusteriListesi;
            this.layoutView2.Name = "layoutView2";
            this.layoutView2.OptionsSelection.MultiSelect = true;
            this.layoutView2.TemplateCard = null;
            // 
            // layoutViewColumn11
            // 
            this.layoutViewColumn11.Caption = "ID";
            this.layoutViewColumn11.FieldName = "ID";
            this.layoutViewColumn11.LayoutViewField = this.layoutViewField_colID1;
            this.layoutViewColumn11.Name = "layoutViewColumn11";
            // 
            // layoutViewField_colID1
            // 
            this.layoutViewField_colID1.EditorPreferredWidth = 129;
            this.layoutViewField_colID1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colID1.Name = "layoutViewField_colID1";
            this.layoutViewField_colID1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colID1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colID1.TextToControlDistance = 5;
            // 
            // layoutViewColumn12
            // 
            this.layoutViewColumn12.Caption = "Müşteri Kodu";
            this.layoutViewColumn12.FieldName = "MusteriKodu";
            this.layoutViewColumn12.LayoutViewField = this.layoutViewField_colMusteriKodu2;
            this.layoutViewColumn12.Name = "layoutViewColumn12";
            this.layoutViewColumn12.Width = 84;
            // 
            // layoutViewField_colMusteriKodu2
            // 
            this.layoutViewField_colMusteriKodu2.EditorPreferredWidth = 129;
            this.layoutViewField_colMusteriKodu2.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField_colMusteriKodu2.Name = "layoutViewField_colMusteriKodu2";
            this.layoutViewField_colMusteriKodu2.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colMusteriKodu2.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colMusteriKodu2.TextToControlDistance = 5;
            // 
            // layoutViewColumn13
            // 
            this.layoutViewColumn13.Caption = "Adı";
            this.layoutViewColumn13.FieldName = "Adi";
            this.layoutViewColumn13.LayoutViewField = this.layoutViewField_colAdi1;
            this.layoutViewColumn13.Name = "layoutViewColumn13";
            this.layoutViewColumn13.Width = 118;
            // 
            // layoutViewField_colAdi1
            // 
            this.layoutViewField_colAdi1.EditorPreferredWidth = 129;
            this.layoutViewField_colAdi1.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField_colAdi1.Name = "layoutViewField_colAdi1";
            this.layoutViewField_colAdi1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colAdi1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colAdi1.TextToControlDistance = 5;
            // 
            // layoutViewColumn14
            // 
            this.layoutViewColumn14.Caption = "Sektorü";
            this.layoutViewColumn14.FieldName = "Sektoru";
            this.layoutViewColumn14.LayoutViewField = this.layoutViewField_colSektoru1;
            this.layoutViewColumn14.Name = "layoutViewColumn14";
            this.layoutViewColumn14.Width = 133;
            // 
            // layoutViewField_colSektoru1
            // 
            this.layoutViewField_colSektoru1.EditorPreferredWidth = 129;
            this.layoutViewField_colSektoru1.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField_colSektoru1.Name = "layoutViewField_colSektoru1";
            this.layoutViewField_colSektoru1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colSektoru1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colSektoru1.TextToControlDistance = 5;
            // 
            // layoutViewColumn15
            // 
            this.layoutViewColumn15.Caption = "Ilçe";
            this.layoutViewColumn15.ColumnEdit = this.repLupIlce;
            this.layoutViewColumn15.FieldName = "Ilce";
            this.layoutViewColumn15.LayoutViewField = this.layoutViewField_colIlce1;
            this.layoutViewColumn15.Name = "layoutViewColumn15";
            this.layoutViewColumn15.Width = 101;
            // 
            // layoutViewField_colIlce1
            // 
            this.layoutViewField_colIlce1.EditorPreferredWidth = 129;
            this.layoutViewField_colIlce1.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField_colIlce1.Name = "layoutViewField_colIlce1";
            this.layoutViewField_colIlce1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colIlce1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colIlce1.TextToControlDistance = 5;
            // 
            // layoutViewColumn16
            // 
            this.layoutViewColumn16.Caption = "Şehir";
            this.layoutViewColumn16.ColumnEdit = this.repLupSehir;
            this.layoutViewColumn16.FieldName = "SehirKodu";
            this.layoutViewColumn16.LayoutViewField = this.layoutViewField_colSehirKodu1;
            this.layoutViewColumn16.Name = "layoutViewColumn16";
            this.layoutViewColumn16.Width = 96;
            // 
            // layoutViewField_colSehirKodu1
            // 
            this.layoutViewField_colSehirKodu1.EditorPreferredWidth = 129;
            this.layoutViewField_colSehirKodu1.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField_colSehirKodu1.Name = "layoutViewField_colSehirKodu1";
            this.layoutViewField_colSehirKodu1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colSehirKodu1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colSehirKodu1.TextToControlDistance = 5;
            // 
            // layoutViewColumn17
            // 
            this.layoutViewColumn17.Caption = "Ülke";
            this.layoutViewColumn17.ColumnEdit = this.repLupUlke;
            this.layoutViewColumn17.FieldName = "UlkeKodu";
            this.layoutViewColumn17.LayoutViewField = this.layoutViewField_colUlkeKodu1;
            this.layoutViewColumn17.Name = "layoutViewColumn17";
            this.layoutViewColumn17.Width = 104;
            // 
            // layoutViewField_colUlkeKodu1
            // 
            this.layoutViewField_colUlkeKodu1.EditorPreferredWidth = 129;
            this.layoutViewField_colUlkeKodu1.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField_colUlkeKodu1.Name = "layoutViewField_colUlkeKodu1";
            this.layoutViewField_colUlkeKodu1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colUlkeKodu1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colUlkeKodu1.TextToControlDistance = 5;
            // 
            // layoutViewColumn18
            // 
            this.layoutViewColumn18.Caption = "Adres";
            this.layoutViewColumn18.FieldName = "Adres";
            this.layoutViewColumn18.LayoutViewField = this.layoutViewField_colAdres1;
            this.layoutViewColumn18.Name = "layoutViewColumn18";
            this.layoutViewColumn18.Width = 165;
            // 
            // layoutViewField_colAdres1
            // 
            this.layoutViewField_colAdres1.EditorPreferredWidth = 129;
            this.layoutViewField_colAdres1.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField_colAdres1.Name = "layoutViewField_colAdres1";
            this.layoutViewField_colAdres1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField_colAdres1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colAdres1.TextToControlDistance = 5;
            // 
            // layoutViewColumn19
            // 
            this.layoutViewColumn19.Caption = "Vergi Dairesi";
            this.layoutViewColumn19.FieldName = "VergiDairesi";
            this.layoutViewColumn19.LayoutViewField = this.layoutViewField_colVergiDairesi1;
            this.layoutViewColumn19.Name = "layoutViewColumn19";
            this.layoutViewColumn19.Width = 87;
            // 
            // layoutViewField_colVergiDairesi1
            // 
            this.layoutViewField_colVergiDairesi1.EditorPreferredWidth = 129;
            this.layoutViewField_colVergiDairesi1.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField_colVergiDairesi1.Name = "layoutViewField_colVergiDairesi1";
            this.layoutViewField_colVergiDairesi1.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField_colVergiDairesi1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField_colVergiDairesi1.TextToControlDistance = 5;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3,
            this.bar4,
            this.bar5,
            this.bar6,
            this.bar7,
            this.bar8,
            this.bar9,
            this.bar10,
            this.bar11,
            this.bar13,
            this.bar12,
            this.bar14});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl3);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl4);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl5);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl6);
            this.barManager1.DockControls.Add(this.btnYenileEgitimler);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl7);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl8);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl9);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl10);
            this.barManager1.Form = this;
            this.barManager1.Images = this.SmallImage;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.btnRefresh,
            this.btnYetkiliKisiEkle,
            this.btnSaveYetkiliKisiler,
            this.btnRefreshYetkiliKisiler,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.btnYazZiyaret,
            this.btnYenileZiyaret,
            this.btnEkleZiyaret,
            this.btnKaydetZiyaret,
            this.btnSilZiyaret,
            this.btnYenileTeklif,
            this.btnEkleTeklif,
            this.btnKaydetTeklif,
            this.btnYazTeklif,
            this.btnSilTeklif,
            this.btnYenileSozlesmeler,
            this.btnEkleSozlemeler,
            this.btnKayderSozlesmeler,
            this.btnYazSozlemeler,
            this.btnSilSozlemeler,
            this.btnYenileProjeler,
            this.btnEkleProjeler,
            this.btnKaydetProjeler,
            this.btnYazProjeler,
            this.btnSilProjeler,
            this.btnSerRefresh,
            this.btnEkleSertifka,
            this.btnKaydetSertifika,
            this.btnYazSertifika,
            this.btnSilSertifika,
            this.btnYenileOdemeler,
            this.btnEkleOdemeler,
            this.btnKaydetOdemeler,
            this.btnYazdirOdemeler,
            this.btnSilOdemeler,
            this.btnEkleProjeDetay,
            this.btnSilProjeDetay,
            this.btnKarneGiris,
            this.barButtonItem11,
            this.btnAttachFileMusteri,
            this.barButtonItem8,
            this.btnKaydet,
            this.btnYazilimYenile,
            this.btnYazilimEkle,
            this.btnYazilimKaydet,
            this.btnYazilimYazdir,
            this.btnYazilimSil,
            this.btnEmailGonderim,
            this.barButtonItem7,
            this.barButtonItem9,
            this.barButtonItem10,
            this.btnProjeDetay});
            this.barManager1.MaxItemId = 82;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4});
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(382, 201);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4, true)});
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar1.Text = "Tools";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "Yenile";
            this.btnRefresh.Id = 13;
            this.btnRefresh.ImageIndex = 41;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Ekle";
            this.barButtonItem3.Id = 23;
            this.barButtonItem3.ImageIndex = 42;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick_1);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Düzelt";
            this.barButtonItem1.Id = 11;
            this.barButtonItem1.ImageIndex = 7;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Yazdir";
            this.barButtonItem2.Id = 12;
            this.barButtonItem2.ImageIndex = 9;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Sil";
            this.barButtonItem4.Id = 24;
            this.barButtonItem4.ImageIndex = 13;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick_3);
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(327, 191);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRefreshYetkiliKisiler),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYetkiliKisiEkle, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSaveYetkiliKisiler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5, true)});
            this.bar2.Offset = 3;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Custom 3";
            // 
            // btnRefreshYetkiliKisiler
            // 
            this.btnRefreshYetkiliKisiler.Caption = "Yenile";
            this.btnRefreshYetkiliKisiler.Id = 20;
            this.btnRefreshYetkiliKisiler.ImageIndex = 41;
            this.btnRefreshYetkiliKisiler.Name = "btnRefreshYetkiliKisiler";
            this.btnRefreshYetkiliKisiler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick_1);
            // 
            // btnYetkiliKisiEkle
            // 
            this.btnYetkiliKisiEkle.Caption = "+";
            this.btnYetkiliKisiEkle.Id = 18;
            this.btnYetkiliKisiEkle.ImageIndex = 42;
            this.btnYetkiliKisiEkle.Name = "btnYetkiliKisiEkle";
            this.btnYetkiliKisiEkle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYetkiliKisiEkle_ItemClick);
            // 
            // btnSaveYetkiliKisiler
            // 
            this.btnSaveYetkiliKisiler.Caption = "Kaydet";
            this.btnSaveYetkiliKisiler.Id = 19;
            this.btnSaveYetkiliKisiler.ImageIndex = 10;
            this.btnSaveYetkiliKisiler.Name = "btnSaveYetkiliKisiler";
            this.btnSaveYetkiliKisiler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSaveYetkiliKisiler_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Yazdir";
            this.barButtonItem6.Id = 27;
            this.barButtonItem6.ImageIndex = 9;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Sil";
            this.barButtonItem5.Id = 25;
            this.barButtonItem5.ImageIndex = 13;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 4";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar3.FloatLocation = new System.Drawing.Point(333, 187);
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYenileZiyaret),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleZiyaret, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydetZiyaret, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazZiyaret, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilZiyaret, true)});
            this.bar3.Offset = 3;
            this.bar3.StandaloneBarDockControl = this.standaloneBarDockControl3;
            this.bar3.Text = "Custom 4";
            // 
            // btnYenileZiyaret
            // 
            this.btnYenileZiyaret.Caption = "Yenile";
            this.btnYenileZiyaret.Id = 29;
            this.btnYenileZiyaret.ImageIndex = 41;
            this.btnYenileZiyaret.Name = "btnYenileZiyaret";
            this.btnYenileZiyaret.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYenileZiyaret_ItemClick);
            // 
            // btnEkleZiyaret
            // 
            this.btnEkleZiyaret.Caption = "Ekle";
            this.btnEkleZiyaret.Id = 30;
            this.btnEkleZiyaret.ImageIndex = 42;
            this.btnEkleZiyaret.Name = "btnEkleZiyaret";
            this.btnEkleZiyaret.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleZiyaret_ItemClick);
            // 
            // btnKaydetZiyaret
            // 
            this.btnKaydetZiyaret.Caption = "Kaydet";
            this.btnKaydetZiyaret.Id = 31;
            this.btnKaydetZiyaret.ImageIndex = 21;
            this.btnKaydetZiyaret.Name = "btnKaydetZiyaret";
            this.btnKaydetZiyaret.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydetZiyaret_ItemClick);
            // 
            // btnYazZiyaret
            // 
            this.btnYazZiyaret.Caption = "Yazdir";
            this.btnYazZiyaret.Id = 28;
            this.btnYazZiyaret.ImageIndex = 9;
            this.btnYazZiyaret.Name = "btnYazZiyaret";
            this.btnYazZiyaret.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazZiyaret_ItemClick);
            // 
            // btnSilZiyaret
            // 
            this.btnSilZiyaret.Caption = "Sil";
            this.btnSilZiyaret.Id = 32;
            this.btnSilZiyaret.ImageIndex = 13;
            this.btnSilZiyaret.Name = "btnSilZiyaret";
            this.btnSilZiyaret.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilZiyaret_ItemClick);
            // 
            // standaloneBarDockControl3
            // 
            this.standaloneBarDockControl3.CausesValidation = false;
            this.standaloneBarDockControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl3.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl3.Name = "standaloneBarDockControl3";
            this.standaloneBarDockControl3.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl3.Text = "standaloneBarDockControl3";
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(391, 175);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYenileTeklif),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleTeklif, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydetTeklif, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazTeklif, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilTeklif, true)});
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl4;
            this.bar4.Text = "Custom 5";
            // 
            // btnYenileTeklif
            // 
            this.btnYenileTeklif.Caption = "Yenile";
            this.btnYenileTeklif.Id = 33;
            this.btnYenileTeklif.ImageIndex = 41;
            this.btnYenileTeklif.Name = "btnYenileTeklif";
            this.btnYenileTeklif.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYenileTeklif_ItemClick);
            // 
            // btnEkleTeklif
            // 
            this.btnEkleTeklif.Caption = "Ekle";
            this.btnEkleTeklif.Id = 34;
            this.btnEkleTeklif.ImageIndex = 42;
            this.btnEkleTeklif.Name = "btnEkleTeklif";
            this.btnEkleTeklif.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleTeklif_ItemClick);
            // 
            // btnKaydetTeklif
            // 
            this.btnKaydetTeklif.Caption = "Kaydet";
            this.btnKaydetTeklif.Id = 36;
            this.btnKaydetTeklif.ImageIndex = 10;
            this.btnKaydetTeklif.Name = "btnKaydetTeklif";
            this.btnKaydetTeklif.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydetTeklif_ItemClick);
            // 
            // btnYazTeklif
            // 
            this.btnYazTeklif.Caption = "Yazdır";
            this.btnYazTeklif.Id = 37;
            this.btnYazTeklif.ImageIndex = 9;
            this.btnYazTeklif.Name = "btnYazTeklif";
            this.btnYazTeklif.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazTeklif_ItemClick);
            // 
            // btnSilTeklif
            // 
            this.btnSilTeklif.Caption = "Sil";
            this.btnSilTeklif.Id = 38;
            this.btnSilTeklif.ImageIndex = 13;
            this.btnSilTeklif.Name = "btnSilTeklif";
            this.btnSilTeklif.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilTeklif_ItemClick);
            // 
            // standaloneBarDockControl4
            // 
            this.standaloneBarDockControl4.CausesValidation = false;
            this.standaloneBarDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl4.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl4.Name = "standaloneBarDockControl4";
            this.standaloneBarDockControl4.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl4.Text = "standaloneBarDockControl4";
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 6";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar5.FloatLocation = new System.Drawing.Point(340, 167);
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYenileSozlesmeler),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleSozlemeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazSozlemeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilSozlemeler, true)});
            this.bar5.Offset = 1;
            this.bar5.StandaloneBarDockControl = this.standaloneBarDockControl5;
            this.bar5.Text = "Custom 6";
            // 
            // btnYenileSozlesmeler
            // 
            this.btnYenileSozlesmeler.Caption = "Yenile";
            this.btnYenileSozlesmeler.Id = 39;
            this.btnYenileSozlesmeler.ImageIndex = 41;
            this.btnYenileSozlesmeler.Name = "btnYenileSozlesmeler";
            this.btnYenileSozlesmeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYenileSozlesmeler_ItemClick);
            // 
            // btnEkleSozlemeler
            // 
            this.btnEkleSozlemeler.Caption = "Ekle";
            this.btnEkleSozlemeler.Id = 40;
            this.btnEkleSozlemeler.ImageIndex = 42;
            this.btnEkleSozlemeler.Name = "btnEkleSozlemeler";
            this.btnEkleSozlemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleSozlemeler_ItemClick);
            // 
            // btnKaydet
            // 
            this.btnKaydet.Caption = "Kaydet";
            this.btnKaydet.Id = 70;
            this.btnKaydet.ImageIndex = 21;
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKayderSozlesmeler_ItemClick);
            // 
            // btnYazSozlemeler
            // 
            this.btnYazSozlemeler.Caption = "Yadir";
            this.btnYazSozlemeler.Id = 42;
            this.btnYazSozlemeler.ImageIndex = 9;
            this.btnYazSozlemeler.Name = "btnYazSozlemeler";
            this.btnYazSozlemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazSozlemeler_ItemClick);
            // 
            // btnSilSozlemeler
            // 
            this.btnSilSozlemeler.Caption = "Sil";
            this.btnSilSozlemeler.Id = 43;
            this.btnSilSozlemeler.ImageIndex = 13;
            this.btnSilSozlemeler.Name = "btnSilSozlemeler";
            this.btnSilSozlemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilSozlemeler_ItemClick);
            // 
            // standaloneBarDockControl5
            // 
            this.standaloneBarDockControl5.CausesValidation = false;
            this.standaloneBarDockControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl5.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl5.Name = "standaloneBarDockControl5";
            this.standaloneBarDockControl5.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl5.Text = "standaloneBarDockControl5";
            // 
            // bar6
            // 
            this.bar6.BarName = "Custom 7";
            this.bar6.DockCol = 0;
            this.bar6.DockRow = 0;
            this.bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar6.FloatLocation = new System.Drawing.Point(330, 165);
            this.bar6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYenileProjeler),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleProjeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydetProjeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazProjeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilProjeler, true)});
            this.bar6.StandaloneBarDockControl = this.standaloneBarDockControl6;
            this.bar6.Text = "Custom 7";
            // 
            // btnYenileProjeler
            // 
            this.btnYenileProjeler.Caption = "Yenile";
            this.btnYenileProjeler.Id = 44;
            this.btnYenileProjeler.ImageIndex = 41;
            this.btnYenileProjeler.Name = "btnYenileProjeler";
            this.btnYenileProjeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYenileProjeler_ItemClick);
            // 
            // btnEkleProjeler
            // 
            this.btnEkleProjeler.Caption = "Ekle";
            this.btnEkleProjeler.Id = 45;
            this.btnEkleProjeler.ImageIndex = 42;
            this.btnEkleProjeler.Name = "btnEkleProjeler";
            this.btnEkleProjeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleProjeler_ItemClick);
            // 
            // btnKaydetProjeler
            // 
            this.btnKaydetProjeler.Caption = "Kaydet";
            this.btnKaydetProjeler.Id = 46;
            this.btnKaydetProjeler.ImageIndex = 10;
            this.btnKaydetProjeler.Name = "btnKaydetProjeler";
            this.btnKaydetProjeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydetProjeler_ItemClick);
            // 
            // btnYazProjeler
            // 
            this.btnYazProjeler.Caption = "Yazdir";
            this.btnYazProjeler.Id = 47;
            this.btnYazProjeler.ImageIndex = 9;
            this.btnYazProjeler.Name = "btnYazProjeler";
            // 
            // btnSilProjeler
            // 
            this.btnSilProjeler.Caption = "Sil";
            this.btnSilProjeler.Id = 48;
            this.btnSilProjeler.ImageIndex = 13;
            this.btnSilProjeler.Name = "btnSilProjeler";
            this.btnSilProjeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilProjeler_ItemClick);
            // 
            // standaloneBarDockControl6
            // 
            this.standaloneBarDockControl6.CausesValidation = false;
            this.standaloneBarDockControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl6.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl6.Name = "standaloneBarDockControl6";
            this.standaloneBarDockControl6.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl6.Text = "standaloneBarDockControl6";
            // 
            // bar7
            // 
            this.bar7.BarName = "Custom 8";
            this.bar7.DockCol = 0;
            this.bar7.DockRow = 0;
            this.bar7.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar7.FloatLocation = new System.Drawing.Point(477, 195);
            this.bar7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSerRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleSertifka, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydetSertifika, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazSertifika, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilSertifika, true)});
            this.bar7.Offset = 1;
            this.bar7.StandaloneBarDockControl = this.btnYenileEgitimler;
            this.bar7.Text = "Custom 8";
            // 
            // btnSerRefresh
            // 
            this.btnSerRefresh.Caption = "Yenile";
            this.btnSerRefresh.Id = 49;
            this.btnSerRefresh.ImageIndex = 41;
            this.btnSerRefresh.Name = "btnSerRefresh";
            this.btnSerRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSerRefresh_ItemClick);
            // 
            // btnEkleSertifka
            // 
            this.btnEkleSertifka.Caption = "Ekle";
            this.btnEkleSertifka.Id = 50;
            this.btnEkleSertifka.ImageIndex = 42;
            this.btnEkleSertifka.Name = "btnEkleSertifka";
            this.btnEkleSertifka.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleSertifka_ItemClick);
            // 
            // btnKaydetSertifika
            // 
            this.btnKaydetSertifika.Caption = "Kaydet";
            this.btnKaydetSertifika.Id = 51;
            this.btnKaydetSertifika.ImageIndex = 10;
            this.btnKaydetSertifika.Name = "btnKaydetSertifika";
            this.btnKaydetSertifika.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydetSertifika_ItemClick);
            // 
            // btnYazSertifika
            // 
            this.btnYazSertifika.Caption = "Yazdir";
            this.btnYazSertifika.Id = 52;
            this.btnYazSertifika.ImageIndex = 9;
            this.btnYazSertifika.Name = "btnYazSertifika";
            this.btnYazSertifika.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazSertifika_ItemClick);
            // 
            // btnSilSertifika
            // 
            this.btnSilSertifika.Caption = "Sil";
            this.btnSilSertifika.Id = 53;
            this.btnSilSertifika.ImageIndex = 13;
            this.btnSilSertifika.Name = "btnSilSertifika";
            this.btnSilSertifika.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilSertifika_ItemClick);
            // 
            // btnYenileEgitimler
            // 
            this.btnYenileEgitimler.CausesValidation = false;
            this.btnYenileEgitimler.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnYenileEgitimler.Location = new System.Drawing.Point(0, 0);
            this.btnYenileEgitimler.Name = "btnYenileEgitimler";
            this.btnYenileEgitimler.Size = new System.Drawing.Size(1352, 29);
            this.btnYenileEgitimler.Text = "standaloneBarDockControl7";
            // 
            // bar8
            // 
            this.bar8.BarName = "Custom 9";
            this.bar8.DockCol = 0;
            this.bar8.DockRow = 0;
            this.bar8.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar8.FloatLocation = new System.Drawing.Point(329, 191);
            this.bar8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYenileOdemeler),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleOdemeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydetOdemeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazdirOdemeler, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilOdemeler, true)});
            this.bar8.Offset = 4;
            this.bar8.StandaloneBarDockControl = this.standaloneBarDockControl7;
            this.bar8.Text = "Custom 9";
            // 
            // btnYenileOdemeler
            // 
            this.btnYenileOdemeler.Caption = "Yenile";
            this.btnYenileOdemeler.Id = 54;
            this.btnYenileOdemeler.ImageIndex = 41;
            this.btnYenileOdemeler.Name = "btnYenileOdemeler";
            this.btnYenileOdemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYenileOdemeler_ItemClick);
            // 
            // btnEkleOdemeler
            // 
            this.btnEkleOdemeler.Caption = "Ekle";
            this.btnEkleOdemeler.Id = 55;
            this.btnEkleOdemeler.ImageIndex = 42;
            this.btnEkleOdemeler.Name = "btnEkleOdemeler";
            this.btnEkleOdemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleOdemeler_ItemClick);
            // 
            // btnKaydetOdemeler
            // 
            this.btnKaydetOdemeler.Caption = "Kaydet";
            this.btnKaydetOdemeler.Id = 56;
            this.btnKaydetOdemeler.ImageIndex = 10;
            this.btnKaydetOdemeler.Name = "btnKaydetOdemeler";
            this.btnKaydetOdemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydetOdemeler_ItemClick);
            // 
            // btnYazdirOdemeler
            // 
            this.btnYazdirOdemeler.Caption = "Yazdir";
            this.btnYazdirOdemeler.Id = 57;
            this.btnYazdirOdemeler.ImageIndex = 9;
            this.btnYazdirOdemeler.Name = "btnYazdirOdemeler";
            this.btnYazdirOdemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazdirOdemeler_ItemClick);
            // 
            // btnSilOdemeler
            // 
            this.btnSilOdemeler.Caption = "Sil";
            this.btnSilOdemeler.Id = 58;
            this.btnSilOdemeler.ImageIndex = 13;
            this.btnSilOdemeler.Name = "btnSilOdemeler";
            this.btnSilOdemeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilOdemeler_ItemClick);
            // 
            // standaloneBarDockControl7
            // 
            this.standaloneBarDockControl7.CausesValidation = false;
            this.standaloneBarDockControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl7.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl7.Name = "standaloneBarDockControl7";
            this.standaloneBarDockControl7.Size = new System.Drawing.Size(1357, 29);
            this.standaloneBarDockControl7.Text = "standaloneBarDockControl7";
            // 
            // bar9
            // 
            this.bar9.BarName = "Custom 10";
            this.bar9.DockCol = 0;
            this.bar9.DockRow = 0;
            this.bar9.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar9.FloatLocation = new System.Drawing.Point(406, 409);
            this.bar9.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleProjeDetay),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSilProjeDetay, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKarneGiris)});
            this.bar9.Offset = 2;
            this.bar9.StandaloneBarDockControl = this.standaloneBarDockControl8;
            this.bar9.Text = "Custom 10";
            // 
            // btnEkleProjeDetay
            // 
            this.btnEkleProjeDetay.Caption = "Ekle";
            this.btnEkleProjeDetay.Id = 61;
            this.btnEkleProjeDetay.ImageIndex = 42;
            this.btnEkleProjeDetay.Name = "btnEkleProjeDetay";
            this.btnEkleProjeDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleProjeDetay_ItemClick);
            // 
            // btnSilProjeDetay
            // 
            this.btnSilProjeDetay.Caption = "Sil";
            this.btnSilProjeDetay.Id = 62;
            this.btnSilProjeDetay.ImageIndex = 13;
            this.btnSilProjeDetay.Name = "btnSilProjeDetay";
            this.btnSilProjeDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSilProjeDetay_ItemClick);
            // 
            // btnKarneGiris
            // 
            this.btnKarneGiris.Caption = "Karne Giriş";
            this.btnKarneGiris.Id = 65;
            this.btnKarneGiris.ImageIndex = 27;
            this.btnKarneGiris.Name = "btnKarneGiris";
            toolTipTitleItem1.Text = "Bilgi";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Karne girişi";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnKarneGiris.SuperTip = superToolTip1;
            this.btnKarneGiris.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // standaloneBarDockControl8
            // 
            this.standaloneBarDockControl8.CausesValidation = false;
            this.standaloneBarDockControl8.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl8.Name = "standaloneBarDockControl8";
            this.standaloneBarDockControl8.Size = new System.Drawing.Size(1346, 26);
            this.standaloneBarDockControl8.Text = "standaloneBarDockControl8";
            // 
            // bar10
            // 
            this.bar10.BarName = "Custom 11";
            this.bar10.DockCol = 0;
            this.bar10.DockRow = 0;
            this.bar10.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar10.FloatLocation = new System.Drawing.Point(325, 277);
            this.bar10.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11)});
            this.bar10.Offset = 2;
            this.bar10.StandaloneBarDockControl = this.standaloneBarDockControl9;
            this.bar10.Text = "Custom 11";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Karne Notlari";
            this.barButtonItem11.Id = 67;
            this.barButtonItem11.ImageIndex = 27;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // standaloneBarDockControl9
            // 
            this.standaloneBarDockControl9.CausesValidation = false;
            this.standaloneBarDockControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl9.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl9.Name = "standaloneBarDockControl9";
            this.standaloneBarDockControl9.Size = new System.Drawing.Size(1348, 30);
            this.standaloneBarDockControl9.Text = "standaloneBarDockControl9";
            // 
            // bar11
            // 
            this.bar11.BarName = "Custom 12";
            this.bar11.DockCol = 1;
            this.bar11.DockRow = 0;
            this.bar11.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar11.FloatLocation = new System.Drawing.Point(342, 189);
            this.bar11.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAttachFileMusteri),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEmailGonderim, true)});
            this.bar11.Offset = 149;
            this.bar11.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar11.Text = "Custom 12";
            // 
            // btnAttachFileMusteri
            // 
            this.btnAttachFileMusteri.Caption = "Attach";
            this.btnAttachFileMusteri.Id = 68;
            this.btnAttachFileMusteri.ImageIndex = 48;
            this.btnAttachFileMusteri.Name = "btnAttachFileMusteri";
            toolTipTitleItem2.Text = "Bilgi";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Dosya Ekle";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnAttachFileMusteri.SuperTip = superToolTip2;
            this.btnAttachFileMusteri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAttachFileMusteri_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Etiket";
            this.barButtonItem8.Id = 69;
            this.barButtonItem8.ImageIndex = 46;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // btnEmailGonderim
            // 
            this.btnEmailGonderim.Caption = "Email";
            this.btnEmailGonderim.Id = 76;
            this.btnEmailGonderim.ImageIndex = 49;
            this.btnEmailGonderim.Name = "btnEmailGonderim";
            toolTipTitleItem3.Text = "Bilgi";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Email gönderim";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.btnEmailGonderim.SuperTip = superToolTip3;
            this.btnEmailGonderim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEmailGonderim_ItemClick);
            // 
            // bar13
            // 
            this.bar13.BarName = "Custom 14";
            this.bar13.DockCol = 0;
            this.bar13.DockRow = 0;
            this.bar13.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar13.FloatLocation = new System.Drawing.Point(466, 226);
            this.bar13.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazilimYenile),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazilimEkle, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazilimKaydet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazilimYazdir, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazilimSil, true)});
            this.bar13.StandaloneBarDockControl = this.standaloneBarDockControl10;
            this.bar13.Text = "Custom 14";
            // 
            // btnYazilimYenile
            // 
            this.btnYazilimYenile.Caption = "Yenile";
            this.btnYazilimYenile.Id = 71;
            this.btnYazilimYenile.ImageIndex = 41;
            this.btnYazilimYenile.Name = "btnYazilimYenile";
            this.btnYazilimYenile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazilimYenile_ItemClick);
            // 
            // btnYazilimEkle
            // 
            this.btnYazilimEkle.Caption = "Ekle";
            this.btnYazilimEkle.Id = 72;
            this.btnYazilimEkle.ImageIndex = 42;
            this.btnYazilimEkle.Name = "btnYazilimEkle";
            this.btnYazilimEkle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazilimEkle_ItemClick);
            // 
            // btnYazilimKaydet
            // 
            this.btnYazilimKaydet.Caption = "Kaydet";
            this.btnYazilimKaydet.Id = 73;
            this.btnYazilimKaydet.ImageIndex = 10;
            this.btnYazilimKaydet.Name = "btnYazilimKaydet";
            this.btnYazilimKaydet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazilimKaydet_ItemClick);
            // 
            // btnYazilimYazdir
            // 
            this.btnYazilimYazdir.Caption = "Yazdir";
            this.btnYazilimYazdir.Id = 74;
            this.btnYazilimYazdir.ImageIndex = 9;
            this.btnYazilimYazdir.Name = "btnYazilimYazdir";
            this.btnYazilimYazdir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazilimYazdir_ItemClick);
            // 
            // btnYazilimSil
            // 
            this.btnYazilimSil.Caption = "Sil";
            this.btnYazilimSil.Id = 75;
            this.btnYazilimSil.ImageIndex = 13;
            this.btnYazilimSil.Name = "btnYazilimSil";
            this.btnYazilimSil.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazilimSil_ItemClick);
            // 
            // standaloneBarDockControl10
            // 
            this.standaloneBarDockControl10.CausesValidation = false;
            this.standaloneBarDockControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl10.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl10.Name = "standaloneBarDockControl10";
            this.standaloneBarDockControl10.Size = new System.Drawing.Size(1352, 26);
            this.standaloneBarDockControl10.Text = "standaloneBarDockControl10";
            // 
            // bar12
            // 
            this.bar12.BarName = "Custom 15";
            this.bar12.DockCol = 2;
            this.bar12.DockRow = 0;
            this.bar12.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar12.FloatLocation = new System.Drawing.Point(443, 186);
            this.bar12.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7)});
            this.bar12.Offset = 340;
            this.bar12.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar12.Text = "Custom 15";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Gruplama";
            this.barButtonItem7.Id = 78;
            this.barButtonItem7.ImageIndex = 31;
            this.barButtonItem7.Name = "barButtonItem7";
            toolTipTitleItem4.Text = "Bilgi";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Gruplama alanının açılmasını sağlar";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barButtonItem7.SuperTip = superToolTip4;
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick_1);
            // 
            // bar14
            // 
            this.bar14.BarName = "Custom 16";
            this.bar14.DockCol = 1;
            this.bar14.DockRow = 0;
            this.bar14.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar14.FloatLocation = new System.Drawing.Point(442, 202);
            this.bar14.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnProjeDetay)});
            this.bar14.Offset = 198;
            this.bar14.StandaloneBarDockControl = this.standaloneBarDockControl6;
            this.bar14.Text = "Custom 16";
            // 
            // btnProjeDetay
            // 
            this.btnProjeDetay.Caption = "ProjeDetayi";
            this.btnProjeDetay.Id = 81;
            this.btnProjeDetay.ImageIndex = 58;
            this.btnProjeDetay.Name = "btnProjeDetay";
            this.btnProjeDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnProjeDetay_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 436);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 436);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 436);
            // 
            // btnKayderSozlesmeler
            // 
            this.btnKayderSozlesmeler.Caption = "Kaydet";
            this.btnKayderSozlesmeler.Id = 41;
            this.btnKayderSozlesmeler.ImageIndex = 21;
            this.btnKayderSozlesmeler.Name = "btnKayderSozlesmeler";
            this.btnKayderSozlesmeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKayderSozlesmeler_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Yetkili Kişiler";
            this.barButtonItem9.Id = 79;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Ziyaretler";
            this.barButtonItem10.Id = 80;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(617, 178);
            this.textEdit1.MenuManager = this.barManager1;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(100, 20);
            this.textEdit1.TabIndex = 15;
            // 
            // tbMain
            // 
            this.tbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMain.Images = this.SmallImage;
            this.tbMain.Location = new System.Drawing.Point(0, 0);
            this.tbMain.Name = "tbMain";
            this.tbMain.SelectedTabPage = this.tbMusList;
            this.tbMain.Size = new System.Drawing.Size(1362, 436);
            this.tbMain.TabIndex = 20;
            this.tbMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbMusList,
            this.tbYetkiliKisi,
            this.tbZiyaretler,
            this.tbTeklifler,
            this.tbSozlesme,
            this.tbOdemeler,
            this.tbProjeler,
            this.tbilaveBilgi});
            this.tbMain.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // tbMusList
            // 
            this.tbMusList.Controls.Add(this.grdMusteriListesi);
            this.tbMusList.Controls.Add(this.standaloneBarDockControl2);
            this.tbMusList.Name = "tbMusList";
            this.tbMusList.Size = new System.Drawing.Size(1357, 408);
            this.tbMusList.Text = "Müşteri Tanım Giriş";
            // 
            // tbYetkiliKisi
            // 
            this.tbYetkiliKisi.Controls.Add(this.grpYetkiliKisiler);
            this.tbYetkiliKisi.Controls.Add(this.standaloneBarDockControl1);
            this.tbYetkiliKisi.ImageIndex = 29;
            this.tbYetkiliKisi.Name = "tbYetkiliKisi";
            this.tbYetkiliKisi.Size = new System.Drawing.Size(1357, 408);
            this.tbYetkiliKisi.Text = "Yetkili Kişiler";
            // 
            // grpYetkiliKisiler
            // 
            this.grpYetkiliKisiler.Appearance.Font = new System.Drawing.Font("Tahoma", 8F);
            this.grpYetkiliKisiler.Appearance.ForeColor = System.Drawing.Color.Black;
            this.grpYetkiliKisiler.Appearance.Options.UseFont = true;
            this.grpYetkiliKisiler.Appearance.Options.UseForeColor = true;
            this.grpYetkiliKisiler.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpYetkiliKisiler.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpYetkiliKisiler.AppearanceCaption.Options.UseFont = true;
            this.grpYetkiliKisiler.AppearanceCaption.Options.UseForeColor = true;
            this.grpYetkiliKisiler.Controls.Add(this.grdYetkiliKisiler);
            this.grpYetkiliKisiler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpYetkiliKisiler.Location = new System.Drawing.Point(0, 29);
            this.grpYetkiliKisiler.Name = "grpYetkiliKisiler";
            this.grpYetkiliKisiler.Size = new System.Drawing.Size(1357, 379);
            this.grpYetkiliKisiler.TabIndex = 0;
            this.grpYetkiliKisiler.Text = "...";
            // 
            // grdYetkiliKisiler
            // 
            this.grdYetkiliKisiler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdYetkiliKisiler.Location = new System.Drawing.Point(2, 31);
            this.grdYetkiliKisiler.MainView = this.lvYetkiliKisiler;
            this.grdYetkiliKisiler.Name = "grdYetkiliKisiler";
            this.grdYetkiliKisiler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4,
            this.repositoryItemPictureEdit4,
            this.repositoryItemImageEdit2,
            this.repositoryItemPictureEdit3,
            this.repositoryItemMemoEdit1,
            this.repYetkiliStatu,
            this.repTel2});
            this.grdYetkiliKisiler.Size = new System.Drawing.Size(1353, 346);
            this.grdYetkiliKisiler.TabIndex = 6;
            this.grdYetkiliKisiler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.lvYetkiliKisiler});
            // 
            // lvYetkiliKisiler
            // 
            this.lvYetkiliKisiler.CardHorzInterval = 1;
            this.lvYetkiliKisiler.CardMinSize = new System.Drawing.Size(272, 435);
            this.lvYetkiliKisiler.CardVertInterval = 0;
            this.lvYetkiliKisiler.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn1,
            this.cEMP_STATU,
            this.cEMP_ADI,
            this.cEMP_SOYADI,
            this.cEMP_GOREVI,
            this.cEMP_EMAIL,
            this.cEMP_TELEFON1,
            this.cEMP_TELEFON2,
            this.cPicture,
            this.cNote});
            this.lvYetkiliKisiler.GridControl = this.grdYetkiliKisiler;
            this.lvYetkiliKisiler.Name = "lvYetkiliKisiler";
            this.lvYetkiliKisiler.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.lvYetkiliKisiler.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.CustomSize;
            this.lvYetkiliKisiler.OptionsSelection.MultiSelect = true;
            this.lvYetkiliKisiler.OptionsView.ContentAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lvYetkiliKisiler.OptionsView.ShowCardCaption = false;
            this.lvYetkiliKisiler.OptionsView.ShowHeaderPanel = false;
            this.lvYetkiliKisiler.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiRow;
            this.lvYetkiliKisiler.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.cNote, DevExpress.Data.ColumnSortOrder.Descending)});
            this.lvYetkiliKisiler.TemplateCard = this.layoutViewCard2;
            // 
            // layoutViewColumn1
            // 
            this.layoutViewColumn1.Caption = "Müşteri Kodu";
            this.layoutViewColumn1.FieldName = "EmpFirmaKodu";
            this.layoutViewColumn1.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this.layoutViewColumn1.Name = "layoutViewColumn1";
            this.layoutViewColumn1.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 201;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 28);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 5;
            // 
            // cEMP_STATU
            // 
            this.cEMP_STATU.Caption = "Statü";
            this.cEMP_STATU.ColumnEdit = this.repYetkiliStatu;
            this.cEMP_STATU.FieldName = "Emp_YetkiliKisi";
            this.cEMP_STATU.LayoutViewField = this.layoutViewField_layoutViewColumn110_7;
            this.cEMP_STATU.Name = "cEMP_STATU";
            // 
            // repYetkiliStatu
            // 
            this.repYetkiliStatu.AutoHeight = false;
            this.repYetkiliStatu.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repYetkiliStatu.DisplayMember = "Tanim";
            this.repYetkiliStatu.Name = "repYetkiliStatu";
            this.repYetkiliStatu.NullText = "";
            this.repYetkiliStatu.ValueMember = "Tanim";
            // 
            // layoutViewField_layoutViewColumn110_7
            // 
            this.layoutViewField_layoutViewColumn110_7.EditorPreferredWidth = 201;
            this.layoutViewField_layoutViewColumn110_7.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn110_7.Name = "layoutViewField_layoutViewColumn110_7";
            this.layoutViewField_layoutViewColumn110_7.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_layoutViewColumn110_7.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_layoutViewColumn110_7.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_layoutViewColumn110_7.TextToControlDistance = 5;
            // 
            // cEMP_ADI
            // 
            this.cEMP_ADI.Caption = "Adı";
            this.cEMP_ADI.FieldName = "EmpAdi";
            this.cEMP_ADI.LayoutViewField = this.layoutViewField_cEMP_ADI;
            this.cEMP_ADI.Name = "cEMP_ADI";
            // 
            // layoutViewField_cEMP_ADI
            // 
            this.layoutViewField_cEMP_ADI.EditorPreferredWidth = 201;
            this.layoutViewField_cEMP_ADI.Location = new System.Drawing.Point(0, 56);
            this.layoutViewField_cEMP_ADI.Name = "layoutViewField_cEMP_ADI";
            this.layoutViewField_cEMP_ADI.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cEMP_ADI.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_cEMP_ADI.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cEMP_ADI.TextToControlDistance = 5;
            // 
            // cEMP_SOYADI
            // 
            this.cEMP_SOYADI.Caption = "Soyadı";
            this.cEMP_SOYADI.FieldName = "EmpSoyadi";
            this.cEMP_SOYADI.LayoutViewField = this.layoutViewField_cEMP_SOYADI;
            this.cEMP_SOYADI.Name = "cEMP_SOYADI";
            // 
            // layoutViewField_cEMP_SOYADI
            // 
            this.layoutViewField_cEMP_SOYADI.EditorPreferredWidth = 201;
            this.layoutViewField_cEMP_SOYADI.Location = new System.Drawing.Point(0, 84);
            this.layoutViewField_cEMP_SOYADI.Name = "layoutViewField_cEMP_SOYADI";
            this.layoutViewField_cEMP_SOYADI.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cEMP_SOYADI.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_cEMP_SOYADI.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cEMP_SOYADI.TextToControlDistance = 5;
            // 
            // cEMP_GOREVI
            // 
            this.cEMP_GOREVI.Caption = "Görevi";
            this.cEMP_GOREVI.FieldName = "EmpGorevi";
            this.cEMP_GOREVI.LayoutViewField = this.layoutViewField_cEMP_GOREVI;
            this.cEMP_GOREVI.Name = "cEMP_GOREVI";
            // 
            // layoutViewField_cEMP_GOREVI
            // 
            this.layoutViewField_cEMP_GOREVI.EditorPreferredWidth = 201;
            this.layoutViewField_cEMP_GOREVI.Location = new System.Drawing.Point(0, 112);
            this.layoutViewField_cEMP_GOREVI.Name = "layoutViewField_cEMP_GOREVI";
            this.layoutViewField_cEMP_GOREVI.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cEMP_GOREVI.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_cEMP_GOREVI.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cEMP_GOREVI.TextToControlDistance = 5;
            // 
            // cEMP_EMAIL
            // 
            this.cEMP_EMAIL.Caption = "Email";
            this.cEMP_EMAIL.FieldName = "EmpEmail";
            this.cEMP_EMAIL.LayoutViewField = this.layoutViewField_cEMP_EMAIL;
            this.cEMP_EMAIL.Name = "cEMP_EMAIL";
            // 
            // layoutViewField_cEMP_EMAIL
            // 
            this.layoutViewField_cEMP_EMAIL.EditorPreferredWidth = 201;
            this.layoutViewField_cEMP_EMAIL.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField_cEMP_EMAIL.Name = "layoutViewField_cEMP_EMAIL";
            this.layoutViewField_cEMP_EMAIL.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cEMP_EMAIL.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_cEMP_EMAIL.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cEMP_EMAIL.TextToControlDistance = 5;
            // 
            // cEMP_TELEFON1
            // 
            this.cEMP_TELEFON1.Caption = "Telefon 1";
            this.cEMP_TELEFON1.ColumnEdit = this.repTel2;
            this.cEMP_TELEFON1.FieldName = "EmpTelefon1";
            this.cEMP_TELEFON1.LayoutViewField = this.layoutViewField_cEMP_TELEFON1;
            this.cEMP_TELEFON1.Name = "cEMP_TELEFON1";
            // 
            // repTel2
            // 
            this.repTel2.AutoHeight = false;
            this.repTel2.Mask.EditMask = "(\\d?\\d?\\d?)\\d\\d\\d-\\d\\d\\d\\d";
            this.repTel2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.repTel2.Name = "repTel2";
            // 
            // layoutViewField_cEMP_TELEFON1
            // 
            this.layoutViewField_cEMP_TELEFON1.EditorPreferredWidth = 201;
            this.layoutViewField_cEMP_TELEFON1.Location = new System.Drawing.Point(0, 168);
            this.layoutViewField_cEMP_TELEFON1.Name = "layoutViewField_cEMP_TELEFON1";
            this.layoutViewField_cEMP_TELEFON1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cEMP_TELEFON1.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_cEMP_TELEFON1.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cEMP_TELEFON1.TextToControlDistance = 5;
            // 
            // cEMP_TELEFON2
            // 
            this.cEMP_TELEFON2.Caption = "Telefon 2";
            this.cEMP_TELEFON2.ColumnEdit = this.repTel2;
            this.cEMP_TELEFON2.FieldName = "EmpTelefon2";
            this.cEMP_TELEFON2.LayoutViewField = this.layoutViewField_cEMP_TELEFON2;
            this.cEMP_TELEFON2.Name = "cEMP_TELEFON2";
            // 
            // layoutViewField_cEMP_TELEFON2
            // 
            this.layoutViewField_cEMP_TELEFON2.EditorPreferredWidth = 201;
            this.layoutViewField_cEMP_TELEFON2.Location = new System.Drawing.Point(0, 196);
            this.layoutViewField_cEMP_TELEFON2.Name = "layoutViewField_cEMP_TELEFON2";
            this.layoutViewField_cEMP_TELEFON2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cEMP_TELEFON2.Size = new System.Drawing.Size(280, 28);
            this.layoutViewField_cEMP_TELEFON2.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cEMP_TELEFON2.TextToControlDistance = 5;
            // 
            // cPicture
            // 
            this.cPicture.ColumnEdit = this.repositoryItemPictureEdit3;
            this.cPicture.FieldName = "EmpFoto";
            this.cPicture.LayoutViewField = this.layoutViewField_cPicture;
            this.cPicture.Name = "cPicture";
            // 
            // repositoryItemPictureEdit3
            // 
            this.repositoryItemPictureEdit3.Name = "repositoryItemPictureEdit3";
            this.repositoryItemPictureEdit3.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            // 
            // layoutViewField_cPicture
            // 
            this.layoutViewField_cPicture.EditorPreferredWidth = 272;
            this.layoutViewField_cPicture.Location = new System.Drawing.Point(0, 224);
            this.layoutViewField_cPicture.Name = "layoutViewField_cPicture";
            this.layoutViewField_cPicture.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutViewField_cPicture.Size = new System.Drawing.Size(280, 30);
            this.layoutViewField_cPicture.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_cPicture.TextToControlDistance = 0;
            this.layoutViewField_cPicture.TextVisible = false;
            // 
            // cNote
            // 
            this.cNote.Caption = " Not";
            this.cNote.ColumnEdit = this.repositoryItemMemoEdit1;
            this.cNote.FieldName = "Note";
            this.cNote.LayoutViewField = this.layoutViewField_cNote;
            this.cNote.Name = "cNote";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // layoutViewField_cNote
            // 
            this.layoutViewField_cNote.EditorPreferredWidth = 205;
            this.layoutViewField_cNote.Location = new System.Drawing.Point(0, 254);
            this.layoutViewField_cNote.MaxSize = new System.Drawing.Size(264, 50);
            this.layoutViewField_cNote.MinSize = new System.Drawing.Size(264, 50);
            this.layoutViewField_cNote.Name = "layoutViewField_cNote";
            this.layoutViewField_cNote.Size = new System.Drawing.Size(280, 50);
            this.layoutViewField_cNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_cNote.TextSize = new System.Drawing.Size(66, 20);
            this.layoutViewField_cNote.TextToControlDistance = 5;
            // 
            // layoutViewCard2
            // 
            this.layoutViewCard2.CustomizationFormText = "TemplateCard";
            this.layoutViewCard2.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard2.GroupBordersVisible = false;
            this.layoutViewCard2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1,
            this.layoutViewField_cEMP_ADI,
            this.layoutViewField_cEMP_SOYADI,
            this.layoutViewField_cEMP_GOREVI,
            this.layoutViewField_cEMP_EMAIL,
            this.layoutViewField_cEMP_TELEFON1,
            this.layoutViewField_cEMP_TELEFON2,
            this.layoutViewField_cPicture,
            this.layoutViewField_cNote,
            this.layoutViewField_layoutViewColumn110_7});
            this.layoutViewCard2.Name = "layoutViewCard2";
            this.layoutViewCard2.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard2.Text = "TemplateCard";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            this.repositoryItemComboBox5.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            this.repositoryItemLookUpEdit3.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.NullText = "";
            this.repositoryItemLookUpEdit4.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit4
            // 
            this.repositoryItemPictureEdit4.Name = "repositoryItemPictureEdit4";
            // 
            // repositoryItemImageEdit2
            // 
            this.repositoryItemImageEdit2.AutoHeight = false;
            this.repositoryItemImageEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit2.Name = "repositoryItemImageEdit2";
            // 
            // tbZiyaretler
            // 
            this.tbZiyaretler.Controls.Add(this.grpZiyaret);
            this.tbZiyaretler.Controls.Add(this.standaloneBarDockControl3);
            this.tbZiyaretler.Name = "tbZiyaretler";
            this.tbZiyaretler.Size = new System.Drawing.Size(1357, 408);
            this.tbZiyaretler.Text = "Ziyaretler";
            // 
            // grpZiyaret
            // 
            this.grpZiyaret.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpZiyaret.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpZiyaret.AppearanceCaption.Options.UseFont = true;
            this.grpZiyaret.AppearanceCaption.Options.UseForeColor = true;
            this.grpZiyaret.Controls.Add(this.grdZiyaretler);
            this.grpZiyaret.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpZiyaret.Location = new System.Drawing.Point(0, 29);
            this.grpZiyaret.Name = "grpZiyaret";
            this.grpZiyaret.Size = new System.Drawing.Size(1357, 379);
            this.grpZiyaret.TabIndex = 2;
            this.grpZiyaret.Text = "...";
            // 
            // grdZiyaretler
            // 
            this.grdZiyaretler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdZiyaretler.Location = new System.Drawing.Point(2, 31);
            this.grdZiyaretler.MainView = this.grdVZiyaretler;
            this.grdZiyaretler.Name = "grdZiyaretler";
            this.grdZiyaretler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox7,
            this.repositoryItemComboBox8,
            this.repositoryItemComboBox9,
            this.repositoryItemLookUpEdit9,
            this.repositoryItemLookUpEdit10,
            this.repositoryItemPictureEdit5,
            this.repositoryItemImageEdit3,
            this.repositoryItemPictureEdit6,
            this.repositoryItemLookUpEdit8,
            this.repositoryItemLookUpEdit7,
            this.repositoryItemLookUpEdit6,
            this.repositoryItemLookUpEdit5,
            this.repGorusulenKisiler,
            this.repositoryItemMemoEdit2,
            this.repTarih,
            this.repAnaGrup,
            this.repAltGrup,
            this.repCheckUpTar,
            this.repZiyNedeniM,
            this.repPromosyon,
            this.repGorusenKisiler,
            this.repGorusenKisiler2});
            this.grdZiyaretler.Size = new System.Drawing.Size(1353, 346);
            this.grdZiyaretler.TabIndex = 7;
            this.grdZiyaretler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVZiyaretler,
            this.layoutView3,
            this.layoutView4});
            // 
            // grdVZiyaretler
            // 
            this.grdVZiyaretler.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn9,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn10,
            this.gridColumn7,
            this.gridColumn8,
            this.czZiyaretNedeni,
            this.czPromosyon,
            this.czPromosyonMiktar});
            this.grdVZiyaretler.GridControl = this.grdZiyaretler;
            this.grdVZiyaretler.Images = this.SmallImage;
            this.grdVZiyaretler.Name = "grdVZiyaretler";
            this.grdVZiyaretler.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVZiyaretler.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVZiyaretler.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVZiyaretler.OptionsSelection.MultiSelect = true;
            this.grdVZiyaretler.OptionsView.ColumnAutoWidth = false;
            this.grdVZiyaretler.OptionsView.RowAutoHeight = true;
            this.grdVZiyaretler.OptionsView.ShowAutoFilterRow = true;
            this.grdVZiyaretler.OptionsView.ShowGroupPanel = false;
            this.grdVZiyaretler.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdVZiyaretler_CellValueChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Müşteri Kodu";
            this.gridColumn2.FieldName = "zFirmaKodu";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Width = 20;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Tarih";
            this.gridColumn3.ColumnEdit = this.repTarih;
            this.gridColumn3.FieldName = "zTarih";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 78;
            // 
            // repTarih
            // 
            this.repTarih.AutoHeight = false;
            this.repTarih.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTarih.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repTarih.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repTarih.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repTarih.Name = "repTarih";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Check Up Tar.";
            this.gridColumn9.ColumnEdit = this.repCheckUpTar;
            this.gridColumn9.FieldName = "zCheckUpTarihi";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 70;
            // 
            // repCheckUpTar
            // 
            this.repCheckUpTar.AutoHeight = false;
            this.repCheckUpTar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCheckUpTar.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repCheckUpTar.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repCheckUpTar.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repCheckUpTar.Name = "repCheckUpTar";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Açıklamalar";
            this.gridColumn4.ColumnEdit = this.repositoryItemMemoEdit2;
            this.gridColumn4.FieldName = "zNot";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 7;
            this.gridColumn4.Width = 263;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Görüşülen Kişiler";
            this.gridColumn5.ColumnEdit = this.repGorusulenKisiler;
            this.gridColumn5.FieldName = "zGorusulenKisiler";
            this.gridColumn5.ImageIndex = 29;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            this.gridColumn5.Width = 229;
            // 
            // repGorusulenKisiler
            // 
            this.repGorusulenKisiler.AutoHeight = false;
            this.repGorusulenKisiler.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGorusulenKisiler.DisplayMember = "Tanim";
            this.repGorusulenKisiler.Name = "repGorusulenKisiler";
            this.repGorusulenKisiler.ValueMember = "Tanim";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Görüşen Kişiler";
            this.gridColumn10.ColumnEdit = this.repGorusenKisiler2;
            this.gridColumn10.FieldName = "zGorusenKisiler";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            this.gridColumn10.Width = 186;
            // 
            // repGorusenKisiler2
            // 
            this.repGorusenKisiler2.AutoHeight = false;
            this.repGorusenKisiler2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGorusenKisiler2.DisplayMember = "Tanim";
            this.repGorusenKisiler2.Name = "repGorusenKisiler2";
            this.repGorusenKisiler2.ValueMember = "Kod";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ana Grup";
            this.gridColumn7.ColumnEdit = this.repAnaGrup;
            this.gridColumn7.FieldName = "zAnaGrup";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            this.gridColumn7.Width = 142;
            // 
            // repAnaGrup
            // 
            this.repAnaGrup.AutoHeight = false;
            this.repAnaGrup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAnaGrup.DisplayMember = "Tanim";
            this.repAnaGrup.Name = "repAnaGrup";
            this.repAnaGrup.NullText = "";
            this.repAnaGrup.ValueMember = "Kod";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Alt Grup";
            this.gridColumn8.ColumnEdit = this.repAltGrup;
            this.gridColumn8.FieldName = "zAltGrup";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            this.gridColumn8.Width = 147;
            // 
            // repAltGrup
            // 
            this.repAltGrup.AutoHeight = false;
            this.repAltGrup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAltGrup.DisplayMember = "Tanim";
            this.repAltGrup.Name = "repAltGrup";
            this.repAltGrup.NullText = "";
            this.repAltGrup.ValueMember = "Kod";
            // 
            // czZiyaretNedeni
            // 
            this.czZiyaretNedeni.Caption = "Ziyaret Nedeni";
            this.czZiyaretNedeni.ColumnEdit = this.repZiyNedeniM;
            this.czZiyaretNedeni.FieldName = "zZiyaretNedeni";
            this.czZiyaretNedeni.Name = "czZiyaretNedeni";
            this.czZiyaretNedeni.Visible = true;
            this.czZiyaretNedeni.VisibleIndex = 2;
            this.czZiyaretNedeni.Width = 98;
            // 
            // repZiyNedeniM
            // 
            this.repZiyNedeniM.AutoHeight = false;
            this.repZiyNedeniM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repZiyNedeniM.DisplayMember = "Tanim";
            this.repZiyNedeniM.Name = "repZiyNedeniM";
            this.repZiyNedeniM.NullText = "";
            this.repZiyNedeniM.ValueMember = "Kod";
            // 
            // czPromosyon
            // 
            this.czPromosyon.Caption = "Promosyon";
            this.czPromosyon.ColumnEdit = this.repPromosyon;
            this.czPromosyon.FieldName = "zPromosyon";
            this.czPromosyon.Name = "czPromosyon";
            this.czPromosyon.Visible = true;
            this.czPromosyon.VisibleIndex = 8;
            // 
            // repPromosyon
            // 
            this.repPromosyon.AutoHeight = false;
            this.repPromosyon.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repPromosyon.DisplayMember = "Tanim";
            this.repPromosyon.Name = "repPromosyon";
            this.repPromosyon.NullText = "";
            this.repPromosyon.ValueMember = "Kod";
            // 
            // czPromosyonMiktar
            // 
            this.czPromosyonMiktar.Caption = "PMik.";
            this.czPromosyonMiktar.DisplayFormat.FormatString = "f0";
            this.czPromosyonMiktar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.czPromosyonMiktar.FieldName = "zPromosyonMiktar";
            this.czPromosyonMiktar.Name = "czPromosyonMiktar";
            this.czPromosyonMiktar.ToolTip = "Promosyon miktar";
            this.czPromosyonMiktar.Visible = true;
            this.czPromosyonMiktar.VisibleIndex = 9;
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox8.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            this.repositoryItemComboBox8.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox9
            // 
            this.repositoryItemComboBox9.AutoHeight = false;
            this.repositoryItemComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox9.Name = "repositoryItemComboBox9";
            // 
            // repositoryItemLookUpEdit9
            // 
            this.repositoryItemLookUpEdit9.AutoHeight = false;
            this.repositoryItemLookUpEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit9.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit9.Name = "repositoryItemLookUpEdit9";
            this.repositoryItemLookUpEdit9.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit10
            // 
            this.repositoryItemLookUpEdit10.AutoHeight = false;
            this.repositoryItemLookUpEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit10.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit10.Name = "repositoryItemLookUpEdit10";
            this.repositoryItemLookUpEdit10.NullText = "";
            this.repositoryItemLookUpEdit10.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit5
            // 
            this.repositoryItemPictureEdit5.Name = "repositoryItemPictureEdit5";
            // 
            // repositoryItemImageEdit3
            // 
            this.repositoryItemImageEdit3.AutoHeight = false;
            this.repositoryItemImageEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit3.Name = "repositoryItemImageEdit3";
            // 
            // repositoryItemPictureEdit6
            // 
            this.repositoryItemPictureEdit6.Name = "repositoryItemPictureEdit6";
            // 
            // repositoryItemLookUpEdit8
            // 
            this.repositoryItemLookUpEdit8.AutoHeight = false;
            this.repositoryItemLookUpEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit8.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit8.Name = "repositoryItemLookUpEdit8";
            this.repositoryItemLookUpEdit8.NullText = "";
            this.repositoryItemLookUpEdit8.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit7
            // 
            this.repositoryItemLookUpEdit7.AutoHeight = false;
            this.repositoryItemLookUpEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit7.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit7.Name = "repositoryItemLookUpEdit7";
            this.repositoryItemLookUpEdit7.NullText = "";
            this.repositoryItemLookUpEdit7.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit6
            // 
            this.repositoryItemLookUpEdit6.AutoHeight = false;
            this.repositoryItemLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit6.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit6.Name = "repositoryItemLookUpEdit6";
            this.repositoryItemLookUpEdit6.NullText = "";
            this.repositoryItemLookUpEdit6.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit5
            // 
            this.repositoryItemLookUpEdit5.AutoHeight = false;
            this.repositoryItemLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit5.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit5.DropDownRows = 20;
            this.repositoryItemLookUpEdit5.Name = "repositoryItemLookUpEdit5";
            this.repositoryItemLookUpEdit5.NullText = "";
            this.repositoryItemLookUpEdit5.PopupWidth = 600;
            this.repositoryItemLookUpEdit5.ValueMember = "Kod";
            // 
            // repGorusenKisiler
            // 
            this.repGorusenKisiler.AutoHeight = false;
            this.repGorusenKisiler.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repGorusenKisiler.DisplayMember = "Tanim";
            this.repGorusenKisiler.Name = "repGorusenKisiler";
            this.repGorusenKisiler.NullText = "";
            this.repGorusenKisiler.ValueMember = "Kod";
            // 
            // layoutView3
            // 
            this.layoutView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn20,
            this.layoutViewColumn21,
            this.layoutViewColumn22,
            this.layoutViewColumn23,
            this.layoutViewColumn24,
            this.layoutViewColumn25,
            this.layoutViewColumn26,
            this.layoutViewColumn27,
            this.layoutViewColumn28});
            this.layoutView3.GridControl = this.grdZiyaretler;
            this.layoutView3.Name = "layoutView3";
            this.layoutView3.OptionsSelection.MultiSelect = true;
            this.layoutView3.TemplateCard = null;
            // 
            // layoutViewColumn20
            // 
            this.layoutViewColumn20.Caption = "ID";
            this.layoutViewColumn20.FieldName = "ID";
            this.layoutViewColumn20.LayoutViewField = this.layoutViewField1;
            this.layoutViewColumn20.Name = "layoutViewColumn20";
            // 
            // layoutViewField1
            // 
            this.layoutViewField1.EditorPreferredWidth = 129;
            this.layoutViewField1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField1.Name = "layoutViewField1";
            this.layoutViewField1.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField1.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField1.TextToControlDistance = 5;
            // 
            // layoutViewColumn21
            // 
            this.layoutViewColumn21.Caption = "Müşteri Kodu";
            this.layoutViewColumn21.FieldName = "MusteriKodu";
            this.layoutViewColumn21.LayoutViewField = this.layoutViewField2;
            this.layoutViewColumn21.Name = "layoutViewColumn21";
            this.layoutViewColumn21.Width = 84;
            // 
            // layoutViewField2
            // 
            this.layoutViewField2.EditorPreferredWidth = 129;
            this.layoutViewField2.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField2.Name = "layoutViewField2";
            this.layoutViewField2.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField2.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField2.TextToControlDistance = 5;
            // 
            // layoutViewColumn22
            // 
            this.layoutViewColumn22.Caption = "Adı";
            this.layoutViewColumn22.FieldName = "Adi";
            this.layoutViewColumn22.LayoutViewField = this.layoutViewField3;
            this.layoutViewColumn22.Name = "layoutViewColumn22";
            this.layoutViewColumn22.Width = 118;
            // 
            // layoutViewField3
            // 
            this.layoutViewField3.EditorPreferredWidth = 129;
            this.layoutViewField3.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField3.Name = "layoutViewField3";
            this.layoutViewField3.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField3.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField3.TextToControlDistance = 5;
            // 
            // layoutViewColumn23
            // 
            this.layoutViewColumn23.Caption = "Sektorü";
            this.layoutViewColumn23.FieldName = "Sektoru";
            this.layoutViewColumn23.LayoutViewField = this.layoutViewField4;
            this.layoutViewColumn23.Name = "layoutViewColumn23";
            this.layoutViewColumn23.Width = 133;
            // 
            // layoutViewField4
            // 
            this.layoutViewField4.EditorPreferredWidth = 129;
            this.layoutViewField4.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField4.Name = "layoutViewField4";
            this.layoutViewField4.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField4.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField4.TextToControlDistance = 5;
            // 
            // layoutViewColumn24
            // 
            this.layoutViewColumn24.Caption = "Ilçe";
            this.layoutViewColumn24.ColumnEdit = this.repositoryItemLookUpEdit6;
            this.layoutViewColumn24.FieldName = "Ilce";
            this.layoutViewColumn24.LayoutViewField = this.layoutViewField5;
            this.layoutViewColumn24.Name = "layoutViewColumn24";
            this.layoutViewColumn24.Width = 101;
            // 
            // layoutViewField5
            // 
            this.layoutViewField5.EditorPreferredWidth = 129;
            this.layoutViewField5.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField5.Name = "layoutViewField5";
            this.layoutViewField5.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField5.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField5.TextToControlDistance = 5;
            // 
            // layoutViewColumn25
            // 
            this.layoutViewColumn25.Caption = "Şehir";
            this.layoutViewColumn25.ColumnEdit = this.repositoryItemLookUpEdit7;
            this.layoutViewColumn25.FieldName = "SehirKodu";
            this.layoutViewColumn25.LayoutViewField = this.layoutViewField6;
            this.layoutViewColumn25.Name = "layoutViewColumn25";
            this.layoutViewColumn25.Width = 96;
            // 
            // layoutViewField6
            // 
            this.layoutViewField6.EditorPreferredWidth = 129;
            this.layoutViewField6.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField6.Name = "layoutViewField6";
            this.layoutViewField6.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField6.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField6.TextToControlDistance = 5;
            // 
            // layoutViewColumn26
            // 
            this.layoutViewColumn26.Caption = "Ülke";
            this.layoutViewColumn26.ColumnEdit = this.repositoryItemLookUpEdit8;
            this.layoutViewColumn26.FieldName = "UlkeKodu";
            this.layoutViewColumn26.LayoutViewField = this.layoutViewField7;
            this.layoutViewColumn26.Name = "layoutViewColumn26";
            this.layoutViewColumn26.Width = 104;
            // 
            // layoutViewField7
            // 
            this.layoutViewField7.EditorPreferredWidth = 129;
            this.layoutViewField7.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField7.Name = "layoutViewField7";
            this.layoutViewField7.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField7.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField7.TextToControlDistance = 5;
            // 
            // layoutViewColumn27
            // 
            this.layoutViewColumn27.Caption = "Adres";
            this.layoutViewColumn27.FieldName = "Adres";
            this.layoutViewColumn27.LayoutViewField = this.layoutViewField8;
            this.layoutViewColumn27.Name = "layoutViewColumn27";
            this.layoutViewColumn27.Width = 165;
            // 
            // layoutViewField8
            // 
            this.layoutViewField8.EditorPreferredWidth = 129;
            this.layoutViewField8.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField8.Name = "layoutViewField8";
            this.layoutViewField8.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField8.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField8.TextToControlDistance = 5;
            // 
            // layoutViewColumn28
            // 
            this.layoutViewColumn28.Caption = "Vergi Dairesi";
            this.layoutViewColumn28.FieldName = "VergiDairesi";
            this.layoutViewColumn28.LayoutViewField = this.layoutViewField9;
            this.layoutViewColumn28.Name = "layoutViewColumn28";
            this.layoutViewColumn28.Width = 87;
            // 
            // layoutViewField9
            // 
            this.layoutViewField9.EditorPreferredWidth = 129;
            this.layoutViewField9.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField9.Name = "layoutViewField9";
            this.layoutViewField9.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField9.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField9.TextToControlDistance = 5;
            // 
            // layoutView4
            // 
            this.layoutView4.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn29,
            this.layoutViewColumn30,
            this.layoutViewColumn31,
            this.layoutViewColumn32,
            this.layoutViewColumn33,
            this.layoutViewColumn34,
            this.layoutViewColumn35,
            this.layoutViewColumn36,
            this.layoutViewColumn37});
            this.layoutView4.GridControl = this.grdZiyaretler;
            this.layoutView4.Name = "layoutView4";
            this.layoutView4.OptionsSelection.MultiSelect = true;
            this.layoutView4.OptionsView.ShowCardCaption = false;
            this.layoutView4.TemplateCard = null;
            // 
            // layoutViewColumn29
            // 
            this.layoutViewColumn29.Caption = "ID";
            this.layoutViewColumn29.FieldName = "ID";
            this.layoutViewColumn29.LayoutViewField = this.layoutViewField10;
            this.layoutViewColumn29.Name = "layoutViewColumn29";
            // 
            // layoutViewField10
            // 
            this.layoutViewField10.EditorPreferredWidth = 253;
            this.layoutViewField10.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField10.Name = "layoutViewField10";
            this.layoutViewField10.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField10.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField10.TextToControlDistance = 5;
            // 
            // layoutViewColumn30
            // 
            this.layoutViewColumn30.Caption = "Müşteri Kodu";
            this.layoutViewColumn30.FieldName = "MusteriKodu";
            this.layoutViewColumn30.LayoutViewField = this.layoutViewField11;
            this.layoutViewColumn30.Name = "layoutViewColumn30";
            this.layoutViewColumn30.Width = 84;
            // 
            // layoutViewField11
            // 
            this.layoutViewField11.EditorPreferredWidth = 253;
            this.layoutViewField11.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField11.Name = "layoutViewField11";
            this.layoutViewField11.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField11.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField11.TextToControlDistance = 5;
            // 
            // layoutViewColumn31
            // 
            this.layoutViewColumn31.Caption = "Adı";
            this.layoutViewColumn31.FieldName = "Adi";
            this.layoutViewColumn31.LayoutViewField = this.layoutViewField12;
            this.layoutViewColumn31.Name = "layoutViewColumn31";
            this.layoutViewColumn31.Width = 118;
            // 
            // layoutViewField12
            // 
            this.layoutViewField12.EditorPreferredWidth = 253;
            this.layoutViewField12.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField12.Name = "layoutViewField12";
            this.layoutViewField12.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField12.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField12.TextToControlDistance = 5;
            // 
            // layoutViewColumn32
            // 
            this.layoutViewColumn32.Caption = "Sektorü";
            this.layoutViewColumn32.FieldName = "Sektoru";
            this.layoutViewColumn32.LayoutViewField = this.layoutViewField13;
            this.layoutViewColumn32.Name = "layoutViewColumn32";
            this.layoutViewColumn32.Width = 133;
            // 
            // layoutViewField13
            // 
            this.layoutViewField13.EditorPreferredWidth = 253;
            this.layoutViewField13.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField13.Name = "layoutViewField13";
            this.layoutViewField13.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField13.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField13.TextToControlDistance = 5;
            // 
            // layoutViewColumn33
            // 
            this.layoutViewColumn33.Caption = "Ilçe";
            this.layoutViewColumn33.ColumnEdit = this.repositoryItemLookUpEdit6;
            this.layoutViewColumn33.FieldName = "Ilce";
            this.layoutViewColumn33.LayoutViewField = this.layoutViewField14;
            this.layoutViewColumn33.Name = "layoutViewColumn33";
            this.layoutViewColumn33.Width = 101;
            // 
            // layoutViewField14
            // 
            this.layoutViewField14.EditorPreferredWidth = 253;
            this.layoutViewField14.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField14.Name = "layoutViewField14";
            this.layoutViewField14.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField14.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField14.TextToControlDistance = 5;
            // 
            // layoutViewColumn34
            // 
            this.layoutViewColumn34.Caption = "Şehir";
            this.layoutViewColumn34.ColumnEdit = this.repositoryItemLookUpEdit7;
            this.layoutViewColumn34.FieldName = "SehirKodu";
            this.layoutViewColumn34.LayoutViewField = this.layoutViewField15;
            this.layoutViewColumn34.Name = "layoutViewColumn34";
            this.layoutViewColumn34.Width = 96;
            // 
            // layoutViewField15
            // 
            this.layoutViewField15.EditorPreferredWidth = 253;
            this.layoutViewField15.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField15.Name = "layoutViewField15";
            this.layoutViewField15.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField15.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField15.TextToControlDistance = 5;
            // 
            // layoutViewColumn35
            // 
            this.layoutViewColumn35.Caption = "Ülke";
            this.layoutViewColumn35.ColumnEdit = this.repositoryItemLookUpEdit8;
            this.layoutViewColumn35.FieldName = "UlkeKodu";
            this.layoutViewColumn35.LayoutViewField = this.layoutViewField16;
            this.layoutViewColumn35.Name = "layoutViewColumn35";
            this.layoutViewColumn35.Width = 104;
            // 
            // layoutViewField16
            // 
            this.layoutViewField16.EditorPreferredWidth = 253;
            this.layoutViewField16.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField16.Name = "layoutViewField16";
            this.layoutViewField16.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField16.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField16.TextToControlDistance = 5;
            // 
            // layoutViewColumn36
            // 
            this.layoutViewColumn36.Caption = "Adres";
            this.layoutViewColumn36.FieldName = "Adres";
            this.layoutViewColumn36.LayoutViewField = this.layoutViewField17;
            this.layoutViewColumn36.Name = "layoutViewColumn36";
            this.layoutViewColumn36.Width = 165;
            // 
            // layoutViewField17
            // 
            this.layoutViewField17.EditorPreferredWidth = 253;
            this.layoutViewField17.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField17.Name = "layoutViewField17";
            this.layoutViewField17.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField17.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField17.TextToControlDistance = 5;
            // 
            // layoutViewColumn37
            // 
            this.layoutViewColumn37.Caption = "Vergi Dairesi";
            this.layoutViewColumn37.FieldName = "VergiDairesi";
            this.layoutViewColumn37.LayoutViewField = this.layoutViewField18;
            this.layoutViewColumn37.Name = "layoutViewColumn37";
            this.layoutViewColumn37.Width = 87;
            // 
            // layoutViewField18
            // 
            this.layoutViewField18.EditorPreferredWidth = 253;
            this.layoutViewField18.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField18.Name = "layoutViewField18";
            this.layoutViewField18.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField18.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField18.TextToControlDistance = 5;
            // 
            // tbTeklifler
            // 
            this.tbTeklifler.Controls.Add(this.grpTeklifler);
            this.tbTeklifler.Controls.Add(this.standaloneBarDockControl4);
            this.tbTeklifler.Name = "tbTeklifler";
            this.tbTeklifler.Size = new System.Drawing.Size(1357, 408);
            this.tbTeklifler.Text = "Teklifler";
            // 
            // grpTeklifler
            // 
            this.grpTeklifler.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpTeklifler.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpTeklifler.AppearanceCaption.Options.UseFont = true;
            this.grpTeklifler.AppearanceCaption.Options.UseForeColor = true;
            this.grpTeklifler.Controls.Add(this.grdTeklifler);
            this.grpTeklifler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpTeklifler.Location = new System.Drawing.Point(0, 29);
            this.grpTeklifler.Name = "grpTeklifler";
            this.grpTeklifler.Size = new System.Drawing.Size(1357, 379);
            this.grpTeklifler.TabIndex = 12;
            this.grpTeklifler.Text = "...";
            // 
            // grdTeklifler
            // 
            this.grdTeklifler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdTeklifler.Location = new System.Drawing.Point(2, 31);
            this.grdTeklifler.MainView = this.grdVTeklifler;
            this.grdTeklifler.Name = "grdTeklifler";
            this.grdTeklifler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox10,
            this.repositoryItemComboBox11,
            this.repositoryItemComboBox12,
            this.repositoryItemLookUpEdit11,
            this.repositoryItemLookUpEdit12,
            this.repositoryItemPictureEdit7,
            this.repositoryItemImageEdit4,
            this.repositoryItemPictureEdit8,
            this.repositoryItemLookUpEdit13,
            this.repositoryItemLookUpEdit14,
            this.repositoryItemLookUpEdit15,
            this.repositoryItemLookUpEdit16,
            this.repItemChk1,
            this.repositoryItemMemoEdit3,
            this.repositoryItemDateEdit2,
            this.repChkTip,
            this.repChkHazirlayan,
            this.repChkTekSonuc,
            this.repChkNedenler,
            this.repositoryItemDateEdit3,
            this.repChkLSonuc,
            this.repCmbKisi,
            this.repAltGrupTeklif,
            this.repAnaGrupTeklif,
            this.repDate,
            this.repSegman,
            this.repTekChkTeklifAnaGroup,
            this.repTekChkTeklifAltGroup});
            this.grdTeklifler.Size = new System.Drawing.Size(1353, 346);
            this.grdTeklifler.TabIndex = 11;
            this.grdTeklifler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVTeklifler,
            this.layoutView5,
            this.layoutView6});
            // 
            // grdVTeklifler
            // 
            this.grdVTeklifler.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cTekID,
            this.cTekFirmaKodu,
            this.cTekGorusmeTarihi,
            this.cTekVerilenKisiler,
            this.czAnaGrup,
            this.czAltGrup,
            this.cTekTipi,
            this.cTekTarih,
            this.cTekHazirlayan,
            this.cTekSonucu,
            this.cIkincilKisi,
            this.cBirincilKisi,
            this.cTekKazanmaKaybetmeNedeni,
            this.cTekKazanmaKaybetmeNotu});
            this.grdVTeklifler.GridControl = this.grdTeklifler;
            this.grdVTeklifler.Images = this.SmallImage;
            this.grdVTeklifler.Name = "grdVTeklifler";
            this.grdVTeklifler.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVTeklifler.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVTeklifler.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVTeklifler.OptionsSelection.MultiSelect = true;
            this.grdVTeklifler.OptionsView.ColumnAutoWidth = false;
            this.grdVTeklifler.OptionsView.RowAutoHeight = true;
            this.grdVTeklifler.OptionsView.ShowAutoFilterRow = true;
            this.grdVTeklifler.OptionsView.ShowGroupPanel = false;
            this.grdVTeklifler.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdVTeklifler_CellValueChanged);
            // 
            // cTekID
            // 
            this.cTekID.Caption = "No";
            this.cTekID.FieldName = "TekID";
            this.cTekID.Name = "cTekID";
            this.cTekID.OptionsColumn.AllowEdit = false;
            this.cTekID.ToolTip = "Teklif numarası";
            this.cTekID.Visible = true;
            this.cTekID.VisibleIndex = 0;
            this.cTekID.Width = 47;
            // 
            // cTekFirmaKodu
            // 
            this.cTekFirmaKodu.Caption = "Müşteri Kodu";
            this.cTekFirmaKodu.FieldName = "TekFirmaKodu";
            this.cTekFirmaKodu.Name = "cTekFirmaKodu";
            this.cTekFirmaKodu.OptionsColumn.AllowEdit = false;
            this.cTekFirmaKodu.Visible = true;
            this.cTekFirmaKodu.VisibleIndex = 1;
            this.cTekFirmaKodu.Width = 59;
            // 
            // cTekGorusmeTarihi
            // 
            this.cTekGorusmeTarihi.Caption = "G.Tarih";
            this.cTekGorusmeTarihi.ColumnEdit = this.repositoryItemDateEdit2;
            this.cTekGorusmeTarihi.FieldName = "TekGorusmeTarihi";
            this.cTekGorusmeTarihi.Name = "cTekGorusmeTarihi";
            this.cTekGorusmeTarihi.ToolTip = "Görüşme Tarihi";
            this.cTekGorusmeTarihi.Visible = true;
            this.cTekGorusmeTarihi.VisibleIndex = 2;
            this.cTekGorusmeTarihi.Width = 77;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit2.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // cTekVerilenKisiler
            // 
            this.cTekVerilenKisiler.Caption = "Tekl.V.Kişiler";
            this.cTekVerilenKisiler.ColumnEdit = this.repItemChk1;
            this.cTekVerilenKisiler.FieldName = "TekVerilenKisiler";
            this.cTekVerilenKisiler.ImageIndex = 29;
            this.cTekVerilenKisiler.Name = "cTekVerilenKisiler";
            this.cTekVerilenKisiler.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cTekVerilenKisiler.ToolTip = "Teklif verilen kişiler";
            this.cTekVerilenKisiler.Visible = true;
            this.cTekVerilenKisiler.VisibleIndex = 3;
            this.cTekVerilenKisiler.Width = 126;
            // 
            // repItemChk1
            // 
            this.repItemChk1.AutoHeight = false;
            this.repItemChk1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemChk1.DisplayMember = "Tanim";
            this.repItemChk1.Name = "repItemChk1";
            this.repItemChk1.ValueMember = "Tanim";
            // 
            // czAnaGrup
            // 
            this.czAnaGrup.Caption = "Ana Grup";
            this.czAnaGrup.ColumnEdit = this.repTekChkTeklifAnaGroup;
            this.czAnaGrup.FieldName = "zAnaGrup";
            this.czAnaGrup.Name = "czAnaGrup";
            this.czAnaGrup.Visible = true;
            this.czAnaGrup.VisibleIndex = 4;
            this.czAnaGrup.Width = 154;
            // 
            // repTekChkTeklifAnaGroup
            // 
            this.repTekChkTeklifAnaGroup.AutoHeight = false;
            this.repTekChkTeklifAnaGroup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTekChkTeklifAnaGroup.DisplayMember = "Tanim";
            this.repTekChkTeklifAnaGroup.Name = "repTekChkTeklifAnaGroup";
            this.repTekChkTeklifAnaGroup.ValueMember = "Kod";
            // 
            // czAltGrup
            // 
            this.czAltGrup.Caption = "Alt Grup";
            this.czAltGrup.ColumnEdit = this.repTekChkTeklifAltGroup;
            this.czAltGrup.FieldName = "zAltGrup";
            this.czAltGrup.Name = "czAltGrup";
            this.czAltGrup.Visible = true;
            this.czAltGrup.VisibleIndex = 5;
            this.czAltGrup.Width = 176;
            // 
            // repTekChkTeklifAltGroup
            // 
            this.repTekChkTeklifAltGroup.AutoHeight = false;
            this.repTekChkTeklifAltGroup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTekChkTeklifAltGroup.DisplayMember = "Tanim";
            this.repTekChkTeklifAltGroup.Name = "repTekChkTeklifAltGroup";
            this.repTekChkTeklifAltGroup.ValueMember = "Kod";
            // 
            // cTekTipi
            // 
            this.cTekTipi.Caption = "Tek.Konusu";
            this.cTekTipi.ColumnEdit = this.repChkTip;
            this.cTekTipi.FieldName = "TekTipi";
            this.cTekTipi.Name = "cTekTipi";
            this.cTekTipi.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.cTekTipi.ToolTip = "Teklif Konusu";
            this.cTekTipi.Visible = true;
            this.cTekTipi.VisibleIndex = 8;
            this.cTekTipi.Width = 102;
            // 
            // repChkTip
            // 
            this.repChkTip.AutoHeight = false;
            this.repChkTip.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkTip.DisplayMember = "Tanim";
            this.repChkTip.Name = "repChkTip";
            this.repChkTip.ValueMember = "Tanim";
            // 
            // cTekTarih
            // 
            this.cTekTarih.Caption = "TekTarih";
            this.cTekTarih.ColumnEdit = this.repositoryItemDateEdit3;
            this.cTekTarih.FieldName = "TekTarih";
            this.cTekTarih.Name = "cTekTarih";
            this.cTekTarih.ToolTip = "Teklif tarihi";
            this.cTekTarih.Visible = true;
            this.cTekTarih.VisibleIndex = 9;
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit3.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit3.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // cTekHazirlayan
            // 
            this.cTekHazirlayan.Caption = "Hazirlayan";
            this.cTekHazirlayan.ColumnEdit = this.repChkHazirlayan;
            this.cTekHazirlayan.FieldName = "TekHazirlayan";
            this.cTekHazirlayan.Name = "cTekHazirlayan";
            this.cTekHazirlayan.ToolTip = "Teklif hazırlayan";
            this.cTekHazirlayan.Visible = true;
            this.cTekHazirlayan.VisibleIndex = 10;
            this.cTekHazirlayan.Width = 114;
            // 
            // repChkHazirlayan
            // 
            this.repChkHazirlayan.AutoHeight = false;
            this.repChkHazirlayan.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkHazirlayan.DisplayMember = "Tanim";
            this.repChkHazirlayan.Name = "repChkHazirlayan";
            this.repChkHazirlayan.ValueMember = "Tanim";
            // 
            // cTekSonucu
            // 
            this.cTekSonucu.Caption = "Sonuç";
            this.cTekSonucu.ColumnEdit = this.repChkLSonuc;
            this.cTekSonucu.FieldName = "TekSonucu";
            this.cTekSonucu.Name = "cTekSonucu";
            this.cTekSonucu.ToolTip = "Teklif sonucu";
            this.cTekSonucu.Visible = true;
            this.cTekSonucu.VisibleIndex = 11;
            this.cTekSonucu.Width = 103;
            // 
            // repChkLSonuc
            // 
            this.repChkLSonuc.AutoHeight = false;
            this.repChkLSonuc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkLSonuc.DisplayMember = "Tanim";
            this.repChkLSonuc.Name = "repChkLSonuc";
            this.repChkLSonuc.NullText = "";
            this.repChkLSonuc.ValueMember = "Tanim";
            // 
            // cIkincilKisi
            // 
            this.cIkincilKisi.Caption = "2.Cil Kişi";
            this.cIkincilKisi.FieldName = "IkincilKisi";
            this.cIkincilKisi.Name = "cIkincilKisi";
            this.cIkincilKisi.Visible = true;
            this.cIkincilKisi.VisibleIndex = 7;
            this.cIkincilKisi.Width = 114;
            // 
            // cBirincilKisi
            // 
            this.cBirincilKisi.Caption = "1.Cil Kişi";
            this.cBirincilKisi.FieldName = "BirincilKisi";
            this.cBirincilKisi.Name = "cBirincilKisi";
            this.cBirincilKisi.Visible = true;
            this.cBirincilKisi.VisibleIndex = 6;
            this.cBirincilKisi.Width = 119;
            // 
            // cTekKazanmaKaybetmeNedeni
            // 
            this.cTekKazanmaKaybetmeNedeni.Caption = "K/K Nedenlr.";
            this.cTekKazanmaKaybetmeNedeni.ColumnEdit = this.repChkNedenler;
            this.cTekKazanmaKaybetmeNedeni.FieldName = "TekKazanmaKaybetmeNedeni";
            this.cTekKazanmaKaybetmeNedeni.Name = "cTekKazanmaKaybetmeNedeni";
            this.cTekKazanmaKaybetmeNedeni.ToolTip = "Teklifi kazanma yada kaybetme nedenleri";
            this.cTekKazanmaKaybetmeNedeni.Visible = true;
            this.cTekKazanmaKaybetmeNedeni.VisibleIndex = 12;
            this.cTekKazanmaKaybetmeNedeni.Width = 94;
            // 
            // repChkNedenler
            // 
            this.repChkNedenler.AutoHeight = false;
            this.repChkNedenler.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkNedenler.DisplayMember = "Tanim";
            this.repChkNedenler.Name = "repChkNedenler";
            this.repChkNedenler.ValueMember = "Tanim";
            // 
            // cTekKazanmaKaybetmeNotu
            // 
            this.cTekKazanmaKaybetmeNotu.Caption = "K/K Açık.";
            this.cTekKazanmaKaybetmeNotu.ColumnEdit = this.repositoryItemMemoEdit3;
            this.cTekKazanmaKaybetmeNotu.FieldName = "TekKazanmaKaybetmeNotu";
            this.cTekKazanmaKaybetmeNotu.Name = "cTekKazanmaKaybetmeNotu";
            this.cTekKazanmaKaybetmeNotu.ToolTip = "Teklifi kazanma yada kaybetme açıklamaları";
            this.cTekKazanmaKaybetmeNotu.Visible = true;
            this.cTekKazanmaKaybetmeNotu.VisibleIndex = 13;
            this.cTekKazanmaKaybetmeNotu.Width = 353;
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // repositoryItemComboBox10
            // 
            this.repositoryItemComboBox10.AutoHeight = false;
            this.repositoryItemComboBox10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox10.Name = "repositoryItemComboBox10";
            // 
            // repositoryItemComboBox11
            // 
            this.repositoryItemComboBox11.AutoHeight = false;
            this.repositoryItemComboBox11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox11.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox11.Name = "repositoryItemComboBox11";
            this.repositoryItemComboBox11.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox12
            // 
            this.repositoryItemComboBox12.AutoHeight = false;
            this.repositoryItemComboBox12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox12.Name = "repositoryItemComboBox12";
            // 
            // repositoryItemLookUpEdit11
            // 
            this.repositoryItemLookUpEdit11.AutoHeight = false;
            this.repositoryItemLookUpEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit11.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit11.Name = "repositoryItemLookUpEdit11";
            this.repositoryItemLookUpEdit11.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit12
            // 
            this.repositoryItemLookUpEdit12.AutoHeight = false;
            this.repositoryItemLookUpEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit12.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit12.Name = "repositoryItemLookUpEdit12";
            this.repositoryItemLookUpEdit12.NullText = "";
            this.repositoryItemLookUpEdit12.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit7
            // 
            this.repositoryItemPictureEdit7.Name = "repositoryItemPictureEdit7";
            // 
            // repositoryItemImageEdit4
            // 
            this.repositoryItemImageEdit4.AutoHeight = false;
            this.repositoryItemImageEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit4.Name = "repositoryItemImageEdit4";
            // 
            // repositoryItemPictureEdit8
            // 
            this.repositoryItemPictureEdit8.Name = "repositoryItemPictureEdit8";
            // 
            // repositoryItemLookUpEdit13
            // 
            this.repositoryItemLookUpEdit13.AutoHeight = false;
            this.repositoryItemLookUpEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit13.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit13.Name = "repositoryItemLookUpEdit13";
            this.repositoryItemLookUpEdit13.NullText = "";
            this.repositoryItemLookUpEdit13.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit14
            // 
            this.repositoryItemLookUpEdit14.AutoHeight = false;
            this.repositoryItemLookUpEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit14.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit14.Name = "repositoryItemLookUpEdit14";
            this.repositoryItemLookUpEdit14.NullText = "";
            this.repositoryItemLookUpEdit14.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit15
            // 
            this.repositoryItemLookUpEdit15.AutoHeight = false;
            this.repositoryItemLookUpEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit15.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit15.Name = "repositoryItemLookUpEdit15";
            this.repositoryItemLookUpEdit15.NullText = "";
            this.repositoryItemLookUpEdit15.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit16
            // 
            this.repositoryItemLookUpEdit16.AutoHeight = false;
            this.repositoryItemLookUpEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit16.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit16.DropDownRows = 20;
            this.repositoryItemLookUpEdit16.Name = "repositoryItemLookUpEdit16";
            this.repositoryItemLookUpEdit16.NullText = "";
            this.repositoryItemLookUpEdit16.PopupWidth = 600;
            this.repositoryItemLookUpEdit16.ValueMember = "Kod";
            // 
            // repChkTekSonuc
            // 
            this.repChkTekSonuc.AutoHeight = false;
            this.repChkTekSonuc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkTekSonuc.DisplayMember = "Tanim";
            this.repChkTekSonuc.Name = "repChkTekSonuc";
            this.repChkTekSonuc.ValueMember = "Tanim";
            // 
            // repCmbKisi
            // 
            this.repCmbKisi.AutoHeight = false;
            this.repCmbKisi.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCmbKisi.Items.AddRange(new object[] {
            "1.Cil Kişi",
            "2.Cil Kişi"});
            this.repCmbKisi.Name = "repCmbKisi";
            // 
            // repAltGrupTeklif
            // 
            this.repAltGrupTeklif.AutoHeight = false;
            this.repAltGrupTeklif.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAltGrupTeklif.DisplayMember = "Tanim";
            this.repAltGrupTeklif.Name = "repAltGrupTeklif";
            this.repAltGrupTeklif.NullText = "";
            this.repAltGrupTeklif.ValueMember = "Kod";
            // 
            // repAnaGrupTeklif
            // 
            this.repAnaGrupTeklif.AutoHeight = false;
            this.repAnaGrupTeklif.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAnaGrupTeklif.DisplayMember = "Tanim";
            this.repAnaGrupTeklif.Name = "repAnaGrupTeklif";
            this.repAnaGrupTeklif.NullText = "";
            this.repAnaGrupTeklif.ValueMember = "Kod";
            // 
            // repDate
            // 
            this.repDate.AutoHeight = false;
            this.repDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repDate.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repDate.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repDate.Name = "repDate";
            // 
            // repSegman
            // 
            this.repSegman.AutoHeight = false;
            this.repSegman.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSegman.DisplayMember = "Kod";
            this.repSegman.Name = "repSegman";
            this.repSegman.NullText = "";
            this.repSegman.ValueMember = "Kod";
            // 
            // layoutView5
            // 
            this.layoutView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn38,
            this.layoutViewColumn39,
            this.layoutViewColumn40,
            this.layoutViewColumn41,
            this.layoutViewColumn42,
            this.layoutViewColumn43,
            this.layoutViewColumn44,
            this.layoutViewColumn45,
            this.layoutViewColumn46});
            this.layoutView5.GridControl = this.grdTeklifler;
            this.layoutView5.Name = "layoutView5";
            this.layoutView5.OptionsSelection.MultiSelect = true;
            this.layoutView5.TemplateCard = null;
            // 
            // layoutViewColumn38
            // 
            this.layoutViewColumn38.Caption = "ID";
            this.layoutViewColumn38.FieldName = "ID";
            this.layoutViewColumn38.LayoutViewField = this.layoutViewField19;
            this.layoutViewColumn38.Name = "layoutViewColumn38";
            // 
            // layoutViewField19
            // 
            this.layoutViewField19.EditorPreferredWidth = 129;
            this.layoutViewField19.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField19.Name = "layoutViewField19";
            this.layoutViewField19.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField19.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField19.TextToControlDistance = 5;
            // 
            // layoutViewColumn39
            // 
            this.layoutViewColumn39.Caption = "Müşteri Kodu";
            this.layoutViewColumn39.FieldName = "MusteriKodu";
            this.layoutViewColumn39.LayoutViewField = this.layoutViewField20;
            this.layoutViewColumn39.Name = "layoutViewColumn39";
            this.layoutViewColumn39.Width = 84;
            // 
            // layoutViewField20
            // 
            this.layoutViewField20.EditorPreferredWidth = 129;
            this.layoutViewField20.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField20.Name = "layoutViewField20";
            this.layoutViewField20.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField20.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField20.TextToControlDistance = 5;
            // 
            // layoutViewColumn40
            // 
            this.layoutViewColumn40.Caption = "Adı";
            this.layoutViewColumn40.FieldName = "Adi";
            this.layoutViewColumn40.LayoutViewField = this.layoutViewField21;
            this.layoutViewColumn40.Name = "layoutViewColumn40";
            this.layoutViewColumn40.Width = 118;
            // 
            // layoutViewField21
            // 
            this.layoutViewField21.EditorPreferredWidth = 129;
            this.layoutViewField21.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField21.Name = "layoutViewField21";
            this.layoutViewField21.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField21.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField21.TextToControlDistance = 5;
            // 
            // layoutViewColumn41
            // 
            this.layoutViewColumn41.Caption = "Sektorü";
            this.layoutViewColumn41.FieldName = "Sektoru";
            this.layoutViewColumn41.LayoutViewField = this.layoutViewField22;
            this.layoutViewColumn41.Name = "layoutViewColumn41";
            this.layoutViewColumn41.Width = 133;
            // 
            // layoutViewField22
            // 
            this.layoutViewField22.EditorPreferredWidth = 129;
            this.layoutViewField22.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField22.Name = "layoutViewField22";
            this.layoutViewField22.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField22.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField22.TextToControlDistance = 5;
            // 
            // layoutViewColumn42
            // 
            this.layoutViewColumn42.Caption = "Ilçe";
            this.layoutViewColumn42.ColumnEdit = this.repositoryItemLookUpEdit15;
            this.layoutViewColumn42.FieldName = "Ilce";
            this.layoutViewColumn42.LayoutViewField = this.layoutViewField23;
            this.layoutViewColumn42.Name = "layoutViewColumn42";
            this.layoutViewColumn42.Width = 101;
            // 
            // layoutViewField23
            // 
            this.layoutViewField23.EditorPreferredWidth = 129;
            this.layoutViewField23.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField23.Name = "layoutViewField23";
            this.layoutViewField23.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField23.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField23.TextToControlDistance = 5;
            // 
            // layoutViewColumn43
            // 
            this.layoutViewColumn43.Caption = "Şehir";
            this.layoutViewColumn43.ColumnEdit = this.repositoryItemLookUpEdit14;
            this.layoutViewColumn43.FieldName = "SehirKodu";
            this.layoutViewColumn43.LayoutViewField = this.layoutViewField24;
            this.layoutViewColumn43.Name = "layoutViewColumn43";
            this.layoutViewColumn43.Width = 96;
            // 
            // layoutViewField24
            // 
            this.layoutViewField24.EditorPreferredWidth = 129;
            this.layoutViewField24.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField24.Name = "layoutViewField24";
            this.layoutViewField24.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField24.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField24.TextToControlDistance = 5;
            // 
            // layoutViewColumn44
            // 
            this.layoutViewColumn44.Caption = "Ülke";
            this.layoutViewColumn44.ColumnEdit = this.repositoryItemLookUpEdit13;
            this.layoutViewColumn44.FieldName = "UlkeKodu";
            this.layoutViewColumn44.LayoutViewField = this.layoutViewField25;
            this.layoutViewColumn44.Name = "layoutViewColumn44";
            this.layoutViewColumn44.Width = 104;
            // 
            // layoutViewField25
            // 
            this.layoutViewField25.EditorPreferredWidth = 129;
            this.layoutViewField25.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField25.Name = "layoutViewField25";
            this.layoutViewField25.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField25.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField25.TextToControlDistance = 5;
            // 
            // layoutViewColumn45
            // 
            this.layoutViewColumn45.Caption = "Adres";
            this.layoutViewColumn45.FieldName = "Adres";
            this.layoutViewColumn45.LayoutViewField = this.layoutViewField26;
            this.layoutViewColumn45.Name = "layoutViewColumn45";
            this.layoutViewColumn45.Width = 165;
            // 
            // layoutViewField26
            // 
            this.layoutViewField26.EditorPreferredWidth = 129;
            this.layoutViewField26.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField26.Name = "layoutViewField26";
            this.layoutViewField26.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField26.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField26.TextToControlDistance = 5;
            // 
            // layoutViewColumn46
            // 
            this.layoutViewColumn46.Caption = "Vergi Dairesi";
            this.layoutViewColumn46.FieldName = "VergiDairesi";
            this.layoutViewColumn46.LayoutViewField = this.layoutViewField27;
            this.layoutViewColumn46.Name = "layoutViewColumn46";
            this.layoutViewColumn46.Width = 87;
            // 
            // layoutViewField27
            // 
            this.layoutViewField27.EditorPreferredWidth = 129;
            this.layoutViewField27.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField27.Name = "layoutViewField27";
            this.layoutViewField27.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField27.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField27.TextToControlDistance = 5;
            // 
            // layoutView6
            // 
            this.layoutView6.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn47,
            this.layoutViewColumn48,
            this.layoutViewColumn49,
            this.layoutViewColumn50,
            this.layoutViewColumn51,
            this.layoutViewColumn52,
            this.layoutViewColumn53,
            this.layoutViewColumn54,
            this.layoutViewColumn55});
            this.layoutView6.GridControl = this.grdTeklifler;
            this.layoutView6.Name = "layoutView6";
            this.layoutView6.OptionsSelection.MultiSelect = true;
            this.layoutView6.OptionsView.ShowCardCaption = false;
            this.layoutView6.TemplateCard = null;
            // 
            // layoutViewColumn47
            // 
            this.layoutViewColumn47.Caption = "ID";
            this.layoutViewColumn47.FieldName = "ID";
            this.layoutViewColumn47.LayoutViewField = this.layoutViewField28;
            this.layoutViewColumn47.Name = "layoutViewColumn47";
            // 
            // layoutViewField28
            // 
            this.layoutViewField28.EditorPreferredWidth = 253;
            this.layoutViewField28.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField28.Name = "layoutViewField28";
            this.layoutViewField28.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField28.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField28.TextToControlDistance = 5;
            // 
            // layoutViewColumn48
            // 
            this.layoutViewColumn48.Caption = "Müşteri Kodu";
            this.layoutViewColumn48.FieldName = "MusteriKodu";
            this.layoutViewColumn48.LayoutViewField = this.layoutViewField29;
            this.layoutViewColumn48.Name = "layoutViewColumn48";
            this.layoutViewColumn48.Width = 84;
            // 
            // layoutViewField29
            // 
            this.layoutViewField29.EditorPreferredWidth = 253;
            this.layoutViewField29.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField29.Name = "layoutViewField29";
            this.layoutViewField29.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField29.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField29.TextToControlDistance = 5;
            // 
            // layoutViewColumn49
            // 
            this.layoutViewColumn49.Caption = "Adı";
            this.layoutViewColumn49.FieldName = "Adi";
            this.layoutViewColumn49.LayoutViewField = this.layoutViewField30;
            this.layoutViewColumn49.Name = "layoutViewColumn49";
            this.layoutViewColumn49.Width = 118;
            // 
            // layoutViewField30
            // 
            this.layoutViewField30.EditorPreferredWidth = 253;
            this.layoutViewField30.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField30.Name = "layoutViewField30";
            this.layoutViewField30.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField30.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField30.TextToControlDistance = 5;
            // 
            // layoutViewColumn50
            // 
            this.layoutViewColumn50.Caption = "Sektorü";
            this.layoutViewColumn50.FieldName = "Sektoru";
            this.layoutViewColumn50.LayoutViewField = this.layoutViewField31;
            this.layoutViewColumn50.Name = "layoutViewColumn50";
            this.layoutViewColumn50.Width = 133;
            // 
            // layoutViewField31
            // 
            this.layoutViewField31.EditorPreferredWidth = 253;
            this.layoutViewField31.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField31.Name = "layoutViewField31";
            this.layoutViewField31.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField31.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField31.TextToControlDistance = 5;
            // 
            // layoutViewColumn51
            // 
            this.layoutViewColumn51.Caption = "Ilçe";
            this.layoutViewColumn51.ColumnEdit = this.repositoryItemLookUpEdit15;
            this.layoutViewColumn51.FieldName = "Ilce";
            this.layoutViewColumn51.LayoutViewField = this.layoutViewField32;
            this.layoutViewColumn51.Name = "layoutViewColumn51";
            this.layoutViewColumn51.Width = 101;
            // 
            // layoutViewField32
            // 
            this.layoutViewField32.EditorPreferredWidth = 253;
            this.layoutViewField32.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField32.Name = "layoutViewField32";
            this.layoutViewField32.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField32.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField32.TextToControlDistance = 5;
            // 
            // layoutViewColumn52
            // 
            this.layoutViewColumn52.Caption = "Şehir";
            this.layoutViewColumn52.ColumnEdit = this.repositoryItemLookUpEdit14;
            this.layoutViewColumn52.FieldName = "SehirKodu";
            this.layoutViewColumn52.LayoutViewField = this.layoutViewField33;
            this.layoutViewColumn52.Name = "layoutViewColumn52";
            this.layoutViewColumn52.Width = 96;
            // 
            // layoutViewField33
            // 
            this.layoutViewField33.EditorPreferredWidth = 253;
            this.layoutViewField33.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField33.Name = "layoutViewField33";
            this.layoutViewField33.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField33.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField33.TextToControlDistance = 5;
            // 
            // layoutViewColumn53
            // 
            this.layoutViewColumn53.Caption = "Ülke";
            this.layoutViewColumn53.ColumnEdit = this.repositoryItemLookUpEdit13;
            this.layoutViewColumn53.FieldName = "UlkeKodu";
            this.layoutViewColumn53.LayoutViewField = this.layoutViewField34;
            this.layoutViewColumn53.Name = "layoutViewColumn53";
            this.layoutViewColumn53.Width = 104;
            // 
            // layoutViewField34
            // 
            this.layoutViewField34.EditorPreferredWidth = 253;
            this.layoutViewField34.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField34.Name = "layoutViewField34";
            this.layoutViewField34.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField34.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField34.TextToControlDistance = 5;
            // 
            // layoutViewColumn54
            // 
            this.layoutViewColumn54.Caption = "Adres";
            this.layoutViewColumn54.FieldName = "Adres";
            this.layoutViewColumn54.LayoutViewField = this.layoutViewField35;
            this.layoutViewColumn54.Name = "layoutViewColumn54";
            this.layoutViewColumn54.Width = 165;
            // 
            // layoutViewField35
            // 
            this.layoutViewField35.EditorPreferredWidth = 253;
            this.layoutViewField35.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField35.Name = "layoutViewField35";
            this.layoutViewField35.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField35.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField35.TextToControlDistance = 5;
            // 
            // layoutViewColumn55
            // 
            this.layoutViewColumn55.Caption = "Vergi Dairesi";
            this.layoutViewColumn55.FieldName = "VergiDairesi";
            this.layoutViewColumn55.LayoutViewField = this.layoutViewField36;
            this.layoutViewColumn55.Name = "layoutViewColumn55";
            this.layoutViewColumn55.Width = 87;
            // 
            // layoutViewField36
            // 
            this.layoutViewField36.EditorPreferredWidth = 253;
            this.layoutViewField36.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField36.Name = "layoutViewField36";
            this.layoutViewField36.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField36.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField36.TextToControlDistance = 5;
            // 
            // tbSozlesme
            // 
            this.tbSozlesme.Controls.Add(this.grpSozlesmeler);
            this.tbSozlesme.Controls.Add(this.standaloneBarDockControl5);
            this.tbSozlesme.Name = "tbSozlesme";
            this.tbSozlesme.Size = new System.Drawing.Size(1357, 408);
            this.tbSozlesme.Text = "Sözleşmeler";
            this.tbSozlesme.Leave += new System.EventHandler(this.tbSozlesme_Leave);
            // 
            // grpSozlesmeler
            // 
            this.grpSozlesmeler.Appearance.BackColor = System.Drawing.Color.Black;
            this.grpSozlesmeler.Appearance.Options.UseBackColor = true;
            this.grpSozlesmeler.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpSozlesmeler.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpSozlesmeler.AppearanceCaption.Options.UseFont = true;
            this.grpSozlesmeler.AppearanceCaption.Options.UseForeColor = true;
            this.grpSozlesmeler.AutoSize = true;
            this.grpSozlesmeler.Controls.Add(this.grdSozlesmeler);
            this.grpSozlesmeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSozlesmeler.Location = new System.Drawing.Point(0, 29);
            this.grpSozlesmeler.Name = "grpSozlesmeler";
            this.grpSozlesmeler.Size = new System.Drawing.Size(1357, 379);
            this.grpSozlesmeler.TabIndex = 13;
            this.grpSozlesmeler.Text = "...";
            // 
            // grdSozlesmeler
            // 
            this.grdSozlesmeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdSozlesmeler.Location = new System.Drawing.Point(2, 31);
            this.grdSozlesmeler.MainView = this.grdVSozlesmeler;
            this.grdSozlesmeler.Name = "grdSozlesmeler";
            this.grdSozlesmeler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox13,
            this.repositoryItemComboBox14,
            this.repositoryItemComboBox15,
            this.repositoryItemLookUpEdit18,
            this.repositoryItemLookUpEdit19,
            this.repositoryItemPictureEdit9,
            this.repositoryItemImageEdit5,
            this.repositoryItemPictureEdit10,
            this.repositoryItemLookUpEdit20,
            this.repositoryItemLookUpEdit21,
            this.repositoryItemLookUpEdit22,
            this.repositoryItemLookUpEdit23,
            this.repositoryItemCheckedComboBoxEdit1,
            this.repMem,
            this.repositoryItemDateEdit4,
            this.repositoryItemCheckedComboBoxEdit2,
            this.repositoryItemCheckedComboBoxEdit3,
            this.repositoryItemCheckedComboBoxEdit5,
            this.repositoryItemCheckedComboBoxEdit4,
            this.repositoryItemDateEdit5,
            this.repositoryItemLookUpEdit17,
            this.repositoryItemDateEdit6,
            this.repositoryItemMemoEdit5,
            this.repositoryItemComboBox16,
            this.repBr,
            this.repChkSatYap,
            this.repositoryItemDateEdit7,
            this.repositoryItemDateEdit8,
            this.repSozlTeklNo,
            this.repSozIcerik});
            this.grdSozlesmeler.Size = new System.Drawing.Size(1353, 346);
            this.grdSozlesmeler.TabIndex = 11;
            this.grdSozlesmeler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVSozlesmeler,
            this.layoutView7,
            this.layoutView8});
            // 
            // grdVSozlesmeler
            // 
            this.grdVSozlesmeler.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.cSoz_TeklifNo,
            this.cSoz_Musteri_Kodu,
            this.cSoz_NeredenUlasildi,
            this.cSoz_Imza_Tar,
            this.cSoz_Icerik,
            this.cSoz_Teklif_Fiyat,
            this.cSoz_Teklif_Fiyat_Birim,
            this.cSoz_Imz_Tutar,
            this.cSoz_Imz_Tutar_Birim,
            this.cSoz_Satis_Yapan,
            this.cSoz_Performans_Prim,
            this.cSoz_Performans_Prim_Birim,
            this.cSoz_Baslangıc,
            this.cSoz_Bitis});
            this.grdVSozlesmeler.GridControl = this.grdSozlesmeler;
            this.grdVSozlesmeler.Images = this.SmallImage;
            this.grdVSozlesmeler.Name = "grdVSozlesmeler";
            this.grdVSozlesmeler.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVSozlesmeler.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVSozlesmeler.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVSozlesmeler.OptionsSelection.MultiSelect = true;
            this.grdVSozlesmeler.OptionsView.ColumnAutoWidth = false;
            this.grdVSozlesmeler.OptionsView.RowAutoHeight = true;
            this.grdVSozlesmeler.OptionsView.ShowAutoFilterRow = true;
            this.grdVSozlesmeler.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "No";
            this.gridColumn6.FieldName = "Soz_Id";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 34;
            // 
            // cSoz_TeklifNo
            // 
            this.cSoz_TeklifNo.Caption = "Tkl.No";
            this.cSoz_TeklifNo.ColumnEdit = this.repSozlTeklNo;
            this.cSoz_TeklifNo.FieldName = "Soz_TeklifNo";
            this.cSoz_TeklifNo.Name = "cSoz_TeklifNo";
            this.cSoz_TeklifNo.ToolTip = "Teklif numaraları";
            this.cSoz_TeklifNo.Visible = true;
            this.cSoz_TeklifNo.VisibleIndex = 1;
            this.cSoz_TeklifNo.Width = 63;
            // 
            // repSozlTeklNo
            // 
            this.repSozlTeklNo.AutoHeight = false;
            this.repSozlTeklNo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSozlTeklNo.DisplayMember = "Tanim";
            this.repSozlTeklNo.Name = "repSozlTeklNo";
            this.repSozlTeklNo.ValueMember = "Tanim";
            // 
            // cSoz_Musteri_Kodu
            // 
            this.cSoz_Musteri_Kodu.Caption = "Müşteri Kodu";
            this.cSoz_Musteri_Kodu.FieldName = "Soz_Musteri_Kodu";
            this.cSoz_Musteri_Kodu.Name = "cSoz_Musteri_Kodu";
            this.cSoz_Musteri_Kodu.OptionsColumn.AllowEdit = false;
            this.cSoz_Musteri_Kodu.Width = 25;
            // 
            // cSoz_NeredenUlasildi
            // 
            this.cSoz_NeredenUlasildi.Caption = "Nerd.Ulş.";
            this.cSoz_NeredenUlasildi.ColumnEdit = this.repMem;
            this.cSoz_NeredenUlasildi.FieldName = "Soz_NeredenUlasildi";
            this.cSoz_NeredenUlasildi.Name = "cSoz_NeredenUlasildi";
            this.cSoz_NeredenUlasildi.ToolTip = "Nereden ulaşıldı";
            this.cSoz_NeredenUlasildi.Width = 189;
            // 
            // repMem
            // 
            this.repMem.Appearance.Options.UseTextOptions = true;
            this.repMem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repMem.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repMem.AppearanceDisabled.Options.UseTextOptions = true;
            this.repMem.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repMem.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repMem.AppearanceFocused.Options.UseTextOptions = true;
            this.repMem.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repMem.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repMem.AppearanceReadOnly.Options.UseTextOptions = true;
            this.repMem.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repMem.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repMem.Name = "repMem";
            // 
            // cSoz_Imza_Tar
            // 
            this.cSoz_Imza_Tar.Caption = "Ver.Tarihi";
            this.cSoz_Imza_Tar.ColumnEdit = this.repositoryItemDateEdit6;
            this.cSoz_Imza_Tar.FieldName = "Soz_Imza_Tar";
            this.cSoz_Imza_Tar.Name = "cSoz_Imza_Tar";
            this.cSoz_Imza_Tar.ToolTip = "Sözleşme veriliş tarihi";
            this.cSoz_Imza_Tar.Visible = true;
            this.cSoz_Imza_Tar.VisibleIndex = 2;
            this.cSoz_Imza_Tar.Width = 57;
            // 
            // repositoryItemDateEdit6
            // 
            this.repositoryItemDateEdit6.AutoHeight = false;
            this.repositoryItemDateEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit6.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit6.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit6.Name = "repositoryItemDateEdit6";
            // 
            // cSoz_Icerik
            // 
            this.cSoz_Icerik.Caption = "İçerik";
            this.cSoz_Icerik.ColumnEdit = this.repSozIcerik;
            this.cSoz_Icerik.FieldName = "Soz_Icerik";
            this.cSoz_Icerik.Name = "cSoz_Icerik";
            this.cSoz_Icerik.Visible = true;
            this.cSoz_Icerik.VisibleIndex = 3;
            this.cSoz_Icerik.Width = 112;
            // 
            // repSozIcerik
            // 
            this.repSozIcerik.AutoHeight = false;
            this.repSozIcerik.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSozIcerik.DisplayMember = "Tanim";
            this.repSozIcerik.Name = "repSozIcerik";
            this.repSozIcerik.NullText = "";
            this.repSozIcerik.ValueMember = "Kod";
            // 
            // cSoz_Teklif_Fiyat
            // 
            this.cSoz_Teklif_Fiyat.Caption = "Tkl. Fiyat";
            this.cSoz_Teklif_Fiyat.DisplayFormat.FormatString = "n2";
            this.cSoz_Teklif_Fiyat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cSoz_Teklif_Fiyat.FieldName = "Soz_Teklif_Fiyat";
            this.cSoz_Teklif_Fiyat.Name = "cSoz_Teklif_Fiyat";
            this.cSoz_Teklif_Fiyat.ToolTip = "Teklif Fiyat";
            this.cSoz_Teklif_Fiyat.Visible = true;
            this.cSoz_Teklif_Fiyat.VisibleIndex = 4;
            // 
            // cSoz_Teklif_Fiyat_Birim
            // 
            this.cSoz_Teklif_Fiyat_Birim.Caption = "Br.";
            this.cSoz_Teklif_Fiyat_Birim.ColumnEdit = this.repBr;
            this.cSoz_Teklif_Fiyat_Birim.FieldName = "Soz_Teklif_Fiyat_Birim";
            this.cSoz_Teklif_Fiyat_Birim.Name = "cSoz_Teklif_Fiyat_Birim";
            this.cSoz_Teklif_Fiyat_Birim.Visible = true;
            this.cSoz_Teklif_Fiyat_Birim.VisibleIndex = 5;
            // 
            // repBr
            // 
            this.repBr.AutoHeight = false;
            this.repBr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repBr.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repBr.Name = "repBr";
            this.repBr.NullText = "TL";
            this.repBr.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cSoz_Imz_Tutar
            // 
            this.cSoz_Imz_Tutar.Caption = "İmz.Tutar";
            this.cSoz_Imz_Tutar.DisplayFormat.FormatString = "n2";
            this.cSoz_Imz_Tutar.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cSoz_Imz_Tutar.FieldName = "Soz_Imz_Tutar";
            this.cSoz_Imz_Tutar.Name = "cSoz_Imz_Tutar";
            this.cSoz_Imz_Tutar.ToolTip = "İmzalanan Tutar";
            this.cSoz_Imz_Tutar.Visible = true;
            this.cSoz_Imz_Tutar.VisibleIndex = 6;
            // 
            // cSoz_Imz_Tutar_Birim
            // 
            this.cSoz_Imz_Tutar_Birim.Caption = "Br.";
            this.cSoz_Imz_Tutar_Birim.ColumnEdit = this.repBr;
            this.cSoz_Imz_Tutar_Birim.FieldName = "Soz_Imz_Tutar_Birim";
            this.cSoz_Imz_Tutar_Birim.Name = "cSoz_Imz_Tutar_Birim";
            this.cSoz_Imz_Tutar_Birim.Visible = true;
            this.cSoz_Imz_Tutar_Birim.VisibleIndex = 7;
            this.cSoz_Imz_Tutar_Birim.Width = 34;
            // 
            // cSoz_Satis_Yapan
            // 
            this.cSoz_Satis_Yapan.Caption = "Satışı Yp.";
            this.cSoz_Satis_Yapan.ColumnEdit = this.repChkSatYap;
            this.cSoz_Satis_Yapan.FieldName = "Soz_Satis_Yapan";
            this.cSoz_Satis_Yapan.Name = "cSoz_Satis_Yapan";
            this.cSoz_Satis_Yapan.ToolTip = "Satışı yapan";
            this.cSoz_Satis_Yapan.Visible = true;
            this.cSoz_Satis_Yapan.VisibleIndex = 8;
            this.cSoz_Satis_Yapan.Width = 153;
            // 
            // repChkSatYap
            // 
            this.repChkSatYap.AutoHeight = false;
            this.repChkSatYap.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkSatYap.DisplayMember = "Tanim";
            this.repChkSatYap.Name = "repChkSatYap";
            this.repChkSatYap.ValueMember = "Tanim";
            // 
            // cSoz_Performans_Prim
            // 
            this.cSoz_Performans_Prim.Caption = "Per.Prim";
            this.cSoz_Performans_Prim.DisplayFormat.FormatString = "n2";
            this.cSoz_Performans_Prim.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cSoz_Performans_Prim.FieldName = "Soz_Performans_Prim";
            this.cSoz_Performans_Prim.Name = "cSoz_Performans_Prim";
            this.cSoz_Performans_Prim.ToolTip = "Performans primi";
            this.cSoz_Performans_Prim.Visible = true;
            this.cSoz_Performans_Prim.VisibleIndex = 9;
            this.cSoz_Performans_Prim.Width = 85;
            // 
            // cSoz_Performans_Prim_Birim
            // 
            this.cSoz_Performans_Prim_Birim.Caption = "Br.";
            this.cSoz_Performans_Prim_Birim.ColumnEdit = this.repBr;
            this.cSoz_Performans_Prim_Birim.FieldName = "Soz_Performans_Prim_Birim";
            this.cSoz_Performans_Prim_Birim.Name = "cSoz_Performans_Prim_Birim";
            this.cSoz_Performans_Prim_Birim.Visible = true;
            this.cSoz_Performans_Prim_Birim.VisibleIndex = 10;
            this.cSoz_Performans_Prim_Birim.Width = 41;
            // 
            // cSoz_Baslangıc
            // 
            this.cSoz_Baslangıc.Caption = "Sz.Başl.";
            this.cSoz_Baslangıc.ColumnEdit = this.repositoryItemDateEdit7;
            this.cSoz_Baslangıc.FieldName = "Soz_Baslangıc";
            this.cSoz_Baslangıc.Name = "cSoz_Baslangıc";
            this.cSoz_Baslangıc.ToolTip = "Sözleşme başlangıç";
            this.cSoz_Baslangıc.Visible = true;
            this.cSoz_Baslangıc.VisibleIndex = 11;
            // 
            // repositoryItemDateEdit7
            // 
            this.repositoryItemDateEdit7.AutoHeight = false;
            this.repositoryItemDateEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit7.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit7.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit7.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit7.Name = "repositoryItemDateEdit7";
            // 
            // cSoz_Bitis
            // 
            this.cSoz_Bitis.Caption = "Sz.Bitş.";
            this.cSoz_Bitis.ColumnEdit = this.repositoryItemDateEdit8;
            this.cSoz_Bitis.FieldName = "Soz_Bitis";
            this.cSoz_Bitis.Name = "cSoz_Bitis";
            this.cSoz_Bitis.ToolTip = "Sözleşme bitiş";
            this.cSoz_Bitis.Visible = true;
            this.cSoz_Bitis.VisibleIndex = 12;
            // 
            // repositoryItemDateEdit8
            // 
            this.repositoryItemDateEdit8.AutoHeight = false;
            this.repositoryItemDateEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit8.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit8.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit8.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit8.Name = "repositoryItemDateEdit8";
            // 
            // repositoryItemComboBox13
            // 
            this.repositoryItemComboBox13.AutoHeight = false;
            this.repositoryItemComboBox13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox13.Name = "repositoryItemComboBox13";
            // 
            // repositoryItemComboBox14
            // 
            this.repositoryItemComboBox14.AutoHeight = false;
            this.repositoryItemComboBox14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox14.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox14.Name = "repositoryItemComboBox14";
            this.repositoryItemComboBox14.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox15
            // 
            this.repositoryItemComboBox15.AutoHeight = false;
            this.repositoryItemComboBox15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox15.Name = "repositoryItemComboBox15";
            // 
            // repositoryItemLookUpEdit18
            // 
            this.repositoryItemLookUpEdit18.AutoHeight = false;
            this.repositoryItemLookUpEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit18.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit18.Name = "repositoryItemLookUpEdit18";
            this.repositoryItemLookUpEdit18.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit19
            // 
            this.repositoryItemLookUpEdit19.AutoHeight = false;
            this.repositoryItemLookUpEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit19.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit19.Name = "repositoryItemLookUpEdit19";
            this.repositoryItemLookUpEdit19.NullText = "";
            this.repositoryItemLookUpEdit19.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit9
            // 
            this.repositoryItemPictureEdit9.Name = "repositoryItemPictureEdit9";
            // 
            // repositoryItemImageEdit5
            // 
            this.repositoryItemImageEdit5.AutoHeight = false;
            this.repositoryItemImageEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit5.Name = "repositoryItemImageEdit5";
            // 
            // repositoryItemPictureEdit10
            // 
            this.repositoryItemPictureEdit10.Name = "repositoryItemPictureEdit10";
            // 
            // repositoryItemLookUpEdit20
            // 
            this.repositoryItemLookUpEdit20.AutoHeight = false;
            this.repositoryItemLookUpEdit20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit20.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit20.Name = "repositoryItemLookUpEdit20";
            this.repositoryItemLookUpEdit20.NullText = "";
            this.repositoryItemLookUpEdit20.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit21
            // 
            this.repositoryItemLookUpEdit21.AutoHeight = false;
            this.repositoryItemLookUpEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit21.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit21.Name = "repositoryItemLookUpEdit21";
            this.repositoryItemLookUpEdit21.NullText = "";
            this.repositoryItemLookUpEdit21.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit22
            // 
            this.repositoryItemLookUpEdit22.AutoHeight = false;
            this.repositoryItemLookUpEdit22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit22.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit22.Name = "repositoryItemLookUpEdit22";
            this.repositoryItemLookUpEdit22.NullText = "";
            this.repositoryItemLookUpEdit22.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit23
            // 
            this.repositoryItemLookUpEdit23.AutoHeight = false;
            this.repositoryItemLookUpEdit23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit23.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit23.DropDownRows = 20;
            this.repositoryItemLookUpEdit23.Name = "repositoryItemLookUpEdit23";
            this.repositoryItemLookUpEdit23.NullText = "";
            this.repositoryItemLookUpEdit23.PopupWidth = 600;
            this.repositoryItemLookUpEdit23.ValueMember = "Kod";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            this.repositoryItemCheckedComboBoxEdit1.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit4.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit4.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            // 
            // repositoryItemCheckedComboBoxEdit2
            // 
            this.repositoryItemCheckedComboBoxEdit2.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit2.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit2.Name = "repositoryItemCheckedComboBoxEdit2";
            this.repositoryItemCheckedComboBoxEdit2.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit3
            // 
            this.repositoryItemCheckedComboBoxEdit3.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit3.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit3.Name = "repositoryItemCheckedComboBoxEdit3";
            this.repositoryItemCheckedComboBoxEdit3.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit5
            // 
            this.repositoryItemCheckedComboBoxEdit5.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit5.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit5.Name = "repositoryItemCheckedComboBoxEdit5";
            this.repositoryItemCheckedComboBoxEdit5.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit4
            // 
            this.repositoryItemCheckedComboBoxEdit4.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit4.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit4.Name = "repositoryItemCheckedComboBoxEdit4";
            this.repositoryItemCheckedComboBoxEdit4.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit5.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit5.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            // 
            // repositoryItemLookUpEdit17
            // 
            this.repositoryItemLookUpEdit17.AutoHeight = false;
            this.repositoryItemLookUpEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit17.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit17.Name = "repositoryItemLookUpEdit17";
            this.repositoryItemLookUpEdit17.ValueMember = "Tanim";
            // 
            // repositoryItemMemoEdit5
            // 
            this.repositoryItemMemoEdit5.Name = "repositoryItemMemoEdit5";
            // 
            // repositoryItemComboBox16
            // 
            this.repositoryItemComboBox16.AutoHeight = false;
            this.repositoryItemComboBox16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox16.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox16.Name = "repositoryItemComboBox16";
            this.repositoryItemComboBox16.NullText = "TL";
            this.repositoryItemComboBox16.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // layoutView7
            // 
            this.layoutView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn56,
            this.layoutViewColumn57,
            this.layoutViewColumn58,
            this.layoutViewColumn59,
            this.layoutViewColumn60,
            this.layoutViewColumn61,
            this.layoutViewColumn62,
            this.layoutViewColumn63,
            this.layoutViewColumn64});
            this.layoutView7.GridControl = this.grdSozlesmeler;
            this.layoutView7.Name = "layoutView7";
            this.layoutView7.OptionsSelection.MultiSelect = true;
            this.layoutView7.TemplateCard = null;
            // 
            // layoutViewColumn56
            // 
            this.layoutViewColumn56.Caption = "ID";
            this.layoutViewColumn56.FieldName = "ID";
            this.layoutViewColumn56.LayoutViewField = this.layoutViewField37;
            this.layoutViewColumn56.Name = "layoutViewColumn56";
            // 
            // layoutViewField37
            // 
            this.layoutViewField37.EditorPreferredWidth = 129;
            this.layoutViewField37.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField37.Name = "layoutViewField37";
            this.layoutViewField37.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField37.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField37.TextToControlDistance = 5;
            // 
            // layoutViewColumn57
            // 
            this.layoutViewColumn57.Caption = "Müşteri Kodu";
            this.layoutViewColumn57.FieldName = "MusteriKodu";
            this.layoutViewColumn57.LayoutViewField = this.layoutViewField38;
            this.layoutViewColumn57.Name = "layoutViewColumn57";
            this.layoutViewColumn57.Width = 84;
            // 
            // layoutViewField38
            // 
            this.layoutViewField38.EditorPreferredWidth = 129;
            this.layoutViewField38.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField38.Name = "layoutViewField38";
            this.layoutViewField38.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField38.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField38.TextToControlDistance = 5;
            // 
            // layoutViewColumn58
            // 
            this.layoutViewColumn58.Caption = "Adı";
            this.layoutViewColumn58.FieldName = "Adi";
            this.layoutViewColumn58.LayoutViewField = this.layoutViewField39;
            this.layoutViewColumn58.Name = "layoutViewColumn58";
            this.layoutViewColumn58.Width = 118;
            // 
            // layoutViewField39
            // 
            this.layoutViewField39.EditorPreferredWidth = 129;
            this.layoutViewField39.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField39.Name = "layoutViewField39";
            this.layoutViewField39.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField39.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField39.TextToControlDistance = 5;
            // 
            // layoutViewColumn59
            // 
            this.layoutViewColumn59.Caption = "Sektorü";
            this.layoutViewColumn59.FieldName = "Sektoru";
            this.layoutViewColumn59.LayoutViewField = this.layoutViewField40;
            this.layoutViewColumn59.Name = "layoutViewColumn59";
            this.layoutViewColumn59.Width = 133;
            // 
            // layoutViewField40
            // 
            this.layoutViewField40.EditorPreferredWidth = 129;
            this.layoutViewField40.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField40.Name = "layoutViewField40";
            this.layoutViewField40.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField40.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField40.TextToControlDistance = 5;
            // 
            // layoutViewColumn60
            // 
            this.layoutViewColumn60.Caption = "Ilçe";
            this.layoutViewColumn60.ColumnEdit = this.repositoryItemLookUpEdit22;
            this.layoutViewColumn60.FieldName = "Ilce";
            this.layoutViewColumn60.LayoutViewField = this.layoutViewField41;
            this.layoutViewColumn60.Name = "layoutViewColumn60";
            this.layoutViewColumn60.Width = 101;
            // 
            // layoutViewField41
            // 
            this.layoutViewField41.EditorPreferredWidth = 129;
            this.layoutViewField41.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField41.Name = "layoutViewField41";
            this.layoutViewField41.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField41.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField41.TextToControlDistance = 5;
            // 
            // layoutViewColumn61
            // 
            this.layoutViewColumn61.Caption = "Şehir";
            this.layoutViewColumn61.ColumnEdit = this.repositoryItemLookUpEdit21;
            this.layoutViewColumn61.FieldName = "SehirKodu";
            this.layoutViewColumn61.LayoutViewField = this.layoutViewField42;
            this.layoutViewColumn61.Name = "layoutViewColumn61";
            this.layoutViewColumn61.Width = 96;
            // 
            // layoutViewField42
            // 
            this.layoutViewField42.EditorPreferredWidth = 129;
            this.layoutViewField42.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField42.Name = "layoutViewField42";
            this.layoutViewField42.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField42.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField42.TextToControlDistance = 5;
            // 
            // layoutViewColumn62
            // 
            this.layoutViewColumn62.Caption = "Ülke";
            this.layoutViewColumn62.ColumnEdit = this.repositoryItemLookUpEdit20;
            this.layoutViewColumn62.FieldName = "UlkeKodu";
            this.layoutViewColumn62.LayoutViewField = this.layoutViewField43;
            this.layoutViewColumn62.Name = "layoutViewColumn62";
            this.layoutViewColumn62.Width = 104;
            // 
            // layoutViewField43
            // 
            this.layoutViewField43.EditorPreferredWidth = 129;
            this.layoutViewField43.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField43.Name = "layoutViewField43";
            this.layoutViewField43.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField43.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField43.TextToControlDistance = 5;
            // 
            // layoutViewColumn63
            // 
            this.layoutViewColumn63.Caption = "Adres";
            this.layoutViewColumn63.FieldName = "Adres";
            this.layoutViewColumn63.LayoutViewField = this.layoutViewField44;
            this.layoutViewColumn63.Name = "layoutViewColumn63";
            this.layoutViewColumn63.Width = 165;
            // 
            // layoutViewField44
            // 
            this.layoutViewField44.EditorPreferredWidth = 129;
            this.layoutViewField44.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField44.Name = "layoutViewField44";
            this.layoutViewField44.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField44.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField44.TextToControlDistance = 5;
            // 
            // layoutViewColumn64
            // 
            this.layoutViewColumn64.Caption = "Vergi Dairesi";
            this.layoutViewColumn64.FieldName = "VergiDairesi";
            this.layoutViewColumn64.LayoutViewField = this.layoutViewField45;
            this.layoutViewColumn64.Name = "layoutViewColumn64";
            this.layoutViewColumn64.Width = 87;
            // 
            // layoutViewField45
            // 
            this.layoutViewField45.EditorPreferredWidth = 129;
            this.layoutViewField45.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField45.Name = "layoutViewField45";
            this.layoutViewField45.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField45.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField45.TextToControlDistance = 5;
            // 
            // layoutView8
            // 
            this.layoutView8.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn65,
            this.layoutViewColumn66,
            this.layoutViewColumn67,
            this.layoutViewColumn68,
            this.layoutViewColumn69,
            this.layoutViewColumn70,
            this.layoutViewColumn71,
            this.layoutViewColumn72,
            this.layoutViewColumn73});
            this.layoutView8.GridControl = this.grdSozlesmeler;
            this.layoutView8.Name = "layoutView8";
            this.layoutView8.OptionsSelection.MultiSelect = true;
            this.layoutView8.OptionsView.ShowCardCaption = false;
            this.layoutView8.TemplateCard = null;
            // 
            // layoutViewColumn65
            // 
            this.layoutViewColumn65.Caption = "ID";
            this.layoutViewColumn65.FieldName = "ID";
            this.layoutViewColumn65.LayoutViewField = this.layoutViewField46;
            this.layoutViewColumn65.Name = "layoutViewColumn65";
            // 
            // layoutViewField46
            // 
            this.layoutViewField46.EditorPreferredWidth = 253;
            this.layoutViewField46.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField46.Name = "layoutViewField46";
            this.layoutViewField46.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField46.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField46.TextToControlDistance = 5;
            // 
            // layoutViewColumn66
            // 
            this.layoutViewColumn66.Caption = "Müşteri Kodu";
            this.layoutViewColumn66.FieldName = "MusteriKodu";
            this.layoutViewColumn66.LayoutViewField = this.layoutViewField47;
            this.layoutViewColumn66.Name = "layoutViewColumn66";
            this.layoutViewColumn66.Width = 84;
            // 
            // layoutViewField47
            // 
            this.layoutViewField47.EditorPreferredWidth = 253;
            this.layoutViewField47.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField47.Name = "layoutViewField47";
            this.layoutViewField47.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField47.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField47.TextToControlDistance = 5;
            // 
            // layoutViewColumn67
            // 
            this.layoutViewColumn67.Caption = "Adı";
            this.layoutViewColumn67.FieldName = "Adi";
            this.layoutViewColumn67.LayoutViewField = this.layoutViewField48;
            this.layoutViewColumn67.Name = "layoutViewColumn67";
            this.layoutViewColumn67.Width = 118;
            // 
            // layoutViewField48
            // 
            this.layoutViewField48.EditorPreferredWidth = 253;
            this.layoutViewField48.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField48.Name = "layoutViewField48";
            this.layoutViewField48.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField48.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField48.TextToControlDistance = 5;
            // 
            // layoutViewColumn68
            // 
            this.layoutViewColumn68.Caption = "Sektorü";
            this.layoutViewColumn68.FieldName = "Sektoru";
            this.layoutViewColumn68.LayoutViewField = this.layoutViewField49;
            this.layoutViewColumn68.Name = "layoutViewColumn68";
            this.layoutViewColumn68.Width = 133;
            // 
            // layoutViewField49
            // 
            this.layoutViewField49.EditorPreferredWidth = 253;
            this.layoutViewField49.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField49.Name = "layoutViewField49";
            this.layoutViewField49.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField49.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField49.TextToControlDistance = 5;
            // 
            // layoutViewColumn69
            // 
            this.layoutViewColumn69.Caption = "Ilçe";
            this.layoutViewColumn69.ColumnEdit = this.repositoryItemLookUpEdit22;
            this.layoutViewColumn69.FieldName = "Ilce";
            this.layoutViewColumn69.LayoutViewField = this.layoutViewField50;
            this.layoutViewColumn69.Name = "layoutViewColumn69";
            this.layoutViewColumn69.Width = 101;
            // 
            // layoutViewField50
            // 
            this.layoutViewField50.EditorPreferredWidth = 253;
            this.layoutViewField50.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField50.Name = "layoutViewField50";
            this.layoutViewField50.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField50.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField50.TextToControlDistance = 5;
            // 
            // layoutViewColumn70
            // 
            this.layoutViewColumn70.Caption = "Şehir";
            this.layoutViewColumn70.ColumnEdit = this.repositoryItemLookUpEdit21;
            this.layoutViewColumn70.FieldName = "SehirKodu";
            this.layoutViewColumn70.LayoutViewField = this.layoutViewField51;
            this.layoutViewColumn70.Name = "layoutViewColumn70";
            this.layoutViewColumn70.Width = 96;
            // 
            // layoutViewField51
            // 
            this.layoutViewField51.EditorPreferredWidth = 253;
            this.layoutViewField51.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField51.Name = "layoutViewField51";
            this.layoutViewField51.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField51.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField51.TextToControlDistance = 5;
            // 
            // layoutViewColumn71
            // 
            this.layoutViewColumn71.Caption = "Ülke";
            this.layoutViewColumn71.ColumnEdit = this.repositoryItemLookUpEdit20;
            this.layoutViewColumn71.FieldName = "UlkeKodu";
            this.layoutViewColumn71.LayoutViewField = this.layoutViewField52;
            this.layoutViewColumn71.Name = "layoutViewColumn71";
            this.layoutViewColumn71.Width = 104;
            // 
            // layoutViewField52
            // 
            this.layoutViewField52.EditorPreferredWidth = 253;
            this.layoutViewField52.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField52.Name = "layoutViewField52";
            this.layoutViewField52.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField52.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField52.TextToControlDistance = 5;
            // 
            // layoutViewColumn72
            // 
            this.layoutViewColumn72.Caption = "Adres";
            this.layoutViewColumn72.FieldName = "Adres";
            this.layoutViewColumn72.LayoutViewField = this.layoutViewField53;
            this.layoutViewColumn72.Name = "layoutViewColumn72";
            this.layoutViewColumn72.Width = 165;
            // 
            // layoutViewField53
            // 
            this.layoutViewField53.EditorPreferredWidth = 253;
            this.layoutViewField53.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField53.Name = "layoutViewField53";
            this.layoutViewField53.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField53.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField53.TextToControlDistance = 5;
            // 
            // layoutViewColumn73
            // 
            this.layoutViewColumn73.Caption = "Vergi Dairesi";
            this.layoutViewColumn73.FieldName = "VergiDairesi";
            this.layoutViewColumn73.LayoutViewField = this.layoutViewField54;
            this.layoutViewColumn73.Name = "layoutViewColumn73";
            this.layoutViewColumn73.Width = 87;
            // 
            // layoutViewField54
            // 
            this.layoutViewField54.EditorPreferredWidth = 253;
            this.layoutViewField54.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField54.Name = "layoutViewField54";
            this.layoutViewField54.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField54.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField54.TextToControlDistance = 5;
            // 
            // tbOdemeler
            // 
            this.tbOdemeler.Controls.Add(this.grpOdemeler);
            this.tbOdemeler.Controls.Add(this.standaloneBarDockControl7);
            this.tbOdemeler.Name = "tbOdemeler";
            this.tbOdemeler.Size = new System.Drawing.Size(1357, 408);
            this.tbOdemeler.Text = "Ödemeler";
            // 
            // grpOdemeler
            // 
            this.grpOdemeler.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpOdemeler.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpOdemeler.AppearanceCaption.Options.UseFont = true;
            this.grpOdemeler.AppearanceCaption.Options.UseForeColor = true;
            this.grpOdemeler.Controls.Add(this.grdOdemeler);
            this.grpOdemeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpOdemeler.Location = new System.Drawing.Point(0, 29);
            this.grpOdemeler.Name = "grpOdemeler";
            this.grpOdemeler.Size = new System.Drawing.Size(1357, 379);
            this.grpOdemeler.TabIndex = 14;
            this.grpOdemeler.Text = "...";
            // 
            // grdOdemeler
            // 
            this.grdOdemeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdOdemeler.Location = new System.Drawing.Point(2, 31);
            this.grdOdemeler.MainView = this.grdVOdemeler;
            this.grdOdemeler.Name = "grdOdemeler";
            this.grdOdemeler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox18,
            this.repositoryItemComboBox19,
            this.repositoryItemComboBox20,
            this.repositoryItemLookUpEdit24,
            this.repositoryItemLookUpEdit25,
            this.repositoryItemPictureEdit11,
            this.repositoryItemImageEdit6,
            this.repositoryItemPictureEdit12,
            this.repositoryItemLookUpEdit26,
            this.repositoryItemLookUpEdit27,
            this.repositoryItemLookUpEdit28,
            this.repositoryItemLookUpEdit29,
            this.repositoryItemCheckedComboBoxEdit8,
            this.repositoryItemMemoEdit4,
            this.repositoryItemDateEdit12,
            this.repositoryItemCheckedComboBoxEdit9,
            this.repositoryItemCheckedComboBoxEdit10,
            this.repositoryItemCheckedComboBoxEdit11,
            this.repositoryItemCheckedComboBoxEdit12,
            this.repositoryItemDateEdit13,
            this.repositoryItemLookUpEdit30,
            this.repositoryItemDateEdit9,
            this.repositoryItemMemoEdit6,
            this.repositoryItemComboBox21,
            this.repositoryItemComboBox17,
            this.repositoryItemCheckedComboBoxEdit7,
            this.repositoryItemDateEdit10,
            this.repositoryItemDateEdit11,
            this.repositoryItemCheckedComboBoxEdit6,
            this.repChkOdemeSozNo,
            this.repositoryItemComboBox22,
            this.repLFaturaTipi,
            this.repFatBr,
            this.repLOdmSekli,
            this.repLUlsSekli,
            this.repLKnkSekli,
            this.repYmkSekl,
            this.repNot});
            this.grdOdemeler.Size = new System.Drawing.Size(1353, 346);
            this.grdOdemeler.TabIndex = 12;
            this.grdOdemeler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVOdemeler,
            this.layoutView9,
            this.layoutView10});
            // 
            // grdVOdemeler
            // 
            this.grdVOdemeler.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cOde_Id,
            this.cOde_Musteri_Kodu,
            this.cOde_SozlesmeNo,
            this.cOde_Fatura,
            this.cOde_Fatura_Fiyat,
            this.cOde_Fatura_Fiyat_Br,
            this.cOde_OdemeSekli,
            this.cOde_OdemeSekli_Fiyat,
            this.cOde_OdemeSekli_Br,
            this.cOde_Ulasim_Giderleri,
            this.cOde_Ulasim_Giderleri_Fiyat,
            this.cOde_Ulasim_Giderleri_Br,
            this.cOde_Konaklama_Giderleri,
            this.cOde_Konaklama_Giderleri_Fiyat,
            this.cOde_Konaklama_Giderleri_Br,
            this.cOde_Yemek_Gideleri,
            this.cOde_Yemek_Giderleri_Fiyat,
            this.cOde_Yemek_Giderleri_Br,
            this.cOde_Notlar});
            this.grdVOdemeler.GridControl = this.grdOdemeler;
            this.grdVOdemeler.Images = this.SmallImage;
            this.grdVOdemeler.Name = "grdVOdemeler";
            this.grdVOdemeler.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVOdemeler.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVOdemeler.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVOdemeler.OptionsSelection.MultiSelect = true;
            this.grdVOdemeler.OptionsView.ColumnAutoWidth = false;
            this.grdVOdemeler.OptionsView.RowAutoHeight = true;
            this.grdVOdemeler.OptionsView.ShowAutoFilterRow = true;
            this.grdVOdemeler.OptionsView.ShowGroupPanel = false;
            // 
            // cOde_Id
            // 
            this.cOde_Id.Caption = "Id";
            this.cOde_Id.FieldName = "Ode_Id";
            this.cOde_Id.Name = "cOde_Id";
            // 
            // cOde_Musteri_Kodu
            // 
            this.cOde_Musteri_Kodu.Caption = "Must.Kodu";
            this.cOde_Musteri_Kodu.FieldName = "Ode_Musteri_Kodu";
            this.cOde_Musteri_Kodu.Name = "cOde_Musteri_Kodu";
            this.cOde_Musteri_Kodu.OptionsColumn.AllowEdit = false;
            this.cOde_Musteri_Kodu.Width = 30;
            // 
            // cOde_SozlesmeNo
            // 
            this.cOde_SozlesmeNo.Caption = "Sz.No";
            this.cOde_SozlesmeNo.ColumnEdit = this.repChkOdemeSozNo;
            this.cOde_SozlesmeNo.FieldName = "Ode_SozlesmeNo";
            this.cOde_SozlesmeNo.Name = "cOde_SozlesmeNo";
            this.cOde_SozlesmeNo.ToolTip = "Sözleşme no";
            this.cOde_SozlesmeNo.Visible = true;
            this.cOde_SozlesmeNo.VisibleIndex = 0;
            this.cOde_SozlesmeNo.Width = 53;
            // 
            // repChkOdemeSozNo
            // 
            this.repChkOdemeSozNo.AutoHeight = false;
            this.repChkOdemeSozNo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkOdemeSozNo.DisplayMember = "Tanim";
            this.repChkOdemeSozNo.Name = "repChkOdemeSozNo";
            this.repChkOdemeSozNo.ValueMember = "Tanim";
            // 
            // cOde_Fatura
            // 
            this.cOde_Fatura.Caption = "Fatura Sekl.";
            this.cOde_Fatura.ColumnEdit = this.repLFaturaTipi;
            this.cOde_Fatura.FieldName = "Ode_Fatura";
            this.cOde_Fatura.Name = "cOde_Fatura";
            this.cOde_Fatura.ToolTip = "Fatura şekli";
            this.cOde_Fatura.Visible = true;
            this.cOde_Fatura.VisibleIndex = 1;
            // 
            // repLFaturaTipi
            // 
            this.repLFaturaTipi.AutoHeight = false;
            this.repLFaturaTipi.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLFaturaTipi.DisplayMember = "Tanim";
            this.repLFaturaTipi.Name = "repLFaturaTipi";
            this.repLFaturaTipi.NullText = "";
            this.repLFaturaTipi.ValueMember = "Tanim";
            // 
            // cOde_Fatura_Fiyat
            // 
            this.cOde_Fatura_Fiyat.Caption = "Fat.Tut.";
            this.cOde_Fatura_Fiyat.DisplayFormat.FormatString = "n2";
            this.cOde_Fatura_Fiyat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cOde_Fatura_Fiyat.FieldName = "Ode_Fatura_Fiyat";
            this.cOde_Fatura_Fiyat.Name = "cOde_Fatura_Fiyat";
            this.cOde_Fatura_Fiyat.ToolTip = "Fatura tutarı";
            this.cOde_Fatura_Fiyat.Visible = true;
            this.cOde_Fatura_Fiyat.VisibleIndex = 2;
            this.cOde_Fatura_Fiyat.Width = 80;
            // 
            // cOde_Fatura_Fiyat_Br
            // 
            this.cOde_Fatura_Fiyat_Br.Caption = "Fat.Br.";
            this.cOde_Fatura_Fiyat_Br.ColumnEdit = this.repFatBr;
            this.cOde_Fatura_Fiyat_Br.FieldName = "Ode_Fatura_Fiyat_Br";
            this.cOde_Fatura_Fiyat_Br.Name = "cOde_Fatura_Fiyat_Br";
            this.cOde_Fatura_Fiyat_Br.ToolTip = "Fatura birimi";
            this.cOde_Fatura_Fiyat_Br.Visible = true;
            this.cOde_Fatura_Fiyat_Br.VisibleIndex = 3;
            // 
            // repFatBr
            // 
            this.repFatBr.AutoHeight = false;
            this.repFatBr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repFatBr.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repFatBr.Name = "repFatBr";
            this.repFatBr.NullText = "TL";
            this.repFatBr.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cOde_OdemeSekli
            // 
            this.cOde_OdemeSekli.Caption = "Odm.Sekl.";
            this.cOde_OdemeSekli.ColumnEdit = this.repLOdmSekli;
            this.cOde_OdemeSekli.FieldName = "Ode_OdemeSekli";
            this.cOde_OdemeSekli.Name = "cOde_OdemeSekli";
            this.cOde_OdemeSekli.ToolTip = "Ödeme şekli";
            this.cOde_OdemeSekli.Visible = true;
            this.cOde_OdemeSekli.VisibleIndex = 4;
            // 
            // repLOdmSekli
            // 
            this.repLOdmSekli.AutoHeight = false;
            this.repLOdmSekli.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLOdmSekli.DisplayMember = "Tanim";
            this.repLOdmSekli.Name = "repLOdmSekli";
            this.repLOdmSekli.NullText = "";
            this.repLOdmSekli.ValueMember = "Tanim";
            // 
            // cOde_OdemeSekli_Fiyat
            // 
            this.cOde_OdemeSekli_Fiyat.Caption = "Odm.Tut.";
            this.cOde_OdemeSekli_Fiyat.DisplayFormat.FormatString = "n2";
            this.cOde_OdemeSekli_Fiyat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cOde_OdemeSekli_Fiyat.FieldName = "Ode_OdemeSekli_Fiyat";
            this.cOde_OdemeSekli_Fiyat.Name = "cOde_OdemeSekli_Fiyat";
            this.cOde_OdemeSekli_Fiyat.ToolTip = "Ödeme tutarı";
            // 
            // cOde_OdemeSekli_Br
            // 
            this.cOde_OdemeSekli_Br.Caption = "Odm.Br.";
            this.cOde_OdemeSekli_Br.ColumnEdit = this.repFatBr;
            this.cOde_OdemeSekli_Br.FieldName = "Ode_OdemeSekli_Br";
            this.cOde_OdemeSekli_Br.Name = "cOde_OdemeSekli_Br";
            this.cOde_OdemeSekli_Br.ToolTip = "Odeme birimi";
            this.cOde_OdemeSekli_Br.Width = 55;
            // 
            // cOde_Ulasim_Giderleri
            // 
            this.cOde_Ulasim_Giderleri.Caption = "Ulş.Gidr.";
            this.cOde_Ulasim_Giderleri.ColumnEdit = this.repLUlsSekli;
            this.cOde_Ulasim_Giderleri.FieldName = "Ode_Ulasim_Giderleri";
            this.cOde_Ulasim_Giderleri.Name = "cOde_Ulasim_Giderleri";
            this.cOde_Ulasim_Giderleri.ToolTip = "Ulaşım giderleri";
            this.cOde_Ulasim_Giderleri.Visible = true;
            this.cOde_Ulasim_Giderleri.VisibleIndex = 5;
            // 
            // repLUlsSekli
            // 
            this.repLUlsSekli.AutoHeight = false;
            this.repLUlsSekli.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLUlsSekli.DisplayMember = "Tanim";
            this.repLUlsSekli.Name = "repLUlsSekli";
            this.repLUlsSekli.NullText = "";
            this.repLUlsSekli.ValueMember = "Tanim";
            // 
            // cOde_Ulasim_Giderleri_Fiyat
            // 
            this.cOde_Ulasim_Giderleri_Fiyat.Caption = "Ulş.Tut.";
            this.cOde_Ulasim_Giderleri_Fiyat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cOde_Ulasim_Giderleri_Fiyat.FieldName = "Ode_Ulasim_Giderleri_Fiyat";
            this.cOde_Ulasim_Giderleri_Fiyat.Name = "cOde_Ulasim_Giderleri_Fiyat";
            this.cOde_Ulasim_Giderleri_Fiyat.ToolTip = "Ulaşım tutarı";
            // 
            // cOde_Ulasim_Giderleri_Br
            // 
            this.cOde_Ulasim_Giderleri_Br.Caption = "Ulş.Br.";
            this.cOde_Ulasim_Giderleri_Br.ColumnEdit = this.repFatBr;
            this.cOde_Ulasim_Giderleri_Br.FieldName = "Ode_Ulasim_Giderleri_Br";
            this.cOde_Ulasim_Giderleri_Br.Name = "cOde_Ulasim_Giderleri_Br";
            this.cOde_Ulasim_Giderleri_Br.ToolTip = "Ulaşım birimi";
            this.cOde_Ulasim_Giderleri_Br.Width = 45;
            // 
            // cOde_Konaklama_Giderleri
            // 
            this.cOde_Konaklama_Giderleri.Caption = "Knk.Gidr.";
            this.cOde_Konaklama_Giderleri.ColumnEdit = this.repLKnkSekli;
            this.cOde_Konaklama_Giderleri.FieldName = "Ode_Konaklama_Giderleri";
            this.cOde_Konaklama_Giderleri.Name = "cOde_Konaklama_Giderleri";
            this.cOde_Konaklama_Giderleri.ToolTip = "Konaklama giderleri";
            this.cOde_Konaklama_Giderleri.Visible = true;
            this.cOde_Konaklama_Giderleri.VisibleIndex = 6;
            this.cOde_Konaklama_Giderleri.Width = 90;
            // 
            // repLKnkSekli
            // 
            this.repLKnkSekli.AutoHeight = false;
            this.repLKnkSekli.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLKnkSekli.DisplayMember = "Tanim";
            this.repLKnkSekli.Name = "repLKnkSekli";
            this.repLKnkSekli.NullText = "";
            this.repLKnkSekli.ValueMember = "Tanim";
            // 
            // cOde_Konaklama_Giderleri_Fiyat
            // 
            this.cOde_Konaklama_Giderleri_Fiyat.Caption = "Knk.Tut.";
            this.cOde_Konaklama_Giderleri_Fiyat.DisplayFormat.FormatString = "n2";
            this.cOde_Konaklama_Giderleri_Fiyat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cOde_Konaklama_Giderleri_Fiyat.FieldName = "Ode_Konaklama_Giderleri_Fiyat";
            this.cOde_Konaklama_Giderleri_Fiyat.Name = "cOde_Konaklama_Giderleri_Fiyat";
            this.cOde_Konaklama_Giderleri_Fiyat.ToolTip = "Konaklama tutarı";
            this.cOde_Konaklama_Giderleri_Fiyat.Width = 85;
            // 
            // cOde_Konaklama_Giderleri_Br
            // 
            this.cOde_Konaklama_Giderleri_Br.Caption = "Knk.Br.";
            this.cOde_Konaklama_Giderleri_Br.ColumnEdit = this.repFatBr;
            this.cOde_Konaklama_Giderleri_Br.FieldName = "Ode_Konaklama_Giderleri_Br";
            this.cOde_Konaklama_Giderleri_Br.Name = "cOde_Konaklama_Giderleri_Br";
            this.cOde_Konaklama_Giderleri_Br.ToolTip = "Konaklama birimi";
            this.cOde_Konaklama_Giderleri_Br.Width = 47;
            // 
            // cOde_Yemek_Gideleri
            // 
            this.cOde_Yemek_Gideleri.Caption = "Ymk.Gidr.";
            this.cOde_Yemek_Gideleri.ColumnEdit = this.repYmkSekl;
            this.cOde_Yemek_Gideleri.FieldName = "Ode_Yemek_Gideleri";
            this.cOde_Yemek_Gideleri.Name = "cOde_Yemek_Gideleri";
            this.cOde_Yemek_Gideleri.ToolTip = "Yeme-İçme Giderleri";
            this.cOde_Yemek_Gideleri.Visible = true;
            this.cOde_Yemek_Gideleri.VisibleIndex = 7;
            // 
            // repYmkSekl
            // 
            this.repYmkSekl.AutoHeight = false;
            this.repYmkSekl.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repYmkSekl.DisplayMember = "Tanim";
            this.repYmkSekl.Name = "repYmkSekl";
            this.repYmkSekl.NullText = "";
            this.repYmkSekl.ValueMember = "Tanim";
            // 
            // cOde_Yemek_Giderleri_Fiyat
            // 
            this.cOde_Yemek_Giderleri_Fiyat.Caption = "Ymk.Tut.";
            this.cOde_Yemek_Giderleri_Fiyat.DisplayFormat.FormatString = "n2";
            this.cOde_Yemek_Giderleri_Fiyat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cOde_Yemek_Giderleri_Fiyat.FieldName = "Ode_Yemek_Giderleri_Fiyat";
            this.cOde_Yemek_Giderleri_Fiyat.Name = "cOde_Yemek_Giderleri_Fiyat";
            this.cOde_Yemek_Giderleri_Fiyat.ToolTip = "Yemek giderleri tutarı";
            // 
            // cOde_Yemek_Giderleri_Br
            // 
            this.cOde_Yemek_Giderleri_Br.Caption = "Ymk.Br.";
            this.cOde_Yemek_Giderleri_Br.ColumnEdit = this.repFatBr;
            this.cOde_Yemek_Giderleri_Br.FieldName = "Ode_Yemek_Giderleri_Br";
            this.cOde_Yemek_Giderleri_Br.Name = "cOde_Yemek_Giderleri_Br";
            this.cOde_Yemek_Giderleri_Br.ToolTip = "Yemek tutarı birimi";
            this.cOde_Yemek_Giderleri_Br.Width = 53;
            // 
            // cOde_Notlar
            // 
            this.cOde_Notlar.Caption = "Not";
            this.cOde_Notlar.ColumnEdit = this.repNot;
            this.cOde_Notlar.FieldName = "Ode_Notlar";
            this.cOde_Notlar.Name = "cOde_Notlar";
            this.cOde_Notlar.Visible = true;
            this.cOde_Notlar.VisibleIndex = 8;
            this.cOde_Notlar.Width = 171;
            // 
            // repNot
            // 
            this.repNot.Name = "repNot";
            // 
            // repositoryItemComboBox18
            // 
            this.repositoryItemComboBox18.AutoHeight = false;
            this.repositoryItemComboBox18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox18.Name = "repositoryItemComboBox18";
            // 
            // repositoryItemComboBox19
            // 
            this.repositoryItemComboBox19.AutoHeight = false;
            this.repositoryItemComboBox19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox19.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox19.Name = "repositoryItemComboBox19";
            this.repositoryItemComboBox19.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox20
            // 
            this.repositoryItemComboBox20.AutoHeight = false;
            this.repositoryItemComboBox20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox20.Name = "repositoryItemComboBox20";
            // 
            // repositoryItemLookUpEdit24
            // 
            this.repositoryItemLookUpEdit24.AutoHeight = false;
            this.repositoryItemLookUpEdit24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit24.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit24.Name = "repositoryItemLookUpEdit24";
            this.repositoryItemLookUpEdit24.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit25
            // 
            this.repositoryItemLookUpEdit25.AutoHeight = false;
            this.repositoryItemLookUpEdit25.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit25.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit25.Name = "repositoryItemLookUpEdit25";
            this.repositoryItemLookUpEdit25.NullText = "";
            this.repositoryItemLookUpEdit25.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit11
            // 
            this.repositoryItemPictureEdit11.Name = "repositoryItemPictureEdit11";
            // 
            // repositoryItemImageEdit6
            // 
            this.repositoryItemImageEdit6.AutoHeight = false;
            this.repositoryItemImageEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit6.Name = "repositoryItemImageEdit6";
            // 
            // repositoryItemPictureEdit12
            // 
            this.repositoryItemPictureEdit12.Name = "repositoryItemPictureEdit12";
            // 
            // repositoryItemLookUpEdit26
            // 
            this.repositoryItemLookUpEdit26.AutoHeight = false;
            this.repositoryItemLookUpEdit26.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit26.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit26.Name = "repositoryItemLookUpEdit26";
            this.repositoryItemLookUpEdit26.NullText = "";
            this.repositoryItemLookUpEdit26.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit27
            // 
            this.repositoryItemLookUpEdit27.AutoHeight = false;
            this.repositoryItemLookUpEdit27.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit27.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit27.Name = "repositoryItemLookUpEdit27";
            this.repositoryItemLookUpEdit27.NullText = "";
            this.repositoryItemLookUpEdit27.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit28
            // 
            this.repositoryItemLookUpEdit28.AutoHeight = false;
            this.repositoryItemLookUpEdit28.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit28.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit28.Name = "repositoryItemLookUpEdit28";
            this.repositoryItemLookUpEdit28.NullText = "";
            this.repositoryItemLookUpEdit28.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit29
            // 
            this.repositoryItemLookUpEdit29.AutoHeight = false;
            this.repositoryItemLookUpEdit29.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit29.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit29.DropDownRows = 20;
            this.repositoryItemLookUpEdit29.Name = "repositoryItemLookUpEdit29";
            this.repositoryItemLookUpEdit29.NullText = "";
            this.repositoryItemLookUpEdit29.PopupWidth = 600;
            this.repositoryItemLookUpEdit29.ValueMember = "Kod";
            // 
            // repositoryItemCheckedComboBoxEdit8
            // 
            this.repositoryItemCheckedComboBoxEdit8.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit8.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit8.Name = "repositoryItemCheckedComboBoxEdit8";
            this.repositoryItemCheckedComboBoxEdit8.ValueMember = "Tanim";
            // 
            // repositoryItemMemoEdit4
            // 
            this.repositoryItemMemoEdit4.Name = "repositoryItemMemoEdit4";
            // 
            // repositoryItemDateEdit12
            // 
            this.repositoryItemDateEdit12.AutoHeight = false;
            this.repositoryItemDateEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit12.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit12.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit12.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit12.Name = "repositoryItemDateEdit12";
            // 
            // repositoryItemCheckedComboBoxEdit9
            // 
            this.repositoryItemCheckedComboBoxEdit9.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit9.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit9.Name = "repositoryItemCheckedComboBoxEdit9";
            this.repositoryItemCheckedComboBoxEdit9.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit10
            // 
            this.repositoryItemCheckedComboBoxEdit10.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit10.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit10.Name = "repositoryItemCheckedComboBoxEdit10";
            this.repositoryItemCheckedComboBoxEdit10.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit11
            // 
            this.repositoryItemCheckedComboBoxEdit11.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit11.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit11.Name = "repositoryItemCheckedComboBoxEdit11";
            this.repositoryItemCheckedComboBoxEdit11.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit12
            // 
            this.repositoryItemCheckedComboBoxEdit12.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit12.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit12.Name = "repositoryItemCheckedComboBoxEdit12";
            this.repositoryItemCheckedComboBoxEdit12.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit13
            // 
            this.repositoryItemDateEdit13.AutoHeight = false;
            this.repositoryItemDateEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit13.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit13.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit13.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit13.Name = "repositoryItemDateEdit13";
            // 
            // repositoryItemLookUpEdit30
            // 
            this.repositoryItemLookUpEdit30.AutoHeight = false;
            this.repositoryItemLookUpEdit30.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit30.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit30.Name = "repositoryItemLookUpEdit30";
            this.repositoryItemLookUpEdit30.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit9
            // 
            this.repositoryItemDateEdit9.AutoHeight = false;
            this.repositoryItemDateEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit9.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit9.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit9.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit9.Name = "repositoryItemDateEdit9";
            // 
            // repositoryItemMemoEdit6
            // 
            this.repositoryItemMemoEdit6.Name = "repositoryItemMemoEdit6";
            // 
            // repositoryItemComboBox21
            // 
            this.repositoryItemComboBox21.AutoHeight = false;
            this.repositoryItemComboBox21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox21.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox21.Name = "repositoryItemComboBox21";
            this.repositoryItemComboBox21.NullText = "TL";
            this.repositoryItemComboBox21.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox17
            // 
            this.repositoryItemComboBox17.AutoHeight = false;
            this.repositoryItemComboBox17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox17.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox17.Name = "repositoryItemComboBox17";
            this.repositoryItemComboBox17.NullText = "TL";
            this.repositoryItemComboBox17.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemCheckedComboBoxEdit7
            // 
            this.repositoryItemCheckedComboBoxEdit7.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit7.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit7.Name = "repositoryItemCheckedComboBoxEdit7";
            this.repositoryItemCheckedComboBoxEdit7.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit10
            // 
            this.repositoryItemDateEdit10.AutoHeight = false;
            this.repositoryItemDateEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit10.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit10.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit10.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit10.Name = "repositoryItemDateEdit10";
            // 
            // repositoryItemDateEdit11
            // 
            this.repositoryItemDateEdit11.AutoHeight = false;
            this.repositoryItemDateEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit11.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit11.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit11.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit11.Name = "repositoryItemDateEdit11";
            // 
            // repositoryItemCheckedComboBoxEdit6
            // 
            this.repositoryItemCheckedComboBoxEdit6.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit6.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit6.Name = "repositoryItemCheckedComboBoxEdit6";
            this.repositoryItemCheckedComboBoxEdit6.ValueMember = "Tanim";
            // 
            // repositoryItemComboBox22
            // 
            this.repositoryItemComboBox22.AutoHeight = false;
            this.repositoryItemComboBox22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox22.Items.AddRange(new object[] {
            "PROFORMA",
            "RESMİ",
            "VS."});
            this.repositoryItemComboBox22.Name = "repositoryItemComboBox22";
            // 
            // layoutView9
            // 
            this.layoutView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn74,
            this.layoutViewColumn75,
            this.layoutViewColumn76,
            this.layoutViewColumn77,
            this.layoutViewColumn78,
            this.layoutViewColumn79,
            this.layoutViewColumn80,
            this.layoutViewColumn81,
            this.layoutViewColumn82});
            this.layoutView9.GridControl = this.grdOdemeler;
            this.layoutView9.Name = "layoutView9";
            this.layoutView9.OptionsSelection.MultiSelect = true;
            this.layoutView9.TemplateCard = null;
            // 
            // layoutViewColumn74
            // 
            this.layoutViewColumn74.Caption = "ID";
            this.layoutViewColumn74.FieldName = "ID";
            this.layoutViewColumn74.LayoutViewField = this.layoutViewField55;
            this.layoutViewColumn74.Name = "layoutViewColumn74";
            // 
            // layoutViewField55
            // 
            this.layoutViewField55.EditorPreferredWidth = 129;
            this.layoutViewField55.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField55.Name = "layoutViewField55";
            this.layoutViewField55.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField55.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField55.TextToControlDistance = 5;
            // 
            // layoutViewColumn75
            // 
            this.layoutViewColumn75.Caption = "Müşteri Kodu";
            this.layoutViewColumn75.FieldName = "MusteriKodu";
            this.layoutViewColumn75.LayoutViewField = this.layoutViewField56;
            this.layoutViewColumn75.Name = "layoutViewColumn75";
            this.layoutViewColumn75.Width = 84;
            // 
            // layoutViewField56
            // 
            this.layoutViewField56.EditorPreferredWidth = 129;
            this.layoutViewField56.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField56.Name = "layoutViewField56";
            this.layoutViewField56.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField56.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField56.TextToControlDistance = 5;
            // 
            // layoutViewColumn76
            // 
            this.layoutViewColumn76.Caption = "Adı";
            this.layoutViewColumn76.FieldName = "Adi";
            this.layoutViewColumn76.LayoutViewField = this.layoutViewField57;
            this.layoutViewColumn76.Name = "layoutViewColumn76";
            this.layoutViewColumn76.Width = 118;
            // 
            // layoutViewField57
            // 
            this.layoutViewField57.EditorPreferredWidth = 129;
            this.layoutViewField57.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField57.Name = "layoutViewField57";
            this.layoutViewField57.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField57.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField57.TextToControlDistance = 5;
            // 
            // layoutViewColumn77
            // 
            this.layoutViewColumn77.Caption = "Sektorü";
            this.layoutViewColumn77.FieldName = "Sektoru";
            this.layoutViewColumn77.LayoutViewField = this.layoutViewField58;
            this.layoutViewColumn77.Name = "layoutViewColumn77";
            this.layoutViewColumn77.Width = 133;
            // 
            // layoutViewField58
            // 
            this.layoutViewField58.EditorPreferredWidth = 129;
            this.layoutViewField58.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField58.Name = "layoutViewField58";
            this.layoutViewField58.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField58.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField58.TextToControlDistance = 5;
            // 
            // layoutViewColumn78
            // 
            this.layoutViewColumn78.Caption = "Ilçe";
            this.layoutViewColumn78.ColumnEdit = this.repositoryItemLookUpEdit28;
            this.layoutViewColumn78.FieldName = "Ilce";
            this.layoutViewColumn78.LayoutViewField = this.layoutViewField59;
            this.layoutViewColumn78.Name = "layoutViewColumn78";
            this.layoutViewColumn78.Width = 101;
            // 
            // layoutViewField59
            // 
            this.layoutViewField59.EditorPreferredWidth = 129;
            this.layoutViewField59.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField59.Name = "layoutViewField59";
            this.layoutViewField59.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField59.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField59.TextToControlDistance = 5;
            // 
            // layoutViewColumn79
            // 
            this.layoutViewColumn79.Caption = "Şehir";
            this.layoutViewColumn79.ColumnEdit = this.repositoryItemLookUpEdit27;
            this.layoutViewColumn79.FieldName = "SehirKodu";
            this.layoutViewColumn79.LayoutViewField = this.layoutViewField60;
            this.layoutViewColumn79.Name = "layoutViewColumn79";
            this.layoutViewColumn79.Width = 96;
            // 
            // layoutViewField60
            // 
            this.layoutViewField60.EditorPreferredWidth = 129;
            this.layoutViewField60.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField60.Name = "layoutViewField60";
            this.layoutViewField60.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField60.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField60.TextToControlDistance = 5;
            // 
            // layoutViewColumn80
            // 
            this.layoutViewColumn80.Caption = "Ülke";
            this.layoutViewColumn80.ColumnEdit = this.repositoryItemLookUpEdit26;
            this.layoutViewColumn80.FieldName = "UlkeKodu";
            this.layoutViewColumn80.LayoutViewField = this.layoutViewField61;
            this.layoutViewColumn80.Name = "layoutViewColumn80";
            this.layoutViewColumn80.Width = 104;
            // 
            // layoutViewField61
            // 
            this.layoutViewField61.EditorPreferredWidth = 129;
            this.layoutViewField61.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField61.Name = "layoutViewField61";
            this.layoutViewField61.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField61.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField61.TextToControlDistance = 5;
            // 
            // layoutViewColumn81
            // 
            this.layoutViewColumn81.Caption = "Adres";
            this.layoutViewColumn81.FieldName = "Adres";
            this.layoutViewColumn81.LayoutViewField = this.layoutViewField62;
            this.layoutViewColumn81.Name = "layoutViewColumn81";
            this.layoutViewColumn81.Width = 165;
            // 
            // layoutViewField62
            // 
            this.layoutViewField62.EditorPreferredWidth = 129;
            this.layoutViewField62.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField62.Name = "layoutViewField62";
            this.layoutViewField62.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField62.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField62.TextToControlDistance = 5;
            // 
            // layoutViewColumn82
            // 
            this.layoutViewColumn82.Caption = "Vergi Dairesi";
            this.layoutViewColumn82.FieldName = "VergiDairesi";
            this.layoutViewColumn82.LayoutViewField = this.layoutViewField63;
            this.layoutViewColumn82.Name = "layoutViewColumn82";
            this.layoutViewColumn82.Width = 87;
            // 
            // layoutViewField63
            // 
            this.layoutViewField63.EditorPreferredWidth = 129;
            this.layoutViewField63.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField63.Name = "layoutViewField63";
            this.layoutViewField63.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField63.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField63.TextToControlDistance = 5;
            // 
            // layoutView10
            // 
            this.layoutView10.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn83,
            this.layoutViewColumn84,
            this.layoutViewColumn85,
            this.layoutViewColumn86,
            this.layoutViewColumn87,
            this.layoutViewColumn88,
            this.layoutViewColumn89,
            this.layoutViewColumn90,
            this.layoutViewColumn91});
            this.layoutView10.GridControl = this.grdOdemeler;
            this.layoutView10.Name = "layoutView10";
            this.layoutView10.OptionsSelection.MultiSelect = true;
            this.layoutView10.OptionsView.ShowCardCaption = false;
            this.layoutView10.TemplateCard = null;
            // 
            // layoutViewColumn83
            // 
            this.layoutViewColumn83.Caption = "ID";
            this.layoutViewColumn83.FieldName = "ID";
            this.layoutViewColumn83.LayoutViewField = this.layoutViewField64;
            this.layoutViewColumn83.Name = "layoutViewColumn83";
            // 
            // layoutViewField64
            // 
            this.layoutViewField64.EditorPreferredWidth = 253;
            this.layoutViewField64.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField64.Name = "layoutViewField64";
            this.layoutViewField64.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField64.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField64.TextToControlDistance = 5;
            // 
            // layoutViewColumn84
            // 
            this.layoutViewColumn84.Caption = "Müşteri Kodu";
            this.layoutViewColumn84.FieldName = "MusteriKodu";
            this.layoutViewColumn84.LayoutViewField = this.layoutViewField65;
            this.layoutViewColumn84.Name = "layoutViewColumn84";
            this.layoutViewColumn84.Width = 84;
            // 
            // layoutViewField65
            // 
            this.layoutViewField65.EditorPreferredWidth = 253;
            this.layoutViewField65.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField65.Name = "layoutViewField65";
            this.layoutViewField65.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField65.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField65.TextToControlDistance = 5;
            // 
            // layoutViewColumn85
            // 
            this.layoutViewColumn85.Caption = "Adı";
            this.layoutViewColumn85.FieldName = "Adi";
            this.layoutViewColumn85.LayoutViewField = this.layoutViewField66;
            this.layoutViewColumn85.Name = "layoutViewColumn85";
            this.layoutViewColumn85.Width = 118;
            // 
            // layoutViewField66
            // 
            this.layoutViewField66.EditorPreferredWidth = 253;
            this.layoutViewField66.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField66.Name = "layoutViewField66";
            this.layoutViewField66.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField66.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField66.TextToControlDistance = 5;
            // 
            // layoutViewColumn86
            // 
            this.layoutViewColumn86.Caption = "Sektorü";
            this.layoutViewColumn86.FieldName = "Sektoru";
            this.layoutViewColumn86.LayoutViewField = this.layoutViewField67;
            this.layoutViewColumn86.Name = "layoutViewColumn86";
            this.layoutViewColumn86.Width = 133;
            // 
            // layoutViewField67
            // 
            this.layoutViewField67.EditorPreferredWidth = 253;
            this.layoutViewField67.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField67.Name = "layoutViewField67";
            this.layoutViewField67.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField67.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField67.TextToControlDistance = 5;
            // 
            // layoutViewColumn87
            // 
            this.layoutViewColumn87.Caption = "Ilçe";
            this.layoutViewColumn87.ColumnEdit = this.repositoryItemLookUpEdit28;
            this.layoutViewColumn87.FieldName = "Ilce";
            this.layoutViewColumn87.LayoutViewField = this.layoutViewField68;
            this.layoutViewColumn87.Name = "layoutViewColumn87";
            this.layoutViewColumn87.Width = 101;
            // 
            // layoutViewField68
            // 
            this.layoutViewField68.EditorPreferredWidth = 253;
            this.layoutViewField68.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField68.Name = "layoutViewField68";
            this.layoutViewField68.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField68.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField68.TextToControlDistance = 5;
            // 
            // layoutViewColumn88
            // 
            this.layoutViewColumn88.Caption = "Şehir";
            this.layoutViewColumn88.ColumnEdit = this.repositoryItemLookUpEdit27;
            this.layoutViewColumn88.FieldName = "SehirKodu";
            this.layoutViewColumn88.LayoutViewField = this.layoutViewField69;
            this.layoutViewColumn88.Name = "layoutViewColumn88";
            this.layoutViewColumn88.Width = 96;
            // 
            // layoutViewField69
            // 
            this.layoutViewField69.EditorPreferredWidth = 253;
            this.layoutViewField69.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField69.Name = "layoutViewField69";
            this.layoutViewField69.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField69.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField69.TextToControlDistance = 5;
            // 
            // layoutViewColumn89
            // 
            this.layoutViewColumn89.Caption = "Ülke";
            this.layoutViewColumn89.ColumnEdit = this.repositoryItemLookUpEdit26;
            this.layoutViewColumn89.FieldName = "UlkeKodu";
            this.layoutViewColumn89.LayoutViewField = this.layoutViewField70;
            this.layoutViewColumn89.Name = "layoutViewColumn89";
            this.layoutViewColumn89.Width = 104;
            // 
            // layoutViewField70
            // 
            this.layoutViewField70.EditorPreferredWidth = 253;
            this.layoutViewField70.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField70.Name = "layoutViewField70";
            this.layoutViewField70.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField70.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField70.TextToControlDistance = 5;
            // 
            // layoutViewColumn90
            // 
            this.layoutViewColumn90.Caption = "Adres";
            this.layoutViewColumn90.FieldName = "Adres";
            this.layoutViewColumn90.LayoutViewField = this.layoutViewField71;
            this.layoutViewColumn90.Name = "layoutViewColumn90";
            this.layoutViewColumn90.Width = 165;
            // 
            // layoutViewField71
            // 
            this.layoutViewField71.EditorPreferredWidth = 253;
            this.layoutViewField71.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField71.Name = "layoutViewField71";
            this.layoutViewField71.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField71.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField71.TextToControlDistance = 5;
            // 
            // layoutViewColumn91
            // 
            this.layoutViewColumn91.Caption = "Vergi Dairesi";
            this.layoutViewColumn91.FieldName = "VergiDairesi";
            this.layoutViewColumn91.LayoutViewField = this.layoutViewField72;
            this.layoutViewColumn91.Name = "layoutViewColumn91";
            this.layoutViewColumn91.Width = 87;
            // 
            // layoutViewField72
            // 
            this.layoutViewField72.EditorPreferredWidth = 253;
            this.layoutViewField72.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField72.Name = "layoutViewField72";
            this.layoutViewField72.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField72.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField72.TextToControlDistance = 5;
            // 
            // tbProjeler
            // 
            this.tbProjeler.Controls.Add(this.grpProjeler);
            this.tbProjeler.Controls.Add(this.standaloneBarDockControl6);
            this.tbProjeler.Name = "tbProjeler";
            this.tbProjeler.Size = new System.Drawing.Size(1357, 408);
            this.tbProjeler.Text = "Projeler";
            // 
            // grpProjeler
            // 
            this.grpProjeler.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpProjeler.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpProjeler.AppearanceCaption.Options.UseFont = true;
            this.grpProjeler.AppearanceCaption.Options.UseForeColor = true;
            this.grpProjeler.Controls.Add(this.pnlProje);
            this.grpProjeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpProjeler.Location = new System.Drawing.Point(0, 29);
            this.grpProjeler.Name = "grpProjeler";
            this.grpProjeler.Size = new System.Drawing.Size(1357, 379);
            this.grpProjeler.TabIndex = 15;
            this.grpProjeler.Text = "...";
            // 
            // pnlProje
            // 
            this.pnlProje.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlProje.Controls.Add(this.tbMainProjeler);
            this.pnlProje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlProje.Location = new System.Drawing.Point(2, 31);
            this.pnlProje.Name = "pnlProje";
            this.pnlProje.Size = new System.Drawing.Size(1353, 346);
            this.pnlProje.TabIndex = 15;
            // 
            // tbMainProjeler
            // 
            this.tbMainProjeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMainProjeler.Location = new System.Drawing.Point(0, 0);
            this.tbMainProjeler.Name = "tbMainProjeler";
            this.tbMainProjeler.SelectedTabPage = this.xtraTabPage1;
            this.tbMainProjeler.Size = new System.Drawing.Size(1353, 346);
            this.tbMainProjeler.TabIndex = 16;
            this.tbMainProjeler.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            this.tbMainProjeler.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged_1);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.grdProjeler);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1348, 321);
            this.xtraTabPage1.Text = "Projeler";
            // 
            // grdProjeler
            // 
            this.grdProjeler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProjeler.Location = new System.Drawing.Point(0, 0);
            this.grdProjeler.MainView = this.grdVProjeler;
            this.grdProjeler.Name = "grdProjeler";
            this.grdProjeler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repChkSozNo,
            this.repTar,
            this.repProMem,
            this.repAnaGrupProje,
            this.repButtonEdit,
            this.zProjeDetay});
            this.grdProjeler.Size = new System.Drawing.Size(1348, 321);
            this.grdProjeler.TabIndex = 13;
            this.grdProjeler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVProjeler,
            this.layoutView11,
            this.layoutView12});
            // 
            // grdVProjeler
            // 
            this.grdVProjeler.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cPro_Id,
            this.cPro_Musteri_Kodu,
            this.cPro_Sozlesme_No,
            this.cPro_ProjeTanim,
            this.cPro_SunumTarihi,
            this.gridColumn11,
            this.cPro_Chk_Tarih,
            this.cPro_Chk_Sonucu,
            this.cPro_Analiz_Tarihi,
            this.cPro_Analiz_Sonucu,
            this.cPro_Analiz_Rapor_Tarihi,
            this.grdPro_ProjeTanim,
            this.cPro_ScaleWidth});
            this.grdVProjeler.GridControl = this.grdProjeler;
            this.grdVProjeler.Images = this.SmallImage;
            this.grdVProjeler.Name = "grdVProjeler";
            this.grdVProjeler.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVProjeler.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVProjeler.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVProjeler.OptionsSelection.MultiSelect = true;
            this.grdVProjeler.OptionsView.ColumnAutoWidth = false;
            this.grdVProjeler.OptionsView.RowAutoHeight = true;
            this.grdVProjeler.OptionsView.ShowAutoFilterRow = true;
            this.grdVProjeler.OptionsView.ShowGroupPanel = false;
            this.grdVProjeler.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdVProjeler_FocusedRowChanged);
            this.grdVProjeler.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdVProjeler_CellValueChanged);
            // 
            // cPro_Id
            // 
            this.cPro_Id.Caption = "No";
            this.cPro_Id.FieldName = "Pro_Id";
            this.cPro_Id.Name = "cPro_Id";
            this.cPro_Id.Visible = true;
            this.cPro_Id.VisibleIndex = 0;
            this.cPro_Id.Width = 46;
            // 
            // cPro_Musteri_Kodu
            // 
            this.cPro_Musteri_Kodu.Caption = "Must.Kodu";
            this.cPro_Musteri_Kodu.FieldName = "Pro_Musteri_Kodu";
            this.cPro_Musteri_Kodu.Name = "cPro_Musteri_Kodu";
            this.cPro_Musteri_Kodu.OptionsColumn.AllowEdit = false;
            this.cPro_Musteri_Kodu.Width = 20;
            // 
            // cPro_Sozlesme_No
            // 
            this.cPro_Sozlesme_No.Caption = "Sz.No";
            this.cPro_Sozlesme_No.ColumnEdit = this.repChkSozNo;
            this.cPro_Sozlesme_No.FieldName = "Pro_Sozlesme_No";
            this.cPro_Sozlesme_No.Name = "cPro_Sozlesme_No";
            this.cPro_Sozlesme_No.ToolTip = "Sözleşme numarası";
            this.cPro_Sozlesme_No.Visible = true;
            this.cPro_Sozlesme_No.VisibleIndex = 1;
            this.cPro_Sozlesme_No.Width = 57;
            // 
            // repChkSozNo
            // 
            this.repChkSozNo.AutoHeight = false;
            this.repChkSozNo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkSozNo.DisplayMember = "Tanim";
            this.repChkSozNo.Name = "repChkSozNo";
            this.repChkSozNo.ValueMember = "Tanim";
            // 
            // cPro_ProjeTanim
            // 
            this.cPro_ProjeTanim.Caption = "Ana Grup";
            this.cPro_ProjeTanim.ColumnEdit = this.repAnaGrupProje;
            this.cPro_ProjeTanim.FieldName = "Pro_AnaGrup";
            this.cPro_ProjeTanim.Name = "cPro_ProjeTanim";
            this.cPro_ProjeTanim.ToolTip = "Proje Adı";
            this.cPro_ProjeTanim.Visible = true;
            this.cPro_ProjeTanim.VisibleIndex = 2;
            this.cPro_ProjeTanim.Width = 128;
            // 
            // repAnaGrupProje
            // 
            this.repAnaGrupProje.AutoHeight = false;
            this.repAnaGrupProje.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAnaGrupProje.DisplayMember = "Tanim";
            this.repAnaGrupProje.Name = "repAnaGrupProje";
            this.repAnaGrupProje.NullText = "";
            this.repAnaGrupProje.ValueMember = "Kod";
            // 
            // cPro_SunumTarihi
            // 
            this.cPro_SunumTarihi.Caption = "Snm.Tar.";
            this.cPro_SunumTarihi.ColumnEdit = this.repTar;
            this.cPro_SunumTarihi.FieldName = "Pro_SunumTarihi";
            this.cPro_SunumTarihi.Name = "cPro_SunumTarihi";
            this.cPro_SunumTarihi.ToolTip = "Sunum tarihi";
            this.cPro_SunumTarihi.Width = 71;
            // 
            // repTar
            // 
            this.repTar.AutoHeight = false;
            this.repTar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTar.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repTar.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repTar.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repTar.Name = "repTar";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = " ";
            this.gridColumn11.ColumnEdit = this.zProjeDetay;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            this.gridColumn11.Width = 30;
            // 
            // zProjeDetay
            // 
            this.zProjeDetay.AutoHeight = false;
            this.zProjeDetay.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.zProjeDetay.Name = "zProjeDetay";
            this.zProjeDetay.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.zProjeDetay.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.zProjeDetay_ButtonClick);
            // 
            // cPro_Chk_Tarih
            // 
            this.cPro_Chk_Tarih.Caption = "Chk.Tar.";
            this.cPro_Chk_Tarih.ColumnEdit = this.repTar;
            this.cPro_Chk_Tarih.FieldName = "Pro_Chk_Tarih";
            this.cPro_Chk_Tarih.Name = "cPro_Chk_Tarih";
            this.cPro_Chk_Tarih.ToolTip = "Check tarihi";
            this.cPro_Chk_Tarih.Width = 73;
            // 
            // cPro_Chk_Sonucu
            // 
            this.cPro_Chk_Sonucu.Caption = "Chk.Snc.";
            this.cPro_Chk_Sonucu.ColumnEdit = this.repProMem;
            this.cPro_Chk_Sonucu.FieldName = "Pro_Chk_Sonucu";
            this.cPro_Chk_Sonucu.Name = "cPro_Chk_Sonucu";
            this.cPro_Chk_Sonucu.ToolTip = "Check sonucu";
            this.cPro_Chk_Sonucu.Width = 121;
            // 
            // repProMem
            // 
            this.repProMem.Name = "repProMem";
            // 
            // cPro_Analiz_Tarihi
            // 
            this.cPro_Analiz_Tarihi.Caption = "Anlz.Tar.";
            this.cPro_Analiz_Tarihi.ColumnEdit = this.repTar;
            this.cPro_Analiz_Tarihi.FieldName = "Pro_Analiz_Tarihi";
            this.cPro_Analiz_Tarihi.Name = "cPro_Analiz_Tarihi";
            this.cPro_Analiz_Tarihi.ToolTip = "Analiz tarihi";
            this.cPro_Analiz_Tarihi.Width = 64;
            // 
            // cPro_Analiz_Sonucu
            // 
            this.cPro_Analiz_Sonucu.Caption = "Anlz.Sonc.";
            this.cPro_Analiz_Sonucu.ColumnEdit = this.repProMem;
            this.cPro_Analiz_Sonucu.FieldName = "Pro_Analiz_Sonucu";
            this.cPro_Analiz_Sonucu.Name = "cPro_Analiz_Sonucu";
            this.cPro_Analiz_Sonucu.ToolTip = "Analiz sonucu";
            this.cPro_Analiz_Sonucu.Width = 128;
            // 
            // cPro_Analiz_Rapor_Tarihi
            // 
            this.cPro_Analiz_Rapor_Tarihi.Caption = "Rap.Tar.";
            this.cPro_Analiz_Rapor_Tarihi.ColumnEdit = this.repTar;
            this.cPro_Analiz_Rapor_Tarihi.FieldName = "Pro_Analiz_Rapor_Tarihi";
            this.cPro_Analiz_Rapor_Tarihi.Name = "cPro_Analiz_Rapor_Tarihi";
            this.cPro_Analiz_Rapor_Tarihi.ToolTip = "Rapor tarihi";
            // 
            // grdPro_ProjeTanim
            // 
            this.grdPro_ProjeTanim.Caption = "Proje Tanımı";
            this.grdPro_ProjeTanim.FieldName = "Pro_ProjeTanim";
            this.grdPro_ProjeTanim.Name = "grdPro_ProjeTanim";
            this.grdPro_ProjeTanim.Visible = true;
            this.grdPro_ProjeTanim.VisibleIndex = 3;
            this.grdPro_ProjeTanim.Width = 239;
            // 
            // cPro_ScaleWidth
            // 
            this.cPro_ScaleWidth.Caption = "Pro_ScaleWidth";
            this.cPro_ScaleWidth.FieldName = "Pro_ScaleWidth";
            this.cPro_ScaleWidth.Name = "cPro_ScaleWidth";
            // 
            // repButtonEdit
            // 
            this.repButtonEdit.AutoHeight = false;
            this.repButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repButtonEdit.Name = "repButtonEdit";
            this.repButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repButtonEdit_ButtonClick_1);
            // 
            // layoutView11
            // 
            this.layoutView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn92,
            this.layoutViewColumn93,
            this.layoutViewColumn94,
            this.layoutViewColumn95,
            this.layoutViewColumn96,
            this.layoutViewColumn97,
            this.layoutViewColumn98,
            this.layoutViewColumn99,
            this.layoutViewColumn100});
            this.layoutView11.GridControl = this.grdProjeler;
            this.layoutView11.Name = "layoutView11";
            this.layoutView11.OptionsSelection.MultiSelect = true;
            this.layoutView11.TemplateCard = null;
            // 
            // layoutViewColumn92
            // 
            this.layoutViewColumn92.Caption = "ID";
            this.layoutViewColumn92.FieldName = "ID";
            this.layoutViewColumn92.LayoutViewField = this.layoutViewField73;
            this.layoutViewColumn92.Name = "layoutViewColumn92";
            // 
            // layoutViewField73
            // 
            this.layoutViewField73.EditorPreferredWidth = 129;
            this.layoutViewField73.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField73.Name = "layoutViewField73";
            this.layoutViewField73.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField73.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField73.TextToControlDistance = 5;
            // 
            // layoutViewColumn93
            // 
            this.layoutViewColumn93.Caption = "Müşteri Kodu";
            this.layoutViewColumn93.FieldName = "MusteriKodu";
            this.layoutViewColumn93.LayoutViewField = this.layoutViewField74;
            this.layoutViewColumn93.Name = "layoutViewColumn93";
            this.layoutViewColumn93.Width = 84;
            // 
            // layoutViewField74
            // 
            this.layoutViewField74.EditorPreferredWidth = 129;
            this.layoutViewField74.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField74.Name = "layoutViewField74";
            this.layoutViewField74.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField74.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField74.TextToControlDistance = 5;
            // 
            // layoutViewColumn94
            // 
            this.layoutViewColumn94.Caption = "Adı";
            this.layoutViewColumn94.FieldName = "Adi";
            this.layoutViewColumn94.LayoutViewField = this.layoutViewField75;
            this.layoutViewColumn94.Name = "layoutViewColumn94";
            this.layoutViewColumn94.Width = 118;
            // 
            // layoutViewField75
            // 
            this.layoutViewField75.EditorPreferredWidth = 129;
            this.layoutViewField75.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField75.Name = "layoutViewField75";
            this.layoutViewField75.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField75.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField75.TextToControlDistance = 5;
            // 
            // layoutViewColumn95
            // 
            this.layoutViewColumn95.Caption = "Sektorü";
            this.layoutViewColumn95.FieldName = "Sektoru";
            this.layoutViewColumn95.LayoutViewField = this.layoutViewField76;
            this.layoutViewColumn95.Name = "layoutViewColumn95";
            this.layoutViewColumn95.Width = 133;
            // 
            // layoutViewField76
            // 
            this.layoutViewField76.EditorPreferredWidth = 129;
            this.layoutViewField76.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField76.Name = "layoutViewField76";
            this.layoutViewField76.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField76.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField76.TextToControlDistance = 5;
            // 
            // layoutViewColumn96
            // 
            this.layoutViewColumn96.Caption = "Ilçe";
            this.layoutViewColumn96.ColumnEdit = this.repositoryItemLookUpEdit31;
            this.layoutViewColumn96.FieldName = "Ilce";
            this.layoutViewColumn96.LayoutViewField = this.layoutViewField77;
            this.layoutViewColumn96.Name = "layoutViewColumn96";
            this.layoutViewColumn96.Width = 101;
            // 
            // repositoryItemLookUpEdit31
            // 
            this.repositoryItemLookUpEdit31.AutoHeight = false;
            this.repositoryItemLookUpEdit31.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit31.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit31.LookAndFeel.SkinName = "Metropolis";
            this.repositoryItemLookUpEdit31.Name = "repositoryItemLookUpEdit31";
            this.repositoryItemLookUpEdit31.NullText = "";
            this.repositoryItemLookUpEdit31.ValueMember = "Tanim";
            // 
            // layoutViewField77
            // 
            this.layoutViewField77.EditorPreferredWidth = 129;
            this.layoutViewField77.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField77.Name = "layoutViewField77";
            this.layoutViewField77.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField77.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField77.TextToControlDistance = 5;
            // 
            // layoutViewColumn97
            // 
            this.layoutViewColumn97.Caption = "Şehir";
            this.layoutViewColumn97.ColumnEdit = this.repositoryItemLookUpEdit32;
            this.layoutViewColumn97.FieldName = "SehirKodu";
            this.layoutViewColumn97.LayoutViewField = this.layoutViewField78;
            this.layoutViewColumn97.Name = "layoutViewColumn97";
            this.layoutViewColumn97.Width = 96;
            // 
            // repositoryItemLookUpEdit32
            // 
            this.repositoryItemLookUpEdit32.AutoHeight = false;
            this.repositoryItemLookUpEdit32.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit32.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit32.Name = "repositoryItemLookUpEdit32";
            this.repositoryItemLookUpEdit32.NullText = "";
            this.repositoryItemLookUpEdit32.ValueMember = "Kod";
            // 
            // layoutViewField78
            // 
            this.layoutViewField78.EditorPreferredWidth = 129;
            this.layoutViewField78.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField78.Name = "layoutViewField78";
            this.layoutViewField78.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField78.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField78.TextToControlDistance = 5;
            // 
            // layoutViewColumn98
            // 
            this.layoutViewColumn98.Caption = "Ülke";
            this.layoutViewColumn98.ColumnEdit = this.repositoryItemLookUpEdit33;
            this.layoutViewColumn98.FieldName = "UlkeKodu";
            this.layoutViewColumn98.LayoutViewField = this.layoutViewField79;
            this.layoutViewColumn98.Name = "layoutViewColumn98";
            this.layoutViewColumn98.Width = 104;
            // 
            // repositoryItemLookUpEdit33
            // 
            this.repositoryItemLookUpEdit33.AutoHeight = false;
            this.repositoryItemLookUpEdit33.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit33.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit33.Name = "repositoryItemLookUpEdit33";
            this.repositoryItemLookUpEdit33.NullText = "";
            this.repositoryItemLookUpEdit33.ValueMember = "Kod";
            // 
            // layoutViewField79
            // 
            this.layoutViewField79.EditorPreferredWidth = 129;
            this.layoutViewField79.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField79.Name = "layoutViewField79";
            this.layoutViewField79.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField79.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField79.TextToControlDistance = 5;
            // 
            // layoutViewColumn99
            // 
            this.layoutViewColumn99.Caption = "Adres";
            this.layoutViewColumn99.FieldName = "Adres";
            this.layoutViewColumn99.LayoutViewField = this.layoutViewField80;
            this.layoutViewColumn99.Name = "layoutViewColumn99";
            this.layoutViewColumn99.Width = 165;
            // 
            // layoutViewField80
            // 
            this.layoutViewField80.EditorPreferredWidth = 129;
            this.layoutViewField80.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField80.Name = "layoutViewField80";
            this.layoutViewField80.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField80.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField80.TextToControlDistance = 5;
            // 
            // layoutViewColumn100
            // 
            this.layoutViewColumn100.Caption = "Vergi Dairesi";
            this.layoutViewColumn100.FieldName = "VergiDairesi";
            this.layoutViewColumn100.LayoutViewField = this.layoutViewField81;
            this.layoutViewColumn100.Name = "layoutViewColumn100";
            this.layoutViewColumn100.Width = 87;
            // 
            // layoutViewField81
            // 
            this.layoutViewField81.EditorPreferredWidth = 129;
            this.layoutViewField81.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField81.Name = "layoutViewField81";
            this.layoutViewField81.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField81.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField81.TextToControlDistance = 5;
            // 
            // layoutView12
            // 
            this.layoutView12.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn101,
            this.layoutViewColumn102,
            this.layoutViewColumn103,
            this.layoutViewColumn104,
            this.layoutViewColumn105,
            this.layoutViewColumn106,
            this.layoutViewColumn107,
            this.layoutViewColumn108,
            this.layoutViewColumn109});
            this.layoutView12.GridControl = this.grdProjeler;
            this.layoutView12.Name = "layoutView12";
            this.layoutView12.OptionsSelection.MultiSelect = true;
            this.layoutView12.OptionsView.ShowCardCaption = false;
            this.layoutView12.TemplateCard = null;
            // 
            // layoutViewColumn101
            // 
            this.layoutViewColumn101.Caption = "ID";
            this.layoutViewColumn101.FieldName = "ID";
            this.layoutViewColumn101.LayoutViewField = this.layoutViewField82;
            this.layoutViewColumn101.Name = "layoutViewColumn101";
            // 
            // layoutViewField82
            // 
            this.layoutViewField82.EditorPreferredWidth = 253;
            this.layoutViewField82.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField82.Name = "layoutViewField82";
            this.layoutViewField82.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField82.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField82.TextToControlDistance = 5;
            // 
            // layoutViewColumn102
            // 
            this.layoutViewColumn102.Caption = "Müşteri Kodu";
            this.layoutViewColumn102.FieldName = "MusteriKodu";
            this.layoutViewColumn102.LayoutViewField = this.layoutViewField83;
            this.layoutViewColumn102.Name = "layoutViewColumn102";
            this.layoutViewColumn102.Width = 84;
            // 
            // layoutViewField83
            // 
            this.layoutViewField83.EditorPreferredWidth = 253;
            this.layoutViewField83.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField83.Name = "layoutViewField83";
            this.layoutViewField83.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField83.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField83.TextToControlDistance = 5;
            // 
            // layoutViewColumn103
            // 
            this.layoutViewColumn103.Caption = "Adı";
            this.layoutViewColumn103.FieldName = "Adi";
            this.layoutViewColumn103.LayoutViewField = this.layoutViewField84;
            this.layoutViewColumn103.Name = "layoutViewColumn103";
            this.layoutViewColumn103.Width = 118;
            // 
            // layoutViewField84
            // 
            this.layoutViewField84.EditorPreferredWidth = 253;
            this.layoutViewField84.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField84.Name = "layoutViewField84";
            this.layoutViewField84.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField84.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField84.TextToControlDistance = 5;
            // 
            // layoutViewColumn104
            // 
            this.layoutViewColumn104.Caption = "Sektorü";
            this.layoutViewColumn104.FieldName = "Sektoru";
            this.layoutViewColumn104.LayoutViewField = this.layoutViewField85;
            this.layoutViewColumn104.Name = "layoutViewColumn104";
            this.layoutViewColumn104.Width = 133;
            // 
            // layoutViewField85
            // 
            this.layoutViewField85.EditorPreferredWidth = 253;
            this.layoutViewField85.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField85.Name = "layoutViewField85";
            this.layoutViewField85.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField85.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField85.TextToControlDistance = 5;
            // 
            // layoutViewColumn105
            // 
            this.layoutViewColumn105.Caption = "Ilçe";
            this.layoutViewColumn105.ColumnEdit = this.repositoryItemLookUpEdit31;
            this.layoutViewColumn105.FieldName = "Ilce";
            this.layoutViewColumn105.LayoutViewField = this.layoutViewField86;
            this.layoutViewColumn105.Name = "layoutViewColumn105";
            this.layoutViewColumn105.Width = 101;
            // 
            // layoutViewField86
            // 
            this.layoutViewField86.EditorPreferredWidth = 253;
            this.layoutViewField86.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField86.Name = "layoutViewField86";
            this.layoutViewField86.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField86.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField86.TextToControlDistance = 5;
            // 
            // layoutViewColumn106
            // 
            this.layoutViewColumn106.Caption = "Şehir";
            this.layoutViewColumn106.ColumnEdit = this.repositoryItemLookUpEdit32;
            this.layoutViewColumn106.FieldName = "SehirKodu";
            this.layoutViewColumn106.LayoutViewField = this.layoutViewField87;
            this.layoutViewColumn106.Name = "layoutViewColumn106";
            this.layoutViewColumn106.Width = 96;
            // 
            // layoutViewField87
            // 
            this.layoutViewField87.EditorPreferredWidth = 253;
            this.layoutViewField87.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField87.Name = "layoutViewField87";
            this.layoutViewField87.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField87.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField87.TextToControlDistance = 5;
            // 
            // layoutViewColumn107
            // 
            this.layoutViewColumn107.Caption = "Ülke";
            this.layoutViewColumn107.ColumnEdit = this.repositoryItemLookUpEdit33;
            this.layoutViewColumn107.FieldName = "UlkeKodu";
            this.layoutViewColumn107.LayoutViewField = this.layoutViewField88;
            this.layoutViewColumn107.Name = "layoutViewColumn107";
            this.layoutViewColumn107.Width = 104;
            // 
            // layoutViewField88
            // 
            this.layoutViewField88.EditorPreferredWidth = 253;
            this.layoutViewField88.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField88.Name = "layoutViewField88";
            this.layoutViewField88.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField88.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField88.TextToControlDistance = 5;
            // 
            // layoutViewColumn108
            // 
            this.layoutViewColumn108.Caption = "Adres";
            this.layoutViewColumn108.FieldName = "Adres";
            this.layoutViewColumn108.LayoutViewField = this.layoutViewField89;
            this.layoutViewColumn108.Name = "layoutViewColumn108";
            this.layoutViewColumn108.Width = 165;
            // 
            // layoutViewField89
            // 
            this.layoutViewField89.EditorPreferredWidth = 253;
            this.layoutViewField89.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField89.Name = "layoutViewField89";
            this.layoutViewField89.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField89.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField89.TextToControlDistance = 5;
            // 
            // layoutViewColumn109
            // 
            this.layoutViewColumn109.Caption = "Vergi Dairesi";
            this.layoutViewColumn109.FieldName = "VergiDairesi";
            this.layoutViewColumn109.LayoutViewField = this.layoutViewField90;
            this.layoutViewColumn109.Name = "layoutViewColumn109";
            this.layoutViewColumn109.Width = 87;
            // 
            // layoutViewField90
            // 
            this.layoutViewField90.EditorPreferredWidth = 253;
            this.layoutViewField90.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField90.Name = "layoutViewField90";
            this.layoutViewField90.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField90.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField90.TextToControlDistance = 5;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.grdProjeDetay);
            this.xtraTabPage2.Controls.Add(this.standaloneBarDockControl8);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.PageVisible = false;
            this.xtraTabPage2.Size = new System.Drawing.Size(1348, 321);
            this.xtraTabPage2.Text = "Proje Detayları";
            // 
            // grdProjeDetay
            // 
            this.grdProjeDetay.Location = new System.Drawing.Point(168, 42);
            this.grdProjeDetay.MainView = this.grdVProjeDetay;
            this.grdProjeDetay.Name = "grdProjeDetay";
            this.grdProjeDetay.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox23,
            this.repositoryItemComboBox24,
            this.repositoryItemComboBox25,
            this.repositoryItemLookUpEdit34,
            this.repositoryItemLookUpEdit35,
            this.repositoryItemPictureEdit14,
            this.repositoryItemImageEdit7,
            this.repositoryItemPictureEdit13,
            this.repositoryItemMemoEdit7,
            this.repTarProDet,
            this.repositoryItemCheckedComboBoxEdit13,
            this.repAnaGrupProje1,
            this.repChkDanisman,
            this.repAltGrupProje,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit14});
            this.grdProjeDetay.Size = new System.Drawing.Size(1346, 289);
            this.grdProjeDetay.TabIndex = 15;
            this.grdProjeDetay.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVProjeDetay});
            // 
            // grdVProjeDetay
            // 
            this.grdVProjeDetay.CardHorzInterval = 1;
            this.grdVProjeDetay.CardMinSize = new System.Drawing.Size(473, 452);
            this.grdVProjeDetay.CardVertInterval = 0;
            this.grdVProjeDetay.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.cPdt_ProjeNo,
            this.cPdt_ProjeDetayTanim,
            this.cPdt_ProjeLideri,
            this.cPdt_ProjeSorumlusu,
            this.cPdt_ProjeBaslangicTarihi,
            this.cPdt_ProjeBitisTarihi,
            this.cPdt_ProjeModulAdi1,
            this.cPdt_ProjeModulDanismani1,
            this.cPdt_ProjeModulAdi2,
            this.cPdt_ProjeModulDanismani2,
            this.cPdt_ProjeModulAdi3,
            this.cPdt_ProjeModulDanismani3,
            this.cPdt_ProjeModulAdi4,
            this.cPdt_ProjeModulDanismani4,
            this.cPdt_ProjeModulAdi5,
            this.cPdt_ProjeModulDanismani5,
            this.cPdt_ProjeModulAdi6,
            this.cPdt_ProjeModulDanismani6,
            this.cPdt_ProjeModulAdi7,
            this.cPdt_ProjeModulDanismani7,
            this.cPdt_ProjeModulAdi8,
            this.cPdt_ProjeModulDanismani9,
            this.cPdt_ProjeModulAdi9,
            this.Pdt_ProjeModulDanismani10,
            this.cPdt_ProjeModulAdi10,
            this.cPdt_ProjeModulDanismani11,
            this.cPdt_ProjeModulAdi11,
            this.cPdt_ProjeModulDanismani12});
            this.grdVProjeDetay.GridControl = this.grdProjeDetay;
            this.grdVProjeDetay.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn110});
            this.grdVProjeDetay.Name = "grdVProjeDetay";
            this.grdVProjeDetay.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.grdVProjeDetay.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.CustomSize;
            this.grdVProjeDetay.OptionsItemText.TextToControlDistance = 0;
            this.grdVProjeDetay.OptionsSelection.MultiSelect = true;
            this.grdVProjeDetay.OptionsView.ContentAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.grdVProjeDetay.OptionsView.ShowCardCaption = false;
            this.grdVProjeDetay.OptionsView.ShowHeaderPanel = false;
            this.grdVProjeDetay.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiRow;
            this.grdVProjeDetay.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.cPdt_ProjeBaslangicTarihi, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.grdVProjeDetay.TemplateCard = this.layoutViewCard1;
            this.grdVProjeDetay.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdVProjeDetay_FocusedRowChanged);
            this.grdVProjeDetay.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdVProjeDetay_CellValueChanged_1);
            this.grdVProjeDetay.DoubleClick += new System.EventHandler(this.grdVProjeDetay_DoubleClick);
            // 
            // cPdt_ProjeNo
            // 
            this.cPdt_ProjeNo.Caption = "Pdt_ProjeNo";
            this.cPdt_ProjeNo.FieldName = "Pdt_ProjeNo";
            this.cPdt_ProjeNo.LayoutViewField = this.layoutViewField_layoutViewColumn110;
            this.cPdt_ProjeNo.Name = "cPdt_ProjeNo";
            // 
            // layoutViewField_layoutViewColumn110
            // 
            this.layoutViewField_layoutViewColumn110.EditorPreferredWidth = 10;
            this.layoutViewField_layoutViewColumn110.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn110.Name = "layoutViewField_layoutViewColumn110";
            this.layoutViewField_layoutViewColumn110.Size = new System.Drawing.Size(492, 361);
            this.layoutViewField_layoutViewColumn110.TextSize = new System.Drawing.Size(0, 20);
            this.layoutViewField_layoutViewColumn110.TextToControlDistance = 5;
            // 
            // cPdt_ProjeDetayTanim
            // 
            this.cPdt_ProjeDetayTanim.Caption = "Prj. Detay Tnm.";
            this.cPdt_ProjeDetayTanim.ColumnEdit = this.repAltGrupProje;
            this.cPdt_ProjeDetayTanim.CustomizationCaption = "Dty.Tnm.";
            this.cPdt_ProjeDetayTanim.FieldName = "Pdt_ProjeDetayTanim";
            this.cPdt_ProjeDetayTanim.LayoutViewField = this.layoutViewField_layoutViewColumn110_8;
            this.cPdt_ProjeDetayTanim.Name = "cPdt_ProjeDetayTanim";
            // 
            // repAltGrupProje
            // 
            this.repAltGrupProje.AutoHeight = false;
            this.repAltGrupProje.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAltGrupProje.DisplayMember = "Tanim";
            this.repAltGrupProje.Name = "repAltGrupProje";
            this.repAltGrupProje.NullText = "";
            this.repAltGrupProje.ValueMember = "Kod";
            // 
            // layoutViewField_layoutViewColumn110_8
            // 
            this.layoutViewField_layoutViewColumn110_8.EditorPreferredWidth = 374;
            this.layoutViewField_layoutViewColumn110_8.Location = new System.Drawing.Point(0, 24);
            this.layoutViewField_layoutViewColumn110_8.Name = "layoutViewField_layoutViewColumn110_8";
            this.layoutViewField_layoutViewColumn110_8.Size = new System.Drawing.Size(453, 24);
            this.layoutViewField_layoutViewColumn110_8.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeLideri
            // 
            this.cPdt_ProjeLideri.Caption = "Prj. Lideri";
            this.cPdt_ProjeLideri.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeLideri.FieldName = "Pdt_ProjeLideri";
            this.cPdt_ProjeLideri.LayoutViewField = this.layoutViewField_layoutViewColumn110_9;
            this.cPdt_ProjeLideri.Name = "cPdt_ProjeLideri";
            // 
            // repChkDanisman
            // 
            this.repChkDanisman.AutoHeight = false;
            this.repChkDanisman.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repChkDanisman.DisplayMember = "Tanim";
            this.repChkDanisman.Name = "repChkDanisman";
            this.repChkDanisman.ValueMember = "Kod";
            // 
            // layoutViewField_layoutViewColumn110_9
            // 
            this.layoutViewField_layoutViewColumn110_9.EditorPreferredWidth = 374;
            this.layoutViewField_layoutViewColumn110_9.Location = new System.Drawing.Point(0, 48);
            this.layoutViewField_layoutViewColumn110_9.Name = "layoutViewField_layoutViewColumn110_9";
            this.layoutViewField_layoutViewColumn110_9.Size = new System.Drawing.Size(453, 24);
            this.layoutViewField_layoutViewColumn110_9.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeSorumlusu
            // 
            this.cPdt_ProjeSorumlusu.Caption = "Prj. Sorumlusu";
            this.cPdt_ProjeSorumlusu.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeSorumlusu.CustomizationCaption = "Prj. Sorumlusu";
            this.cPdt_ProjeSorumlusu.FieldName = "Pdt_ProjeSorumlusu";
            this.cPdt_ProjeSorumlusu.LayoutViewField = this.layoutViewField_layoutViewColumn110_5;
            this.cPdt_ProjeSorumlusu.Name = "cPdt_ProjeSorumlusu";
            // 
            // layoutViewField_layoutViewColumn110_5
            // 
            this.layoutViewField_layoutViewColumn110_5.EditorPreferredWidth = 374;
            this.layoutViewField_layoutViewColumn110_5.Location = new System.Drawing.Point(0, 72);
            this.layoutViewField_layoutViewColumn110_5.Name = "layoutViewField_layoutViewColumn110_5";
            this.layoutViewField_layoutViewColumn110_5.OptionsToolTip.ToolTipTitle = "Proje Sorumlusu";
            this.layoutViewField_layoutViewColumn110_5.Size = new System.Drawing.Size(453, 24);
            this.layoutViewField_layoutViewColumn110_5.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeBaslangicTarihi
            // 
            this.cPdt_ProjeBaslangicTarihi.Caption = "Başl.Tar.";
            this.cPdt_ProjeBaslangicTarihi.ColumnEdit = this.repTarProDet;
            this.cPdt_ProjeBaslangicTarihi.FieldName = "Pdt_ProjeBaslangicTarihi";
            this.cPdt_ProjeBaslangicTarihi.LayoutViewField = this.layoutViewField_layoutViewColumn110_1;
            this.cPdt_ProjeBaslangicTarihi.Name = "cPdt_ProjeBaslangicTarihi";
            // 
            // repTarProDet
            // 
            this.repTarProDet.AutoHeight = false;
            this.repTarProDet.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repTarProDet.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repTarProDet.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repTarProDet.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repTarProDet.Name = "repTarProDet";
            // 
            // layoutViewField_layoutViewColumn110_1
            // 
            this.layoutViewField_layoutViewColumn110_1.EditorPreferredWidth = 113;
            this.layoutViewField_layoutViewColumn110_1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn110_1.Name = "layoutViewField_layoutViewColumn110_1";
            this.layoutViewField_layoutViewColumn110_1.Size = new System.Drawing.Size(192, 24);
            this.layoutViewField_layoutViewColumn110_1.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeBitisTarihi
            // 
            this.cPdt_ProjeBitisTarihi.Caption = "Bitş.Tar.";
            this.cPdt_ProjeBitisTarihi.ColumnEdit = this.repTarProDet;
            this.cPdt_ProjeBitisTarihi.CustomizationCaption = "Bitş.Tar";
            this.cPdt_ProjeBitisTarihi.FieldName = "Pdt_ProjeBitisTarihi";
            this.cPdt_ProjeBitisTarihi.LayoutViewField = this.layoutViewField_layoutViewColumn110_2;
            this.cPdt_ProjeBitisTarihi.Name = "cPdt_ProjeBitisTarihi";
            // 
            // layoutViewField_layoutViewColumn110_2
            // 
            this.layoutViewField_layoutViewColumn110_2.EditorPreferredWidth = 210;
            this.layoutViewField_layoutViewColumn110_2.Location = new System.Drawing.Point(192, 0);
            this.layoutViewField_layoutViewColumn110_2.Name = "layoutViewField_layoutViewColumn110_2";
            this.layoutViewField_layoutViewColumn110_2.Size = new System.Drawing.Size(261, 24);
            this.layoutViewField_layoutViewColumn110_2.TextSize = new System.Drawing.Size(47, 20);
            // 
            // cPdt_ProjeModulAdi1
            // 
            this.cPdt_ProjeModulAdi1.Caption = "Proje 1";
            this.cPdt_ProjeModulAdi1.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi1.FieldName = "Pdt_ProjeModulAdi1";
            this.cPdt_ProjeModulAdi1.LayoutViewField = this.layoutViewField_layoutViewColumn110_3;
            this.cPdt_ProjeModulAdi1.Name = "cPdt_ProjeModulAdi1";
            // 
            // repAnaGrupProje1
            // 
            this.repAnaGrupProje1.AutoHeight = false;
            this.repAnaGrupProje1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAnaGrupProje1.DisplayMember = "Tanim";
            this.repAnaGrupProje1.Name = "repAnaGrupProje1";
            this.repAnaGrupProje1.NullText = "";
            this.repAnaGrupProje1.ValueMember = "Kod";
            // 
            // layoutViewField_layoutViewColumn110_3
            // 
            this.layoutViewField_layoutViewColumn110_3.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn110_3.Location = new System.Drawing.Point(193, 97);
            this.layoutViewField_layoutViewColumn110_3.Name = "layoutViewField_layoutViewColumn110_3";
            this.layoutViewField_layoutViewColumn110_3.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn110_3.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani1
            // 
            this.cPdt_ProjeModulDanismani1.Caption = "Danışman1";
            this.cPdt_ProjeModulDanismani1.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani1.FieldName = "Pdt_ProjeModulDanismani1";
            this.cPdt_ProjeModulDanismani1.LayoutViewField = this.layoutViewField_layoutViewColumn110_4;
            this.cPdt_ProjeModulDanismani1.Name = "cPdt_ProjeModulDanismani1";
            // 
            // layoutViewField_layoutViewColumn110_4
            // 
            this.layoutViewField_layoutViewColumn110_4.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn110_4.Location = new System.Drawing.Point(0, 97);
            this.layoutViewField_layoutViewColumn110_4.Name = "layoutViewField_layoutViewColumn110_4";
            this.layoutViewField_layoutViewColumn110_4.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn110_4.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi2
            // 
            this.cPdt_ProjeModulAdi2.Caption = "Proje 2";
            this.cPdt_ProjeModulAdi2.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi2.FieldName = "Pdt_ProjeModulAdi2";
            this.cPdt_ProjeModulAdi2.LayoutViewField = this.layoutViewField_layoutViewColumn110_6;
            this.cPdt_ProjeModulAdi2.Name = "cPdt_ProjeModulAdi2";
            // 
            // layoutViewField_layoutViewColumn110_6
            // 
            this.layoutViewField_layoutViewColumn110_6.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn110_6.Location = new System.Drawing.Point(193, 121);
            this.layoutViewField_layoutViewColumn110_6.Name = "layoutViewField_layoutViewColumn110_6";
            this.layoutViewField_layoutViewColumn110_6.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn110_6.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani2
            // 
            this.cPdt_ProjeModulDanismani2.Caption = "Danışman2";
            this.cPdt_ProjeModulDanismani2.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani2.FieldName = "Pdt_ProjeModulDanismani2";
            this.cPdt_ProjeModulDanismani2.LayoutViewField = this.layoutViewField_layoutViewColumn111;
            this.cPdt_ProjeModulDanismani2.Name = "cPdt_ProjeModulDanismani2";
            // 
            // layoutViewField_layoutViewColumn111
            // 
            this.layoutViewField_layoutViewColumn111.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn111.Location = new System.Drawing.Point(0, 121);
            this.layoutViewField_layoutViewColumn111.Name = "layoutViewField_layoutViewColumn111";
            this.layoutViewField_layoutViewColumn111.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn111.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi3
            // 
            this.cPdt_ProjeModulAdi3.Caption = "Proje 3";
            this.cPdt_ProjeModulAdi3.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi3.FieldName = "Pdt_ProjeModulAdi3";
            this.cPdt_ProjeModulAdi3.LayoutViewField = this.layoutViewField_layoutViewColumn112;
            this.cPdt_ProjeModulAdi3.Name = "cPdt_ProjeModulAdi3";
            // 
            // layoutViewField_layoutViewColumn112
            // 
            this.layoutViewField_layoutViewColumn112.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn112.Location = new System.Drawing.Point(193, 145);
            this.layoutViewField_layoutViewColumn112.Name = "layoutViewField_layoutViewColumn112";
            this.layoutViewField_layoutViewColumn112.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn112.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani3
            // 
            this.cPdt_ProjeModulDanismani3.Caption = "Danışman3";
            this.cPdt_ProjeModulDanismani3.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani3.FieldName = "Pdt_ProjeModulDanismani3";
            this.cPdt_ProjeModulDanismani3.LayoutViewField = this.layoutViewField_layoutViewColumn113;
            this.cPdt_ProjeModulDanismani3.Name = "cPdt_ProjeModulDanismani3";
            // 
            // layoutViewField_layoutViewColumn113
            // 
            this.layoutViewField_layoutViewColumn113.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn113.Location = new System.Drawing.Point(0, 145);
            this.layoutViewField_layoutViewColumn113.Name = "layoutViewField_layoutViewColumn113";
            this.layoutViewField_layoutViewColumn113.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn113.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi4
            // 
            this.cPdt_ProjeModulAdi4.Caption = "Proje 4";
            this.cPdt_ProjeModulAdi4.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi4.FieldName = "Pdt_ProjeModulAdi4";
            this.cPdt_ProjeModulAdi4.LayoutViewField = this.layoutViewField_layoutViewColumn114;
            this.cPdt_ProjeModulAdi4.Name = "cPdt_ProjeModulAdi4";
            // 
            // layoutViewField_layoutViewColumn114
            // 
            this.layoutViewField_layoutViewColumn114.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn114.Location = new System.Drawing.Point(193, 169);
            this.layoutViewField_layoutViewColumn114.Name = "layoutViewField_layoutViewColumn114";
            this.layoutViewField_layoutViewColumn114.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn114.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani4
            // 
            this.cPdt_ProjeModulDanismani4.Caption = "Danışman4";
            this.cPdt_ProjeModulDanismani4.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani4.FieldName = "Pdt_ProjeModulDanismani4";
            this.cPdt_ProjeModulDanismani4.LayoutViewField = this.layoutViewField_layoutViewColumn115;
            this.cPdt_ProjeModulDanismani4.Name = "cPdt_ProjeModulDanismani4";
            // 
            // layoutViewField_layoutViewColumn115
            // 
            this.layoutViewField_layoutViewColumn115.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn115.Location = new System.Drawing.Point(0, 169);
            this.layoutViewField_layoutViewColumn115.Name = "layoutViewField_layoutViewColumn115";
            this.layoutViewField_layoutViewColumn115.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn115.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi5
            // 
            this.cPdt_ProjeModulAdi5.Caption = "Proje 5";
            this.cPdt_ProjeModulAdi5.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi5.FieldName = "Pdt_ProjeModulAdi5";
            this.cPdt_ProjeModulAdi5.LayoutViewField = this.layoutViewField_layoutViewColumn116;
            this.cPdt_ProjeModulAdi5.Name = "cPdt_ProjeModulAdi5";
            // 
            // layoutViewField_layoutViewColumn116
            // 
            this.layoutViewField_layoutViewColumn116.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn116.Location = new System.Drawing.Point(193, 193);
            this.layoutViewField_layoutViewColumn116.Name = "layoutViewField_layoutViewColumn116";
            this.layoutViewField_layoutViewColumn116.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn116.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani5
            // 
            this.cPdt_ProjeModulDanismani5.Caption = "Danışman5";
            this.cPdt_ProjeModulDanismani5.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani5.FieldName = "Pdt_ProjeModulDanismani5";
            this.cPdt_ProjeModulDanismani5.LayoutViewField = this.layoutViewField_layoutViewColumn117;
            this.cPdt_ProjeModulDanismani5.Name = "cPdt_ProjeModulDanismani5";
            // 
            // layoutViewField_layoutViewColumn117
            // 
            this.layoutViewField_layoutViewColumn117.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn117.Location = new System.Drawing.Point(0, 193);
            this.layoutViewField_layoutViewColumn117.Name = "layoutViewField_layoutViewColumn117";
            this.layoutViewField_layoutViewColumn117.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn117.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi6
            // 
            this.cPdt_ProjeModulAdi6.Caption = "Proje 6";
            this.cPdt_ProjeModulAdi6.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi6.FieldName = "Pdt_ProjeModulAdi6";
            this.cPdt_ProjeModulAdi6.LayoutViewField = this.layoutViewField_layoutViewColumn118;
            this.cPdt_ProjeModulAdi6.Name = "cPdt_ProjeModulAdi6";
            // 
            // layoutViewField_layoutViewColumn118
            // 
            this.layoutViewField_layoutViewColumn118.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn118.Location = new System.Drawing.Point(193, 217);
            this.layoutViewField_layoutViewColumn118.Name = "layoutViewField_layoutViewColumn118";
            this.layoutViewField_layoutViewColumn118.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn118.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani6
            // 
            this.cPdt_ProjeModulDanismani6.Caption = "Danışman6";
            this.cPdt_ProjeModulDanismani6.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani6.FieldName = "Pdt_ProjeModulDanismani6";
            this.cPdt_ProjeModulDanismani6.LayoutViewField = this.layoutViewField_layoutViewColumn119;
            this.cPdt_ProjeModulDanismani6.Name = "cPdt_ProjeModulDanismani6";
            // 
            // layoutViewField_layoutViewColumn119
            // 
            this.layoutViewField_layoutViewColumn119.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn119.Location = new System.Drawing.Point(0, 217);
            this.layoutViewField_layoutViewColumn119.Name = "layoutViewField_layoutViewColumn119";
            this.layoutViewField_layoutViewColumn119.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn119.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi7
            // 
            this.cPdt_ProjeModulAdi7.Caption = "Proje 7";
            this.cPdt_ProjeModulAdi7.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi7.FieldName = "Pdt_ProjeModulAdi7";
            this.cPdt_ProjeModulAdi7.LayoutViewField = this.layoutViewField_layoutViewColumn120;
            this.cPdt_ProjeModulAdi7.Name = "cPdt_ProjeModulAdi7";
            // 
            // layoutViewField_layoutViewColumn120
            // 
            this.layoutViewField_layoutViewColumn120.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn120.Location = new System.Drawing.Point(193, 241);
            this.layoutViewField_layoutViewColumn120.Name = "layoutViewField_layoutViewColumn120";
            this.layoutViewField_layoutViewColumn120.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn120.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani7
            // 
            this.cPdt_ProjeModulDanismani7.Caption = "Danışman7";
            this.cPdt_ProjeModulDanismani7.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani7.FieldName = "Pdt_ProjeModulDanismani7";
            this.cPdt_ProjeModulDanismani7.LayoutViewField = this.layoutViewField_layoutViewColumn121;
            this.cPdt_ProjeModulDanismani7.Name = "cPdt_ProjeModulDanismani7";
            // 
            // layoutViewField_layoutViewColumn121
            // 
            this.layoutViewField_layoutViewColumn121.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn121.Location = new System.Drawing.Point(0, 241);
            this.layoutViewField_layoutViewColumn121.Name = "layoutViewField_layoutViewColumn121";
            this.layoutViewField_layoutViewColumn121.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn121.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi8
            // 
            this.cPdt_ProjeModulAdi8.Caption = "Proje 8";
            this.cPdt_ProjeModulAdi8.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi8.FieldName = "Pdt_ProjeModulAdi8";
            this.cPdt_ProjeModulAdi8.LayoutViewField = this.layoutViewField_layoutViewColumn122;
            this.cPdt_ProjeModulAdi8.Name = "cPdt_ProjeModulAdi8";
            // 
            // layoutViewField_layoutViewColumn122
            // 
            this.layoutViewField_layoutViewColumn122.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn122.Location = new System.Drawing.Point(193, 265);
            this.layoutViewField_layoutViewColumn122.Name = "layoutViewField_layoutViewColumn122";
            this.layoutViewField_layoutViewColumn122.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn122.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani9
            // 
            this.cPdt_ProjeModulDanismani9.Caption = "Danışman8";
            this.cPdt_ProjeModulDanismani9.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani9.FieldName = "Pdt_ProjeModulDanismani9";
            this.cPdt_ProjeModulDanismani9.LayoutViewField = this.layoutViewField_layoutViewColumn123;
            this.cPdt_ProjeModulDanismani9.Name = "cPdt_ProjeModulDanismani9";
            // 
            // layoutViewField_layoutViewColumn123
            // 
            this.layoutViewField_layoutViewColumn123.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn123.Location = new System.Drawing.Point(0, 265);
            this.layoutViewField_layoutViewColumn123.Name = "layoutViewField_layoutViewColumn123";
            this.layoutViewField_layoutViewColumn123.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn123.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi9
            // 
            this.cPdt_ProjeModulAdi9.Caption = "Proje 9";
            this.cPdt_ProjeModulAdi9.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi9.FieldName = "Pdt_ProjeModulAdi9";
            this.cPdt_ProjeModulAdi9.LayoutViewField = this.layoutViewField_layoutViewColumn124;
            this.cPdt_ProjeModulAdi9.Name = "cPdt_ProjeModulAdi9";
            // 
            // layoutViewField_layoutViewColumn124
            // 
            this.layoutViewField_layoutViewColumn124.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn124.Location = new System.Drawing.Point(193, 289);
            this.layoutViewField_layoutViewColumn124.Name = "layoutViewField_layoutViewColumn124";
            this.layoutViewField_layoutViewColumn124.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn124.TextSize = new System.Drawing.Size(45, 20);
            // 
            // Pdt_ProjeModulDanismani10
            // 
            this.Pdt_ProjeModulDanismani10.Caption = "Danışman9";
            this.Pdt_ProjeModulDanismani10.ColumnEdit = this.repChkDanisman;
            this.Pdt_ProjeModulDanismani10.FieldName = "Pdt_ProjeModulDanismani10";
            this.Pdt_ProjeModulDanismani10.LayoutViewField = this.layoutViewField_layoutViewColumn125;
            this.Pdt_ProjeModulDanismani10.Name = "Pdt_ProjeModulDanismani10";
            // 
            // layoutViewField_layoutViewColumn125
            // 
            this.layoutViewField_layoutViewColumn125.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn125.Location = new System.Drawing.Point(0, 289);
            this.layoutViewField_layoutViewColumn125.Name = "layoutViewField_layoutViewColumn125";
            this.layoutViewField_layoutViewColumn125.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn125.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi10
            // 
            this.cPdt_ProjeModulAdi10.Caption = "Proje 10";
            this.cPdt_ProjeModulAdi10.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi10.FieldName = "Pdt_ProjeModulAdi10";
            this.cPdt_ProjeModulAdi10.LayoutViewField = this.layoutViewField_layoutViewColumn126;
            this.cPdt_ProjeModulAdi10.Name = "cPdt_ProjeModulAdi10";
            // 
            // layoutViewField_layoutViewColumn126
            // 
            this.layoutViewField_layoutViewColumn126.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn126.Location = new System.Drawing.Point(193, 313);
            this.layoutViewField_layoutViewColumn126.Name = "layoutViewField_layoutViewColumn126";
            this.layoutViewField_layoutViewColumn126.Size = new System.Drawing.Size(260, 24);
            this.layoutViewField_layoutViewColumn126.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani11
            // 
            this.cPdt_ProjeModulDanismani11.Caption = "Danışman10";
            this.cPdt_ProjeModulDanismani11.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani11.FieldName = "Pdt_ProjeModulDanismani11";
            this.cPdt_ProjeModulDanismani11.LayoutViewField = this.layoutViewField_layoutViewColumn127;
            this.cPdt_ProjeModulDanismani11.Name = "cPdt_ProjeModulDanismani11";
            // 
            // layoutViewField_layoutViewColumn127
            // 
            this.layoutViewField_layoutViewColumn127.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn127.Location = new System.Drawing.Point(0, 313);
            this.layoutViewField_layoutViewColumn127.Name = "layoutViewField_layoutViewColumn127";
            this.layoutViewField_layoutViewColumn127.Size = new System.Drawing.Size(193, 24);
            this.layoutViewField_layoutViewColumn127.TextSize = new System.Drawing.Size(75, 20);
            // 
            // cPdt_ProjeModulAdi11
            // 
            this.cPdt_ProjeModulAdi11.Caption = "Proje 11";
            this.cPdt_ProjeModulAdi11.ColumnEdit = this.repAnaGrupProje1;
            this.cPdt_ProjeModulAdi11.FieldName = "Pdt_ProjeModulAdi10";
            this.cPdt_ProjeModulAdi11.LayoutViewField = this.layoutViewField_layoutViewColumn128;
            this.cPdt_ProjeModulAdi11.Name = "cPdt_ProjeModulAdi11";
            // 
            // layoutViewField_layoutViewColumn128
            // 
            this.layoutViewField_layoutViewColumn128.EditorPreferredWidth = 211;
            this.layoutViewField_layoutViewColumn128.Location = new System.Drawing.Point(193, 337);
            this.layoutViewField_layoutViewColumn128.Name = "layoutViewField_layoutViewColumn128";
            this.layoutViewField_layoutViewColumn128.Size = new System.Drawing.Size(260, 95);
            this.layoutViewField_layoutViewColumn128.TextSize = new System.Drawing.Size(45, 20);
            // 
            // cPdt_ProjeModulDanismani12
            // 
            this.cPdt_ProjeModulDanismani12.Caption = "Danışman11";
            this.cPdt_ProjeModulDanismani12.ColumnEdit = this.repChkDanisman;
            this.cPdt_ProjeModulDanismani12.FieldName = "Pdt_ProjeModulDanismani12";
            this.cPdt_ProjeModulDanismani12.LayoutViewField = this.layoutViewField_layoutViewColumn129;
            this.cPdt_ProjeModulDanismani12.Name = "cPdt_ProjeModulDanismani12";
            // 
            // layoutViewField_layoutViewColumn129
            // 
            this.layoutViewField_layoutViewColumn129.EditorPreferredWidth = 114;
            this.layoutViewField_layoutViewColumn129.Location = new System.Drawing.Point(0, 337);
            this.layoutViewField_layoutViewColumn129.Name = "layoutViewField_layoutViewColumn129";
            this.layoutViewField_layoutViewColumn129.Size = new System.Drawing.Size(193, 95);
            this.layoutViewField_layoutViewColumn129.TextSize = new System.Drawing.Size(75, 20);
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn110_1,
            this.layoutViewField_layoutViewColumn110_2,
            this.layoutViewField_layoutViewColumn110_9,
            this.layoutViewField_layoutViewColumn110_5,
            this.layoutViewField_layoutViewColumn110_8,
            this.layoutViewField_layoutViewColumn110_4,
            this.layoutViewField_layoutViewColumn110_3,
            this.layoutViewField_layoutViewColumn111,
            this.layoutViewField_layoutViewColumn110_6,
            this.layoutViewField_layoutViewColumn113,
            this.layoutViewField_layoutViewColumn112,
            this.layoutViewField_layoutViewColumn115,
            this.layoutViewField_layoutViewColumn114,
            this.layoutViewField_layoutViewColumn117,
            this.layoutViewField_layoutViewColumn116,
            this.layoutViewField_layoutViewColumn119,
            this.layoutViewField_layoutViewColumn118,
            this.layoutViewField_layoutViewColumn121,
            this.layoutViewField_layoutViewColumn120,
            this.layoutViewField_layoutViewColumn123,
            this.layoutViewField_layoutViewColumn122,
            this.layoutViewField_layoutViewColumn125,
            this.layoutViewField_layoutViewColumn124,
            this.layoutViewField_layoutViewColumn127,
            this.layoutViewField_layoutViewColumn126,
            this.layoutViewField_layoutViewColumn129,
            this.layoutViewField_layoutViewColumn128,
            this.item1});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 0;
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(0, 96);
            this.item1.Name = "item1";
            this.item1.Size = new System.Drawing.Size(453, 1);
            this.item1.Text = "item1";
            // 
            // repositoryItemComboBox23
            // 
            this.repositoryItemComboBox23.AutoHeight = false;
            this.repositoryItemComboBox23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox23.Name = "repositoryItemComboBox23";
            // 
            // repositoryItemComboBox24
            // 
            this.repositoryItemComboBox24.AutoHeight = false;
            this.repositoryItemComboBox24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox24.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox24.Name = "repositoryItemComboBox24";
            this.repositoryItemComboBox24.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox25
            // 
            this.repositoryItemComboBox25.AutoHeight = false;
            this.repositoryItemComboBox25.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox25.Name = "repositoryItemComboBox25";
            // 
            // repositoryItemLookUpEdit34
            // 
            this.repositoryItemLookUpEdit34.AutoHeight = false;
            this.repositoryItemLookUpEdit34.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit34.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit34.Name = "repositoryItemLookUpEdit34";
            this.repositoryItemLookUpEdit34.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit35
            // 
            this.repositoryItemLookUpEdit35.AutoHeight = false;
            this.repositoryItemLookUpEdit35.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit35.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit35.Name = "repositoryItemLookUpEdit35";
            this.repositoryItemLookUpEdit35.NullText = "";
            this.repositoryItemLookUpEdit35.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit14
            // 
            this.repositoryItemPictureEdit14.Name = "repositoryItemPictureEdit14";
            // 
            // repositoryItemImageEdit7
            // 
            this.repositoryItemImageEdit7.AutoHeight = false;
            this.repositoryItemImageEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit7.Name = "repositoryItemImageEdit7";
            // 
            // repositoryItemPictureEdit13
            // 
            this.repositoryItemPictureEdit13.Name = "repositoryItemPictureEdit13";
            // 
            // repositoryItemMemoEdit7
            // 
            this.repositoryItemMemoEdit7.Name = "repositoryItemMemoEdit7";
            // 
            // repositoryItemCheckedComboBoxEdit13
            // 
            this.repositoryItemCheckedComboBoxEdit13.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit13.Name = "repositoryItemCheckedComboBoxEdit13";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit1.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemDateEdit14
            // 
            this.repositoryItemDateEdit14.AutoHeight = false;
            this.repositoryItemDateEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit14.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit14.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit14.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit14.Name = "repositoryItemDateEdit14";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.pvtKarneNotlari);
            this.xtraTabPage3.Controls.Add(this.standaloneBarDockControl9);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.PageVisible = false;
            this.xtraTabPage3.Size = new System.Drawing.Size(1348, 321);
            this.xtraTabPage3.Text = "Karne Sonuçları";
            // 
            // pvtKarneNotlari
            // 
            this.pvtKarneNotlari.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField1,
            this.pAy,
            this.pivotGridField3});
            this.pvtKarneNotlari.Location = new System.Drawing.Point(105, 46);
            this.pvtKarneNotlari.Name = "pvtKarneNotlari";
            this.pvtKarneNotlari.OptionsView.ShowColumnHeaders = false;
            this.pvtKarneNotlari.OptionsView.ShowCustomTotalsForSingleValues = true;
            this.pvtKarneNotlari.OptionsView.ShowDataHeaders = false;
            this.pvtKarneNotlari.OptionsView.ShowFilterHeaders = false;
            this.pvtKarneNotlari.OptionsView.ShowRowHeaders = false;
            this.pvtKarneNotlari.Size = new System.Drawing.Size(1346, 285);
            this.pvtKarneNotlari.TabIndex = 1;
            this.pvtKarneNotlari.FieldValueDisplayText += new DevExpress.XtraPivotGrid.PivotFieldDisplayTextEventHandler(this.pvtKarneNotlari_FieldValueDisplayText);
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.FieldName = "ModulAdi";
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.Width = 250;
            // 
            // pAy
            // 
            this.pAy.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pAy.AreaIndex = 0;
            this.pAy.FieldName = "Ay";
            this.pAy.Name = "pAy";
            this.pAy.Width = 70;
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField3.AreaIndex = 0;
            this.pivotGridField3.FieldName = "KarneNotu";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // tbilaveBilgi
            // 
            this.tbilaveBilgi.Controls.Add(this.tbEk);
            this.tbilaveBilgi.Name = "tbilaveBilgi";
            this.tbilaveBilgi.Size = new System.Drawing.Size(1357, 408);
            this.tbilaveBilgi.Text = "Ek Bilgiler";
            // 
            // tbEk
            // 
            this.tbEk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbEk.Location = new System.Drawing.Point(0, 0);
            this.tbEk.Name = "tbEk";
            this.tbEk.SelectedTabPage = this.xtraTabPage4;
            this.tbEk.Size = new System.Drawing.Size(1357, 408);
            this.tbEk.TabIndex = 18;
            this.tbEk.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage4,
            this.xtraTabPage5});
            this.tbEk.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tbEk_SelectedPageChanged);
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.grpSertifika);
            this.xtraTabPage4.Controls.Add(this.btnYenileEgitimler);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1352, 383);
            this.xtraTabPage4.Text = "Sertifikalar";
            // 
            // grpSertifika
            // 
            this.grpSertifika.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpSertifika.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpSertifika.AppearanceCaption.Options.UseFont = true;
            this.grpSertifika.AppearanceCaption.Options.UseForeColor = true;
            this.grpSertifika.Controls.Add(this.panelControl1);
            this.grpSertifika.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSertifika.Location = new System.Drawing.Point(0, 29);
            this.grpSertifika.Name = "grpSertifika";
            this.grpSertifika.Size = new System.Drawing.Size(1352, 354);
            this.grpSertifika.TabIndex = 16;
            this.grpSertifika.Text = "...";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.grdSertikalar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(2, 31);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1348, 321);
            this.panelControl1.TabIndex = 15;
            // 
            // grdSertikalar
            // 
            this.grdSertikalar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdSertikalar.Location = new System.Drawing.Point(0, 0);
            this.grdSertikalar.MainView = this.grdVSertikalar;
            this.grdSertikalar.Name = "grdSertikalar";
            this.grdSertikalar.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox28,
            this.repositoryItemComboBox29,
            this.repositoryItemComboBox30,
            this.repositoryItemLookUpEdit38,
            this.repositoryItemLookUpEdit39,
            this.repositoryItemPictureEdit15,
            this.repositoryItemImageEdit8,
            this.repositoryItemPictureEdit16,
            this.repositoryItemLookUpEdit40,
            this.repositoryItemLookUpEdit41,
            this.repositoryItemLookUpEdit42,
            this.repositoryItemLookUpEdit43,
            this.repositoryItemCheckedComboBoxEdit16,
            this.repositoryItemMemoEdit8,
            this.repositoryItemDateEdit18,
            this.repositoryItemCheckedComboBoxEdit17,
            this.repositoryItemCheckedComboBoxEdit18,
            this.repositoryItemCheckedComboBoxEdit19,
            this.repositoryItemCheckedComboBoxEdit20,
            this.repositoryItemDateEdit19,
            this.repositoryItemLookUpEdit44,
            this.repositoryItemDateEdit15,
            this.repositoryItemMemoEdit9,
            this.repositoryItemComboBox31,
            this.repositoryItemComboBox27,
            this.repositoryItemCheckedComboBoxEdit15,
            this.repositoryItemDateEdit16,
            this.repositoryItemDateEdit17,
            this.repositoryItemCheckedComboBoxEdit14,
            this.repositoryItemLookUpEdit37,
            this.repositoryItemComboBox32,
            this.repSertifakalar});
            this.grdSertikalar.Size = new System.Drawing.Size(1348, 321);
            this.grdSertikalar.TabIndex = 13;
            this.grdSertikalar.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVSertikalar,
            this.layoutView13,
            this.layoutView14});
            // 
            // grdVSertikalar
            // 
            this.grdVSertikalar.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.CSID,
            this.CSFirmaKodu,
            this.cSTanim,
            this.cSAktifPasif});
            this.grdVSertikalar.GridControl = this.grdSertikalar;
            this.grdVSertikalar.Images = this.SmallImage;
            this.grdVSertikalar.Name = "grdVSertikalar";
            this.grdVSertikalar.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVSertikalar.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVSertikalar.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVSertikalar.OptionsSelection.MultiSelect = true;
            this.grdVSertikalar.OptionsView.ColumnAutoWidth = false;
            this.grdVSertikalar.OptionsView.RowAutoHeight = true;
            this.grdVSertikalar.OptionsView.ShowAutoFilterRow = true;
            this.grdVSertikalar.OptionsView.ShowGroupPanel = false;
            // 
            // CSID
            // 
            this.CSID.Caption = "ID";
            this.CSID.FieldName = "SID";
            this.CSID.Name = "CSID";
            // 
            // CSFirmaKodu
            // 
            this.CSFirmaKodu.Caption = "SFirmaKodu";
            this.CSFirmaKodu.FieldName = "SFirmaKodu";
            this.CSFirmaKodu.Name = "CSFirmaKodu";
            // 
            // cSTanim
            // 
            this.cSTanim.Caption = "Sertifika";
            this.cSTanim.ColumnEdit = this.repSertifakalar;
            this.cSTanim.FieldName = "STanim";
            this.cSTanim.Name = "cSTanim";
            this.cSTanim.Visible = true;
            this.cSTanim.VisibleIndex = 0;
            this.cSTanim.Width = 173;
            // 
            // repSertifakalar
            // 
            this.repSertifakalar.AutoHeight = false;
            this.repSertifakalar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSertifakalar.DisplayMember = "Tanim";
            this.repSertifakalar.Name = "repSertifakalar";
            this.repSertifakalar.NullText = "";
            this.repSertifakalar.ValueMember = "Kod";
            // 
            // cSAktifPasif
            // 
            this.cSAktifPasif.Caption = "Durum";
            this.cSAktifPasif.ColumnEdit = this.repositoryItemComboBox32;
            this.cSAktifPasif.FieldName = "SAktifPasif";
            this.cSAktifPasif.Name = "cSAktifPasif";
            this.cSAktifPasif.Visible = true;
            this.cSAktifPasif.VisibleIndex = 1;
            // 
            // repositoryItemComboBox32
            // 
            this.repositoryItemComboBox32.AutoHeight = false;
            this.repositoryItemComboBox32.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox32.Items.AddRange(new object[] {
            "Aktif",
            "Pasif"});
            this.repositoryItemComboBox32.Name = "repositoryItemComboBox32";
            // 
            // repositoryItemComboBox28
            // 
            this.repositoryItemComboBox28.AutoHeight = false;
            this.repositoryItemComboBox28.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox28.Name = "repositoryItemComboBox28";
            // 
            // repositoryItemComboBox29
            // 
            this.repositoryItemComboBox29.AutoHeight = false;
            this.repositoryItemComboBox29.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox29.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox29.Name = "repositoryItemComboBox29";
            this.repositoryItemComboBox29.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox30
            // 
            this.repositoryItemComboBox30.AutoHeight = false;
            this.repositoryItemComboBox30.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox30.Name = "repositoryItemComboBox30";
            // 
            // repositoryItemLookUpEdit38
            // 
            this.repositoryItemLookUpEdit38.AutoHeight = false;
            this.repositoryItemLookUpEdit38.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit38.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit38.Name = "repositoryItemLookUpEdit38";
            this.repositoryItemLookUpEdit38.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit39
            // 
            this.repositoryItemLookUpEdit39.AutoHeight = false;
            this.repositoryItemLookUpEdit39.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit39.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit39.Name = "repositoryItemLookUpEdit39";
            this.repositoryItemLookUpEdit39.NullText = "";
            this.repositoryItemLookUpEdit39.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit15
            // 
            this.repositoryItemPictureEdit15.Name = "repositoryItemPictureEdit15";
            // 
            // repositoryItemImageEdit8
            // 
            this.repositoryItemImageEdit8.AutoHeight = false;
            this.repositoryItemImageEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit8.Name = "repositoryItemImageEdit8";
            // 
            // repositoryItemPictureEdit16
            // 
            this.repositoryItemPictureEdit16.Name = "repositoryItemPictureEdit16";
            // 
            // repositoryItemLookUpEdit40
            // 
            this.repositoryItemLookUpEdit40.AutoHeight = false;
            this.repositoryItemLookUpEdit40.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit40.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit40.Name = "repositoryItemLookUpEdit40";
            this.repositoryItemLookUpEdit40.NullText = "";
            this.repositoryItemLookUpEdit40.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit41
            // 
            this.repositoryItemLookUpEdit41.AutoHeight = false;
            this.repositoryItemLookUpEdit41.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit41.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit41.Name = "repositoryItemLookUpEdit41";
            this.repositoryItemLookUpEdit41.NullText = "";
            this.repositoryItemLookUpEdit41.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit42
            // 
            this.repositoryItemLookUpEdit42.AutoHeight = false;
            this.repositoryItemLookUpEdit42.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit42.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit42.Name = "repositoryItemLookUpEdit42";
            this.repositoryItemLookUpEdit42.NullText = "";
            this.repositoryItemLookUpEdit42.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit43
            // 
            this.repositoryItemLookUpEdit43.AutoHeight = false;
            this.repositoryItemLookUpEdit43.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit43.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit43.DropDownRows = 20;
            this.repositoryItemLookUpEdit43.Name = "repositoryItemLookUpEdit43";
            this.repositoryItemLookUpEdit43.NullText = "";
            this.repositoryItemLookUpEdit43.PopupWidth = 600;
            this.repositoryItemLookUpEdit43.ValueMember = "Kod";
            // 
            // repositoryItemCheckedComboBoxEdit16
            // 
            this.repositoryItemCheckedComboBoxEdit16.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit16.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit16.Name = "repositoryItemCheckedComboBoxEdit16";
            this.repositoryItemCheckedComboBoxEdit16.ValueMember = "Tanim";
            // 
            // repositoryItemMemoEdit8
            // 
            this.repositoryItemMemoEdit8.Appearance.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit8.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit8.AppearanceDisabled.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit8.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit8.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit8.AppearanceFocused.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit8.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit8.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit8.AppearanceReadOnly.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit8.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit8.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit8.Name = "repositoryItemMemoEdit8";
            // 
            // repositoryItemDateEdit18
            // 
            this.repositoryItemDateEdit18.AutoHeight = false;
            this.repositoryItemDateEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit18.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit18.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit18.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit18.Name = "repositoryItemDateEdit18";
            // 
            // repositoryItemCheckedComboBoxEdit17
            // 
            this.repositoryItemCheckedComboBoxEdit17.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit17.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit17.Name = "repositoryItemCheckedComboBoxEdit17";
            this.repositoryItemCheckedComboBoxEdit17.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit18
            // 
            this.repositoryItemCheckedComboBoxEdit18.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit18.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit18.Name = "repositoryItemCheckedComboBoxEdit18";
            this.repositoryItemCheckedComboBoxEdit18.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit19
            // 
            this.repositoryItemCheckedComboBoxEdit19.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit19.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit19.Name = "repositoryItemCheckedComboBoxEdit19";
            this.repositoryItemCheckedComboBoxEdit19.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit20
            // 
            this.repositoryItemCheckedComboBoxEdit20.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit20.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit20.Name = "repositoryItemCheckedComboBoxEdit20";
            this.repositoryItemCheckedComboBoxEdit20.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit19
            // 
            this.repositoryItemDateEdit19.AutoHeight = false;
            this.repositoryItemDateEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit19.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit19.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit19.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit19.Name = "repositoryItemDateEdit19";
            // 
            // repositoryItemLookUpEdit44
            // 
            this.repositoryItemLookUpEdit44.AutoHeight = false;
            this.repositoryItemLookUpEdit44.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit44.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit44.Name = "repositoryItemLookUpEdit44";
            this.repositoryItemLookUpEdit44.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit15
            // 
            this.repositoryItemDateEdit15.AutoHeight = false;
            this.repositoryItemDateEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit15.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit15.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit15.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit15.Name = "repositoryItemDateEdit15";
            // 
            // repositoryItemMemoEdit9
            // 
            this.repositoryItemMemoEdit9.Name = "repositoryItemMemoEdit9";
            // 
            // repositoryItemComboBox31
            // 
            this.repositoryItemComboBox31.AutoHeight = false;
            this.repositoryItemComboBox31.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox31.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox31.Name = "repositoryItemComboBox31";
            this.repositoryItemComboBox31.NullText = "TL";
            this.repositoryItemComboBox31.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox27
            // 
            this.repositoryItemComboBox27.AutoHeight = false;
            this.repositoryItemComboBox27.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox27.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox27.Name = "repositoryItemComboBox27";
            this.repositoryItemComboBox27.NullText = "TL";
            this.repositoryItemComboBox27.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemCheckedComboBoxEdit15
            // 
            this.repositoryItemCheckedComboBoxEdit15.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit15.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit15.Name = "repositoryItemCheckedComboBoxEdit15";
            this.repositoryItemCheckedComboBoxEdit15.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit16
            // 
            this.repositoryItemDateEdit16.AutoHeight = false;
            this.repositoryItemDateEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit16.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit16.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit16.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit16.Name = "repositoryItemDateEdit16";
            // 
            // repositoryItemDateEdit17
            // 
            this.repositoryItemDateEdit17.AutoHeight = false;
            this.repositoryItemDateEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit17.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit17.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit17.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit17.Name = "repositoryItemDateEdit17";
            // 
            // repositoryItemCheckedComboBoxEdit14
            // 
            this.repositoryItemCheckedComboBoxEdit14.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit14.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit14.Name = "repositoryItemCheckedComboBoxEdit14";
            this.repositoryItemCheckedComboBoxEdit14.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit37
            // 
            this.repositoryItemLookUpEdit37.AutoHeight = false;
            this.repositoryItemLookUpEdit37.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit37.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit37.Name = "repositoryItemLookUpEdit37";
            this.repositoryItemLookUpEdit37.NullText = "";
            this.repositoryItemLookUpEdit37.ValueMember = "Kod";
            // 
            // layoutView13
            // 
            this.layoutView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn110,
            this.layoutViewColumn111,
            this.layoutViewColumn112,
            this.layoutViewColumn113,
            this.layoutViewColumn114,
            this.layoutViewColumn115,
            this.layoutViewColumn116,
            this.layoutViewColumn117,
            this.layoutViewColumn118});
            this.layoutView13.GridControl = this.grdSertikalar;
            this.layoutView13.Name = "layoutView13";
            this.layoutView13.OptionsSelection.MultiSelect = true;
            this.layoutView13.TemplateCard = null;
            // 
            // layoutViewColumn110
            // 
            this.layoutViewColumn110.Caption = "ID";
            this.layoutViewColumn110.FieldName = "ID";
            this.layoutViewColumn110.LayoutViewField = this.layoutViewField91;
            this.layoutViewColumn110.Name = "layoutViewColumn110";
            // 
            // layoutViewField91
            // 
            this.layoutViewField91.EditorPreferredWidth = 129;
            this.layoutViewField91.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField91.Name = "layoutViewField91";
            this.layoutViewField91.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField91.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField91.TextToControlDistance = 5;
            // 
            // layoutViewColumn111
            // 
            this.layoutViewColumn111.Caption = "Müşteri Kodu";
            this.layoutViewColumn111.FieldName = "MusteriKodu";
            this.layoutViewColumn111.LayoutViewField = this.layoutViewField92;
            this.layoutViewColumn111.Name = "layoutViewColumn111";
            this.layoutViewColumn111.Width = 84;
            // 
            // layoutViewField92
            // 
            this.layoutViewField92.EditorPreferredWidth = 129;
            this.layoutViewField92.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField92.Name = "layoutViewField92";
            this.layoutViewField92.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField92.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField92.TextToControlDistance = 5;
            // 
            // layoutViewColumn112
            // 
            this.layoutViewColumn112.Caption = "Adı";
            this.layoutViewColumn112.FieldName = "Adi";
            this.layoutViewColumn112.LayoutViewField = this.layoutViewField93;
            this.layoutViewColumn112.Name = "layoutViewColumn112";
            this.layoutViewColumn112.Width = 118;
            // 
            // layoutViewField93
            // 
            this.layoutViewField93.EditorPreferredWidth = 129;
            this.layoutViewField93.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField93.Name = "layoutViewField93";
            this.layoutViewField93.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField93.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField93.TextToControlDistance = 5;
            // 
            // layoutViewColumn113
            // 
            this.layoutViewColumn113.Caption = "Sektorü";
            this.layoutViewColumn113.FieldName = "Sektoru";
            this.layoutViewColumn113.LayoutViewField = this.layoutViewField94;
            this.layoutViewColumn113.Name = "layoutViewColumn113";
            this.layoutViewColumn113.Width = 133;
            // 
            // layoutViewField94
            // 
            this.layoutViewField94.EditorPreferredWidth = 129;
            this.layoutViewField94.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField94.Name = "layoutViewField94";
            this.layoutViewField94.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField94.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField94.TextToControlDistance = 5;
            // 
            // layoutViewColumn114
            // 
            this.layoutViewColumn114.Caption = "Ilçe";
            this.layoutViewColumn114.ColumnEdit = this.repositoryItemLookUpEdit42;
            this.layoutViewColumn114.FieldName = "Ilce";
            this.layoutViewColumn114.LayoutViewField = this.layoutViewField95;
            this.layoutViewColumn114.Name = "layoutViewColumn114";
            this.layoutViewColumn114.Width = 101;
            // 
            // layoutViewField95
            // 
            this.layoutViewField95.EditorPreferredWidth = 129;
            this.layoutViewField95.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField95.Name = "layoutViewField95";
            this.layoutViewField95.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField95.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField95.TextToControlDistance = 5;
            // 
            // layoutViewColumn115
            // 
            this.layoutViewColumn115.Caption = "Şehir";
            this.layoutViewColumn115.ColumnEdit = this.repositoryItemLookUpEdit41;
            this.layoutViewColumn115.FieldName = "SehirKodu";
            this.layoutViewColumn115.LayoutViewField = this.layoutViewField96;
            this.layoutViewColumn115.Name = "layoutViewColumn115";
            this.layoutViewColumn115.Width = 96;
            // 
            // layoutViewField96
            // 
            this.layoutViewField96.EditorPreferredWidth = 129;
            this.layoutViewField96.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField96.Name = "layoutViewField96";
            this.layoutViewField96.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField96.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField96.TextToControlDistance = 5;
            // 
            // layoutViewColumn116
            // 
            this.layoutViewColumn116.Caption = "Ülke";
            this.layoutViewColumn116.ColumnEdit = this.repositoryItemLookUpEdit40;
            this.layoutViewColumn116.FieldName = "UlkeKodu";
            this.layoutViewColumn116.LayoutViewField = this.layoutViewField97;
            this.layoutViewColumn116.Name = "layoutViewColumn116";
            this.layoutViewColumn116.Width = 104;
            // 
            // layoutViewField97
            // 
            this.layoutViewField97.EditorPreferredWidth = 129;
            this.layoutViewField97.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField97.Name = "layoutViewField97";
            this.layoutViewField97.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField97.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField97.TextToControlDistance = 5;
            // 
            // layoutViewColumn117
            // 
            this.layoutViewColumn117.Caption = "Adres";
            this.layoutViewColumn117.FieldName = "Adres";
            this.layoutViewColumn117.LayoutViewField = this.layoutViewField98;
            this.layoutViewColumn117.Name = "layoutViewColumn117";
            this.layoutViewColumn117.Width = 165;
            // 
            // layoutViewField98
            // 
            this.layoutViewField98.EditorPreferredWidth = 129;
            this.layoutViewField98.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField98.Name = "layoutViewField98";
            this.layoutViewField98.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField98.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField98.TextToControlDistance = 5;
            // 
            // layoutViewColumn118
            // 
            this.layoutViewColumn118.Caption = "Vergi Dairesi";
            this.layoutViewColumn118.FieldName = "VergiDairesi";
            this.layoutViewColumn118.LayoutViewField = this.layoutViewField99;
            this.layoutViewColumn118.Name = "layoutViewColumn118";
            this.layoutViewColumn118.Width = 87;
            // 
            // layoutViewField99
            // 
            this.layoutViewField99.EditorPreferredWidth = 129;
            this.layoutViewField99.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField99.Name = "layoutViewField99";
            this.layoutViewField99.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField99.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField99.TextToControlDistance = 5;
            // 
            // layoutView14
            // 
            this.layoutView14.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn119,
            this.layoutViewColumn120,
            this.layoutViewColumn121,
            this.layoutViewColumn122,
            this.layoutViewColumn123,
            this.layoutViewColumn124,
            this.layoutViewColumn125,
            this.layoutViewColumn126,
            this.layoutViewColumn127});
            this.layoutView14.GridControl = this.grdSertikalar;
            this.layoutView14.Name = "layoutView14";
            this.layoutView14.OptionsSelection.MultiSelect = true;
            this.layoutView14.OptionsView.ShowCardCaption = false;
            this.layoutView14.TemplateCard = null;
            // 
            // layoutViewColumn119
            // 
            this.layoutViewColumn119.Caption = "ID";
            this.layoutViewColumn119.FieldName = "ID";
            this.layoutViewColumn119.LayoutViewField = this.layoutViewField100;
            this.layoutViewColumn119.Name = "layoutViewColumn119";
            // 
            // layoutViewField100
            // 
            this.layoutViewField100.EditorPreferredWidth = 253;
            this.layoutViewField100.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField100.Name = "layoutViewField100";
            this.layoutViewField100.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField100.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField100.TextToControlDistance = 5;
            // 
            // layoutViewColumn120
            // 
            this.layoutViewColumn120.Caption = "Müşteri Kodu";
            this.layoutViewColumn120.FieldName = "MusteriKodu";
            this.layoutViewColumn120.LayoutViewField = this.layoutViewField101;
            this.layoutViewColumn120.Name = "layoutViewColumn120";
            this.layoutViewColumn120.Width = 84;
            // 
            // layoutViewField101
            // 
            this.layoutViewField101.EditorPreferredWidth = 253;
            this.layoutViewField101.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField101.Name = "layoutViewField101";
            this.layoutViewField101.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField101.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField101.TextToControlDistance = 5;
            // 
            // layoutViewColumn121
            // 
            this.layoutViewColumn121.Caption = "Adı";
            this.layoutViewColumn121.FieldName = "Adi";
            this.layoutViewColumn121.LayoutViewField = this.layoutViewField102;
            this.layoutViewColumn121.Name = "layoutViewColumn121";
            this.layoutViewColumn121.Width = 118;
            // 
            // layoutViewField102
            // 
            this.layoutViewField102.EditorPreferredWidth = 253;
            this.layoutViewField102.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField102.Name = "layoutViewField102";
            this.layoutViewField102.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField102.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField102.TextToControlDistance = 5;
            // 
            // layoutViewColumn122
            // 
            this.layoutViewColumn122.Caption = "Sektorü";
            this.layoutViewColumn122.FieldName = "Sektoru";
            this.layoutViewColumn122.LayoutViewField = this.layoutViewField103;
            this.layoutViewColumn122.Name = "layoutViewColumn122";
            this.layoutViewColumn122.Width = 133;
            // 
            // layoutViewField103
            // 
            this.layoutViewField103.EditorPreferredWidth = 253;
            this.layoutViewField103.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField103.Name = "layoutViewField103";
            this.layoutViewField103.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField103.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField103.TextToControlDistance = 5;
            // 
            // layoutViewColumn123
            // 
            this.layoutViewColumn123.Caption = "Ilçe";
            this.layoutViewColumn123.ColumnEdit = this.repositoryItemLookUpEdit42;
            this.layoutViewColumn123.FieldName = "Ilce";
            this.layoutViewColumn123.LayoutViewField = this.layoutViewField104;
            this.layoutViewColumn123.Name = "layoutViewColumn123";
            this.layoutViewColumn123.Width = 101;
            // 
            // layoutViewField104
            // 
            this.layoutViewField104.EditorPreferredWidth = 253;
            this.layoutViewField104.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField104.Name = "layoutViewField104";
            this.layoutViewField104.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField104.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField104.TextToControlDistance = 5;
            // 
            // layoutViewColumn124
            // 
            this.layoutViewColumn124.Caption = "Şehir";
            this.layoutViewColumn124.ColumnEdit = this.repositoryItemLookUpEdit41;
            this.layoutViewColumn124.FieldName = "SehirKodu";
            this.layoutViewColumn124.LayoutViewField = this.layoutViewField105;
            this.layoutViewColumn124.Name = "layoutViewColumn124";
            this.layoutViewColumn124.Width = 96;
            // 
            // layoutViewField105
            // 
            this.layoutViewField105.EditorPreferredWidth = 253;
            this.layoutViewField105.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField105.Name = "layoutViewField105";
            this.layoutViewField105.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField105.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField105.TextToControlDistance = 5;
            // 
            // layoutViewColumn125
            // 
            this.layoutViewColumn125.Caption = "Ülke";
            this.layoutViewColumn125.ColumnEdit = this.repositoryItemLookUpEdit40;
            this.layoutViewColumn125.FieldName = "UlkeKodu";
            this.layoutViewColumn125.LayoutViewField = this.layoutViewField106;
            this.layoutViewColumn125.Name = "layoutViewColumn125";
            this.layoutViewColumn125.Width = 104;
            // 
            // layoutViewField106
            // 
            this.layoutViewField106.EditorPreferredWidth = 253;
            this.layoutViewField106.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField106.Name = "layoutViewField106";
            this.layoutViewField106.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField106.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField106.TextToControlDistance = 5;
            // 
            // layoutViewColumn126
            // 
            this.layoutViewColumn126.Caption = "Adres";
            this.layoutViewColumn126.FieldName = "Adres";
            this.layoutViewColumn126.LayoutViewField = this.layoutViewField107;
            this.layoutViewColumn126.Name = "layoutViewColumn126";
            this.layoutViewColumn126.Width = 165;
            // 
            // layoutViewField107
            // 
            this.layoutViewField107.EditorPreferredWidth = 253;
            this.layoutViewField107.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField107.Name = "layoutViewField107";
            this.layoutViewField107.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField107.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField107.TextToControlDistance = 5;
            // 
            // layoutViewColumn127
            // 
            this.layoutViewColumn127.Caption = "Vergi Dairesi";
            this.layoutViewColumn127.FieldName = "VergiDairesi";
            this.layoutViewColumn127.LayoutViewField = this.layoutViewField108;
            this.layoutViewColumn127.Name = "layoutViewColumn127";
            this.layoutViewColumn127.Width = 87;
            // 
            // layoutViewField108
            // 
            this.layoutViewField108.EditorPreferredWidth = 253;
            this.layoutViewField108.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField108.Name = "layoutViewField108";
            this.layoutViewField108.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField108.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField108.TextToControlDistance = 5;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.grpKullanilanYazi);
            this.xtraTabPage5.Controls.Add(this.standaloneBarDockControl10);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1352, 383);
            this.xtraTabPage5.Text = "Kullanılan Yazılımlar";
            // 
            // grpKullanilanYazi
            // 
            this.grpKullanilanYazi.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 14F);
            this.grpKullanilanYazi.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpKullanilanYazi.AppearanceCaption.Options.UseFont = true;
            this.grpKullanilanYazi.AppearanceCaption.Options.UseForeColor = true;
            this.grpKullanilanYazi.Controls.Add(this.panelControl3);
            this.grpKullanilanYazi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpKullanilanYazi.Location = new System.Drawing.Point(0, 26);
            this.grpKullanilanYazi.Name = "grpKullanilanYazi";
            this.grpKullanilanYazi.Size = new System.Drawing.Size(1352, 357);
            this.grpKullanilanYazi.TabIndex = 18;
            this.grpKullanilanYazi.Text = "...";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.grdKullanilanYaz);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 31);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1348, 324);
            this.panelControl3.TabIndex = 15;
            // 
            // grdKullanilanYaz
            // 
            this.grdKullanilanYaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdKullanilanYaz.Location = new System.Drawing.Point(0, 0);
            this.grdKullanilanYaz.MainView = this.grdVKullanilanYaz;
            this.grdKullanilanYaz.Name = "grdKullanilanYaz";
            this.grdKullanilanYaz.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox34,
            this.repositoryItemComboBox35,
            this.repositoryItemComboBox36,
            this.repositoryItemLookUpEdit46,
            this.repositoryItemLookUpEdit47,
            this.repositoryItemPictureEdit17,
            this.repositoryItemImageEdit9,
            this.repositoryItemPictureEdit18,
            this.repositoryItemLookUpEdit48,
            this.repositoryItemLookUpEdit49,
            this.repositoryItemLookUpEdit50,
            this.repositoryItemLookUpEdit51,
            this.repositoryItemCheckedComboBoxEdit21,
            this.repositoryItemMemoEdit10,
            this.repositoryItemDateEdit20,
            this.repositoryItemCheckedComboBoxEdit22,
            this.repositoryItemCheckedComboBoxEdit23,
            this.repositoryItemCheckedComboBoxEdit24,
            this.repositoryItemCheckedComboBoxEdit25,
            this.repositoryItemDateEdit21,
            this.repositoryItemLookUpEdit52,
            this.repositoryItemDateEdit22,
            this.repositoryItemMemoEdit11,
            this.repositoryItemComboBox37,
            this.repositoryItemComboBox38,
            this.repositoryItemCheckedComboBoxEdit26,
            this.repositoryItemDateEdit23,
            this.repositoryItemDateEdit24,
            this.repositoryItemCheckedComboBoxEdit27,
            this.repositoryItemLookUpEdit53,
            this.repositoryItemComboBox33,
            this.repositoryItemLookUpEdit45,
            this.repYazilimMarka,
            this.repositoryItemTextEdit7,
            this.repYazilimGrup});
            this.grdKullanilanYaz.Size = new System.Drawing.Size(1348, 324);
            this.grdKullanilanYaz.TabIndex = 13;
            this.grdKullanilanYaz.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVKullanilanYaz,
            this.layoutView15,
            this.layoutView16});
            // 
            // grdVKullanilanYaz
            // 
            this.grdVKullanilanYaz.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cY_Id,
            this.cY_Tipi,
            this.ccY_Marka,
            this.cY_Adi,
            this.cY_KullanimOrani,
            this.cYFirmaKodu});
            this.grdVKullanilanYaz.GridControl = this.grdKullanilanYaz;
            this.grdVKullanilanYaz.Images = this.SmallImage;
            this.grdVKullanilanYaz.Name = "grdVKullanilanYaz";
            this.grdVKullanilanYaz.OptionsBehavior.FocusLeaveOnTab = true;
            this.grdVKullanilanYaz.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVKullanilanYaz.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVKullanilanYaz.OptionsSelection.MultiSelect = true;
            this.grdVKullanilanYaz.OptionsView.ColumnAutoWidth = false;
            this.grdVKullanilanYaz.OptionsView.RowAutoHeight = true;
            this.grdVKullanilanYaz.OptionsView.ShowAutoFilterRow = true;
            this.grdVKullanilanYaz.OptionsView.ShowGroupPanel = false;
            // 
            // cY_Id
            // 
            this.cY_Id.Caption = "ID";
            this.cY_Id.FieldName = "Y_Id";
            this.cY_Id.Name = "cY_Id";
            // 
            // cY_Tipi
            // 
            this.cY_Tipi.Caption = "Grup";
            this.cY_Tipi.ColumnEdit = this.repYazilimGrup;
            this.cY_Tipi.FieldName = "Y_Tipi";
            this.cY_Tipi.Name = "cY_Tipi";
            this.cY_Tipi.Visible = true;
            this.cY_Tipi.VisibleIndex = 0;
            // 
            // repYazilimGrup
            // 
            this.repYazilimGrup.AutoHeight = false;
            this.repYazilimGrup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repYazilimGrup.DisplayMember = "Tanim";
            this.repYazilimGrup.Name = "repYazilimGrup";
            this.repYazilimGrup.NullText = "";
            this.repYazilimGrup.ValueMember = "Kod";
            // 
            // ccY_Marka
            // 
            this.ccY_Marka.Caption = "Marka";
            this.ccY_Marka.ColumnEdit = this.repYazilimMarka;
            this.ccY_Marka.FieldName = "Y_Marka";
            this.ccY_Marka.Name = "ccY_Marka";
            this.ccY_Marka.Visible = true;
            this.ccY_Marka.VisibleIndex = 1;
            // 
            // repYazilimMarka
            // 
            this.repYazilimMarka.AutoHeight = false;
            this.repYazilimMarka.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repYazilimMarka.DisplayMember = "Tanim";
            this.repYazilimMarka.Name = "repYazilimMarka";
            this.repYazilimMarka.NullText = "";
            this.repYazilimMarka.ValueMember = "Kod";
            // 
            // cY_Adi
            // 
            this.cY_Adi.Caption = "Yazilim Adı";
            this.cY_Adi.FieldName = "Y_Adi";
            this.cY_Adi.Name = "cY_Adi";
            this.cY_Adi.Visible = true;
            this.cY_Adi.VisibleIndex = 2;
            this.cY_Adi.Width = 228;
            // 
            // cY_KullanimOrani
            // 
            this.cY_KullanimOrani.Caption = "Yazılım kullanım oranı";
            this.cY_KullanimOrani.ColumnEdit = this.repositoryItemTextEdit7;
            this.cY_KullanimOrani.DisplayFormat.FormatString = "p";
            this.cY_KullanimOrani.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.cY_KullanimOrani.FieldName = "Y_KullanimOrani";
            this.cY_KullanimOrani.Name = "cY_KullanimOrani";
            this.cY_KullanimOrani.ToolTip = "Yazılım kullanım oranı";
            this.cY_KullanimOrani.Visible = true;
            this.cY_KullanimOrani.VisibleIndex = 3;
            this.cY_KullanimOrani.Width = 113;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "p";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // cYFirmaKodu
            // 
            this.cYFirmaKodu.Caption = "FirmaKodu";
            this.cYFirmaKodu.FieldName = "YFirmaKodu";
            this.cYFirmaKodu.Name = "cYFirmaKodu";
            // 
            // repositoryItemComboBox34
            // 
            this.repositoryItemComboBox34.AutoHeight = false;
            this.repositoryItemComboBox34.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox34.Name = "repositoryItemComboBox34";
            // 
            // repositoryItemComboBox35
            // 
            this.repositoryItemComboBox35.AutoHeight = false;
            this.repositoryItemComboBox35.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox35.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox35.Name = "repositoryItemComboBox35";
            this.repositoryItemComboBox35.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox36
            // 
            this.repositoryItemComboBox36.AutoHeight = false;
            this.repositoryItemComboBox36.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox36.Name = "repositoryItemComboBox36";
            // 
            // repositoryItemLookUpEdit46
            // 
            this.repositoryItemLookUpEdit46.AutoHeight = false;
            this.repositoryItemLookUpEdit46.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit46.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit46.Name = "repositoryItemLookUpEdit46";
            this.repositoryItemLookUpEdit46.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit47
            // 
            this.repositoryItemLookUpEdit47.AutoHeight = false;
            this.repositoryItemLookUpEdit47.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit47.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit47.Name = "repositoryItemLookUpEdit47";
            this.repositoryItemLookUpEdit47.NullText = "";
            this.repositoryItemLookUpEdit47.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit17
            // 
            this.repositoryItemPictureEdit17.Name = "repositoryItemPictureEdit17";
            // 
            // repositoryItemImageEdit9
            // 
            this.repositoryItemImageEdit9.AutoHeight = false;
            this.repositoryItemImageEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit9.Name = "repositoryItemImageEdit9";
            // 
            // repositoryItemPictureEdit18
            // 
            this.repositoryItemPictureEdit18.Name = "repositoryItemPictureEdit18";
            // 
            // repositoryItemLookUpEdit48
            // 
            this.repositoryItemLookUpEdit48.AutoHeight = false;
            this.repositoryItemLookUpEdit48.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit48.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit48.Name = "repositoryItemLookUpEdit48";
            this.repositoryItemLookUpEdit48.NullText = "";
            this.repositoryItemLookUpEdit48.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit49
            // 
            this.repositoryItemLookUpEdit49.AutoHeight = false;
            this.repositoryItemLookUpEdit49.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit49.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit49.Name = "repositoryItemLookUpEdit49";
            this.repositoryItemLookUpEdit49.NullText = "";
            this.repositoryItemLookUpEdit49.ValueMember = "Kod";
            // 
            // repositoryItemLookUpEdit50
            // 
            this.repositoryItemLookUpEdit50.AutoHeight = false;
            this.repositoryItemLookUpEdit50.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit50.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit50.Name = "repositoryItemLookUpEdit50";
            this.repositoryItemLookUpEdit50.NullText = "";
            this.repositoryItemLookUpEdit50.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit51
            // 
            this.repositoryItemLookUpEdit51.AutoHeight = false;
            this.repositoryItemLookUpEdit51.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit51.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit51.DropDownRows = 20;
            this.repositoryItemLookUpEdit51.Name = "repositoryItemLookUpEdit51";
            this.repositoryItemLookUpEdit51.NullText = "";
            this.repositoryItemLookUpEdit51.PopupWidth = 600;
            this.repositoryItemLookUpEdit51.ValueMember = "Kod";
            // 
            // repositoryItemCheckedComboBoxEdit21
            // 
            this.repositoryItemCheckedComboBoxEdit21.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit21.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit21.Name = "repositoryItemCheckedComboBoxEdit21";
            this.repositoryItemCheckedComboBoxEdit21.ValueMember = "Tanim";
            // 
            // repositoryItemMemoEdit10
            // 
            this.repositoryItemMemoEdit10.Appearance.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit10.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit10.AppearanceDisabled.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit10.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit10.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit10.AppearanceFocused.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit10.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit10.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit10.AppearanceReadOnly.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit10.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemMemoEdit10.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit10.Name = "repositoryItemMemoEdit10";
            // 
            // repositoryItemDateEdit20
            // 
            this.repositoryItemDateEdit20.AutoHeight = false;
            this.repositoryItemDateEdit20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit20.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit20.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit20.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit20.Name = "repositoryItemDateEdit20";
            // 
            // repositoryItemCheckedComboBoxEdit22
            // 
            this.repositoryItemCheckedComboBoxEdit22.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit22.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit22.Name = "repositoryItemCheckedComboBoxEdit22";
            this.repositoryItemCheckedComboBoxEdit22.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit23
            // 
            this.repositoryItemCheckedComboBoxEdit23.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit23.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit23.Name = "repositoryItemCheckedComboBoxEdit23";
            this.repositoryItemCheckedComboBoxEdit23.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit24
            // 
            this.repositoryItemCheckedComboBoxEdit24.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit24.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit24.Name = "repositoryItemCheckedComboBoxEdit24";
            this.repositoryItemCheckedComboBoxEdit24.ValueMember = "Tanim";
            // 
            // repositoryItemCheckedComboBoxEdit25
            // 
            this.repositoryItemCheckedComboBoxEdit25.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit25.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit25.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit25.Name = "repositoryItemCheckedComboBoxEdit25";
            this.repositoryItemCheckedComboBoxEdit25.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit21
            // 
            this.repositoryItemDateEdit21.AutoHeight = false;
            this.repositoryItemDateEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit21.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit21.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit21.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit21.Name = "repositoryItemDateEdit21";
            // 
            // repositoryItemLookUpEdit52
            // 
            this.repositoryItemLookUpEdit52.AutoHeight = false;
            this.repositoryItemLookUpEdit52.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit52.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit52.Name = "repositoryItemLookUpEdit52";
            this.repositoryItemLookUpEdit52.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit22
            // 
            this.repositoryItemDateEdit22.AutoHeight = false;
            this.repositoryItemDateEdit22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit22.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit22.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit22.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit22.Name = "repositoryItemDateEdit22";
            // 
            // repositoryItemMemoEdit11
            // 
            this.repositoryItemMemoEdit11.Name = "repositoryItemMemoEdit11";
            // 
            // repositoryItemComboBox37
            // 
            this.repositoryItemComboBox37.AutoHeight = false;
            this.repositoryItemComboBox37.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox37.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox37.Name = "repositoryItemComboBox37";
            this.repositoryItemComboBox37.NullText = "TL";
            this.repositoryItemComboBox37.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox38
            // 
            this.repositoryItemComboBox38.AutoHeight = false;
            this.repositoryItemComboBox38.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox38.Items.AddRange(new object[] {
            "TL",
            "EUR",
            "USD"});
            this.repositoryItemComboBox38.Name = "repositoryItemComboBox38";
            this.repositoryItemComboBox38.NullText = "TL";
            this.repositoryItemComboBox38.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemCheckedComboBoxEdit26
            // 
            this.repositoryItemCheckedComboBoxEdit26.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit26.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit26.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit26.Name = "repositoryItemCheckedComboBoxEdit26";
            this.repositoryItemCheckedComboBoxEdit26.ValueMember = "Tanim";
            // 
            // repositoryItemDateEdit23
            // 
            this.repositoryItemDateEdit23.AutoHeight = false;
            this.repositoryItemDateEdit23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit23.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit23.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit23.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit23.Name = "repositoryItemDateEdit23";
            // 
            // repositoryItemDateEdit24
            // 
            this.repositoryItemDateEdit24.AutoHeight = false;
            this.repositoryItemDateEdit24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit24.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit24.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit24.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit24.Name = "repositoryItemDateEdit24";
            // 
            // repositoryItemCheckedComboBoxEdit27
            // 
            this.repositoryItemCheckedComboBoxEdit27.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit27.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit27.DisplayMember = "Tanim";
            this.repositoryItemCheckedComboBoxEdit27.Name = "repositoryItemCheckedComboBoxEdit27";
            this.repositoryItemCheckedComboBoxEdit27.ValueMember = "Tanim";
            // 
            // repositoryItemLookUpEdit53
            // 
            this.repositoryItemLookUpEdit53.AutoHeight = false;
            this.repositoryItemLookUpEdit53.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit53.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit53.Name = "repositoryItemLookUpEdit53";
            this.repositoryItemLookUpEdit53.NullText = "";
            this.repositoryItemLookUpEdit53.ValueMember = "Kod";
            // 
            // repositoryItemComboBox33
            // 
            this.repositoryItemComboBox33.AutoHeight = false;
            this.repositoryItemComboBox33.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox33.Items.AddRange(new object[] {
            "Aktif",
            "Pasif"});
            this.repositoryItemComboBox33.Name = "repositoryItemComboBox33";
            // 
            // repositoryItemLookUpEdit45
            // 
            this.repositoryItemLookUpEdit45.AutoHeight = false;
            this.repositoryItemLookUpEdit45.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit45.DisplayMember = "Tanim";
            this.repositoryItemLookUpEdit45.Name = "repositoryItemLookUpEdit45";
            this.repositoryItemLookUpEdit45.NullText = "";
            this.repositoryItemLookUpEdit45.ValueMember = "Kod";
            // 
            // layoutView15
            // 
            this.layoutView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn128,
            this.layoutViewColumn129,
            this.layoutViewColumn130,
            this.layoutViewColumn131,
            this.layoutViewColumn132,
            this.layoutViewColumn133,
            this.layoutViewColumn134,
            this.layoutViewColumn135,
            this.layoutViewColumn136});
            this.layoutView15.GridControl = this.grdKullanilanYaz;
            this.layoutView15.Name = "layoutView15";
            this.layoutView15.OptionsSelection.MultiSelect = true;
            this.layoutView15.TemplateCard = null;
            // 
            // layoutViewColumn128
            // 
            this.layoutViewColumn128.Caption = "ID";
            this.layoutViewColumn128.FieldName = "ID";
            this.layoutViewColumn128.LayoutViewField = this.layoutViewField109;
            this.layoutViewColumn128.Name = "layoutViewColumn128";
            // 
            // layoutViewField109
            // 
            this.layoutViewField109.EditorPreferredWidth = 129;
            this.layoutViewField109.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField109.Name = "layoutViewField109";
            this.layoutViewField109.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField109.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField109.TextToControlDistance = 5;
            // 
            // layoutViewColumn129
            // 
            this.layoutViewColumn129.Caption = "Müşteri Kodu";
            this.layoutViewColumn129.FieldName = "MusteriKodu";
            this.layoutViewColumn129.LayoutViewField = this.layoutViewField110;
            this.layoutViewColumn129.Name = "layoutViewColumn129";
            this.layoutViewColumn129.Width = 84;
            // 
            // layoutViewField110
            // 
            this.layoutViewField110.EditorPreferredWidth = 129;
            this.layoutViewField110.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField110.Name = "layoutViewField110";
            this.layoutViewField110.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField110.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField110.TextToControlDistance = 5;
            // 
            // layoutViewColumn130
            // 
            this.layoutViewColumn130.Caption = "Adı";
            this.layoutViewColumn130.FieldName = "Adi";
            this.layoutViewColumn130.LayoutViewField = this.layoutViewField111;
            this.layoutViewColumn130.Name = "layoutViewColumn130";
            this.layoutViewColumn130.Width = 118;
            // 
            // layoutViewField111
            // 
            this.layoutViewField111.EditorPreferredWidth = 129;
            this.layoutViewField111.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField111.Name = "layoutViewField111";
            this.layoutViewField111.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField111.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField111.TextToControlDistance = 5;
            // 
            // layoutViewColumn131
            // 
            this.layoutViewColumn131.Caption = "Sektorü";
            this.layoutViewColumn131.FieldName = "Sektoru";
            this.layoutViewColumn131.LayoutViewField = this.layoutViewField112;
            this.layoutViewColumn131.Name = "layoutViewColumn131";
            this.layoutViewColumn131.Width = 133;
            // 
            // layoutViewField112
            // 
            this.layoutViewField112.EditorPreferredWidth = 129;
            this.layoutViewField112.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField112.Name = "layoutViewField112";
            this.layoutViewField112.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField112.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField112.TextToControlDistance = 5;
            // 
            // layoutViewColumn132
            // 
            this.layoutViewColumn132.Caption = "Ilçe";
            this.layoutViewColumn132.ColumnEdit = this.repositoryItemLookUpEdit50;
            this.layoutViewColumn132.FieldName = "Ilce";
            this.layoutViewColumn132.LayoutViewField = this.layoutViewField113;
            this.layoutViewColumn132.Name = "layoutViewColumn132";
            this.layoutViewColumn132.Width = 101;
            // 
            // layoutViewField113
            // 
            this.layoutViewField113.EditorPreferredWidth = 129;
            this.layoutViewField113.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField113.Name = "layoutViewField113";
            this.layoutViewField113.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField113.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField113.TextToControlDistance = 5;
            // 
            // layoutViewColumn133
            // 
            this.layoutViewColumn133.Caption = "Şehir";
            this.layoutViewColumn133.ColumnEdit = this.repositoryItemLookUpEdit49;
            this.layoutViewColumn133.FieldName = "SehirKodu";
            this.layoutViewColumn133.LayoutViewField = this.layoutViewField114;
            this.layoutViewColumn133.Name = "layoutViewColumn133";
            this.layoutViewColumn133.Width = 96;
            // 
            // layoutViewField114
            // 
            this.layoutViewField114.EditorPreferredWidth = 129;
            this.layoutViewField114.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField114.Name = "layoutViewField114";
            this.layoutViewField114.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField114.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField114.TextToControlDistance = 5;
            // 
            // layoutViewColumn134
            // 
            this.layoutViewColumn134.Caption = "Ülke";
            this.layoutViewColumn134.ColumnEdit = this.repositoryItemLookUpEdit48;
            this.layoutViewColumn134.FieldName = "UlkeKodu";
            this.layoutViewColumn134.LayoutViewField = this.layoutViewField115;
            this.layoutViewColumn134.Name = "layoutViewColumn134";
            this.layoutViewColumn134.Width = 104;
            // 
            // layoutViewField115
            // 
            this.layoutViewField115.EditorPreferredWidth = 129;
            this.layoutViewField115.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField115.Name = "layoutViewField115";
            this.layoutViewField115.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField115.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField115.TextToControlDistance = 5;
            // 
            // layoutViewColumn135
            // 
            this.layoutViewColumn135.Caption = "Adres";
            this.layoutViewColumn135.FieldName = "Adres";
            this.layoutViewColumn135.LayoutViewField = this.layoutViewField116;
            this.layoutViewColumn135.Name = "layoutViewColumn135";
            this.layoutViewColumn135.Width = 165;
            // 
            // layoutViewField116
            // 
            this.layoutViewField116.EditorPreferredWidth = 129;
            this.layoutViewField116.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField116.Name = "layoutViewField116";
            this.layoutViewField116.Size = new System.Drawing.Size(204, 20);
            this.layoutViewField116.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField116.TextToControlDistance = 5;
            // 
            // layoutViewColumn136
            // 
            this.layoutViewColumn136.Caption = "Vergi Dairesi";
            this.layoutViewColumn136.FieldName = "VergiDairesi";
            this.layoutViewColumn136.LayoutViewField = this.layoutViewField117;
            this.layoutViewColumn136.Name = "layoutViewColumn136";
            this.layoutViewColumn136.Width = 87;
            // 
            // layoutViewField117
            // 
            this.layoutViewField117.EditorPreferredWidth = 129;
            this.layoutViewField117.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField117.Name = "layoutViewField117";
            this.layoutViewField117.Size = new System.Drawing.Size(204, 92);
            this.layoutViewField117.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField117.TextToControlDistance = 5;
            // 
            // layoutView16
            // 
            this.layoutView16.CardMinSize = new System.Drawing.Size(324, 296);
            this.layoutView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn137,
            this.layoutViewColumn138,
            this.layoutViewColumn139,
            this.layoutViewColumn140,
            this.layoutViewColumn141,
            this.layoutViewColumn142,
            this.layoutViewColumn143,
            this.layoutViewColumn144,
            this.layoutViewColumn145});
            this.layoutView16.GridControl = this.grdKullanilanYaz;
            this.layoutView16.Name = "layoutView16";
            this.layoutView16.OptionsSelection.MultiSelect = true;
            this.layoutView16.OptionsView.ShowCardCaption = false;
            this.layoutView16.TemplateCard = null;
            // 
            // layoutViewColumn137
            // 
            this.layoutViewColumn137.Caption = "ID";
            this.layoutViewColumn137.FieldName = "ID";
            this.layoutViewColumn137.LayoutViewField = this.layoutViewField118;
            this.layoutViewColumn137.Name = "layoutViewColumn137";
            // 
            // layoutViewField118
            // 
            this.layoutViewField118.EditorPreferredWidth = 253;
            this.layoutViewField118.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField118.Name = "layoutViewField118";
            this.layoutViewField118.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField118.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField118.TextToControlDistance = 5;
            // 
            // layoutViewColumn138
            // 
            this.layoutViewColumn138.Caption = "Müşteri Kodu";
            this.layoutViewColumn138.FieldName = "MusteriKodu";
            this.layoutViewColumn138.LayoutViewField = this.layoutViewField119;
            this.layoutViewColumn138.Name = "layoutViewColumn138";
            this.layoutViewColumn138.Width = 84;
            // 
            // layoutViewField119
            // 
            this.layoutViewField119.EditorPreferredWidth = 253;
            this.layoutViewField119.Location = new System.Drawing.Point(0, 20);
            this.layoutViewField119.Name = "layoutViewField119";
            this.layoutViewField119.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField119.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField119.TextToControlDistance = 5;
            // 
            // layoutViewColumn139
            // 
            this.layoutViewColumn139.Caption = "Adı";
            this.layoutViewColumn139.FieldName = "Adi";
            this.layoutViewColumn139.LayoutViewField = this.layoutViewField120;
            this.layoutViewColumn139.Name = "layoutViewColumn139";
            this.layoutViewColumn139.Width = 118;
            // 
            // layoutViewField120
            // 
            this.layoutViewField120.EditorPreferredWidth = 253;
            this.layoutViewField120.Location = new System.Drawing.Point(0, 40);
            this.layoutViewField120.Name = "layoutViewField120";
            this.layoutViewField120.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField120.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField120.TextToControlDistance = 5;
            // 
            // layoutViewColumn140
            // 
            this.layoutViewColumn140.Caption = "Sektorü";
            this.layoutViewColumn140.FieldName = "Sektoru";
            this.layoutViewColumn140.LayoutViewField = this.layoutViewField121;
            this.layoutViewColumn140.Name = "layoutViewColumn140";
            this.layoutViewColumn140.Width = 133;
            // 
            // layoutViewField121
            // 
            this.layoutViewField121.EditorPreferredWidth = 253;
            this.layoutViewField121.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField121.Name = "layoutViewField121";
            this.layoutViewField121.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField121.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField121.TextToControlDistance = 5;
            // 
            // layoutViewColumn141
            // 
            this.layoutViewColumn141.Caption = "Ilçe";
            this.layoutViewColumn141.ColumnEdit = this.repositoryItemLookUpEdit50;
            this.layoutViewColumn141.FieldName = "Ilce";
            this.layoutViewColumn141.LayoutViewField = this.layoutViewField122;
            this.layoutViewColumn141.Name = "layoutViewColumn141";
            this.layoutViewColumn141.Width = 101;
            // 
            // layoutViewField122
            // 
            this.layoutViewField122.EditorPreferredWidth = 253;
            this.layoutViewField122.Location = new System.Drawing.Point(0, 80);
            this.layoutViewField122.Name = "layoutViewField122";
            this.layoutViewField122.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField122.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField122.TextToControlDistance = 5;
            // 
            // layoutViewColumn142
            // 
            this.layoutViewColumn142.Caption = "Şehir";
            this.layoutViewColumn142.ColumnEdit = this.repositoryItemLookUpEdit49;
            this.layoutViewColumn142.FieldName = "SehirKodu";
            this.layoutViewColumn142.LayoutViewField = this.layoutViewField123;
            this.layoutViewColumn142.Name = "layoutViewColumn142";
            this.layoutViewColumn142.Width = 96;
            // 
            // layoutViewField123
            // 
            this.layoutViewField123.EditorPreferredWidth = 253;
            this.layoutViewField123.Location = new System.Drawing.Point(0, 100);
            this.layoutViewField123.Name = "layoutViewField123";
            this.layoutViewField123.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField123.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField123.TextToControlDistance = 5;
            // 
            // layoutViewColumn143
            // 
            this.layoutViewColumn143.Caption = "Ülke";
            this.layoutViewColumn143.ColumnEdit = this.repositoryItemLookUpEdit48;
            this.layoutViewColumn143.FieldName = "UlkeKodu";
            this.layoutViewColumn143.LayoutViewField = this.layoutViewField124;
            this.layoutViewColumn143.Name = "layoutViewColumn143";
            this.layoutViewColumn143.Width = 104;
            // 
            // layoutViewField124
            // 
            this.layoutViewField124.EditorPreferredWidth = 253;
            this.layoutViewField124.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField124.Name = "layoutViewField124";
            this.layoutViewField124.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField124.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField124.TextToControlDistance = 5;
            // 
            // layoutViewColumn144
            // 
            this.layoutViewColumn144.Caption = "Adres";
            this.layoutViewColumn144.FieldName = "Adres";
            this.layoutViewColumn144.LayoutViewField = this.layoutViewField125;
            this.layoutViewColumn144.Name = "layoutViewColumn144";
            this.layoutViewColumn144.Width = 165;
            // 
            // layoutViewField125
            // 
            this.layoutViewField125.EditorPreferredWidth = 253;
            this.layoutViewField125.Location = new System.Drawing.Point(0, 140);
            this.layoutViewField125.Name = "layoutViewField125";
            this.layoutViewField125.Size = new System.Drawing.Size(328, 20);
            this.layoutViewField125.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField125.TextToControlDistance = 5;
            // 
            // layoutViewColumn145
            // 
            this.layoutViewColumn145.Caption = "Vergi Dairesi";
            this.layoutViewColumn145.FieldName = "VergiDairesi";
            this.layoutViewColumn145.LayoutViewField = this.layoutViewField126;
            this.layoutViewColumn145.Name = "layoutViewColumn145";
            this.layoutViewColumn145.Width = 87;
            // 
            // layoutViewField126
            // 
            this.layoutViewField126.EditorPreferredWidth = 253;
            this.layoutViewField126.Location = new System.Drawing.Point(0, 160);
            this.layoutViewField126.Name = "layoutViewField126";
            this.layoutViewField126.Size = new System.Drawing.Size(328, 111);
            this.layoutViewField126.TextSize = new System.Drawing.Size(66, 13);
            this.layoutViewField126.TextToControlDistance = 5;
            // 
            // Firmalar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 436);
            this.Controls.Add(this.tbMain);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Firmalar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.Firmalar_Activated);
            this.Load += new System.EventHandler(this.frmUlkeler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSektoru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupIlce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cIlce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupSehir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cSehirKodu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupUlke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cUlkeKodu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cAdres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cVergiDairesi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdMusteriListesi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVMusteriListesi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSegmantas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSektor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupNereden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepMemNereden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repZiyNedeni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colMusteriKodu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdi1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSektoru1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colIlce1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colSehirKodu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colUlkeKodu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colAdres1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colVergiDairesi1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMain)).EndInit();
            this.tbMain.ResumeLayout(false);
            this.tbMusList.ResumeLayout(false);
            this.tbYetkiliKisi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpYetkiliKisiler)).EndInit();
            this.grpYetkiliKisiler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdYetkiliKisiler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvYetkiliKisiler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYetkiliStatu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_ADI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_SOYADI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_GOREVI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_EMAIL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).EndInit();
            this.tbZiyaretler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpZiyaret)).EndInit();
            this.grpZiyaret.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdZiyaretler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVZiyaretler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarih.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarih)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckUpTar.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckUpTar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGorusulenKisiler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGorusenKisiler2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAltGrup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repZiyNedeniM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPromosyon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repGorusenKisiler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField18)).EndInit();
            this.tbTeklifler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTeklifler)).EndInit();
            this.grpTeklifler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTeklifler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVTeklifler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemChk1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTekChkTeklifAnaGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTekChkTeklifAltGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkTip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkHazirlayan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkLSonuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkNedenler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkTekSonuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCmbKisi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAltGrupTeklif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrupTeklif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSegman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField36)).EndInit();
            this.tbSozlesme.ResumeLayout(false);
            this.tbSozlesme.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSozlesmeler)).EndInit();
            this.grpSozlesmeler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSozlesmeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVSozlesmeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSozlTeklNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repMem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSozIcerik)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repBr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkSatYap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField54)).EndInit();
            this.tbOdemeler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpOdemeler)).EndInit();
            this.grpOdemeler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdOdemeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVOdemeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkOdemeSozNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLFaturaTipi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repFatBr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLOdmSekli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLUlsSekli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLKnkSekli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYmkSekl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit12.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit13.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit9.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit10.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit11.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField72)).EndInit();
            this.tbProjeler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpProjeler)).EndInit();
            this.grpProjeler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlProje)).EndInit();
            this.pnlProje.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbMainProjeler)).EndInit();
            this.tbMainProjeler.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProjeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVProjeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkSozNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrupProje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTar.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zProjeDetay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repProMem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField90)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProjeDetay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVProjeDetay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAltGrupProje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkDanisman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarProDet.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTarProDet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAnaGrupProje1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit14.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit14)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pvtKarneNotlari)).EndInit();
            this.tbilaveBilgi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbEk)).EndInit();
            this.tbEk.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpSertifika)).EndInit();
            this.grpSertifika.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdSertikalar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVSertikalar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSertifakalar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit18.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit19.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit15.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit16.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit17.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField108)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpKullanilanYazi)).EndInit();
            this.grpKullanilanYazi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdKullanilanYaz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVKullanilanYaz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYazilimGrup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYazilimMarka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit20.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit21.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit22.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit23.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit24.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField126)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraTab.XtraTabControl tbMain;
        private DevExpress.XtraTab.XtraTabPage tbMusList;
        private DevExpress.XtraTab.XtraTabPage tbYetkiliKisi;
        private DevExpress.XtraGrid.GridControl grdMusteriListesi;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVMusteriListesi;
        private DevExpress.XtraGrid.Columns.GridColumn cID;
        private DevExpress.XtraGrid.Columns.GridColumn cMusteriKodu;
        private DevExpress.XtraGrid.Columns.GridColumn cAdi;
        private DevExpress.XtraGrid.Columns.GridColumn cSektoru;
        private DevExpress.XtraGrid.Columns.GridColumn cIlce;
        private DevExpress.XtraGrid.Columns.GridColumn cSehirKodu;
        private DevExpress.XtraGrid.Columns.GridColumn cUlkeKodu;
        private DevExpress.XtraGrid.Columns.GridColumn cAdres;
        private DevExpress.XtraGrid.Columns.GridColumn cVergiDairesi;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraTab.XtraTabPage tbZiyaretler;
        private DevExpress.XtraTab.XtraTabPage tbSozlesme;
        private DevExpress.XtraTab.XtraTabPage tbProjeler;
        private DevExpress.XtraTab.XtraTabPage tbilaveBilgi;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupUlke;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupIlce;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupSehir;
        private DevExpress.XtraEditors.GroupControl grpYetkiliKisiler;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnYetkiliKisiEkle;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraGrid.GridControl grdYetkiliKisiler;
        private DevExpress.XtraGrid.Views.Layout.LayoutView lvYetkiliKisiler;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_ADI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_SOYADI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_GOREVI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_EMAIL;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_TELEFON1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_TELEFON2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPicture;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit2;
        private DevExpress.XtraBars.BarButtonItem btnRefreshYetkiliKisiler;
        private DevExpress.XtraBars.BarButtonItem btnSaveYetkiliKisiler;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cID;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colMusteriKodu1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cAdi;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cSektoru;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn6;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cIlce;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn7;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cSehirKodu;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn8;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cUlkeKodu;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn9;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cAdres;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn10;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cVergiDairesi;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn11;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn12;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn13;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn14;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn15;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn16;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn17;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn18;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn19;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colID1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colMusteriKodu2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colAdi1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colSektoru1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colIlce1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colSehirKodu1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colUlkeKodu1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colAdres1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colVergiDairesi1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repSektor;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem btnYenileZiyaret;
        private DevExpress.XtraBars.BarButtonItem btnEkleZiyaret;
        private DevExpress.XtraBars.BarButtonItem btnKaydetZiyaret;
        private DevExpress.XtraBars.BarButtonItem btnYazZiyaret;
        private DevExpress.XtraBars.BarButtonItem btnSilZiyaret;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl3;
        private DevExpress.XtraEditors.GroupControl grpZiyaret;
        private DevExpress.XtraGrid.GridControl grdZiyaretler;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVZiyaretler;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox9;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit6;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn20;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn21;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn22;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn23;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField4;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn24;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField5;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn25;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField6;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn26;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField7;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn27;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField8;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn28;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField9;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView4;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn29;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField10;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn30;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField11;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn31;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField12;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn32;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField13;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn33;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField14;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn34;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField15;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn35;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField16;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn36;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField17;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn37;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField18;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repGorusulenKisiler;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repTarih;
        private DevExpress.XtraTab.XtraTabPage tbTeklifler;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem btnYenileTeklif;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl4;
        private DevExpress.XtraBars.BarButtonItem btnEkleTeklif;
        private DevExpress.XtraBars.BarButtonItem btnKaydetTeklif;
        private DevExpress.XtraBars.BarButtonItem btnYazTeklif;
        private DevExpress.XtraBars.BarButtonItem btnSilTeklif;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl5;
        private DevExpress.XtraBars.BarButtonItem btnYenileSozlesmeler;
        private DevExpress.XtraBars.BarButtonItem btnEkleSozlemeler;
        private DevExpress.XtraBars.BarButtonItem btnKayderSozlesmeler;
        private DevExpress.XtraBars.BarButtonItem btnYazSozlemeler;
        private DevExpress.XtraBars.BarButtonItem btnSilSozlemeler;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl6;
        private DevExpress.XtraBars.Bar bar6;
        private DevExpress.XtraBars.BarButtonItem btnYenileProjeler;
        private DevExpress.XtraBars.BarButtonItem btnEkleProjeler;
        private DevExpress.XtraBars.BarButtonItem btnKaydetProjeler;
        private DevExpress.XtraBars.BarButtonItem btnYazProjeler;
        private DevExpress.XtraBars.BarButtonItem btnSilProjeler;
        private DevExpress.XtraBars.Bar bar7;
        private DevExpress.XtraBars.StandaloneBarDockControl btnYenileEgitimler;
        private DevExpress.XtraBars.BarButtonItem btnSerRefresh;
        private DevExpress.XtraBars.BarButtonItem btnEkleSertifka;
        private DevExpress.XtraBars.BarButtonItem btnKaydetSertifika;
        private DevExpress.XtraBars.BarButtonItem btnYazSertifika;
        private DevExpress.XtraBars.BarButtonItem btnSilSertifika;
        private DevExpress.XtraEditors.GroupControl grpTeklifler;
        private DevExpress.XtraGrid.GridControl grdTeklifler;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVTeklifler;
        private DevExpress.XtraGrid.Columns.GridColumn cTekID;
        private DevExpress.XtraGrid.Columns.GridColumn cTekFirmaKodu;
        private DevExpress.XtraGrid.Columns.GridColumn cTekGorusmeTarihi;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn cTekVerilenKisiler;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repItemChk1;
        private DevExpress.XtraGrid.Columns.GridColumn cTekTipi;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkTip;
        private DevExpress.XtraGrid.Columns.GridColumn cTekTarih;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn cTekHazirlayan;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkHazirlayan;
        private DevExpress.XtraGrid.Columns.GridColumn cTekSonucu;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkTekSonuc;
        private DevExpress.XtraGrid.Columns.GridColumn cTekKazanmaKaybetmeNedeni;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkNedenler;
        private DevExpress.XtraGrid.Columns.GridColumn cTekKazanmaKaybetmeNotu;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox10;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox12;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit12;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit15;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit16;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView5;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn38;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField19;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn39;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField20;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn40;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField21;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn41;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField22;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn42;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField23;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn43;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField24;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn44;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField25;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn45;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField26;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn46;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField27;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView6;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn47;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField28;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn48;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField29;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn49;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField30;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn50;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField31;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn51;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField32;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn52;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField33;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn53;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField34;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn54;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField35;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn55;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField36;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repChkLSonuc;
        private DevExpress.XtraTab.XtraTabPage tbOdemeler;
        private DevExpress.XtraBars.Bar bar8;
        private DevExpress.XtraBars.BarButtonItem btnYenileOdemeler;
        private DevExpress.XtraBars.BarButtonItem btnEkleOdemeler;
        private DevExpress.XtraBars.BarButtonItem btnKaydetOdemeler;
        private DevExpress.XtraBars.BarButtonItem btnYazdirOdemeler;
        private DevExpress.XtraBars.BarButtonItem btnSilOdemeler;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl7;
        private DevExpress.XtraEditors.GroupControl grpSozlesmeler;
        private DevExpress.XtraGrid.GridControl grdSozlesmeler;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVSozlesmeler;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Musteri_Kodu;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_NeredenUlasildi;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repMem;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox13;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox14;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox15;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit19;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit20;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit21;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit22;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit23;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit17;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView7;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn56;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField37;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn57;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField38;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn58;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField39;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn59;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField40;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn60;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField41;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn61;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField42;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn62;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField43;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn63;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField44;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn64;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField45;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView8;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn65;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField46;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn66;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField47;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn67;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField48;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn68;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField49;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn69;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField50;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn70;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField51;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn71;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField52;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn72;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField53;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn73;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField54;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Imza_Tar;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Icerik;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Teklif_Fiyat;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Teklif_Fiyat_Birim;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Imz_Tutar;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Imz_Tutar_Birim;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Satis_Yapan;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Performans_Prim;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Performans_Prim_Birim;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Baslangıc;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox16;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repBr;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkSatYap;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_Bitis;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn cSoz_TeklifNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repSozlTeklNo;
        private DevExpress.XtraEditors.GroupControl grpOdemeler;
        private DevExpress.XtraGrid.GridControl grdOdemeler;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVOdemeler;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Id;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Musteri_Kodu;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_SozlesmeNo;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Fatura;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Fatura_Fiyat;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Fatura_Fiyat_Br;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_OdemeSekli;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_OdemeSekli_Fiyat;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_OdemeSekli_Br;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Ulasim_Giderleri;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Ulasim_Giderleri_Fiyat;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Ulasim_Giderleri_Br;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Konaklama_Giderleri;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Konaklama_Giderleri_Fiyat;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Konaklama_Giderleri_Br;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Yemek_Gideleri;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox18;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox19;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox20;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit24;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit25;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit12;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit26;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit27;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit28;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit29;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit12;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit12;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit30;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox21;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox17;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit6;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView9;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn74;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField55;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn75;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField56;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn76;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField57;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn77;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField58;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn78;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField59;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn79;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField60;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn80;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField61;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn81;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField62;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn82;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField63;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView10;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn83;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField64;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn84;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField65;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn85;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField66;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn86;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField67;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn87;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField68;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn88;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField69;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn89;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField70;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn90;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField71;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn91;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField72;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkOdemeSozNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox22;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLFaturaTipi;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repFatBr;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLOdmSekli;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLUlsSekli;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLKnkSekli;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repYmkSekl;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Yemek_Giderleri_Fiyat;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Yemek_Giderleri_Br;
        private DevExpress.XtraGrid.Columns.GridColumn cOde_Notlar;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repNot;
        private DevExpress.XtraEditors.GroupControl grpProjeler;
        private DevExpress.XtraEditors.PanelControl pnlProje;
        private DevExpress.XtraGrid.GridControl grdProjeler;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVProjeler;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView11;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn92;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField73;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn93;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField74;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn94;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField75;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn95;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField76;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn96;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit31;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField77;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn97;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit32;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField78;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn98;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit33;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField79;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn99;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField80;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn100;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField81;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView12;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn101;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField82;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn102;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField83;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn103;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField84;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn104;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField85;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn105;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField86;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn106;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField87;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn107;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField88;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn108;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField89;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn109;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField90;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Musteri_Kodu;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Sozlesme_No;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_ProjeTanim;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_SunumTarihi;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Chk_Tarih;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Chk_Sonucu;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Analiz_Tarihi;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Analiz_Sonucu;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Analiz_Rapor_Tarihi;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkSozNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repTar;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repProMem;
        private DevExpress.XtraBars.Bar bar9;
        private DevExpress.XtraBars.BarButtonItem btnEkleProjeDetay;
        private DevExpress.XtraBars.BarButtonItem btnSilProjeDetay;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl8;
        private DevExpress.XtraGrid.GridControl grdProjeDetay;
        private DevExpress.XtraGrid.Views.Layout.LayoutView grdVProjeDetay;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox23;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox24;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox25;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit34;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit35;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit7;
        private DevExpress.XtraBars.Bar bar10;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl9;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeNo;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeBaslangicTarihi;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repTarProDet;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeBitisTarihi;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeSorumlusu;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAnaGrupProje1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repChkDanisman;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit13;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi4;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani4;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi5;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani5;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi6;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani6;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi7;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani7;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi8;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani9;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi9;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn Pdt_ProjeModulDanismani10;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi10;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani11;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulAdi11;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeModulDanismani12;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_Id;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_STATU;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_7;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_ADI;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_SOYADI;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_GOREVI;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_EMAIL;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_TELEFON1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_TELEFON2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cPicture;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cNote;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repYetkiliStatu;
        private DevExpress.XtraGrid.Columns.GridColumn cNeredenUlasildi;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit RepMemNereden;
        private DevExpress.XtraGrid.Columns.GridColumn cIkincilKisi;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repCmbKisi;
        private DevExpress.XtraGrid.Columns.GridColumn cBirincilKisi;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAnaGrup;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAltGrup;
        private DevExpress.XtraGrid.Columns.GridColumn czAnaGrup;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAnaGrupTeklif;
        private DevExpress.XtraGrid.Columns.GridColumn czAltGrup;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAltGrupTeklif;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repCheckUpTar;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repZiyNedeni;
        private DevExpress.XtraGrid.Columns.GridColumn czZiyaretNedeni;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repZiyNedeniM;
        private DevExpress.XtraGrid.Columns.GridColumn czPromosyon;
        private DevExpress.XtraGrid.Columns.GridColumn czPromosyonMiktar;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repPromosyon;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repSozIcerik;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeDetayTanim;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAnaGrupProje;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repAltGrupProje;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPdt_ProjeLideri;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit14;
        private DevExpress.XtraBars.BarButtonItem btnKarneGiris;
        private DevExpress.XtraPivotGrid.PivotGridControl pvtKarneNotlari;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pAy;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_8;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_9;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_5;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_3;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_4;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_6;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn111;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn112;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn113;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn114;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn115;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn116;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn117;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn118;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn119;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn120;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn121;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn122;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn123;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn124;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn125;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn126;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn127;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn128;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn129;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private DevExpress.XtraLayout.SimpleSeparator item1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repSegman;
        private DevExpress.XtraGrid.Columns.GridColumn cSegmantasyon;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repSegmantas;
        private DevExpress.XtraGrid.Columns.GridColumn cFirmaTel;
        private DevExpress.XtraGrid.Columns.GridColumn cFimaFax;
        private DevExpress.XtraGrid.Columns.GridColumn cFirmaWebAdresi;
        private DevExpress.XtraGrid.Columns.GridColumn cCalisanKisiSayisi;
        private DevExpress.XtraGrid.Columns.GridColumn cBagliDigerFirmalar;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTel;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTel2;
        private DevExpress.XtraGrid.Columns.GridColumn cType;
        private DevExpress.XtraGrid.Columns.GridColumn cTypeF;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraBars.BarButtonItem btnAttachFileMusteri;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.Bar bar11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repTekChkTeklifAnaGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repTekChkTeklifAltGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLupNereden;
        private DevExpress.XtraGrid.Columns.GridColumn cCiro;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repCiro;
        private DevExpress.XtraGrid.Columns.GridColumn cCiroYuzde;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn cAileSirketi;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox26;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit36;
        private DevExpress.XtraEditors.GroupControl grpSertifika;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdSertikalar;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVSertikalar;
        private DevExpress.XtraGrid.Columns.GridColumn CSID;
        private DevExpress.XtraGrid.Columns.GridColumn CSFirmaKodu;
        private DevExpress.XtraGrid.Columns.GridColumn cSTanim;
        private DevExpress.XtraGrid.Columns.GridColumn cSAktifPasif;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox32;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox28;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox29;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox30;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit38;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit39;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit15;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit40;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit41;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit42;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit43;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit17;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit19;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit20;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit19;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit44;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit15;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox31;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox27;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit15;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit17;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit37;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView13;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn110;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField91;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn111;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField92;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn112;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField93;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn113;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField94;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn114;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField95;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn115;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField96;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn116;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField97;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn117;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField98;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn118;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField99;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView14;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn119;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField100;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn120;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField101;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn121;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField102;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn122;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField103;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn123;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField104;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn124;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField105;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn125;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField106;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn126;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField107;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn127;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField108;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repSertifakalar;
        private DevExpress.XtraBars.BarButtonItem btnKaydet;
        private DevExpress.XtraBars.Bar bar13;
        private DevExpress.XtraBars.BarButtonItem btnYazilimYenile;
        private DevExpress.XtraBars.BarButtonItem btnYazilimEkle;
        private DevExpress.XtraBars.BarButtonItem btnYazilimKaydet;
        private DevExpress.XtraBars.BarButtonItem btnYazilimYazdir;
        private DevExpress.XtraBars.BarButtonItem btnYazilimSil;
        private DevExpress.XtraTab.XtraTabControl tbMainProjeler;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraBars.BarButtonItem btnEmailGonderim;
        private DevExpress.XtraGrid.Columns.GridColumn cYetkili;
        private DevExpress.XtraGrid.Columns.GridColumn cYetkiliPozisyonu;
        private DevExpress.XtraGrid.Columns.GridColumn cEmail;
        private DevExpress.XtraGrid.Columns.GridColumn cEmpID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repGorusenKisiler;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repGorusenKisiler2;
        private DevExpress.XtraGrid.Columns.GridColumn cAltSektor;
        private DevExpress.XtraGrid.Columns.GridColumn cRenk;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn cOlusturan;
        private DevExpress.XtraGrid.Columns.GridColumn cOlusturmaTarihi;
        private DevExpress.XtraGrid.Columns.GridColumn cDegistiren;
        private DevExpress.XtraGrid.Columns.GridColumn cDegistirmeTarihi;
        private DevExpress.XtraBars.Bar bar12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraGrid.Columns.GridColumn cKategori;
        private DevExpress.XtraTab.XtraTabControl tbEk;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl10;
        private DevExpress.XtraEditors.GroupControl grpKullanilanYazi;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl grdKullanilanYaz;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVKullanilanYaz;
        private DevExpress.XtraGrid.Columns.GridColumn cY_Id;
        private DevExpress.XtraGrid.Columns.GridColumn cY_Tipi;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repYazilimGrup;
        private DevExpress.XtraGrid.Columns.GridColumn ccY_Marka;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repYazilimMarka;
        private DevExpress.XtraGrid.Columns.GridColumn cY_Adi;
        private DevExpress.XtraGrid.Columns.GridColumn cY_KullanimOrani;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn cYFirmaKodu;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox34;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox35;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox36;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit46;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit47;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit17;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit48;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit49;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit50;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit51;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit21;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit20;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit22;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit23;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit24;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit25;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit21;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit52;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit22;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox37;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox38;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit26;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit23;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit24;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit27;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit53;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox33;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit45;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView15;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn128;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField109;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn129;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField110;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn130;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField111;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn131;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField112;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn132;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField113;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn133;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField114;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn134;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField115;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn135;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField116;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn136;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField117;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView16;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn137;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField118;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn138;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField119;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn139;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField120;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn140;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField121;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn141;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField122;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn142;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField123;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn143;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField124;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn144;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField125;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn145;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField126;
        private DevExpress.XtraBars.Bar bar14;
        private DevExpress.XtraBars.BarButtonItem btnProjeDetay;
        private DevExpress.XtraGrid.Columns.GridColumn grdPro_ProjeTanim;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repButtonEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit zProjeDetay;
        private DevExpress.XtraGrid.Columns.GridColumn cPro_ScaleWidth;

    }
}