﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Global;
using System.Data.SqlClient;

using System.IO;

namespace GABI_CRM
{
    public partial class EkleMusteriTanim : DevExpress.XtraEditors.XtraForm
    {
        public EkleMusteriTanim()
        {
            InitializeComponent();
        }

        public static string Type = ""; // 1: Normal Müşteri 2: Karvizit müşterileri
        public static string FTITLE = "";
        public static string GelisMusteriKodu = "";


        private string MusteriAdi, MusteriNo;

        //Kişiler
        SqlDataAdapter SqlDapYetkiliKisiler;
        SqlCommandBuilder SqlScbYetkiliKisiler;
        DataTable dtGenelYetkiliKisiler;


        private void GetListKisiler()
        {


            try
            {
                //if (string.IsNullOrEmpty(GelisMusteriKodu)) return;

                string FirmKodu = string.IsNullOrEmpty(GelisMusteriKodu) ? MusteriNo : GelisMusteriKodu;

                this.Text = string.Format("{0}-{1}", MusteriNo, MusteriAdi);
                tbYetkiliKisiler.PageVisible = true;
                Cursor.Current = Cursors.WaitCursor;

                string Sql = string.Format(@"SELECT  * FROM  KISILER Where EmpFirmaKodu='{0}'", FirmKodu);
                SqlDapYetkiliKisiler = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenelYetkiliKisiler = new DataTable();
                SqlScbYetkiliKisiler = new SqlCommandBuilder(SqlDapYetkiliKisiler);
                SqlScbYetkiliKisiler.DataAdapter.Fill(dtGenelYetkiliKisiler);
                grdYetkiliKisiler.DataSource = dtGenelYetkiliKisiler;

                var dsL6 = new DataTable();
                dsL6 = DbConnSql.SelectSQL(@"SELECT GP_VIEW as Tanim  FROM  GEN_PARAMS Where GP_TYPE='YSatutu' ");
                repYetkiliStatu.DataSource = null;
                repYetkiliStatu.DataSource = dsL6;


                Cursor.Current = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void GetList()
        {
            this.Text = FTITLE;
            Cursor.Current = Cursors.WaitCursor;
            string sUlkeler ="SELECT  GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE ='ULKELER' ";
            DABindings.GetXtraLookupBindNm(repLupUlke, sUlkeler);
            string sSektor = @"
                 Select      GP_VIEW as Tanim, GP_VALUE_STR1 as Kod,AnaGrup From   GEN_PARAMS 
                 left join
                 (Select  GP_VIEW as AnaGrup, GP_VALUE_STR1  as KodAna From GEN_PARAMS Where GP_TYPE='SektAna')as g on g.KodAna = GP_VALUE_STR2
                WHERE        (GP_TYPE = 'NACE')
                Order by AnaGrup
            
                ";          
            DABindings.GetXtraLookupBindNm(repSektor, sSektor);
            DABindings.GetXtraLookupBindNm(repKategori, @"SELECT Tanim=GP_VIEW,Kod=GP_VALUE_STR1 FROM  GEN_PARAMS Where GP_TYPE='Ktg' AND GP_DURUM='A'" );

            string sCiroBas = "SELECT      GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM   GEN_PARAMS WHERE        (GP_TYPE = 'CiroB')";           

            DABindings.GetXtraLookupBindNm(repCiro, sCiroBas);
            string sNereden = "SELECT      GP_VIEW as Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS WHERE        (GP_TYPE = 'Nered')";
            DABindings.GetXtraLookupBindNm(repLupNereden, sNereden);
            string sSeg = "SELECT   GP_VALUE_STR1 as Kod ,GP_VIEW as Tanim FROM            GEN_PARAMS WHERE        (GP_TYPE = 'Mseg')";
            DABindings.GetXtraLookupBindNm(repSegmantas, sSeg);
            Cursor.Current = Cursors.Default;
        
        
        
        }

        private byte[] barrImg;
        private void GetPicture(string ID )
        {
           
          
            try
            {

                SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());                         
                SqlCommand cmdSelect = new SqlCommand(@"
                    Select Top 1 
                                    ISNULL(Foto, (select top 1 PIC from TEMP_IMG Where ID=1)) AS Picture                                     
                    From FIRMALAR where MusteriKodu=@ID", conn);
                cmdSelect.Parameters.Add("@ID", SqlDbType.Int, 4);
                cmdSelect.Parameters["@ID"].Value = ID;

                conn.Open();
                barrImg = (byte[])cmdSelect.ExecuteScalar();
                string strfn = Convert.ToString(DateTime.Now.ToFileTime());
                FileStream fs = new FileStream(strfn, FileMode.CreateNew, FileAccess.Write);
                fs.Write(barrImg, 0, barrImg.Length);
                fs.Flush();
                fs.Close();

                picFirmLogo.Image = Image.FromFile(strfn);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {

            
            }
           
        
        
        }

        private void GetPictureNew()
        {


            try
            {

                SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
                SqlCommand cmdSelect = new SqlCommand(@"
                    
                                    select top 1 PIC AS Picture  from TEMP_IMG Where ID=1
            ", conn);
                conn.Open();
                barrImg = (byte[])cmdSelect.ExecuteScalar();
                string strfn = Convert.ToString(DateTime.Now.ToFileTime());
                FileStream fs = new FileStream(strfn, FileMode.CreateNew, FileAccess.Write);
                fs.Write(barrImg, 0, barrImg.Length);
                fs.Flush();
                fs.Close();

                picFirmLogo.Image = Image.FromFile(strfn);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {


            }



        }

        private void GetValues()
        {

            try
            {

            if (GelisMusteriKodu.Length<=1 )  return;
            //if (string.IsNullOrEmpty( GelisMusteriKodu ) ) return;
            
            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format( @"
                SELECT * FROM FIRMALAR Where MusteriKodu='{0}'
            ", GelisMusteriKodu));
            
            repSegmantas.EditValue =dt.Rows[0]["Segmantasyonu"].ToString();
            txtFirmaAdi.Text =dt.Rows[0]["Adi"].ToString();
            repSektor.EditValue =dt.Rows[0]["Sektoru"].ToString();
            repKategori.EditValue =dt.Rows[0]["Kategori"].ToString();
            txtAdres.Text =dt.Rows[0]["Adres"].ToString();
            txtVergiDairesi.Text = dt.Rows[0]["VergiDairesi"].ToString();
            txtVergiNo.Text = dt.Rows[0]["VergiNo"].ToString();
            repLupUlke.EditValue =dt.Rows[0]["UlkeKodu"].ToString();
            repLupSehir.EditValue=dt.Rows[0]["SehirKodu"].ToString();
            repLupIlce.EditValue = string.IsNullOrEmpty(dt.Rows[0]["Ilce"].ToString()) ? -1 :Convert.ToInt32( dt.Rows[0]["Ilce"].ToString());
            repLupNereden.EditValue=dt.Rows[0]["NeredenUlasildi"].ToString();
            txtFirmaTel.Text=dt.Rows[0]["FirmaTel"].ToString();
            txtFirmaFax.Text =dt.Rows[0]["FimaFax"].ToString();
            txtFirmaWeb.Text=dt.Rows[0]["FirmaWebAdresi"].ToString();
            txtCalisanKisi.Text =dt.Rows[0]["CalisanKisiSayisi"].ToString();
            txtBagliDigerFirma.Text =dt.Rows[0]["BagliDigerFirmalar"].ToString();
            repCiro.EditValue =dt.Rows[0]["Ciro"].ToString();
            txtCiroYuzde.Text =dt.Rows[0]["CiroYuzdesi"].ToString();
            cmbAileSirketimi.Text=dt.Rows[0]["AileSirketi"].ToString();
            cmbKartTipi.EditValue = Convert.ToInt32( dt.Rows[0]["Type"].ToString());
            //if (!string.IsNullOrEmpty(dt.Rows[0]["Foto"].ToString()))        
               GetPicture(GelisMusteriKodu);
            GetListKisiler();   
               
            

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        
        }
        private void ClearScreen()
        { 
            repSegmantas.Text ="";
            txtFirmaAdi.Text ="";
            repSektor.Text ="";
            repKategori.Text = "";
            txtAdres.Text ="";
            txtVergiDairesi.Text ="";
            txtVergiNo.Text = "";
            repLupUlke.Text ="";
            repLupSehir.Text ="";
            repLupIlce.Text ="";
            repLupNereden.Text ="";
            txtFirmaTel.Text ="";
            txtFirmaFax.Text ="";
            txtFirmaWeb.Text ="";
            txtCalisanKisi.Text ="";
            txtBagliDigerFirma.Text ="";
            repCiro.Text ="";
            txtCiroYuzde.Text ="0";
            cmbAileSirketimi.Text = "Hayır";
            tbYetkiliKisiler.PageVisible = false;
            MusteriAdi = "";
            MusteriNo = "";
            this.Text = "Firma Ekle";
        }
        private void Save()
        {
            try
            {
            double CiroYuzdesi=0;
            string UlkeKodu, SehirKodu, Ilce, Segmantasyonu, Sektoru,Kategori, NeredenUlasildi, Ciro, Adi;
            MusteriNo = DbConnSql.SelectSQL(@"
                        UPDATE NUMARA
                        SET NUMARA = isnull(NUMARA,5000)+1
                    Select NUMARA AS ID From NUMARA
                    ").Rows[0]["ID"].ToString();
            if (string.IsNullOrEmpty(txtFirmaAdi.Text)) { MessageBox.Show("Firma adını boş geçemezsiniz."); return; } else Adi = txtFirmaAdi.Text;

            if  (string.IsNullOrEmpty( repLupUlke.Text )) {MessageBox.Show("Ülke kodunu boş geçemezsiniz.");return; } else  UlkeKodu = repLupUlke.EditValue.ToString();
            if  (string.IsNullOrEmpty(repLupSehir.Text)) { MessageBox.Show("Şehir kodunu boş geçemezsiniz."); return; } else SehirKodu = repLupSehir.EditValue.ToString();
            if  (string.IsNullOrEmpty(repLupIlce.Text)) { MessageBox.Show("İlçe kodunu boş geçemezsiniz."); return; } else Ilce = repLupIlce.EditValue.ToString();
          

            Segmantasyonu =string.IsNullOrEmpty( repSegmantas.Text) ? "":  repSegmantas.EditValue.ToString();
            Sektoru = string.IsNullOrEmpty(repSektor.Text) ? "" : repSektor.EditValue.ToString();
            Kategori = string.IsNullOrEmpty(repKategori.Text) ? "" : repKategori.EditValue.ToString();
            NeredenUlasildi = string.IsNullOrEmpty(repLupNereden.Text) ? "" : repLupNereden.EditValue.ToString();
            Ciro = string.IsNullOrEmpty(repCiro.Text) ? "" : repCiro.EditValue.ToString();
            CiroYuzdesi = string.IsNullOrEmpty(txtCiroYuzde.Text) ? 0 : Convert.ToDouble(txtCiroYuzde.Text);
            
            string Adres= txtAdres.Text;
            string VergiDairesi= txtVergiDairesi.Text;
            string VergiNo = txtVergiNo.Text;
            string FirmaTel = txtFirmaTel.Text;
            string FimaFax=  txtFirmaFax.Text;
            string FirmaWebAdresi = txtFirmaWeb.Text;
            string CalisanKisiSayisi = string.IsNullOrEmpty(txtCalisanKisi.Text) ? "0" : txtCalisanKisi.Text;  
            string BagliDigerFirmalar=  txtBagliDigerFirma.Text;
            string AileSirketi = cmbAileSirketimi.Text;

            if (barrImg == null) GetPictureNew();


            SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
            conn.Open();
            SqlCommand cmd = new SqlCommand(@" 
                            INSERT INTO  dbo.FIRMALAR 
                                   ( Type 
                                   , MusteriKodu 
                                   , Adi 
                                   , Sektoru 
                                   , Kategori
                                   , Ilce 
                                   , SehirKodu 
                                   , UlkeKodu 
                                   , Adres 
                                   , VergiDairesi 
                                   , VergiNo
                                   , NeredenUlasildi 
                                   , Segmantasyonu 
                                   , FirmaTel 
                                   , FimaFax 
                                   , FirmaWebAdresi 
                                   , CalisanKisiSayisi 
                                   , BagliDigerFirmalar 
                                   , Ciro 
                                   , CiroYuzdesi 
                                   , AileSirketi
                                   , Foto
                                   , CREUSER
                                   , CREDATE
                                 
                                    )
                             VALUES
                                   ( @Type 
                                   , @MusteriKodu 
                                   , @Adi 
                                   , @Sektoru 
                                   , @Kategori
            
                                   , @Ilce 
                                   , @SehirKodu 
                                   , @UlkeKodu 
                                   , @Adres 
                                   , @VergiDairesi 
                                   , @VergiNo
                                   , @NeredenUlasildi 
                                   , @Segmantasyonu 
                                   , @FirmaTel 
                                   , @FimaFax 
                                   , @FirmaWebAdresi 
                                   , @CalisanKisiSayisi 
                                   , @BagliDigerFirmalar 
                                   , @Ciro 
                                   , @CiroYuzdesi 
                                   , @AileSirketi 
                                   , @Foto
                                   , @CREUSER
                                   , getdate()
                                    )
                        ", conn);
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = cmbKartTipi.EditValue;
                cmd.Parameters.Add("@MusteriKodu", SqlDbType.NVarChar).Value = MusteriNo;
                cmd.Parameters.Add("@Adi", SqlDbType.NVarChar).Value = Adi;
                cmd.Parameters.Add("@Sektoru", SqlDbType.NVarChar).Value = Sektoru;
                cmd.Parameters.Add("@Kategori", SqlDbType.NVarChar).Value = Kategori;

                cmd.Parameters.Add("@Ilce", SqlDbType.NVarChar).Value = Ilce;
                cmd.Parameters.Add("@SehirKodu", SqlDbType.NVarChar).Value = SehirKodu;
                cmd.Parameters.Add("@UlkeKodu", SqlDbType.NVarChar).Value = UlkeKodu;
                cmd.Parameters.Add("@Adres", SqlDbType.NVarChar).Value = Adres;
                cmd.Parameters.Add("@VergiDairesi", SqlDbType.NVarChar).Value = VergiDairesi;
                cmd.Parameters.Add("@VergiNo", SqlDbType.NVarChar).Value = VergiNo;
                cmd.Parameters.Add("@NeredenUlasildi", SqlDbType.Text).Value = NeredenUlasildi;
                cmd.Parameters.Add("@Segmantasyonu", SqlDbType.NVarChar).Value = Segmantasyonu;
                cmd.Parameters.Add("@FirmaTel", SqlDbType.NVarChar).Value = FirmaTel;
                cmd.Parameters.Add("@FimaFax", SqlDbType.NVarChar).Value = FimaFax;
                cmd.Parameters.Add("@FirmaWebAdresi", SqlDbType.NVarChar).Value = FirmaWebAdresi;
                cmd.Parameters.Add("@CalisanKisiSayisi", SqlDbType.Int).Value = CalisanKisiSayisi;
                cmd.Parameters.Add("@BagliDigerFirmalar", SqlDbType.NVarChar).Value = BagliDigerFirmalar;
                cmd.Parameters.Add("@Ciro", SqlDbType.NVarChar).Value = Ciro;
                cmd.Parameters.Add("@CiroYuzdesi", SqlDbType.Float).Value = CiroYuzdesi;
                cmd.Parameters.Add("@AileSirketi", SqlDbType.NVarChar).Value = AileSirketi;
                cmd.Parameters.Add("@Foto", SqlDbType.Image, barrImg.Length).Value = barrImg;// string.IsNullOrEmpty(resimAdresi) ? null : resim; /* Seçtigimiz resim dosyasinin byte'larini, tablodaki ilgili alana tasiyacak parametremizi belirtiyoruz. Deger olarak, resim isimli byte dizimizi aktariyoruz. Parametre tipinin, image olduguna dikkat edelim. */
                cmd.Parameters.Add("@CREUSER", SqlDbType.Int).Value = BTI.frmLogin.KullaniciNo;

      
            cmd.ExecuteNonQuery();
                

            MessageBox.Show("Veriler kayıt edildi");
            MusteriNo= DbConnSql.SelectSQL(string.Format( @"Select Top 1 MusteriKodu from dbo.FIRMALAR where Adi='{0}'  ",txtFirmaAdi.Text )).Rows[0][0].ToString() ;
            MusteriAdi = txtFirmaAdi.Text;

            tbYetkiliKisiler.PageVisible = true;
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        
        }
        private void Update()
        {
            try
            {
                double CiroYuzdesi = 0;
                string UlkeKodu, SehirKodu, Ilce, Segmantasyonu, Sektoru,Kategori, NeredenUlasildi,  Ciro, Adi;
               
                if (string.IsNullOrEmpty(txtFirmaAdi.Text)) { MessageBox.Show("Firma adını boş geçemezsiniz."); return; } else Adi = txtFirmaAdi.Text;
                if (string.IsNullOrEmpty(repLupUlke.Text)) { MessageBox.Show("Ülke kodunu boş geçemezsiniz."); return; } else UlkeKodu = repLupUlke.EditValue.ToString();
                if (string.IsNullOrEmpty(repLupSehir.Text)) { MessageBox.Show("Şehir kodunu boş geçemezsiniz."); return; } else SehirKodu = repLupSehir.EditValue.ToString();
                if (string.IsNullOrEmpty(repLupIlce.Text)) { MessageBox.Show("İlçe kodunu boş geçemezsiniz."); return; } else Ilce = repLupIlce.EditValue.ToString();
               

                Segmantasyonu = string.IsNullOrEmpty(repSegmantas.Text) ? "" : repSegmantas.EditValue.ToString();
                Sektoru = string.IsNullOrEmpty(repSektor.Text) ? "" : repSektor.EditValue.ToString();
                Kategori = string.IsNullOrEmpty(repKategori.Text) ? "" : repKategori.EditValue.ToString();
                NeredenUlasildi = string.IsNullOrEmpty(repLupNereden.Text) ? "" : repLupNereden.EditValue.ToString();
                Ciro = string.IsNullOrEmpty(repCiro.Text) ? "" : repCiro.EditValue.ToString();
                CiroYuzdesi = string.IsNullOrEmpty(txtCiroYuzde.Text)  ? 0 :Convert.ToDouble( txtCiroYuzde.Text);
                string Adres = txtAdres.Text;
                string VergiDairesi = txtVergiDairesi.Text;
                string VergiNo = txtVergiNo.Text;
                string FirmaTel = txtFirmaTel.Text;
                string FimaFax = txtFirmaFax.Text;
                string FirmaWebAdresi = txtFirmaWeb.Text;
                string CalisanKisiSayisi = string.IsNullOrEmpty(txtCalisanKisi.Text) ? "0" : txtCalisanKisi.Text;
                string BagliDigerFirmalar = txtBagliDigerFirma.Text;
                string AileSirketi = cmbAileSirketimi.Text;

               

                SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
                conn.Open();
                SqlCommand cmd = new SqlCommand(@" 
                            UPDATE dbo.FIRMALAR 
                                   SET
                                     Type  =@Type
                                   , Adi =@Adi 
                                   , Sektoru = @Sektoru 
                                   , Kategori = @Kategori
                                   , Ilce =@Ilce 
                                   , SehirKodu =@SehirKodu 
                                   , UlkeKodu =@UlkeKodu 
                                   , Adres =@Adres
                                   , VergiDairesi = @VergiDairesi 
                                   , VergiNo = @VergiNo
                                   , NeredenUlasildi =@NeredenUlasildi 
                                   , Segmantasyonu = @Segmantasyonu 
                                   , FirmaTel = @FirmaTel
                                   , FimaFax = @FimaFax 
                                   , FirmaWebAdresi = @FirmaWebAdresi 
                                   , CalisanKisiSayisi =@CalisanKisiSayisi 
                                   , BagliDigerFirmalar =@BagliDigerFirmalar 
                                   , Ciro = @Ciro 
                                   , CiroYuzdesi = @CiroYuzdesi 
                                   , AileSirketi =@AileSirketi 
                                   , Foto = @Foto
                                   , MODUSER = @MODUSER
                                   , MODDATE = GETDATE()                                
                                Where MusteriKodu=@MusteriKodu 
                        ", conn);
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = cmbKartTipi.EditValue; 
                cmd.Parameters.Add("@MusteriKodu", SqlDbType.NVarChar).Value = GelisMusteriKodu;
                cmd.Parameters.Add("@Adi", SqlDbType.NVarChar).Value = Adi;
                cmd.Parameters.Add("@Sektoru", SqlDbType.NVarChar).Value = Sektoru;
                cmd.Parameters.Add("@Kategori", SqlDbType.NVarChar).Value = Kategori;

                cmd.Parameters.Add("@Ilce", SqlDbType.NVarChar).Value = Ilce;
                cmd.Parameters.Add("@SehirKodu", SqlDbType.NVarChar).Value = SehirKodu;
                cmd.Parameters.Add("@UlkeKodu", SqlDbType.NVarChar).Value = UlkeKodu;
                cmd.Parameters.Add("@Adres", SqlDbType.NVarChar).Value = Adres;
                cmd.Parameters.Add("@VergiDairesi", SqlDbType.NVarChar).Value = VergiDairesi;
                cmd.Parameters.Add("@VergiNo", SqlDbType.NVarChar).Value = VergiNo;
                cmd.Parameters.Add("@NeredenUlasildi", SqlDbType.Text).Value = NeredenUlasildi;
                cmd.Parameters.Add("@Segmantasyonu", SqlDbType.NVarChar).Value = Segmantasyonu;
                cmd.Parameters.Add("@FirmaTel", SqlDbType.NVarChar).Value = FirmaTel;
                cmd.Parameters.Add("@FimaFax", SqlDbType.NVarChar).Value = FimaFax;
                cmd.Parameters.Add("@FirmaWebAdresi", SqlDbType.NVarChar).Value = FirmaWebAdresi;
                cmd.Parameters.Add("@CalisanKisiSayisi", SqlDbType.Int).Value = CalisanKisiSayisi;
                cmd.Parameters.Add("@BagliDigerFirmalar", SqlDbType.NVarChar).Value = BagliDigerFirmalar;
                cmd.Parameters.Add("@Ciro", SqlDbType.NVarChar).Value = Ciro;
                cmd.Parameters.Add("@CiroYuzdesi", SqlDbType.Float).Value = CiroYuzdesi;
                cmd.Parameters.Add("@AileSirketi", SqlDbType.NVarChar).Value = AileSirketi;
               
                cmd.Parameters.Add("@Foto", SqlDbType.Image, barrImg.Length).Value = barrImg;// string.IsNullOrEmpty(resimAdresi) ? null : resim; 
          
          
                cmd.Parameters.Add("@MODUSER", SqlDbType.Int).Value = BTI.frmLogin.KullaniciNo;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Veriler kayıt edildi");
                Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void EkleMusteriTanim_Load(object sender, EventArgs e)
        {
            cmbKartTipi.EditValue =Convert.ToInt32(Type);
            GetList();
            GetValues();
        }
        private void repLupUlke_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                string Ulke = repLupUlke.EditValue.ToString();
                if (string.IsNullOrEmpty(Ulke)) return;

                string sSehirler = string.Format("SELECT  GP_VIEW as Tanim, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER'  and GP_VALUE_STR1 ='{0}' ", Ulke);
                DABindings.GetXtraLookupBindNm(repLupSehir, sSehirler);

                if (Ulke == "TR")
                {
                    txtFirmaTel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
                    txtFirmaTel.Properties.Mask.EditMask = @"(\d?\d?\d?)\d\d\d-\d\d\d\d";
                    repTel2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
                    repTel2.Mask.EditMask = @"(\d?\d?\d?)\d\d\d-\d\d\d\d";
                    
                    

                }
                else
                {
                    txtFirmaTel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
                    txtFirmaTel.Properties.Mask.EditMask =@"";
                    repTel2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
                    repTel2.Mask.EditMask = @"";
                    
                
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

           
   
        }
        private void repLupSehir_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                string str = repLupSehir.EditValue.ToString();
                if (string.IsNullOrEmpty(str)) return;
                string sIlceler = string.Format("SELECT  GP_VIEW as Tanim, GP_ID  FROM  GEN_PARAMS Where GP_TYPE ='ILCELER'  and GP_VALUE_STR1 ='{0}'", str);
                DABindings.GetXtraLookupBindNm(repLupIlce, sIlceler);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


           
        }

        
        private void btnKaydet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if  (GelisMusteriKodu.Length<=1 )            
                Save();
            
                
            if(GelisMusteriKodu.Length>2)    
                Update();
        }
        private void EkleMusteriTanim_FormClosed(object sender, FormClosedEventArgs e)
        {
            GelisMusteriKodu = "";
        }
        OpenFileDialog openFileDialog1 = new OpenFileDialog();

      
        private void btnResimSec_Click(object sender, EventArgs e)
        {
            
            
        }

        private void picFirmLogo_EditValueChanged(object sender, EventArgs e)
        {
            
         
        }

        private void btnEkleYetkili_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string MusK = string.IsNullOrEmpty(GelisMusteriKodu) ? MusteriNo : GelisMusteriKodu;
            
            lvYetkiliKisiler.AddNewRow();
            lvYetkiliKisiler.SetRowCellValue(lvYetkiliKisiler.FocusedRowHandle, lvYetkiliKisiler.Columns["EmpFirmaKodu"], MusK);         
      
        }

        private void btnKaydetYetkili_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textEdit1.Focus();
            try
            {
                DataView _dv = new DataView(dtGenelYetkiliKisiler);
                for (int i = 0; i < _dv.Count; i++)
                {
                    string Ulke = _dv[i]["Emp_YetkiliKisi"].ToString();
                    if (string.IsNullOrEmpty(Ulke))
                    {
                        MessageBox.Show("Statüyü boş geçemezsiniz.");
                        return;
                    }

                }

                SqlScbYetkiliKisiler.DataAdapter.Update(dtGenelYetkiliKisiler);
                MessageBox.Show("Veriler güncellendi.");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetListKisiler();
        }

        private void btnPrintYetkili_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            lvYetkiliKisiler.ShowPrintPreview();
        }

        private void btnDeleteYetkili_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            lvYetkiliKisiler.DeleteRow(lvYetkiliKisiler.FocusedRowHandle);
        }

        private void btnNewCard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearScreen();

        }

        private void btnResimSec_Click_1(object sender, EventArgs e)
        {
            string resimAdresi; /* OpenFileDialog kontrolünden seçtigimiz dosyanin tam adresini tutacak genel bir degisken. */

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                picFirmLogo.Image = System.Drawing.Image.FromFile(openFileDialog1.FileName); /* Drawing isim uzayinda yer alan Image sinifinin FromFile metodunu kullanarak belirtilen adresteki dosya PictureBox kontrolü içine çizilir. */
                resimAdresi = openFileDialog1.FileName.ToString();
                //resim yükleme                
                FileStream fsResim = new FileStream(resimAdresi, FileMode.Open, FileAccess.Read);
                BinaryReader brResim = new BinaryReader(fsResim);
                barrImg = brResim.ReadBytes((int)fsResim.Length);
                brResim.Close();
                fsResim.Close();


            }



        }

        private void tbMain_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            try
            {
                //Sözleşme griddinde değişiklik varsa kayıt kontrolü yapılsıngr
            

                switch (tbMain.SelectedTabPageIndex)
                {
                    case 0:

                        break;
                    case 1:
                        // this.Text = string.Format("{0}-{1}", MusteriKodu, MusteriAdi);
                        GetListKisiler();
                        break;

                  

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

    }
}