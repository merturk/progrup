﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;
using System.Data.SqlClient;

namespace GABI_CRM
{
    public partial class frmProjeEkle : DevExpress.XtraEditors.XtraForm
    {
        
        public frmProjeEkle()
        {
            InitializeComponent();
        }
        public static string ProjeId = "", ResourceId = "", FirmaKodu = "1", GelisYeri = "";
        private void ClosedForm()
        {
            ProjeId = "";
            ResourceId = "";
            FirmaKodu = "";
            GelisYeri = "";
        
        }



        private void GetList()
        {
            //Yetkili Kişiler Listesi YETKİLERİNİ GETİR
            cmbFirmaSorum.Properties.Items.Clear();
            var dt1 = new DataTable();
            dt1 = DbConnSql.SelectSQL(string.Format(@"Select EmpAdi+space(1)+EmpSoyadi as Tanim from KISILER Where EmpFirmaKodu='{0}'  ", FirmaKodu));
            for (int i = 0; i < dt1.Rows.Count; i++)
                cmbFirmaSorum.Properties.Items.Add(dt1.Rows[i]["Tanim"].ToString(), CheckState.Unchecked, true);

            cmbFirmaSorum.Properties.SeparatorChar = ',';
            //--------------------------
            //Danışman listesi
            cmbLocalSorumlu.Properties.Items.Clear();
            var dt2 = new DataTable();
            dt2 = DbConnSql.SelectSQL(@"SELECT  Tanim=GP_VIEW,Kod=GP_VALUE_STR1,CREUSER=dbo.GetUserName(GP_CREUSER),CREDATE=GP_CREDATE, MODUSER =dbo.GetUserName(GP_MODUSER), MODDATE= GP_MODDATE FROM  GEN_PARAMS Where GP_TYPE='THazr'   ");
            
            
            for (int i = 0; i < dt2.Rows.Count; i++)
                cmbLocalSorumlu.Properties.Items.Add(dt2.Rows[i]["Tanim"].ToString(), CheckState.Unchecked, true);

            cmbLocalSorumlu.Properties.SeparatorChar = ',';
            //--------------------------
        }

        private void GetScreenValue()
        {
          

                try
                {

                    if (GelisYeri == "Duzeltme")
                    {
                        var dt = new DataTable();
                        dt = DbConnSql.SelectSQL(string.Format(@"SELECT top 1 * FROM GANT_Resource Where Id='{0}'  ", ResourceId));
                        txtSiraNo.Text = dt.Rows[0]["SiraNo"].ToString();
                        txtPlanlananFaaliyet.Text = dt.Rows[0]["Description"].ToString();
                        cmbFirmaSorum.Text = dt.Rows[0]["KarsiTarafSorumlusu"].ToString();
                        cmbLocalSorumlu.Text = dt.Rows[0]["YerelSorumlu"].ToString();
                        datePlnBas.Text = dt.Rows[0]["ProjeBaslamaTarihi"].ToString();
                        datePlnBit.Text = dt.Rows[0]["ProjeBitisTarihi"].ToString();                       
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
        

            
        

            /*
            //Yeni ana başlık
            if  (GelisYeri =="AnaBaslik" )// (string.IsNullOrEmpty(ResourceId) && ( GelisYeri != "Düzeltme" )  )
            {
                cmbFirmaSorum.Enabled = false;
                cmbLocalSorumlu.Enabled = false;
                datePlnBas.Enabled = false;
                datePlnBit.Enabled = false;


            }
            

         
            else //alt başlık
            {
               
           

            }
        */
        
        }

        

        private void frmProjeEkle_Load(object sender, EventArgs e)
        {
            
            GetList();
            GetScreenValue();
        }

        private void ClearScreen()
        {
            txtSiraNo.Text = "";
            txtPlanlananFaaliyet.Text = "";

        }

        private void SaveAnaBaslik()
        {
            try
            {
                if (string.IsNullOrEmpty(txtSiraNo.Text)) {MessageBox.Show("Sira numarasını boş geçemezsiniz."); return;   }
                if (string.IsNullOrEmpty(txtPlanlananFaaliyet.Text)) { MessageBox.Show("Planlanan faaliyet alanını boş geçemezsiniz."); return; }
                DbConnSql.SelectSQL(string.Format(@" [dbo].[GANT_Resource_ins] {0},'{1}','{2}',{3}", ProjeId, txtPlanlananFaaliyet.Text, txtSiraNo.Text, BTI.frmLogin.KullaniciNo));        
                ClearScreen();
                MessageBox.Show("Veriler kayıt edildi.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void SaveAltBaslik()
        {
            try
            {
                if (string.IsNullOrEmpty(txtSiraNo.Text)) { MessageBox.Show("Sira numarasını boş geçemezsiniz."); return; }
                if (string.IsNullOrEmpty(txtPlanlananFaaliyet.Text)) { MessageBox.Show("Planlanan faaliyet alanını boş geçemezsiniz."); return; }
                DbConnSql.SelectSQL(string.Format(@" 
                        INSERT GANT_Resource (ProjeId,ParentId,Description,SiraNo,YerelSorumlu,KarsiTarafSorumlusu,ProjeBaslamaTarihi,ProjeBitisTarihi,CREUSER,CREDATE)
                        SELECT {0},{1},'{2}','{3}','{4}','{5}','{6}','{7}',{8},getdate()
                    ", ProjeId,ResourceId, txtPlanlananFaaliyet.Text, txtSiraNo.Text, cmbFirmaSorum.Text,cmbLocalSorumlu.Text,datePlnBas.Text,datePlnBit.Text, BTI.frmLogin.KullaniciNo));
                ClearScreen();
                MessageBox.Show("Veriler kayıt edildi.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        
        
        }

        private void UpdateRecord()
        {
            try
            {
                if (string.IsNullOrEmpty(txtSiraNo.Text)) { MessageBox.Show("Sira numarasını boş geçemezsiniz."); return; }
                if (string.IsNullOrEmpty(txtPlanlananFaaliyet.Text)) { MessageBox.Show("Planlanan faaliyet alanını boş geçemezsiniz."); return; }
                DbConnSql.SelectSQL(string.Format(@" 
                        UPDATE GANT_Resource 
                         SET 
                                Description= '{1}',
                                SiraNo='{2}',
                                KarsiTarafSorumlusu ='{3}',
                                YerelSorumlu ='{4}',
                                ProjeBaslamaTarihi ='{5}',
                                ProjeBitisTarihi='{6}',
                                MODUSER = '{7}',
                                MODDATE = getdate()
                        WHERE Id={0}
                    ",  ResourceId, txtPlanlananFaaliyet.Text, txtSiraNo.Text, cmbFirmaSorum.Text, cmbLocalSorumlu.Text, datePlnBas.Text, datePlnBit.Text, BTI.frmLogin.KullaniciNo));
                ClearScreen();
                MessageBox.Show("Veriler kayıt edildi.");
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }


        private void btnKaydet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            if (GelisYeri == "AnaBaslik" )  SaveAnaBaslik();
            if (GelisYeri == "AltBaslik")   SaveAltBaslik();
            if (GelisYeri == "Duzeltme")    UpdateRecord();
         
            
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void btnNewCard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ClearScreen();
        }

        private void frmProjeEkle_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClosedForm();
        }


    }
}