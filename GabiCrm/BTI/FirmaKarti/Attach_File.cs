﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;
using System.IO;
using System.Data.SqlClient;
using BTI;

namespace GABI_CRM
{
    public partial class Attach_File : DevExpress.XtraEditors.XtraForm
    {
        public Attach_File()
        {
            InitializeComponent();
        }

        public static string MusteriNo;

        private  void GetList()
        {
            try
            {

          

            bbEvrakId.Caption = string.Format("Seçili müşteri kodu >> [{0}]", MusteriNo);
            DABindings.GetXtraGridBindNoDate(grdDocAttachFile,
            string.Format(@"
                    Select  *,
                          Klasor= Tanim,
                          FILENAME2=KTA_REPORT_NAME,			          
						  Ekleyen		  = dbo.GetUserName(KTA_CREUSER),
						  EklenmeTarihi	  = convert(varchar, KTA_CREDATE, 20)                 
                    From ATTACHMENT_FILE 
                    left join
                    (SELECT  GP_VALUE_STR1 as Kod, GP_VIEW as Tanim FROM  GEN_PARAMS Where GP_TYPE='DKlasor')as k on k.Kod= KTA_KLASOR	
                    Where KTA_JOINID='{0}'
                ", MusteriNo));

            var dsL = new DataTable();
            dsL = DbConnSql.SelectSQL(@"SELECT  GP_VALUE_STR1 as Kod, GP_VIEW as Tanim FROM  GEN_PARAMS Where GP_TYPE='DKlasor'");
            repKlasor.DataSource = null;
            repKlasor.DataSource = dsL;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        private void OpenFile()
        {
            try
            {


            Cursor.Current = Cursors.WaitCursor;

            byte[] FileContent = null;
            string SelectedFileName = "";
            string SelectedExtension = "";
            string FullFileName ="";
            string ID = grdVDocAttachFile.GetFocusedRowCellDisplayText(ColDA_ID).ToString();

            if (string.IsNullOrEmpty(ID)) { MessageBox.Show("Seçili dosya yüklenemedi."); return; }

            // Seçtiğim dosyayı veritabanından getiriyorum
            SqlConnection bag = new SqlConnection(DbConnSql.ConnStringSqlDb());
            SqlCommand command = new SqlCommand(string.Format(@"Select  KTA_REPORT_FILE,KTA_FILENAME,KTA_EXTENSION from ATTACHMENT_FILE Where KTA_ID={0} ", ID), bag);
            bag.Open();

            //Dosyanın içeriğini okuyorum
            SqlDataReader dr = command.ExecuteReader();
            if (dr.Read())
            {
                FileContent = (byte[])dr["KTA_REPORT_FILE"];
                SelectedFileName  = dr["KTA_FILENAME"].ToString();
                SelectedExtension = dr["KTA_EXTENSION"].ToString();
            }
            bag.Close();

            FullFileName=    string.Format(@"c:\Gabi_Layout\{0}{1}", SelectedFileName, SelectedExtension);


            MemoryStream memStream = new MemoryStream(FileContent);
            using (FileStream file = new FileStream(FullFileName, FileMode.Create, FileAccess.Write))
            {
                memStream.WriteTo(file);
            }

            PubObjects.RowHandle2(grdVDocAttachFile);
             System.Diagnostics.Process.Start(string.Format(@"{0}", FullFileName));
            PubObjects.SetBookmark2(grdVDocAttachFile);

            /*
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
              {

                  if (saveFileDialog1.FileName != "")
                  {
                      System.IO.FileInfo fileInfo = new System.IO.FileInfo(saveFileDialog1.FileName);

                      string Path = saveFileDialog1.FileName.ToString();
                      string Extension = fileInfo.Extension;
                      StreamWriter writer = new StreamWriter(memStream);
                      writer.Write(Path);
                      writer.Close();
                  }
              }
            
            */



            /*
            XtraReport1 report = new XtraReport1();
            report.LoadLayout(memStream);
            printControl1.PrintingSystem = report.PrintingSystem;
            report.CreateDocument();
             */

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        private  void AttachFile()
        {
            
            if (repKlasorAna.EditValue== null  )
            {
                MessageBox.Show("Klasör adının boş geçemezsiniz.");
                return;
            
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                if (openFileDialog1.FileName != "")
                {
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(openFileDialog1.FileName);

                    string Path = openFileDialog1.FileName.ToString();
                    string Extension = fileInfo.Extension;
                    /*
                    if (GelisYeri == "DASHBOARD")
                    {
                        if (Extension != ".xml")
                        {
                            MessageBox.Show("Eklemeye çalıştığınız dosya formatı xml uzantılı olmalıdır.");
                            return;
                        }
                    }

                    if (GelisYeri == "SALTREPORT")
                    {
                        if (Extension != ".repx")
                        {
                            MessageBox.Show("Eklemeye çalıştığınız dosya formatı .repx uzantılı olmalıdır.");
                            return;
                        }
                    }
                    */
                    string KlasorKod = repKlasorAna.EditValue.ToString();

                    FileStream fs = new FileStream(Path, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    byte[] Dosya = br.ReadBytes((int)fs.Length);
                    br.Close();
                    fs.Close();

                    

                    SqlConnection bag = new SqlConnection(DbConnSql.ConnStringSqlDb());
                    SqlCommand kmt = new SqlCommand(@"
                            INSERT INTO ATTACHMENT_FILE
                                        ( KTA_USERID,
                                          KTA_KLASOR,
                                          KTA_JOINID,
                                          KTA_REPORT_NAME,
                                          KTA_PATH,
                                          KTA_FILENAME,
                                          KTA_EXTENSION,
                                          KTA_REPORT_FILE,
                                          KTA_COMING_LOCATE,
                                          KTA_CREUSER,
                                          KTA_CREDATE
                                         ) 
                                  Values (
                                          @KTA_USERID,
                                          @KTA_KLASOR,
                                          @KTA_JOINID,
                                          @KTA_REPORT_NAME,
                                          @KTA_PATH,
                                          @KTA_FILENAME,
                                          @KTA_EXTENSION,
                                          @KTA_REPORT_FILE,
                                          @KTA_COMING_LOCATE,
                                          @KTA_CREUSER,
                                          GETDATE()
                                         ) ", bag);

                    kmt.Parameters.Add("@KTA_USERID", SqlDbType.Int, Dosya.Length).Value = -1;
                    kmt.Parameters.Add("@KTA_KLASOR", SqlDbType.NVarChar, Dosya.Length).Value = KlasorKod;
                    kmt.Parameters.Add("@KTA_JOINID", SqlDbType.Int, Dosya.Length).Value = MusteriNo;
                    kmt.Parameters.Add("@KTA_REPORT_NAME", SqlDbType.NVarChar, Dosya.Length).Value = fileInfo.Name;
                    kmt.Parameters.Add("@KTA_PATH", SqlDbType.NVarChar, Dosya.Length).Value = "";
                    kmt.Parameters.Add("@KTA_FILENAME", SqlDbType.NVarChar, Dosya.Length).Value = fileInfo.Name;
                    kmt.Parameters.Add("@KTA_EXTENSION", SqlDbType.NVarChar, Dosya.Length).Value = Extension;
                    kmt.Parameters.Add("@KTA_REPORT_FILE", SqlDbType.VarBinary, Dosya.Length).Value = Dosya;
                    kmt.Parameters.Add("@KTA_COMING_LOCATE", SqlDbType.NVarChar, Dosya.Length).Value = "";
                    kmt.Parameters.Add("@KTA_CREUSER", SqlDbType.Int, Dosya.Length).Value = frmLogin.KullaniciNo;

                    try
                    {
                        bag.Open();
                        kmt.ExecuteNonQuery();
                        MessageBox.Show("İlgili döküman eklendi.");
                        GetList();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                    }

                    finally
                    {
                        bag.Close();
                    }
                }
            }
        }



        private void bbAttachFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            AttachFile();
        }

        private void Attach_File_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void grdVDocAttachFile_DoubleClick(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void bbDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string selectedId = grdVDocAttachFile.GetFocusedRowCellDisplayText(ColDA_ID).ToString();


            DbConnSql.CmdDeleteNew(string.Format( @"Delete ATTACHMENT_FILE where KTA_ID={0}", selectedId));
            GetList();
        }
    }
}