﻿namespace GABI_CRM
{
    partial class EkleMusteriTanim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EkleMusteriTanim));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtFirmaAdi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.repSegmantas = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtAdres = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtVergiDairesi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtFirmaTel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtFirmaFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtFirmaWeb = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtCalisanKisi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtBagliDigerFirma = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.repLupUlke = new DevExpress.XtraEditors.LookUpEdit();
            this.repLupSehir = new DevExpress.XtraEditors.LookUpEdit();
            this.repLupIlce = new DevExpress.XtraEditors.LookUpEdit();
            this.repLupNereden = new DevExpress.XtraEditors.LookUpEdit();
            this.repCiro = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbAileSirketimi = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.barMan = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnNewCard = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydet = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnYenileYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrintYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.txtCiroYuzde = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.cmbKartTipi = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtVergiNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.picFirmLogo = new DevExpress.XtraEditors.PictureEdit();
            this.tbMain = new DevExpress.XtraTab.XtraTabControl();
            this.tbGiris = new DevExpress.XtraTab.XtraTabPage();
            this.repSektor = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.repKategori = new DevExpress.XtraEditors.LookUpEdit();
            this.btnResimSec = new DevExpress.XtraEditors.SimpleButton();
            this.tbYetkiliKisiler = new DevExpress.XtraTab.XtraTabPage();
            this.grdYetkiliKisiler = new DevExpress.XtraGrid.GridControl();
            this.lvYetkiliKisiler = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.layoutViewColumn1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_STATU = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repYetkiliStatu = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.layoutViewField_layoutViewColumn110_7 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_ADI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_ADI = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_SOYADI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_SOYADI = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_GOREVI = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_GOREVI = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_EMAIL = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_EMAIL = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_TELEFON1 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repTel2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutViewField_cEMP_TELEFON1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cEMP_TELEFON2 = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_cEMP_TELEFON2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cPicture = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemPictureEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_cPicture = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.cNote = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.layoutViewField_cNote = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemPictureEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemImageEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaAdi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSegmantas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdres.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVergiDairesi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaTel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaWeb.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCalisanKisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBagliDigerFirma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupUlke.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupSehir.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupIlce.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupNereden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCiro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAileSirketimi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCiroYuzde.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKartTipi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVergiNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMain)).BeginInit();
            this.tbMain.SuspendLayout();
            this.tbGiris.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repSektor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repKategori.Properties)).BeginInit();
            this.tbYetkiliKisiler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdYetkiliKisiler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvYetkiliKisiler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYetkiliStatu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_ADI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_SOYADI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_GOREVI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_EMAIL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(16, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(71, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Segmantasyon";
            // 
            // txtFirmaAdi
            // 
            this.txtFirmaAdi.Location = new System.Drawing.Point(115, 52);
            this.txtFirmaAdi.Name = "txtFirmaAdi";
            this.txtFirmaAdi.Size = new System.Drawing.Size(332, 20);
            this.txtFirmaAdi.TabIndex = 11;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(16, 56);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Firma Adı";
            // 
            // repSegmantas
            // 
            this.repSegmantas.EnterMoveNextControl = true;
            this.repSegmantas.Location = new System.Drawing.Point(115, 27);
            this.repSegmantas.Name = "repSegmantas";
            this.repSegmantas.Properties.AutoSearchColumnIndex = 1;
            this.repSegmantas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSegmantas.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repSegmantas.Properties.DisplayMember = "Tanim";
            this.repSegmantas.Properties.DropDownRows = 25;
            this.repSegmantas.Properties.NullText = "";
            this.repSegmantas.Properties.PopupWidth = 600;
            this.repSegmantas.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repSegmantas.Properties.SortColumnIndex = 1;
            this.repSegmantas.Properties.ValueMember = "Kod";
            this.repSegmantas.Size = new System.Drawing.Size(173, 20);
            this.repSegmantas.TabIndex = 0;
            this.repSegmantas.ToolTip = "Kalıp Tipi";
            this.repSegmantas.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 80);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(37, 13);
            this.labelControl3.TabIndex = 21;
            this.labelControl3.Text = "Sektörü";
            // 
            // txtAdres
            // 
            this.txtAdres.Location = new System.Drawing.Point(115, 125);
            this.txtAdres.Name = "txtAdres";
            this.txtAdres.Size = new System.Drawing.Size(332, 20);
            this.txtAdres.TabIndex = 62;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(16, 129);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "Adres";
            // 
            // txtVergiDairesi
            // 
            this.txtVergiDairesi.Location = new System.Drawing.Point(115, 150);
            this.txtVergiDairesi.Name = "txtVergiDairesi";
            this.txtVergiDairesi.Properties.MaxLength = 50;
            this.txtVergiDairesi.Size = new System.Drawing.Size(139, 20);
            this.txtVergiDairesi.TabIndex = 72;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(16, 154);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(59, 13);
            this.labelControl5.TabIndex = 25;
            this.labelControl5.Text = "Vergi Dairesi";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(16, 180);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(20, 13);
            this.labelControl6.TabIndex = 27;
            this.labelControl6.Text = "Ülke";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(16, 207);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(24, 13);
            this.labelControl7.TabIndex = 29;
            this.labelControl7.Text = "Şehir";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(16, 229);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(17, 13);
            this.labelControl8.TabIndex = 31;
            this.labelControl8.Text = "İlçe";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(16, 253);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(76, 13);
            this.labelControl9.TabIndex = 33;
            this.labelControl9.Text = "Nereden Ulaşıldı";
            // 
            // txtFirmaTel
            // 
            this.txtFirmaTel.Location = new System.Drawing.Point(115, 275);
            this.txtFirmaTel.Name = "txtFirmaTel";
            this.txtFirmaTel.Properties.Mask.EditMask = "(\\d?\\d?\\d?)\\d\\d\\d-\\d\\d\\d\\d";
            this.txtFirmaTel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtFirmaTel.Size = new System.Drawing.Size(332, 20);
            this.txtFirmaTel.TabIndex = 122;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(16, 279);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(43, 13);
            this.labelControl10.TabIndex = 35;
            this.labelControl10.Text = "Firma Tel";
            // 
            // txtFirmaFax
            // 
            this.txtFirmaFax.Location = new System.Drawing.Point(115, 300);
            this.txtFirmaFax.Name = "txtFirmaFax";
            this.txtFirmaFax.Properties.Mask.EditMask = "(\\d?\\d?\\d?)\\d\\d\\d-\\d\\d\\d\\d";
            this.txtFirmaFax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtFirmaFax.Size = new System.Drawing.Size(332, 20);
            this.txtFirmaFax.TabIndex = 132;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(16, 304);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(47, 13);
            this.labelControl11.TabIndex = 37;
            this.labelControl11.Text = "Firma Fax";
            // 
            // txtFirmaWeb
            // 
            this.txtFirmaWeb.Location = new System.Drawing.Point(115, 325);
            this.txtFirmaWeb.Name = "txtFirmaWeb";
            this.txtFirmaWeb.Size = new System.Drawing.Size(332, 20);
            this.txtFirmaWeb.TabIndex = 142;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(16, 329);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(54, 13);
            this.labelControl12.TabIndex = 39;
            this.labelControl12.Text = "Firma Web ";
            // 
            // txtCalisanKisi
            // 
            this.txtCalisanKisi.Location = new System.Drawing.Point(115, 350);
            this.txtCalisanKisi.Name = "txtCalisanKisi";
            this.txtCalisanKisi.Properties.Mask.EditMask = "f0";
            this.txtCalisanKisi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCalisanKisi.Size = new System.Drawing.Size(332, 20);
            this.txtCalisanKisi.TabIndex = 152;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(16, 354);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(82, 13);
            this.labelControl13.TabIndex = 41;
            this.labelControl13.Text = "Çalışan Kişi Sayısı";
            // 
            // txtBagliDigerFirma
            // 
            this.txtBagliDigerFirma.Location = new System.Drawing.Point(115, 375);
            this.txtBagliDigerFirma.Name = "txtBagliDigerFirma";
            this.txtBagliDigerFirma.Size = new System.Drawing.Size(332, 20);
            this.txtBagliDigerFirma.TabIndex = 162;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(16, 379);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(91, 13);
            this.labelControl14.TabIndex = 43;
            this.labelControl14.Text = "Bağlı Diğer Firmalar";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(16, 404);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(19, 13);
            this.labelControl15.TabIndex = 45;
            this.labelControl15.Text = "Ciro";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(16, 429);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(51, 13);
            this.labelControl16.TabIndex = 47;
            this.labelControl16.Text = "Ciro Yüzde";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(16, 455);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(59, 13);
            this.labelControl17.TabIndex = 49;
            this.labelControl17.Text = "Aile Şirketimi";
            // 
            // repLupUlke
            // 
            this.repLupUlke.EnterMoveNextControl = true;
            this.repLupUlke.Location = new System.Drawing.Point(115, 175);
            this.repLupUlke.Name = "repLupUlke";
            this.repLupUlke.Properties.AutoSearchColumnIndex = 1;
            this.repLupUlke.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupUlke.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repLupUlke.Properties.DisplayMember = "Tanim";
            this.repLupUlke.Properties.DropDownRows = 25;
            this.repLupUlke.Properties.NullText = "";
            this.repLupUlke.Properties.PopupWidth = 600;
            this.repLupUlke.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repLupUlke.Properties.SortColumnIndex = 1;
            this.repLupUlke.Properties.ValueMember = "Kod";
            this.repLupUlke.Size = new System.Drawing.Size(332, 20);
            this.repLupUlke.TabIndex = 82;
            this.repLupUlke.ToolTip = "Kalıp Tipi";
            this.repLupUlke.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.repLupUlke.EditValueChanged += new System.EventHandler(this.repLupUlke_EditValueChanged);
            // 
            // repLupSehir
            // 
            this.repLupSehir.EnterMoveNextControl = true;
            this.repLupSehir.Location = new System.Drawing.Point(115, 200);
            this.repLupSehir.Name = "repLupSehir";
            this.repLupSehir.Properties.AutoSearchColumnIndex = 1;
            this.repLupSehir.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupSehir.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repLupSehir.Properties.DisplayMember = "Tanim";
            this.repLupSehir.Properties.DropDownRows = 25;
            this.repLupSehir.Properties.NullText = "";
            this.repLupSehir.Properties.PopupWidth = 600;
            this.repLupSehir.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repLupSehir.Properties.SortColumnIndex = 1;
            this.repLupSehir.Properties.ValueMember = "Kod";
            this.repLupSehir.Size = new System.Drawing.Size(332, 20);
            this.repLupSehir.TabIndex = 92;
            this.repLupSehir.ToolTip = "Kalıp Tipi";
            this.repLupSehir.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.repLupSehir.EditValueChanged += new System.EventHandler(this.repLupSehir_EditValueChanged);
            // 
            // repLupIlce
            // 
            this.repLupIlce.EnterMoveNextControl = true;
            this.repLupIlce.Location = new System.Drawing.Point(115, 225);
            this.repLupIlce.Name = "repLupIlce";
            this.repLupIlce.Properties.AutoSearchColumnIndex = 1;
            this.repLupIlce.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupIlce.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GP_ID", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repLupIlce.Properties.DisplayMember = "Tanim";
            this.repLupIlce.Properties.DropDownRows = 25;
            this.repLupIlce.Properties.NullText = "";
            this.repLupIlce.Properties.PopupWidth = 600;
            this.repLupIlce.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repLupIlce.Properties.SortColumnIndex = 1;
            this.repLupIlce.Properties.ValueMember = "GP_ID";
            this.repLupIlce.Size = new System.Drawing.Size(332, 20);
            this.repLupIlce.TabIndex = 102;
            this.repLupIlce.ToolTip = "Kalıp Tipi";
            this.repLupIlce.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // repLupNereden
            // 
            this.repLupNereden.EnterMoveNextControl = true;
            this.repLupNereden.Location = new System.Drawing.Point(115, 250);
            this.repLupNereden.Name = "repLupNereden";
            this.repLupNereden.Properties.AutoSearchColumnIndex = 1;
            this.repLupNereden.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupNereden.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repLupNereden.Properties.DisplayMember = "Tanim";
            this.repLupNereden.Properties.DropDownRows = 25;
            this.repLupNereden.Properties.NullText = "";
            this.repLupNereden.Properties.PopupWidth = 600;
            this.repLupNereden.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repLupNereden.Properties.SortColumnIndex = 1;
            this.repLupNereden.Properties.ValueMember = "Kod";
            this.repLupNereden.Size = new System.Drawing.Size(332, 20);
            this.repLupNereden.TabIndex = 112;
            this.repLupNereden.ToolTip = "Kalıp Tipi";
            this.repLupNereden.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // repCiro
            // 
            this.repCiro.EnterMoveNextControl = true;
            this.repCiro.Location = new System.Drawing.Point(115, 400);
            this.repCiro.Name = "repCiro";
            this.repCiro.Properties.AutoSearchColumnIndex = 1;
            this.repCiro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCiro.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repCiro.Properties.DisplayMember = "Tanim";
            this.repCiro.Properties.DropDownRows = 25;
            this.repCiro.Properties.NullText = "";
            this.repCiro.Properties.PopupWidth = 600;
            this.repCiro.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repCiro.Properties.SortColumnIndex = 1;
            this.repCiro.Properties.ValueMember = "Kod";
            this.repCiro.Size = new System.Drawing.Size(332, 20);
            this.repCiro.TabIndex = 172;
            this.repCiro.ToolTip = "Kalıp Tipi";
            this.repCiro.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // cmbAileSirketimi
            // 
            this.cmbAileSirketimi.EditValue = "Hayır";
            this.cmbAileSirketimi.Location = new System.Drawing.Point(115, 451);
            this.cmbAileSirketimi.Name = "cmbAileSirketimi";
            this.cmbAileSirketimi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbAileSirketimi.Properties.Items.AddRange(new object[] {
            "Evet",
            "Hayır"});
            this.cmbAileSirketimi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbAileSirketimi.Size = new System.Drawing.Size(332, 20);
            this.cmbAileSirketimi.TabIndex = 192;
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            this.SmallImage.Images.SetKeyName(43, "attach.png");
            this.SmallImage.Images.SetKeyName(44, "paper_clip.png");
            this.SmallImage.Images.SetKeyName(45, "note2.png");
            this.SmallImage.Images.SetKeyName(46, "barcode_arrow_up.png");
            // 
            // barMan
            // 
            this.barMan.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar1});
            this.barMan.DockControls.Add(this.barDockControlTop);
            this.barMan.DockControls.Add(this.barDockControlBottom);
            this.barMan.DockControls.Add(this.barDockControlLeft);
            this.barMan.DockControls.Add(this.barDockControlRight);
            this.barMan.DockControls.Add(this.standaloneBarDockControl1);
            this.barMan.DockControls.Add(this.standaloneBarDockControl2);
            this.barMan.Form = this;
            this.barMan.Images = this.SmallImage;
            this.barMan.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnKaydet,
            this.btnClose,
            this.btnYenileYetkili,
            this.btnEkleYetkili,
            this.btnKaydetYetkili,
            this.btnPrintYetkili,
            this.btnDeleteYetkili,
            this.btnNewCard});
            this.barMan.MainMenu = this.bar2;
            this.barMan.MaxItemId = 8;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(294, 193);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNewCard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Main menu";
            // 
            // btnNewCard
            // 
            this.btnNewCard.Caption = "Yeni";
            this.btnNewCard.Id = 7;
            this.btnNewCard.ImageIndex = 6;
            this.btnNewCard.Name = "btnNewCard";
            this.btnNewCard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNewCard_ItemClick);
            // 
            // btnKaydet
            // 
            this.btnKaydet.Caption = "Kaydet";
            this.btnKaydet.Id = 0;
            this.btnKaydet.ImageIndex = 10;
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydet_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "Kapat";
            this.btnClose.Id = 1;
            this.btnClose.ImageIndex = 22;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(785, 25);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(373, 201);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYenileYetkili),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEkleYetkili, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydetYetkili, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrintYetkili, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeleteYetkili, true)});
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar1.Text = "Custom 3";
            // 
            // btnYenileYetkili
            // 
            this.btnYenileYetkili.Caption = "yenile";
            this.btnYenileYetkili.Id = 2;
            this.btnYenileYetkili.ImageIndex = 41;
            this.btnYenileYetkili.Name = "btnYenileYetkili";
            // 
            // btnEkleYetkili
            // 
            this.btnEkleYetkili.Caption = "ekle";
            this.btnEkleYetkili.Id = 3;
            this.btnEkleYetkili.ImageIndex = 42;
            this.btnEkleYetkili.Name = "btnEkleYetkili";
            this.btnEkleYetkili.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEkleYetkili_ItemClick);
            // 
            // btnKaydetYetkili
            // 
            this.btnKaydetYetkili.Caption = "kaydet";
            this.btnKaydetYetkili.Id = 4;
            this.btnKaydetYetkili.ImageIndex = 10;
            this.btnKaydetYetkili.Name = "btnKaydetYetkili";
            this.btnKaydetYetkili.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydetYetkili_ItemClick);
            // 
            // btnPrintYetkili
            // 
            this.btnPrintYetkili.Caption = "yazdir";
            this.btnPrintYetkili.Id = 5;
            this.btnPrintYetkili.ImageIndex = 9;
            this.btnPrintYetkili.Name = "btnPrintYetkili";
            this.btnPrintYetkili.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrintYetkili_ItemClick);
            // 
            // btnDeleteYetkili
            // 
            this.btnDeleteYetkili.Caption = "sil";
            this.btnDeleteYetkili.Id = 6;
            this.btnDeleteYetkili.ImageIndex = 13;
            this.btnDeleteYetkili.Name = "btnDeleteYetkili";
            this.btnDeleteYetkili.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteYetkili_ItemClick);
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(785, 29);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(790, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 515);
            this.barDockControlBottom.Size = new System.Drawing.Size(790, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(790, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // txtCiroYuzde
            // 
            this.txtCiroYuzde.EditValue = "0";
            this.txtCiroYuzde.Location = new System.Drawing.Point(115, 426);
            this.txtCiroYuzde.Name = "txtCiroYuzde";
            this.txtCiroYuzde.Properties.Mask.EditMask = "n2";
            this.txtCiroYuzde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCiroYuzde.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCiroYuzde.Size = new System.Drawing.Size(332, 20);
            this.txtCiroYuzde.TabIndex = 197;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(102, 429);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(11, 13);
            this.labelControl18.TabIndex = 198;
            this.labelControl18.Text = "%";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(294, 30);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(14, 13);
            this.labelControl19.TabIndex = 203;
            this.labelControl19.Text = "Tip";
            // 
            // cmbKartTipi
            // 
            this.cmbKartTipi.EditValue = 2;
            this.cmbKartTipi.Location = new System.Drawing.Point(314, 28);
            this.cmbKartTipi.MenuManager = this.barMan;
            this.cmbKartTipi.Name = "cmbKartTipi";
            this.cmbKartTipi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbKartTipi.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Aktif Müşteri", 1, 31),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Firma Kartı", 2, 34)});
            this.cmbKartTipi.Properties.SmallImages = this.SmallImage;
            this.cmbKartTipi.Size = new System.Drawing.Size(133, 20);
            this.cmbKartTipi.TabIndex = 204;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(260, 154);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(40, 13);
            this.labelControl20.TabIndex = 209;
            this.labelControl20.Text = "Vergi No";
            // 
            // txtVergiNo
            // 
            this.txtVergiNo.Location = new System.Drawing.Point(306, 150);
            this.txtVergiNo.Name = "txtVergiNo";
            this.txtVergiNo.Properties.MaxLength = 40;
            this.txtVergiNo.Size = new System.Drawing.Size(141, 20);
            this.txtVergiNo.TabIndex = 210;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(464, 31);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(63, 13);
            this.labelControl21.TabIndex = 215;
            this.labelControl21.Text = "Firma Logosu";
            // 
            // picFirmLogo
            // 
            this.picFirmLogo.Location = new System.Drawing.Point(464, 52);
            this.picFirmLogo.MenuManager = this.barMan;
            this.picFirmLogo.Name = "picFirmLogo";
            this.picFirmLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picFirmLogo.Size = new System.Drawing.Size(216, 170);
            this.picFirmLogo.TabIndex = 216;
            this.picFirmLogo.EditValueChanged += new System.EventHandler(this.picFirmLogo_EditValueChanged);
            // 
            // tbMain
            // 
            this.tbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMain.Location = new System.Drawing.Point(0, 0);
            this.tbMain.Name = "tbMain";
            this.tbMain.SelectedTabPage = this.tbGiris;
            this.tbMain.Size = new System.Drawing.Size(790, 515);
            this.tbMain.TabIndex = 222;
            this.tbMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tbGiris,
            this.tbYetkiliKisiler});
            this.tbMain.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tbMain_SelectedPageChanged);
            // 
            // tbGiris
            // 
            this.tbGiris.Controls.Add(this.repSektor);
            this.tbGiris.Controls.Add(this.labelControl22);
            this.tbGiris.Controls.Add(this.repKategori);
            this.tbGiris.Controls.Add(this.btnResimSec);
            this.tbGiris.Controls.Add(this.standaloneBarDockControl1);
            this.tbGiris.Controls.Add(this.labelControl1);
            this.tbGiris.Controls.Add(this.labelControl2);
            this.tbGiris.Controls.Add(this.picFirmLogo);
            this.tbGiris.Controls.Add(this.txtFirmaAdi);
            this.tbGiris.Controls.Add(this.labelControl21);
            this.tbGiris.Controls.Add(this.repSegmantas);
            this.tbGiris.Controls.Add(this.txtVergiNo);
            this.tbGiris.Controls.Add(this.labelControl3);
            this.tbGiris.Controls.Add(this.labelControl20);
            this.tbGiris.Controls.Add(this.labelControl4);
            this.tbGiris.Controls.Add(this.cmbKartTipi);
            this.tbGiris.Controls.Add(this.txtAdres);
            this.tbGiris.Controls.Add(this.labelControl19);
            this.tbGiris.Controls.Add(this.labelControl5);
            this.tbGiris.Controls.Add(this.labelControl18);
            this.tbGiris.Controls.Add(this.txtVergiDairesi);
            this.tbGiris.Controls.Add(this.txtCiroYuzde);
            this.tbGiris.Controls.Add(this.labelControl6);
            this.tbGiris.Controls.Add(this.cmbAileSirketimi);
            this.tbGiris.Controls.Add(this.labelControl7);
            this.tbGiris.Controls.Add(this.repCiro);
            this.tbGiris.Controls.Add(this.labelControl8);
            this.tbGiris.Controls.Add(this.repLupNereden);
            this.tbGiris.Controls.Add(this.labelControl9);
            this.tbGiris.Controls.Add(this.repLupIlce);
            this.tbGiris.Controls.Add(this.labelControl10);
            this.tbGiris.Controls.Add(this.repLupSehir);
            this.tbGiris.Controls.Add(this.txtFirmaTel);
            this.tbGiris.Controls.Add(this.repLupUlke);
            this.tbGiris.Controls.Add(this.labelControl11);
            this.tbGiris.Controls.Add(this.txtFirmaFax);
            this.tbGiris.Controls.Add(this.labelControl17);
            this.tbGiris.Controls.Add(this.labelControl12);
            this.tbGiris.Controls.Add(this.labelControl16);
            this.tbGiris.Controls.Add(this.txtFirmaWeb);
            this.tbGiris.Controls.Add(this.labelControl15);
            this.tbGiris.Controls.Add(this.labelControl13);
            this.tbGiris.Controls.Add(this.txtBagliDigerFirma);
            this.tbGiris.Controls.Add(this.txtCalisanKisi);
            this.tbGiris.Controls.Add(this.labelControl14);
            this.tbGiris.Name = "tbGiris";
            this.tbGiris.Size = new System.Drawing.Size(785, 490);
            this.tbGiris.Text = "Ana Giriş";
            // 
            // repSektor
            // 
            this.repSektor.EnterMoveNextControl = true;
            this.repSektor.Location = new System.Drawing.Point(115, 76);
            this.repSektor.Name = "repSektor";
            this.repSektor.Properties.AutoSearchColumnIndex = 1;
            this.repSektor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repSektor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AnaGrup", 15, "Ana Grup"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod")});
            this.repSektor.Properties.DisplayMember = "Tanim";
            this.repSektor.Properties.DropDownRows = 25;
            this.repSektor.Properties.NullText = "";
            this.repSektor.Properties.PopupWidth = 600;
            this.repSektor.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repSektor.Properties.SortColumnIndex = 1;
            this.repSektor.Properties.ValueMember = "Kod";
            this.repSektor.Size = new System.Drawing.Size(332, 20);
            this.repSektor.TabIndex = 30;
            this.repSektor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(16, 104);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(40, 13);
            this.labelControl22.TabIndex = 220;
            this.labelControl22.Text = "Kategori";
            // 
            // repKategori
            // 
            this.repKategori.EnterMoveNextControl = true;
            this.repKategori.Location = new System.Drawing.Point(115, 100);
            this.repKategori.Name = "repKategori";
            this.repKategori.Properties.AutoSearchColumnIndex = 1;
            this.repKategori.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repKategori.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod")});
            this.repKategori.Properties.DisplayMember = "Tanim";
            this.repKategori.Properties.DropDownRows = 25;
            this.repKategori.Properties.NullText = "";
            this.repKategori.Properties.PopupWidth = 600;
            this.repKategori.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repKategori.Properties.SortColumnIndex = 1;
            this.repKategori.Properties.ValueMember = "Kod";
            this.repKategori.Size = new System.Drawing.Size(332, 20);
            this.repKategori.TabIndex = 40;
            this.repKategori.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // btnResimSec
            // 
            this.btnResimSec.Location = new System.Drawing.Point(464, 224);
            this.btnResimSec.Name = "btnResimSec";
            this.btnResimSec.Size = new System.Drawing.Size(216, 23);
            this.btnResimSec.TabIndex = 218;
            this.btnResimSec.Text = "Resim Seç";
            this.btnResimSec.Click += new System.EventHandler(this.btnResimSec_Click_1);
            // 
            // tbYetkiliKisiler
            // 
            this.tbYetkiliKisiler.Controls.Add(this.grdYetkiliKisiler);
            this.tbYetkiliKisiler.Controls.Add(this.standaloneBarDockControl2);
            this.tbYetkiliKisiler.Controls.Add(this.textEdit1);
            this.tbYetkiliKisiler.Name = "tbYetkiliKisiler";
            this.tbYetkiliKisiler.PageVisible = false;
            this.tbYetkiliKisiler.Size = new System.Drawing.Size(785, 490);
            this.tbYetkiliKisiler.Text = "Yetkili Kişiler";
            // 
            // grdYetkiliKisiler
            // 
            this.grdYetkiliKisiler.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdYetkiliKisiler.Location = new System.Drawing.Point(0, 29);
            this.grdYetkiliKisiler.MainView = this.lvYetkiliKisiler;
            this.grdYetkiliKisiler.Name = "grdYetkiliKisiler";
            this.grdYetkiliKisiler.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4,
            this.repositoryItemPictureEdit4,
            this.repositoryItemImageEdit2,
            this.repositoryItemPictureEdit3,
            this.repositoryItemMemoEdit1,
            this.repYetkiliStatu,
            this.repTel2});
            this.grdYetkiliKisiler.Size = new System.Drawing.Size(785, 461);
            this.grdYetkiliKisiler.TabIndex = 6;
            this.grdYetkiliKisiler.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.lvYetkiliKisiler,
            this.gridView1});
            // 
            // lvYetkiliKisiler
            // 
            this.lvYetkiliKisiler.CardHorzInterval = 10;
            this.lvYetkiliKisiler.CardMinSize = new System.Drawing.Size(266, 337);
            this.lvYetkiliKisiler.CardVertInterval = 10;
            this.lvYetkiliKisiler.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.layoutViewColumn1,
            this.cEMP_STATU,
            this.cEMP_ADI,
            this.cEMP_SOYADI,
            this.cEMP_GOREVI,
            this.cEMP_EMAIL,
            this.cEMP_TELEFON1,
            this.cEMP_TELEFON2,
            this.cPicture,
            this.cNote});
            this.lvYetkiliKisiler.GridControl = this.grdYetkiliKisiler;
            this.lvYetkiliKisiler.Name = "lvYetkiliKisiler";
            this.lvYetkiliKisiler.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            this.lvYetkiliKisiler.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.CustomSize;
            this.lvYetkiliKisiler.OptionsItemText.TextToControlDistance = 0;
            this.lvYetkiliKisiler.OptionsSelection.MultiSelect = true;
            this.lvYetkiliKisiler.OptionsView.ContentAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lvYetkiliKisiler.OptionsView.ShowCardCaption = false;
            this.lvYetkiliKisiler.OptionsView.ShowHeaderPanel = false;
            this.lvYetkiliKisiler.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Carousel;
            this.lvYetkiliKisiler.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.cNote, DevExpress.Data.ColumnSortOrder.Descending)});
            this.lvYetkiliKisiler.TemplateCard = this.layoutViewCard1;
            // 
            // layoutViewColumn1
            // 
            this.layoutViewColumn1.Caption = "Müşteri Kodu";
            this.layoutViewColumn1.FieldName = "EmpFirmaKodu";
            this.layoutViewColumn1.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this.layoutViewColumn1.Name = "layoutViewColumn1";
            this.layoutViewColumn1.OptionsColumn.AllowEdit = false;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 196;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(0, 22);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_STATU
            // 
            this.cEMP_STATU.Caption = "Statü";
            this.cEMP_STATU.ColumnEdit = this.repYetkiliStatu;
            this.cEMP_STATU.FieldName = "Emp_YetkiliKisi";
            this.cEMP_STATU.LayoutViewField = this.layoutViewField_layoutViewColumn110_7;
            this.cEMP_STATU.Name = "cEMP_STATU";
            // 
            // repYetkiliStatu
            // 
            this.repYetkiliStatu.AutoHeight = false;
            this.repYetkiliStatu.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repYetkiliStatu.DisplayMember = "Tanim";
            this.repYetkiliStatu.Name = "repYetkiliStatu";
            this.repYetkiliStatu.NullText = "";
            this.repYetkiliStatu.ValueMember = "Tanim";
            // 
            // layoutViewField_layoutViewColumn110_7
            // 
            this.layoutViewField_layoutViewColumn110_7.EditorPreferredWidth = 196;
            this.layoutViewField_layoutViewColumn110_7.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn110_7.Name = "layoutViewField_layoutViewColumn110_7";
            this.layoutViewField_layoutViewColumn110_7.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn110_7.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_layoutViewColumn110_7.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_ADI
            // 
            this.cEMP_ADI.Caption = "Adı";
            this.cEMP_ADI.FieldName = "EmpAdi";
            this.cEMP_ADI.LayoutViewField = this.layoutViewField_cEMP_ADI;
            this.cEMP_ADI.Name = "cEMP_ADI";
            // 
            // layoutViewField_cEMP_ADI
            // 
            this.layoutViewField_cEMP_ADI.EditorPreferredWidth = 196;
            this.layoutViewField_cEMP_ADI.Location = new System.Drawing.Point(0, 44);
            this.layoutViewField_cEMP_ADI.Name = "layoutViewField_cEMP_ADI";
            this.layoutViewField_cEMP_ADI.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cEMP_ADI.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_cEMP_ADI.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_SOYADI
            // 
            this.cEMP_SOYADI.Caption = "Soyadı";
            this.cEMP_SOYADI.FieldName = "EmpSoyadi";
            this.cEMP_SOYADI.LayoutViewField = this.layoutViewField_cEMP_SOYADI;
            this.cEMP_SOYADI.Name = "cEMP_SOYADI";
            // 
            // layoutViewField_cEMP_SOYADI
            // 
            this.layoutViewField_cEMP_SOYADI.EditorPreferredWidth = 196;
            this.layoutViewField_cEMP_SOYADI.Location = new System.Drawing.Point(0, 66);
            this.layoutViewField_cEMP_SOYADI.Name = "layoutViewField_cEMP_SOYADI";
            this.layoutViewField_cEMP_SOYADI.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cEMP_SOYADI.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_cEMP_SOYADI.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_GOREVI
            // 
            this.cEMP_GOREVI.Caption = "Görevi";
            this.cEMP_GOREVI.FieldName = "EmpGorevi";
            this.cEMP_GOREVI.LayoutViewField = this.layoutViewField_cEMP_GOREVI;
            this.cEMP_GOREVI.Name = "cEMP_GOREVI";
            // 
            // layoutViewField_cEMP_GOREVI
            // 
            this.layoutViewField_cEMP_GOREVI.EditorPreferredWidth = 196;
            this.layoutViewField_cEMP_GOREVI.Location = new System.Drawing.Point(0, 88);
            this.layoutViewField_cEMP_GOREVI.Name = "layoutViewField_cEMP_GOREVI";
            this.layoutViewField_cEMP_GOREVI.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cEMP_GOREVI.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_cEMP_GOREVI.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_EMAIL
            // 
            this.cEMP_EMAIL.Caption = "Email";
            this.cEMP_EMAIL.FieldName = "EmpEmail";
            this.cEMP_EMAIL.LayoutViewField = this.layoutViewField_cEMP_EMAIL;
            this.cEMP_EMAIL.Name = "cEMP_EMAIL";
            // 
            // layoutViewField_cEMP_EMAIL
            // 
            this.layoutViewField_cEMP_EMAIL.EditorPreferredWidth = 196;
            this.layoutViewField_cEMP_EMAIL.Location = new System.Drawing.Point(0, 110);
            this.layoutViewField_cEMP_EMAIL.Name = "layoutViewField_cEMP_EMAIL";
            this.layoutViewField_cEMP_EMAIL.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cEMP_EMAIL.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_cEMP_EMAIL.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_TELEFON1
            // 
            this.cEMP_TELEFON1.Caption = "Telefon 1";
            this.cEMP_TELEFON1.ColumnEdit = this.repTel2;
            this.cEMP_TELEFON1.FieldName = "EmpTelefon1";
            this.cEMP_TELEFON1.LayoutViewField = this.layoutViewField_cEMP_TELEFON1;
            this.cEMP_TELEFON1.Name = "cEMP_TELEFON1";
            // 
            // repTel2
            // 
            this.repTel2.AutoHeight = false;
            this.repTel2.Mask.EditMask = "(\\d?\\d?\\d?)\\d\\d\\d-\\d\\d\\d\\d";
            this.repTel2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.repTel2.Name = "repTel2";
            // 
            // layoutViewField_cEMP_TELEFON1
            // 
            this.layoutViewField_cEMP_TELEFON1.EditorPreferredWidth = 196;
            this.layoutViewField_cEMP_TELEFON1.Location = new System.Drawing.Point(0, 132);
            this.layoutViewField_cEMP_TELEFON1.Name = "layoutViewField_cEMP_TELEFON1";
            this.layoutViewField_cEMP_TELEFON1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cEMP_TELEFON1.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_cEMP_TELEFON1.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cEMP_TELEFON2
            // 
            this.cEMP_TELEFON2.Caption = "Telefon 2";
            this.cEMP_TELEFON2.ColumnEdit = this.repTel2;
            this.cEMP_TELEFON2.FieldName = "EmpTelefon2";
            this.cEMP_TELEFON2.LayoutViewField = this.layoutViewField_cEMP_TELEFON2;
            this.cEMP_TELEFON2.Name = "cEMP_TELEFON2";
            // 
            // layoutViewField_cEMP_TELEFON2
            // 
            this.layoutViewField_cEMP_TELEFON2.EditorPreferredWidth = 196;
            this.layoutViewField_cEMP_TELEFON2.Location = new System.Drawing.Point(0, 154);
            this.layoutViewField_cEMP_TELEFON2.Name = "layoutViewField_cEMP_TELEFON2";
            this.layoutViewField_cEMP_TELEFON2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cEMP_TELEFON2.Size = new System.Drawing.Size(264, 22);
            this.layoutViewField_cEMP_TELEFON2.TextSize = new System.Drawing.Size(66, 20);
            // 
            // cPicture
            // 
            this.cPicture.ColumnEdit = this.repositoryItemPictureEdit3;
            this.cPicture.FieldName = "EmpFoto";
            this.cPicture.LayoutViewField = this.layoutViewField_cPicture;
            this.cPicture.Name = "cPicture";
            // 
            // repositoryItemPictureEdit3
            // 
            this.repositoryItemPictureEdit3.Name = "repositoryItemPictureEdit3";
            this.repositoryItemPictureEdit3.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            // 
            // layoutViewField_cPicture
            // 
            this.layoutViewField_cPicture.EditorPreferredWidth = 262;
            this.layoutViewField_cPicture.Location = new System.Drawing.Point(0, 176);
            this.layoutViewField_cPicture.Name = "layoutViewField_cPicture";
            this.layoutViewField_cPicture.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cPicture.Size = new System.Drawing.Size(264, 109);
            this.layoutViewField_cPicture.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_cPicture.TextToControlDistance = 0;
            this.layoutViewField_cPicture.TextVisible = false;
            // 
            // cNote
            // 
            this.cNote.Caption = " Not";
            this.cNote.ColumnEdit = this.repositoryItemMemoEdit1;
            this.cNote.FieldName = "Note";
            this.cNote.LayoutViewField = this.layoutViewField_cNote;
            this.cNote.Name = "cNote";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // layoutViewField_cNote
            // 
            this.layoutViewField_cNote.EditorPreferredWidth = 196;
            this.layoutViewField_cNote.Location = new System.Drawing.Point(0, 285);
            this.layoutViewField_cNote.MaxSize = new System.Drawing.Size(264, 50);
            this.layoutViewField_cNote.MinSize = new System.Drawing.Size(264, 50);
            this.layoutViewField_cNote.Name = "layoutViewField_cNote";
            this.layoutViewField_cNote.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_cNote.Size = new System.Drawing.Size(264, 50);
            this.layoutViewField_cNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_cNote.TextSize = new System.Drawing.Size(66, 20);
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1,
            this.layoutViewField_cEMP_ADI,
            this.layoutViewField_cEMP_SOYADI,
            this.layoutViewField_cEMP_GOREVI,
            this.layoutViewField_cEMP_EMAIL,
            this.layoutViewField_cEMP_TELEFON1,
            this.layoutViewField_cEMP_TELEFON2,
            this.layoutViewField_cPicture,
            this.layoutViewField_cNote,
            this.layoutViewField_layoutViewColumn110_7});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 0;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.Items.AddRange(new object[] {
            "",
            "%F",
            "δ"});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            this.repositoryItemComboBox5.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.DisplayMember = "QMW_WORKPLACE";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            this.repositoryItemLookUpEdit3.ValueMember = "QMW_WORKPLACE";
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.DisplayMember = "Ulke";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.NullText = "";
            this.repositoryItemLookUpEdit4.ValueMember = "UlkeKodu";
            // 
            // repositoryItemPictureEdit4
            // 
            this.repositoryItemPictureEdit4.Name = "repositoryItemPictureEdit4";
            // 
            // repositoryItemImageEdit2
            // 
            this.repositoryItemImageEdit2.AutoHeight = false;
            this.repositoryItemImageEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit2.Name = "repositoryItemImageEdit2";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdYetkiliKisiler;
            this.gridView1.Name = "gridView1";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(343, 18);
            this.textEdit1.MenuManager = this.barMan;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(100, 20);
            this.textEdit1.TabIndex = 3;
            // 
            // EkleMusteriTanim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(790, 515);
            this.Controls.Add(this.tbMain);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EkleMusteriTanim";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Firma Kartı";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EkleMusteriTanim_FormClosed);
            this.Load += new System.EventHandler(this.EkleMusteriTanim_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaAdi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSegmantas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdres.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVergiDairesi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaTel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirmaWeb.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCalisanKisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBagliDigerFirma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupUlke.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupSehir.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupIlce.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupNereden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCiro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAileSirketimi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCiroYuzde.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKartTipi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVergiNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMain)).EndInit();
            this.tbMain.ResumeLayout(false);
            this.tbGiris.ResumeLayout(false);
            this.tbGiris.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repSektor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repKategori.Properties)).EndInit();
            this.tbYetkiliKisiler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdYetkiliKisiler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvYetkiliKisiler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repYetkiliStatu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn110_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_ADI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_SOYADI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_GOREVI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_EMAIL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cEMP_TELEFON2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_cNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtFirmaAdi;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit repSegmantas;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtAdres;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtVergiDairesi;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtFirmaTel;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtFirmaFax;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtFirmaWeb;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtCalisanKisi;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtBagliDigerFirma;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LookUpEdit repLupUlke;
        private DevExpress.XtraEditors.LookUpEdit repLupSehir;
        private DevExpress.XtraEditors.LookUpEdit repLupIlce;
        private DevExpress.XtraEditors.LookUpEdit repLupNereden;
        private DevExpress.XtraEditors.LookUpEdit repCiro;
        private DevExpress.XtraEditors.ComboBoxEdit cmbAileSirketimi;
        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarManager barMan;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnKaydet;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraEditors.TextEdit txtCiroYuzde;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbKartTipi;
        private DevExpress.XtraEditors.TextEdit txtVergiNo;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.PictureEdit picFirmLogo;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraTab.XtraTabControl tbMain;
        private DevExpress.XtraTab.XtraTabPage tbGiris;
        private DevExpress.XtraTab.XtraTabPage tbYetkiliKisiler;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnYenileYetkili;
        private DevExpress.XtraBars.BarButtonItem btnEkleYetkili;
        private DevExpress.XtraBars.BarButtonItem btnKaydetYetkili;
        private DevExpress.XtraBars.BarButtonItem btnPrintYetkili;
        private DevExpress.XtraBars.BarButtonItem btnDeleteYetkili;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraBars.BarButtonItem btnNewCard;
        private DevExpress.XtraEditors.SimpleButton btnResimSec;
        private DevExpress.XtraGrid.GridControl grdYetkiliKisiler;
        private DevExpress.XtraGrid.Views.Layout.LayoutView lvYetkiliKisiler;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn layoutViewColumn1;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_STATU;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repYetkiliStatu;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_ADI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_SOYADI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_GOREVI;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_EMAIL;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_TELEFON1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTel2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cEMP_TELEFON2;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cPicture;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit3;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn cNote;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn110_7;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_ADI;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_SOYADI;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_GOREVI;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_EMAIL;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_TELEFON1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cEMP_TELEFON2;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cPicture;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_cNote;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LookUpEdit repKategori;
        private DevExpress.XtraEditors.LookUpEdit repSektor;
    }
}