﻿namespace GABI_CRM
{
    partial class frmProjeEkle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProjeEkle));
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.barMan = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnNewCard = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydet = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnYenileYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnEkleYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnKaydetYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrintYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteYetkili = new DevExpress.XtraBars.BarButtonItem();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtPlanlananFaaliyet = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.datePlnBit = new DevExpress.XtraEditors.DateEdit();
            this.datePlnBas = new DevExpress.XtraEditors.DateEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.txtSiraNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbLocalSorumlu = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cmbFirmaSorum = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlanlananFaaliyet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBas.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSiraNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLocalSorumlu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFirmaSorum.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            this.SmallImage.Images.SetKeyName(43, "attach.png");
            this.SmallImage.Images.SetKeyName(44, "paper_clip.png");
            this.SmallImage.Images.SetKeyName(45, "note2.png");
            this.SmallImage.Images.SetKeyName(46, "barcode_arrow_up.png");
            // 
            // barMan
            // 
            this.barMan.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barMan.DockControls.Add(this.barDockControlTop);
            this.barMan.DockControls.Add(this.barDockControlBottom);
            this.barMan.DockControls.Add(this.barDockControlLeft);
            this.barMan.DockControls.Add(this.barDockControlRight);
            this.barMan.Form = this;
            this.barMan.Images = this.SmallImage;
            this.barMan.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnKaydet,
            this.btnClose,
            this.btnYenileYetkili,
            this.btnEkleYetkili,
            this.btnKaydetYetkili,
            this.btnPrintYetkili,
            this.btnDeleteYetkili,
            this.btnNewCard});
            this.barMan.MainMenu = this.bar2;
            this.barMan.MaxItemId = 8;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(486, 142);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNewCard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnNewCard
            // 
            this.btnNewCard.Caption = "Yeni";
            this.btnNewCard.Id = 7;
            this.btnNewCard.ImageIndex = 6;
            this.btnNewCard.Name = "btnNewCard";
            this.btnNewCard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNewCard_ItemClick);
            // 
            // btnKaydet
            // 
            this.btnKaydet.Caption = "Kaydet";
            this.btnKaydet.Id = 0;
            this.btnKaydet.ImageIndex = 10;
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydet_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "Kapat";
            this.btnClose.Id = 1;
            this.btnClose.ImageIndex = 22;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(705, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 183);
            this.barDockControlBottom.Size = new System.Drawing.Size(705, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 157);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(705, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 157);
            // 
            // btnYenileYetkili
            // 
            this.btnYenileYetkili.Caption = "yenile";
            this.btnYenileYetkili.Id = 2;
            this.btnYenileYetkili.ImageIndex = 41;
            this.btnYenileYetkili.Name = "btnYenileYetkili";
            // 
            // btnEkleYetkili
            // 
            this.btnEkleYetkili.Caption = "ekle";
            this.btnEkleYetkili.Id = 3;
            this.btnEkleYetkili.ImageIndex = 42;
            this.btnEkleYetkili.Name = "btnEkleYetkili";
            // 
            // btnKaydetYetkili
            // 
            this.btnKaydetYetkili.Caption = "kaydet";
            this.btnKaydetYetkili.Id = 4;
            this.btnKaydetYetkili.ImageIndex = 10;
            this.btnKaydetYetkili.Name = "btnKaydetYetkili";
            // 
            // btnPrintYetkili
            // 
            this.btnPrintYetkili.Caption = "yazdir";
            this.btnPrintYetkili.Id = 5;
            this.btnPrintYetkili.ImageIndex = 9;
            this.btnPrintYetkili.Name = "btnPrintYetkili";
            // 
            // btnDeleteYetkili
            // 
            this.btnDeleteYetkili.Caption = "sil";
            this.btnDeleteYetkili.Id = 6;
            this.btnDeleteYetkili.ImageIndex = 13;
            this.btnDeleteYetkili.Name = "btnDeleteYetkili";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(21, 49);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(87, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Planlanan Faaliyet";
            // 
            // txtPlanlananFaaliyet
            // 
            this.txtPlanlananFaaliyet.EditValue = "";
            this.txtPlanlananFaaliyet.Location = new System.Drawing.Point(120, 45);
            this.txtPlanlananFaaliyet.Name = "txtPlanlananFaaliyet";
            this.txtPlanlananFaaliyet.Size = new System.Drawing.Size(575, 20);
            this.txtPlanlananFaaliyet.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 70);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(78, 13);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Firma Sorumlusu";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(21, 91);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 13);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "Progrup Sorumlusu";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(21, 112);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(96, 13);
            this.labelControl4.TabIndex = 16;
            this.labelControl4.Text = "Proje Başlama Tarihi";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(21, 133);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(76, 13);
            this.labelControl5.TabIndex = 17;
            this.labelControl5.Text = "Proje Bitiş Tarihi";
            // 
            // datePlnBit
            // 
            this.datePlnBit.EditValue = null;
            this.datePlnBit.Location = new System.Drawing.Point(120, 129);
            this.datePlnBit.MenuManager = this.barMan;
            this.datePlnBit.Name = "datePlnBit";
            this.datePlnBit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePlnBit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePlnBit.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.datePlnBit.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.datePlnBit.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.datePlnBit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.datePlnBit.Size = new System.Drawing.Size(175, 20);
            this.datePlnBit.TabIndex = 5;
            // 
            // datePlnBas
            // 
            this.datePlnBas.EditValue = null;
            this.datePlnBas.Location = new System.Drawing.Point(120, 108);
            this.datePlnBas.MenuManager = this.barMan;
            this.datePlnBas.Name = "datePlnBas";
            this.datePlnBas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePlnBas.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePlnBas.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.datePlnBas.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.datePlnBas.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.datePlnBas.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.datePlnBas.Size = new System.Drawing.Size(175, 20);
            this.datePlnBas.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.datePlnBit);
            this.groupControl3.Controls.Add(this.txtSiraNo);
            this.groupControl3.Controls.Add(this.datePlnBas);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.cmbLocalSorumlu);
            this.groupControl3.Controls.Add(this.cmbFirmaSorum);
            this.groupControl3.Controls.Add(this.labelControl2);
            this.groupControl3.Controls.Add(this.txtPlanlananFaaliyet);
            this.groupControl3.Controls.Add(this.labelControl1);
            this.groupControl3.Controls.Add(this.labelControl3);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 26);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(705, 157);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "Proje Detayları";
            // 
            // txtSiraNo
            // 
            this.txtSiraNo.EditValue = "";
            this.txtSiraNo.Location = new System.Drawing.Point(120, 24);
            this.txtSiraNo.Name = "txtSiraNo";
            this.txtSiraNo.Properties.MaxLength = 15;
            this.txtSiraNo.Size = new System.Drawing.Size(106, 20);
            this.txtSiraNo.TabIndex = 0;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(21, 28);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(34, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Sıra No";
            // 
            // cmbLocalSorumlu
            // 
            this.cmbLocalSorumlu.Location = new System.Drawing.Point(120, 87);
            this.cmbLocalSorumlu.MenuManager = this.barMan;
            this.cmbLocalSorumlu.Name = "cmbLocalSorumlu";
            this.cmbLocalSorumlu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbLocalSorumlu.Size = new System.Drawing.Size(575, 20);
            this.cmbLocalSorumlu.TabIndex = 10;
            // 
            // cmbFirmaSorum
            // 
            this.cmbFirmaSorum.EditValue = "";
            this.cmbFirmaSorum.Location = new System.Drawing.Point(120, 66);
            this.cmbFirmaSorum.MenuManager = this.barMan;
            this.cmbFirmaSorum.Name = "cmbFirmaSorum";
            this.cmbFirmaSorum.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbFirmaSorum.Size = new System.Drawing.Size(575, 20);
            this.cmbFirmaSorum.TabIndex = 5;
            // 
            // frmProjeEkle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 183);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmProjeEkle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Proje Detayları";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmProjeEkle_FormClosed);
            this.Load += new System.EventHandler(this.frmProjeEkle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlanlananFaaliyet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBas.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePlnBas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSiraNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLocalSorumlu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFirmaSorum.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarManager barMan;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnNewCard;
        private DevExpress.XtraBars.BarButtonItem btnKaydet;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnYenileYetkili;
        private DevExpress.XtraBars.BarButtonItem btnEkleYetkili;
        private DevExpress.XtraBars.BarButtonItem btnKaydetYetkili;
        private DevExpress.XtraBars.BarButtonItem btnPrintYetkili;
        private DevExpress.XtraBars.BarButtonItem btnDeleteYetkili;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtPlanlananFaaliyet;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbLocalSorumlu;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbFirmaSorum;
        private DevExpress.XtraEditors.DateEdit datePlnBit;
        private DevExpress.XtraEditors.DateEdit datePlnBas;
        private DevExpress.XtraEditors.TextEdit txtSiraNo;
        private DevExpress.XtraEditors.LabelControl labelControl8;
    }
}