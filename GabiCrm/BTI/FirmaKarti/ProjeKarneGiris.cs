﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using Global;


namespace GABI_CRM
{
    public partial class frmProjeKarneGiris : DevExpress.XtraEditors.XtraForm
    {
        public frmProjeKarneGiris()
        {
            InitializeComponent();
        }

        
        public static string FTITLE  = "...";
        public static string ProjeNo = "";
    

        SqlDataAdapter SqlDap;
        SqlCommandBuilder SqlScb;
        DataTable dtGenel;

        private void GetList()
        {
            this.Text = FTITLE;

            DABindings.GetXtraLookupBindNm(lupProjeTanimlari, string.Format(@"select ModulKodu,ModulTanim,ProjeNo from dbo. ProjeDetayKullanilanModuller Where ProjeNo={0} ", ProjeNo));



         
        }

        private void GetKarneNotlari()
        {
        
           Cursor.Current = Cursors.WaitCursor;

           if (string.IsNullOrEmpty(lupProjeTanimlari.Text))
           {
               MessageBox.Show("Proje tanımını boş geçemezsiniz.");
               return;
           }


           string ProjeModulKodu = !string.IsNullOrEmpty(lupProjeTanimlari.GetColumnValue("ModulKodu").ToString()) ?lupProjeTanimlari.GetColumnValue("ModulKodu").ToString() : "" ;
           if (string.IsNullOrEmpty(ProjeModulKodu))
           {
               MessageBox.Show("Proje tanımını boş geçemezsiniz.");
               return;
           }



           string Sql = string.Format(@"SELECT  * FROM  PROJE_DETAY_DEGERLENDIRME Where Pdtd_Pdt_Id='{0}' and  Pdtd_Pdt_ProjeModulAdi='{1}'", ProjeNo, ProjeModulKodu);
                SqlDap = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenel = new DataTable();
                SqlScb = new SqlCommandBuilder(SqlDap);
                SqlScb.DataAdapter.Fill(dtGenel);
            grd.DataSource = dtGenel;




            Cursor.Current = Cursors.Default;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textEdit1.Focus();
            DataView _dv = new DataView(dtGenel);
            for (int i = 0; i < _dv.Count; i++)
            {
                string kon1 = _dv[i]["Ptdt_KarneTar"].ToString();
                if (string.IsNullOrEmpty(kon1))
                {
                    MessageBox.Show("Karne tarihini boş geçemezsiniz.");
                    return;
                }

                string kon2 = _dv[i]["Ptdt_KarneNotu"].ToString();
                if (string.IsNullOrEmpty(kon2))
                {
                    MessageBox.Show("Karne notunu boş geçemezsiniz.");
                    return;
                }
               
            }
            //if (Glob.ControlDoubleRows(_dv, "GP_VALUE_STR1") == true) { return; }            
           
         
            SqlScb.DataAdapter.Update(dtGenel);
            MessageBox.Show("Veriler güncellendi.");

        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grd.ShowPrintPreview();
        }

        private void grdV_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {

         

            }

            if (e.KeyCode == Keys.Insert)
            {
                     
          

            }
          
        }

        private void frmUlkeler_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void btnEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(lupProjeTanimlari.Text))
            {
                MessageBox.Show("Proje tanımını boş geçemezsiniz.");
                return;
            }

            string ProjeModulKodu = !string.IsNullOrEmpty(lupProjeTanimlari.GetColumnValue("ModulKodu").ToString()) ? lupProjeTanimlari.GetColumnValue("ModulKodu").ToString() : "";

            if  ( string.IsNullOrEmpty(ProjeModulKodu) )
            {
                MessageBox.Show("Proje tanımını boş geçemezsiniz.");
                return;
            }


            textEdit1.Focus();
            grdV.AddNewRow();
            
            grdV.SetRowCellValue(grdV.FocusedRowHandle, grdV.Columns["Pdtd_Pdt_Id"], ProjeNo);
            grdV.SetRowCellValue(grdV.FocusedRowHandle, grdV.Columns["Pdtd_Pdt_ProjeModulAdi"], ProjeModulKodu);
            grdV.FocusedRowHandle = grdV.GetVisibleRowHandle(grdV.FocusedRowHandle);
            grd.RefreshDataSource();
        }

        private void btnSil_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            grdV.DeleteRow(grdV.FocusedRowHandle);
            grd.RefreshDataSource();
        }

        private void lupProjeTanimlari_EditValueChanged(object sender, EventArgs e)
        {
            GetKarneNotlari();
        }


    }
}