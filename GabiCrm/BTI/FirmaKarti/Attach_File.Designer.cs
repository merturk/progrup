﻿namespace GABI_CRM
{
    partial class Attach_File
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Attach_File));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.bm = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.repKlasorAna = new DevExpress.XtraBars.BarEditItem();
            this.repKlasor = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bbAttachFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbEvrakId = new DevExpress.XtraBars.BarStaticItem();
            this.bbDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.grdDocAttachFile = new DevExpress.XtraGrid.GridControl();
            this.grdVDocAttachFile = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColDA_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKlasor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDA_FILENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDA_FILENAME2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDA_PATH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDA_EXTENSION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.ColEkleyen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColEklenmeTarihi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColKTI_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_ITEMNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_CUST_ITEMNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_CALCULATOR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_STOCKGROUP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_QUANTITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_ISCREATE_BOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColKTI_NOTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cardView2 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repKlasor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDocAttachFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVDocAttachFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView2)).BeginInit();
            this.SuspendLayout();
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(22, "add.png");
            this.SmallImage.Images.SetKeyName(23, "file_excel.png");
            this.SmallImage.Images.SetKeyName(24, "pdf.png");
            this.SmallImage.Images.SetKeyName(25, "word.png");
            this.SmallImage.Images.SetKeyName(26, "txt.png");
            this.SmallImage.Images.SetKeyName(27, "bmp.png");
            this.SmallImage.Images.SetKeyName(28, "crystal_jpg.png");
            this.SmallImage.Images.SetKeyName(29, "docx.png");
            this.SmallImage.Images.SetKeyName(30, "gif.png");
            this.SmallImage.Images.SetKeyName(31, "png.png");
            this.SmallImage.Images.SetKeyName(32, "pptx.png");
            this.SmallImage.Images.SetKeyName(33, "zip.png");
            this.SmallImage.Images.SetKeyName(34, "rar.png");
            // 
            // bm
            // 
            this.bm.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.bm.DockControls.Add(this.barDockControlTop);
            this.bm.DockControls.Add(this.barDockControlBottom);
            this.bm.DockControls.Add(this.barDockControlLeft);
            this.bm.DockControls.Add(this.barDockControlRight);
            this.bm.Form = this;
            this.bm.Images = this.SmallImage;
            this.bm.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbAttachFile,
            this.bbEvrakId,
            this.barButtonItem1,
            this.bbDelete,
            this.repKlasorAna});
            this.bm.MaxItemId = 6;
            this.bm.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repKlasor});
            this.bm.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.repKlasorAna),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbAttachFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbEvrakId, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbDelete)});
            this.bar1.Text = "Tools";
            // 
            // repKlasorAna
            // 
            this.repKlasorAna.Caption = "Klasor Adi";
            this.repKlasorAna.Edit = this.repKlasor;
            this.repKlasorAna.Id = 5;
            this.repKlasorAna.Name = "repKlasorAna";
            this.repKlasorAna.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.repKlasorAna.Width = 140;
            // 
            // repKlasor
            // 
            this.repKlasor.AutoHeight = false;
            this.repKlasor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repKlasor.DisplayMember = "Tanim";
            this.repKlasor.Name = "repKlasor";
            this.repKlasor.NullText = "";
            this.repKlasor.ValueMember = "Kod";
            // 
            // bbAttachFile
            // 
            this.bbAttachFile.Caption = "Ekle";
            this.bbAttachFile.Id = 0;
            this.bbAttachFile.ImageIndex = 22;
            this.bbAttachFile.Name = "bbAttachFile";
            this.bbAttachFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbAttachFile_ItemClick);
            // 
            // bbEvrakId
            // 
            this.bbEvrakId.Caption = "  ";
            this.bbEvrakId.Id = 1;
            this.bbEvrakId.Name = "bbEvrakId";
            this.bbEvrakId.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bbDelete
            // 
            this.bbDelete.Caption = "Sil";
            this.bbDelete.Id = 3;
            this.bbDelete.ImageIndex = 13;
            this.bbDelete.Name = "bbDelete";
            toolTipTitleItem1.Text = "Bilgi";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Seçili kaydı sil";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbDelete.SuperTip = superToolTip1;
            this.bbDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbDelete_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(575, 27);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 385);
            this.barDockControlBottom.Size = new System.Drawing.Size(575, 22);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 27);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 358);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(575, 27);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 358);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Sil";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // grdDocAttachFile
            // 
            this.grdDocAttachFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDocAttachFile.Location = new System.Drawing.Point(0, 27);
            this.grdDocAttachFile.MainView = this.grdVDocAttachFile;
            this.grdDocAttachFile.Name = "grdDocAttachFile";
            this.grdDocAttachFile.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.grdDocAttachFile.Size = new System.Drawing.Size(575, 358);
            this.grdDocAttachFile.TabIndex = 22;
            this.grdDocAttachFile.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVDocAttachFile,
            this.gridView1,
            this.cardView2});
            // 
            // grdVDocAttachFile
            // 
            this.grdVDocAttachFile.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColDA_ID,
            this.colKlasor,
            this.ColDA_FILENAME,
            this.ColDA_FILENAME2,
            this.ColDA_PATH,
            this.ColDA_EXTENSION,
            this.ColEkleyen,
            this.ColEklenmeTarihi});
            this.grdVDocAttachFile.GridControl = this.grdDocAttachFile;
            this.grdVDocAttachFile.Name = "grdVDocAttachFile";
            this.grdVDocAttachFile.OptionsBehavior.Editable = false;
            this.grdVDocAttachFile.OptionsView.ColumnAutoWidth = false;
            this.grdVDocAttachFile.OptionsView.ShowGroupPanel = false;
            this.grdVDocAttachFile.DoubleClick += new System.EventHandler(this.grdVDocAttachFile_DoubleClick);
            // 
            // ColDA_ID
            // 
            this.ColDA_ID.Caption = "ID";
            this.ColDA_ID.FieldName = "KTA_ID";
            this.ColDA_ID.Name = "ColDA_ID";
            // 
            // colKlasor
            // 
            this.colKlasor.Caption = "Klasör";
            this.colKlasor.FieldName = "Klasor";
            this.colKlasor.Name = "colKlasor";
            this.colKlasor.Visible = true;
            this.colKlasor.VisibleIndex = 0;
            this.colKlasor.Width = 98;
            // 
            // ColDA_FILENAME
            // 
            this.ColDA_FILENAME.Caption = "Döküman";
            this.ColDA_FILENAME.FieldName = "KTA_FILENAME";
            this.ColDA_FILENAME.Name = "ColDA_FILENAME";
            this.ColDA_FILENAME.ToolTip = "Düküman";
            this.ColDA_FILENAME.Width = 367;
            // 
            // ColDA_FILENAME2
            // 
            this.ColDA_FILENAME2.Caption = "Döküman";
            this.ColDA_FILENAME2.FieldName = "FILENAME2";
            this.ColDA_FILENAME2.Name = "ColDA_FILENAME2";
            this.ColDA_FILENAME2.Visible = true;
            this.ColDA_FILENAME2.VisibleIndex = 1;
            this.ColDA_FILENAME2.Width = 185;
            // 
            // ColDA_PATH
            // 
            this.ColDA_PATH.Caption = "Dosya Yolu";
            this.ColDA_PATH.FieldName = "KTA_PATH";
            this.ColDA_PATH.Name = "ColDA_PATH";
            // 
            // ColDA_EXTENSION
            // 
            this.ColDA_EXTENSION.Caption = " ";
            this.ColDA_EXTENSION.ColumnEdit = this.repositoryItemImageComboBox1;
            this.ColDA_EXTENSION.FieldName = "KTA_EXTENSION";
            this.ColDA_EXTENSION.Name = "ColDA_EXTENSION";
            this.ColDA_EXTENSION.ToolTip = "Dosya Uzantı Simgesi";
            this.ColDA_EXTENSION.Visible = true;
            this.ColDA_EXTENSION.VisibleIndex = 2;
            this.ColDA_EXTENSION.Width = 31;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".xls", ".xls", 23),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".xlsx", ".xlsx", 23),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".doc", ".doc", 25),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".docx", ".docx", 29),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".pptx", ".pptx", 32),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".pdf", ".pdf", 24),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".txt", ".txt", 26),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".bmp", ".bmp", 27),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".jpg", ".jpg", 28),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".gif", ".gif", 30),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".png", ".png", 30),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".rar", ".rar", 34),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(".zip", ".zip", 33)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.SmallImage;
            // 
            // ColEkleyen
            // 
            this.ColEkleyen.Caption = "Ekleyen";
            this.ColEkleyen.FieldName = "Ekleyen";
            this.ColEkleyen.Name = "ColEkleyen";
            this.ColEkleyen.Visible = true;
            this.ColEkleyen.VisibleIndex = 3;
            this.ColEkleyen.Width = 110;
            // 
            // ColEklenmeTarihi
            // 
            this.ColEklenmeTarihi.Caption = "Eklenme Tarihi";
            this.ColEklenmeTarihi.FieldName = "EklenmeTarihi";
            this.ColEklenmeTarihi.Name = "ColEklenmeTarihi";
            this.ColEklenmeTarihi.Visible = true;
            this.ColEklenmeTarihi.VisibleIndex = 4;
            this.ColEklenmeTarihi.Width = 120;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColKTI_ID,
            this.ColKTI_ITEMNO,
            this.ColKTI_CUST_ITEMNO,
            this.ColKTI_CALCULATOR,
            this.ColKTI_STOCKGROUP,
            this.ColKTI_QUANTITY,
            this.ColKTI_ISCREATE_BOM,
            this.ColKTI_NOTE});
            this.gridView1.GridControl = this.grdDocAttachFile;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            // 
            // ColKTI_ID
            // 
            this.ColKTI_ID.Caption = "ID";
            this.ColKTI_ID.FieldName = "KTI_ID";
            this.ColKTI_ID.Name = "ColKTI_ID";
            this.ColKTI_ID.Visible = true;
            this.ColKTI_ID.VisibleIndex = 0;
            // 
            // ColKTI_ITEMNO
            // 
            this.ColKTI_ITEMNO.Caption = "Sıra No";
            this.ColKTI_ITEMNO.FieldName = "KTI_ITEMNO";
            this.ColKTI_ITEMNO.Name = "ColKTI_ITEMNO";
            this.ColKTI_ITEMNO.Visible = true;
            this.ColKTI_ITEMNO.VisibleIndex = 1;
            // 
            // ColKTI_CUST_ITEMNO
            // 
            this.ColKTI_CUST_ITEMNO.Caption = "Müşteri Kalem No";
            this.ColKTI_CUST_ITEMNO.FieldName = "KTI_CUST_ITEMNO";
            this.ColKTI_CUST_ITEMNO.Name = "ColKTI_CUST_ITEMNO";
            this.ColKTI_CUST_ITEMNO.Visible = true;
            this.ColKTI_CUST_ITEMNO.VisibleIndex = 2;
            // 
            // ColKTI_CALCULATOR
            // 
            this.ColKTI_CALCULATOR.Caption = "Hesapcı";
            this.ColKTI_CALCULATOR.FieldName = "KTI_CALCULATOR";
            this.ColKTI_CALCULATOR.Name = "ColKTI_CALCULATOR";
            this.ColKTI_CALCULATOR.Visible = true;
            this.ColKTI_CALCULATOR.VisibleIndex = 3;
            // 
            // ColKTI_STOCKGROUP
            // 
            this.ColKTI_STOCKGROUP.Caption = "Mal Grubu";
            this.ColKTI_STOCKGROUP.FieldName = "KTI_STOCKGROUP";
            this.ColKTI_STOCKGROUP.Name = "ColKTI_STOCKGROUP";
            this.ColKTI_STOCKGROUP.Visible = true;
            this.ColKTI_STOCKGROUP.VisibleIndex = 4;
            // 
            // ColKTI_QUANTITY
            // 
            this.ColKTI_QUANTITY.Caption = "Miktar";
            this.ColKTI_QUANTITY.FieldName = "KTI_QUANTITY";
            this.ColKTI_QUANTITY.Name = "ColKTI_QUANTITY";
            this.ColKTI_QUANTITY.Visible = true;
            this.ColKTI_QUANTITY.VisibleIndex = 5;
            // 
            // ColKTI_ISCREATE_BOM
            // 
            this.ColKTI_ISCREATE_BOM.Caption = "Ürün Ağc Ypl.";
            this.ColKTI_ISCREATE_BOM.FieldName = "KTI_ISCREATE_BOM";
            this.ColKTI_ISCREATE_BOM.Name = "ColKTI_ISCREATE_BOM";
            this.ColKTI_ISCREATE_BOM.ToolTip = "Ürün ağacı yapılsın";
            this.ColKTI_ISCREATE_BOM.Visible = true;
            this.ColKTI_ISCREATE_BOM.VisibleIndex = 6;
            // 
            // ColKTI_NOTE
            // 
            this.ColKTI_NOTE.Caption = "Not";
            this.ColKTI_NOTE.FieldName = "KTI_NOTE";
            this.ColKTI_NOTE.Name = "ColKTI_NOTE";
            this.ColKTI_NOTE.Visible = true;
            this.ColKTI_NOTE.VisibleIndex = 7;
            // 
            // cardView2
            // 
            this.cardView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.cardView2.FocusedCardTopFieldIndex = 0;
            this.cardView2.GridControl = this.grdDocAttachFile;
            this.cardView2.Name = "cardView2";
            this.cardView2.OptionsBehavior.Editable = false;
            this.cardView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "KTI_ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Sıra No";
            this.gridColumn2.FieldName = "KTI_ITEMNO";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Müşteri Kalem No";
            this.gridColumn3.FieldName = "KTI_CUST_ITEMNO";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Hesapcı";
            this.gridColumn4.FieldName = "KTI_CALCULATOR";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Mal Grubu";
            this.gridColumn5.FieldName = "KTI_STOCKGROUP";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Miktar";
            this.gridColumn6.FieldName = "KTI_QUANTITY";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ürün Ağc Ypl.";
            this.gridColumn7.FieldName = "KTI_ISCREATE_BOM";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.ToolTip = "Ürün ağacı yapılsın";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Not";
            this.gridColumn8.FieldName = "KTI_NOTE";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            // 
            // Attach_File
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 407);
            this.Controls.Add(this.grdDocAttachFile);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Attach_File";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Döküman Ekle";
            this.Load += new System.EventHandler(this.Attach_File_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repKlasor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDocAttachFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVDocAttachFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarManager bm;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbAttachFile;
        private DevExpress.XtraBars.BarStaticItem bbEvrakId;
        private DevExpress.XtraBars.BarButtonItem bbDelete;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.GridControl grdDocAttachFile;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVDocAttachFile;
        private DevExpress.XtraGrid.Columns.GridColumn ColDA_ID;
        private DevExpress.XtraGrid.Columns.GridColumn ColDA_FILENAME;
        private DevExpress.XtraGrid.Columns.GridColumn ColDA_FILENAME2;
        private DevExpress.XtraGrid.Columns.GridColumn ColDA_PATH;
        private DevExpress.XtraGrid.Columns.GridColumn ColDA_EXTENSION;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn ColEkleyen;
        private DevExpress.XtraGrid.Columns.GridColumn ColEklenmeTarihi;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_ID;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_ITEMNO;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_CUST_ITEMNO;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_CALCULATOR;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_STOCKGROUP;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_QUANTITY;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_ISCREATE_BOM;
        private DevExpress.XtraGrid.Columns.GridColumn ColKTI_NOTE;
        private DevExpress.XtraGrid.Views.Card.CardView cardView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colKlasor;
        private DevExpress.XtraBars.BarEditItem repKlasorAna;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repKlasor;
    }
}