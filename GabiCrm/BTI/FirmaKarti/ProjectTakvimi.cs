﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using DevExpress.XtraScheduler;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;
namespace GABI_CRM
{
    public partial class frmGantSchedule : DevExpress.XtraEditors.XtraForm
    {
        public frmGantSchedule()
        {
            InitializeComponent();
        }
        public static string ProjeNo = "",MusteriTanimi="",FirmaKodu="";
        public static int ScaleWidth = 11;
        public TimelineView ActiveTimeLineBasedView
        {
            get
            {
                if (schedulerControl1.ActiveViewType == SchedulerViewType.Gantt)
                    return schedulerControl1.GanttView;
                return schedulerControl1.TimelineView;
            }
        }

        private void GetList(string Tarih)
        {
            try
            {

                this.Text = MusteriTanimi;
                schedulerControl1.ActiveViewType = SchedulerViewType.Gantt;
                schedulerControl1.GroupType = SchedulerGroupType.Resource;
                schedulerControl1.GanttView.ShowResourceHeaders = false;
            
                schedulerStorage1.Resources.DataSource = DbConnSql.SelectSQL(string.Format(@"Select *, Id as IdSort From GANT_Resource where ProjeId={0}  ", ProjeNo));
                schedulerStorage1.Appointments.DataSource = DbConnSql.SelectSQL(@"Select *  From GANT_Appointment  ");



                schedulerControl1.Start = string.IsNullOrEmpty(Tarih) ? DateTime.Now.AddDays(-5) : Convert.ToDateTime(Tarih).AddDays(-5);
                ActiveTimeLineBasedView.ResourcesPerPage = Convert.ToInt32(changeScaleWidthItem1.EditValue);
                

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void changeScaleWidthItem1_EditValueChanged(object sender, EventArgs e)
        {
            ActiveTimeLineBasedView.ResourcesPerPage = Convert.ToInt32(changeScaleWidthItem1.EditValue);
            DbConnSql.SelectSQL(string.Format(@" 
                UPDATE PROJELER
                 SET Pro_ScaleWidth = {1}
                WHERE Pro_Id = {0}
                ", ProjeNo, Convert.ToInt32(changeScaleWidthItem1.EditValue) ));

        }

        private void frmGantSchedule_Load(object sender, EventArgs e)
        {
            GetList("");
            changeScaleWidthItem1.EditValue = ScaleWidth;
            ActiveTimeLineBasedView.ResourcesPerPage = ScaleWidth;

        }

        private void trckScaleWidth_EditValueChanged(object sender, EventArgs e)
        {
            ActiveTimeLineBasedView.GetBaseTimeScale().Width = Convert.ToInt32(trckScaleWidth.EditValue);
        }

        private void btnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList("");
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           



        }

        private void btnProjeEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //string ProjeId = resourcesTree1.FocusedNode["Description"].ToString();


        }

        private void altFaaliyetEkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string ProjeId = resourcesTree1.FocusedNode["Id"].ToString();
                frmProjeEkle.ProjeId = ProjeNo;
                frmProjeEkle.FirmaKodu = FirmaKodu;                
                frmProjeEkle.ResourceId = ProjeId;
                frmProjeEkle.GelisYeri = "AltBaslik";
                var frm = new frmProjeEkle();
                frm.ShowDialog();
                GetList("");
            }
            catch (Exception)
            {
                    MessageBox.Show("Kayıt seçimi yaptıktan sonra tekrar deneyin");
                    return;
            }
         
        }

        private void btnAnaFaaliyet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //frmProjeEkle.ResourceId = "";
            frmProjeEkle.GelisYeri = "AnaBaslik";
            var frm = new frmProjeEkle();
            frm.ShowDialog();
            GetList("");
        }

        private void btnResourceDuzelt_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ProjeId = resourcesTree1.FocusedNode["Id"].ToString();           
            frmProjeEkle.ResourceId = ProjeId;
            frmProjeEkle.GelisYeri = "Duzeltme";          
            var frm = new frmProjeEkle();
            frm.ShowDialog();
            GetList("");
        }

        private void schedulerControl1_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            
        }

        private void schedulerControl1_EditAppointmentFormShowing_1(object sender, AppointmentFormEventArgs e)
        {
            DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            GABI_CRM.FirmaKarti.CustomAppointmentForm form = new GABI_CRM.FirmaKarti.CustomAppointmentForm(scheduler, e.Appointment, e.OpenRecurrenceForm);
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
            }
            finally
            {
                form.Dispose();
            }

        }
    }
}