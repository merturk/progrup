﻿namespace BTI
{
    partial class frmMain {

        #region Designer generated code
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            this.popupControlContainer1 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.SmImgMain = new DevExpress.Utils.ImageCollection(this.components);
            this.LarImg = new DevExpress.Utils.ImageCollection(this.components);
            this.dLaf = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.bsiKullanicIsmi = new DevExpress.XtraBars.BarStaticItem();
            this.bsiGirisTarihi = new DevExpress.XtraBars.BarStaticItem();
            this.btnYazilimMarka = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.appM = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.bbStockCodeGenerate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bbUsrViewName = new DevExpress.XtraBars.BarButtonItem();
            this.bbUsrChgPass = new DevExpress.XtraBars.BarButtonItem();
            this.bbUsrDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbUsrNew = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.bbUsrPermision = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.bbDensity = new DevExpress.XtraBars.BarButtonItem();
            this.bbIth = new DevExpress.XtraBars.BarButtonItem();
            this.bbPriMouldMeas = new DevExpress.XtraBars.BarButtonItem();
            this.bbPriCondMeas = new DevExpress.XtraBars.BarButtonItem();
            this.bbHelp = new DevExpress.XtraBars.BarButtonItem();
            this.bbVoltageLevelParams = new DevExpress.XtraBars.BarButtonItem();
            this.bbAGAUrunAgaciOlustur = new DevExpress.XtraBars.BarButtonItem();
            this.bbGerilimSargi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.bbKutuEtiketAG = new DevExpress.XtraBars.BarButtonItem();
            this.bbTrafoEtiket = new DevExpress.XtraBars.BarButtonItem();
            this.bbSeriEslemeMuhur = new DevExpress.XtraBars.BarButtonItem();
            this.bbAkimTestSertifika = new DevExpress.XtraBars.BarButtonItem();
            this.bbYmCards = new DevExpress.XtraBars.BarButtonItem();
            this.btnOGAEtiket = new DevExpress.XtraBars.BarButtonItem();
            this.btnOGGEtiket = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.btnRCEtiket = new DevExpress.XtraBars.BarButtonItem();
            this.bbSayimM10001 = new DevExpress.XtraBars.BarButtonItem();
            this.bbSayimMlz10001 = new DevExpress.XtraBars.BarButtonItem();
            this.bbSaySonuc = new DevExpress.XtraBars.BarButtonItem();
            this.bbDuranVarlikListesi = new DevExpress.XtraBars.BarButtonItem();
            this.bbTrafoEtiketOA = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbDepartmentList = new DevExpress.XtraBars.BarButtonItem();
            this.bbUrtTezgah = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.bbStoktanSatis = new DevExpress.XtraBars.BarButtonItem();
            this.bbUretimdenSatis = new DevExpress.XtraBars.BarButtonItem();
            this.bbFasonMusteriden = new DevExpress.XtraBars.BarButtonItem();
            this.bbFasoncudan = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.bmUlkeler = new DevExpress.XtraBars.BarButtonItem();
            this.bmSehirler = new DevExpress.XtraBars.BarButtonItem();
            this.btnSektorler = new DevExpress.XtraBars.BarButtonItem();
            this.btnRaporlar = new DevExpress.XtraBars.BarButtonItem();
            this.btnFirmalar = new DevExpress.XtraBars.BarButtonItem();
            this.bmIlceler = new DevExpress.XtraBars.BarButtonItem();
            this.btnTtipi = new DevExpress.XtraBars.BarButtonItem();
            this.btnTHazirlayanlar = new DevExpress.XtraBars.BarButtonItem();
            this.btnTeklifSonuclari = new DevExpress.XtraBars.BarButtonItem();
            this.btnTeklifNedenleri = new DevExpress.XtraBars.BarButtonItem();
            this.btnOdemeTipleri = new DevExpress.XtraBars.BarButtonItem();
            this.btnFaturaTipleri = new DevExpress.XtraBars.BarButtonItem();
            this.btnGiderTipleri = new DevExpress.XtraBars.BarButtonItem();
            this.btnProjeTanimlari = new DevExpress.XtraBars.BarButtonItem();
            this.btnDanismanlar = new DevExpress.XtraBars.BarButtonItem();
            this.btnStatu = new DevExpress.XtraBars.BarButtonItem();
            this.btnAnaGruplar = new DevExpress.XtraBars.BarButtonItem();
            this.btnAltGruplar = new DevExpress.XtraBars.BarButtonItem();
            this.btnZiyaretNedenleri = new DevExpress.XtraBars.BarButtonItem();
            this.btnPromosyonTanim = new DevExpress.XtraBars.BarButtonItem();
            this.btnSozlesmeIcerikTip = new DevExpress.XtraBars.BarButtonItem();
            this.btnProjeTanimlariDetay = new DevExpress.XtraBars.BarButtonItem();
            this.btnMusteriListesi = new DevExpress.XtraBars.BarButtonItem();
            this.btnMusteriSeg = new DevExpress.XtraBars.BarButtonItem();
            this.btnKartVizitGiris = new DevExpress.XtraBars.BarButtonItem();
            this.btnNeredenUlasildi = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barLinkContainerItem2 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.btnCiroBaslik = new DevExpress.XtraBars.BarButtonItem();
            this.btnSertifikaTanim = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazMarka = new DevExpress.XtraBars.BarButtonItem();
            this.btnYazG = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.btnDosyaKlasörleri = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.btnEmail = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem3 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.btnTaliGruplar = new DevExpress.XtraBars.BarButtonItem();
            this.btnSektorAna = new DevExpress.XtraBars.BarButtonItem();
            this.btnSektorAlt = new DevExpress.XtraBars.BarButtonItem();
            this.barLinkContainerItem4 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.btnProjeYonetimi = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpParametersGirisleri = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpAdminPanel = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgUsers = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgAdmin = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpHelp = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgHelp = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.repositoryItemRangeTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemRangeTrackBar();
            this.repositoryItemTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.repositoryItemRangeTrackBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemRangeTrackBar();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemPictureEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.repositoryItemZoomTrackBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.repositoryItemFontEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.OthImg = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.bbEnvanterSoftwareList = new DevExpress.XtraBars.BarButtonItem();
            this.SmallImg = new DevExpress.Utils.ImageCollection(this.components);
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileSalesYearly = new DevExpress.XtraEditors.TileItem();
            this.tileCustomerAnalysis = new DevExpress.XtraEditors.TileItem();
            this.tileHedefSapma = new DevExpress.XtraEditors.TileItem();
            this.tileProductAnalysis = new DevExpress.XtraEditors.TileItem();
            this.tileCustomerGroup = new DevExpress.XtraEditors.TileItem();
            this.tileCountrySales = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileKarlilik = new DevExpress.XtraEditors.TileItem();
            this.tileInOut = new DevExpress.XtraEditors.TileItem();
            this.tileProductGroup = new DevExpress.XtraEditors.TileItem();
            this.tileCustomerTop = new DevExpress.XtraEditors.TileItem();
            this.tileOdemePerf = new DevExpress.XtraEditors.TileItem();
            this.tileGeneralReport = new DevExpress.XtraEditors.TileItem();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmImgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LarImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnYazilimMarka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRangeTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRangeTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OthImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImg)).BeginInit();
            this.SuspendLayout();
            // 
            // popupControlContainer1
            // 
            this.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainer1.Location = new System.Drawing.Point(0, 0);
            this.popupControlContainer1.Name = "popupControlContainer1";
            this.popupControlContainer1.Size = new System.Drawing.Size(0, 0);
            this.popupControlContainer1.TabIndex = 6;
            this.popupControlContainer1.Visible = false;
            // 
            // SmImgMain
            // 
            this.SmImgMain.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmImgMain.ImageStream")));
            this.SmImgMain.Images.SetKeyName(29, "userNamePass.png");
            this.SmImgMain.Images.SetKeyName(30, "PassChg.png");
            this.SmImgMain.Images.SetKeyName(31, "userNamePass.png");
            this.SmImgMain.Images.SetKeyName(32, "cancel.png");
            this.SmImgMain.Images.SetKeyName(33, "StockCode.png");
            this.SmImgMain.Images.SetKeyName(34, "Frekans.png");
            this.SmImgMain.Images.SetKeyName(35, "Voltage.png");
            this.SmImgMain.Images.SetKeyName(36, "Kalip.png");
            this.SmImgMain.Images.SetKeyName(37, "ITH.png");
            this.SmImgMain.Images.SetKeyName(38, "primerKalipıletken.png");
            this.SmImgMain.Images.SetKeyName(39, "primerIletken.png");
            this.SmImgMain.Images.SetKeyName(40, "VoltageLevelPar.png");
            this.SmImgMain.Images.SetKeyName(41, "clnfbr.png");
            this.SmImgMain.Images.SetKeyName(42, "tijList.png");
            this.SmImgMain.Images.SetKeyName(43, "calculator1.png");
            this.SmImgMain.Images.SetKeyName(44, "calcOgVoltage.png");
            this.SmImgMain.Images.SetKeyName(45, "calcAgCurrent.png");
            this.SmImgMain.Images.SetKeyName(46, "calcToroid.png");
            this.SmImgMain.Images.SetKeyName(47, "tree.png");
            this.SmImgMain.Images.SetKeyName(48, "rc_qm.png");
            this.SmImgMain.Images.SetKeyName(49, "chart.png");
            this.SmImgMain.Images.SetKeyName(50, "DuranVarlik.png");
            this.SmImgMain.Images.SetKeyName(51, "K.png");
            this.SmImgMain.Images.SetKeyName(52, "BT1.png");
            this.SmImgMain.Images.SetKeyName(53, "date-time_preferences.png");
            this.SmImgMain.Images.SetKeyName(54, "note_pinned.png");
            this.SmImgMain.Images.SetKeyName(55, "businessmen.png");
            this.SmImgMain.Images.SetKeyName(56, "businessman.png");
            this.SmImgMain.Images.SetKeyName(57, "businessman2.png");
            this.SmImgMain.Images.SetKeyName(58, "gabiSml2.png");
            this.SmImgMain.Images.SetKeyName(59, "home.png");
            this.SmImgMain.Images.SetKeyName(60, "pin_blue.png");
            this.SmImgMain.Images.SetKeyName(61, "pin_green.png");
            this.SmImgMain.Images.SetKeyName(62, "pin_grey.png");
            this.SmImgMain.Images.SetKeyName(63, "pin_red.png");
            this.SmImgMain.Images.SetKeyName(64, "pin_yellow.png");
            this.SmImgMain.Images.SetKeyName(65, "import.png");
            this.SmImgMain.Images.SetKeyName(66, "import2.png");
            this.SmImgMain.Images.SetKeyName(67, "export1.png");
            this.SmImgMain.Images.SetKeyName(68, "sort_ascending.png");
            this.SmImgMain.Images.SetKeyName(69, "sort_az_descending.png");
            this.SmImgMain.Images.SetKeyName(70, "businessman_preferences.png");
            this.SmImgMain.Images.SetKeyName(71, "connect.png");
            this.SmImgMain.Images.SetKeyName(72, "currency_euro.png");
            this.SmImgMain.Images.SetKeyName(73, "message_edit.png");
            this.SmImgMain.Images.SetKeyName(74, "messages.png");
            this.SmImgMain.Images.SetKeyName(75, "message.png");
            // 
            // LarImg
            // 
            this.LarImg.ImageSize = new System.Drawing.Size(32, 32);
            this.LarImg.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("LarImg.ImageStream")));
            this.LarImg.Images.SetKeyName(10, "newUsers.png");
            this.LarImg.Images.SetKeyName(11, "delete_user.png");
            this.LarImg.Images.SetKeyName(12, "permision_user.png");
            this.LarImg.Images.SetKeyName(13, "StockCode.png");
            this.LarImg.Images.SetKeyName(14, "calculator1.png");
            this.LarImg.Images.SetKeyName(15, "calcToroid.png");
            this.LarImg.Images.SetKeyName(16, "calcAgCurrent.png");
            this.LarImg.Images.SetKeyName(17, "calcOgVoltage.png");
            this.LarImg.Images.SetKeyName(18, "OGATeklif.png");
            this.LarImg.Images.SetKeyName(19, "OGAIslev.png");
            this.LarImg.Images.SetKeyName(20, "OGGIslev.png");
            this.LarImg.Images.SetKeyName(21, "AGGIslev.png");
            this.LarImg.Images.SetKeyName(22, "ToroidIslev.png");
            this.LarImg.Images.SetKeyName(23, "AGGIslev.png");
            this.LarImg.Images.SetKeyName(24, "OGATREE.png");
            this.LarImg.Images.SetKeyName(25, "AGATREE.png");
            this.LarImg.Images.SetKeyName(26, "AGA_AKIM_ISLEV.png");
            this.LarImg.Images.SetKeyName(27, "OGG_AKIM_ISLEV.png");
            this.LarImg.Images.SetKeyName(28, "OGG_AKIM_TREE.png");
            this.LarImg.Images.SetKeyName(29, "TOROID_TREE.png");
            this.LarImg.Images.SetKeyName(30, "TOROID_ISLEV.png");
            this.LarImg.Images.SetKeyName(31, "Voltage.png");
            this.LarImg.Images.SetKeyName(32, "Frekans.png");
            this.LarImg.Images.SetKeyName(33, "Kalip.png");
            this.LarImg.Images.SetKeyName(34, "ITH.png");
            this.LarImg.Images.SetKeyName(35, "clnfbr.png");
            this.LarImg.Images.SetKeyName(36, "QM1.png");
            this.LarImg.Images.SetKeyName(37, "tree.png");
            this.LarImg.Images.SetKeyName(38, "treeOGA.png");
            this.LarImg.Images.SetKeyName(39, "treeOGG.png");
            this.LarImg.Images.SetKeyName(40, "treeRC.png");
            this.LarImg.Images.SetKeyName(41, "ogg_qm.png");
            this.LarImg.Images.SetKeyName(42, "rc_qm.png");
            this.LarImg.Images.SetKeyName(43, "data_gear.png");
            this.LarImg.Images.SetKeyName(44, "primerSargi.png");
            this.LarImg.Images.SetKeyName(45, "sekonderSargi.png");
            this.LarImg.Images.SetKeyName(46, "oga_sargi.png");
            this.LarImg.Images.SetKeyName(47, "Teklifler.png");
            this.LarImg.Images.SetKeyName(48, "VoltageLevelPar.png");
            this.LarImg.Images.SetKeyName(49, "primerKalipıletken.png");
            this.LarImg.Images.SetKeyName(50, "primerIletken.png");
            this.LarImg.Images.SetKeyName(51, "database.png");
            this.LarImg.Images.SetKeyName(52, "newUsers.png");
            this.LarImg.Images.SetKeyName(53, "nuve2.png");
            this.LarImg.Images.SetKeyName(54, "KaliteParams.png");
            this.LarImg.Images.SetKeyName(55, "TrafoLabel.png");
            this.LarImg.Images.SetKeyName(56, "BoxLabel2.png");
            this.LarImg.Images.SetKeyName(57, "CalcTermoPls.png");
            this.LarImg.Images.SetKeyName(58, "TERMO_ISLEV.png");
            this.LarImg.Images.SetKeyName(59, "treeTP.png");
            this.LarImg.Images.SetKeyName(60, "MixNuve.png");
            this.LarImg.Images.SetKeyName(61, "Cust.png");
            this.LarImg.Images.SetKeyName(62, "Series.png");
            this.LarImg.Images.SetKeyName(63, "Muhurlu.png");
            this.LarImg.Images.SetKeyName(64, "Sertifika.png");
            this.LarImg.Images.SetKeyName(65, "speed.png");
            this.LarImg.Images.SetKeyName(66, "barcode.png");
            this.LarImg.Images.SetKeyName(67, "CTLabels.png");
            this.LarImg.Images.SetKeyName(68, "TrafoLabelCt.png");
            this.LarImg.Images.SetKeyName(69, "findCode2.png");
            this.LarImg.Images.SetKeyName(70, "TrafoLabelOgg.png");
            this.LarImg.Images.SetKeyName(71, "TrafoLabelRC.png");
            this.LarImg.Images.SetKeyName(72, "MSayim.png");
            this.LarImg.Images.SetKeyName(73, "M2Sayim.png");
            this.LarImg.Images.SetKeyName(74, "MSayimAG.png");
            this.LarImg.Images.SetKeyName(75, "M2SayimAG.png");
            this.LarImg.Images.SetKeyName(76, "priceCost.png");
            this.LarImg.Images.SetKeyName(77, "StokEnvKars.png");
            this.LarImg.Images.SetKeyName(78, "chart.png");
            this.LarImg.Images.SetKeyName(79, "Kota2.png");
            this.LarImg.Images.SetKeyName(80, "DuranVarlik.png");
            this.LarImg.Images.SetKeyName(81, "OA_ISLEV2.png");
            this.LarImg.Images.SetKeyName(82, "tree_OA.png");
            this.LarImg.Images.SetKeyName(83, "CalcOA.png");
            this.LarImg.Images.SetKeyName(84, "SertifikaOA.png");
            this.LarImg.Images.SetKeyName(85, "TrafoLabelOA.png");
            this.LarImg.Images.SetKeyName(86, "BoxLabelOA.png");
            this.LarImg.Images.SetKeyName(87, "preferences_composer.png");
            this.LarImg.Images.SetKeyName(88, "Inventory.png");
            this.LarImg.Images.SetKeyName(89, "contacts.png");
            this.LarImg.Images.SetKeyName(90, "Wiki1.png");
            this.LarImg.Images.SetKeyName(91, "download.png");
            this.LarImg.Images.SetKeyName(92, "export.png");
            this.LarImg.Images.SetKeyName(93, "export1.png");
            this.LarImg.Images.SetKeyName(94, "new-folder.png");
            this.LarImg.Images.SetKeyName(95, "upload.png");
            this.LarImg.Images.SetKeyName(96, "Workspace2.png");
            this.LarImg.Images.SetKeyName(97, "data_into.png");
            this.LarImg.Images.SetKeyName(98, "data_previous.png");
            this.LarImg.Images.SetKeyName(99, "index_replace.png");
            this.LarImg.Images.SetKeyName(100, "up_down.png");
            this.LarImg.Images.SetKeyName(101, "paste.png");
            this.LarImg.Images.SetKeyName(102, "truckyellow.png");
            this.LarImg.Images.SetKeyName(103, "barcode_arrow_up.png");
            this.LarImg.Images.SetKeyName(104, "gabiSml2.png");
            this.LarImg.Images.SetKeyName(105, "data_replace.png");
            this.LarImg.Images.SetKeyName(106, "import.png");
            this.LarImg.Images.SetKeyName(107, "R.png");
            this.LarImg.Images.SetKeyName(108, "data_certificate.png");
            this.LarImg.Images.SetKeyName(109, "note_pinned.png");
            this.LarImg.Images.SetKeyName(110, "Tasks.png");
            this.LarImg.Images.SetKeyName(111, "home.png");
            this.LarImg.Images.SetKeyName(112, "web.png");
            this.LarImg.Images.SetKeyName(113, "businessmen.png");
            this.LarImg.Images.SetKeyName(114, "image.png");
            this.LarImg.Images.SetKeyName(115, "Forum.png");
            this.LarImg.Images.SetKeyName(116, "Contact.png");
            this.LarImg.Images.SetKeyName(117, "Document2.png");
            this.LarImg.Images.SetKeyName(118, "Mail4.png");
            this.LarImg.Images.SetKeyName(119, "email.png");
            this.LarImg.Images.SetKeyName(120, "email (2).png");
            this.LarImg.Images.SetKeyName(121, "cal.png");
            this.LarImg.Images.SetKeyName(122, "iphone.png");
            this.LarImg.Images.SetKeyName(123, "calendar_preferences.png");
            // 
            // dLaf
            // 
            this.dLaf.LookAndFeel.SkinName = "Office 2007 Blue";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.bsiKullanicIsmi);
            this.ribbonStatusBar1.ItemLinks.Add(this.bsiGirisTarihi);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 732);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.btnYazilimMarka;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1338, 23);
            // 
            // bsiKullanicIsmi
            // 
            this.bsiKullanicIsmi.Id = 45;
            this.bsiKullanicIsmi.Name = "bsiKullanicIsmi";
            this.bsiKullanicIsmi.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bsiGirisTarihi
            // 
            this.bsiGirisTarihi.Id = 47;
            this.bsiGirisTarihi.Name = "bsiGirisTarihi";
            this.bsiGirisTarihi.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnYazilimMarka
            // 
            this.btnYazilimMarka.ApplicationButtonDropDownControl = this.appM;
            this.btnYazilimMarka.ApplicationIcon = ((System.Drawing.Bitmap)(resources.GetObject("btnYazilimMarka.ApplicationIcon")));
            this.btnYazilimMarka.ExpandCollapseItem.Id = 0;
            this.btnYazilimMarka.Images = this.SmImgMain;
            this.btnYazilimMarka.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnYazilimMarka.ExpandCollapseItem,
            this.bbStockCodeGenerate,
            this.barButtonItem4,
            this.bbUsrViewName,
            this.bbUsrChgPass,
            this.bbUsrDelete,
            this.bbUsrNew,
            this.barButtonItem5,
            this.bbUsrPermision,
            this.barEditItem1,
            this.barEditItem2,
            this.bbDensity,
            this.bbIth,
            this.bbPriMouldMeas,
            this.bbPriCondMeas,
            this.bbHelp,
            this.bbVoltageLevelParams,
            this.bsiKullanicIsmi,
            this.bsiGirisTarihi,
            this.bbAGAUrunAgaciOlustur,
            this.bbGerilimSargi,
            this.barButtonItem8,
            this.barButtonItem9,
            this.bbKutuEtiketAG,
            this.bbTrafoEtiket,
            this.bbSeriEslemeMuhur,
            this.bbAkimTestSertifika,
            this.bbYmCards,
            this.btnOGAEtiket,
            this.btnOGGEtiket,
            this.barButtonItem7,
            this.btnRCEtiket,
            this.bbSayimM10001,
            this.bbSayimMlz10001,
            this.bbSaySonuc,
            this.bbDuranVarlikListesi,
            this.bbTrafoEtiketOA,
            this.barButtonItem12,
            this.barButtonItem10,
            this.barButtonItem1,
            this.bbDepartmentList,
            this.bbUrtTezgah,
            this.barButtonItem3,
            this.bbStoktanSatis,
            this.bbUretimdenSatis,
            this.bbFasonMusteriden,
            this.bbFasoncudan,
            this.ribbonGalleryBarItem1,
            this.barButtonItem6,
            this.bmUlkeler,
            this.bmSehirler,
            this.btnSektorler,
            this.btnRaporlar,
            this.btnFirmalar,
            this.bmIlceler,
            this.btnTtipi,
            this.btnTHazirlayanlar,
            this.btnTeklifSonuclari,
            this.btnTeklifNedenleri,
            this.btnOdemeTipleri,
            this.btnFaturaTipleri,
            this.btnGiderTipleri,
            this.btnProjeTanimlari,
            this.btnDanismanlar,
            this.btnStatu,
            this.btnAnaGruplar,
            this.btnAltGruplar,
            this.btnZiyaretNedenleri,
            this.btnPromosyonTanim,
            this.btnSozlesmeIcerikTip,
            this.btnProjeTanimlariDetay,
            this.btnMusteriListesi,
            this.btnMusteriSeg,
            this.btnKartVizitGiris,
            this.btnNeredenUlasildi,
            this.barLinkContainerItem1,
            this.barLinkContainerItem2,
            this.btnCiroBaslik,
            this.btnSertifikaTanim,
            this.btnYazMarka,
            this.btnYazG,
            this.barButtonItem11,
            this.barButtonItem13,
            this.barButtonGroup1,
            this.btnDosyaKlasörleri,
            this.barButtonItem14,
            this.btnEmail,
            this.barLinkContainerItem3,
            this.btnSektorAna,
            this.btnSektorAlt,
            this.btnTaliGruplar,
            this.barLinkContainerItem4,
            this.barButtonItem15,
            this.barButtonItem16,
            this.btnProjeYonetimi});
            this.btnYazilimMarka.LargeImages = this.LarImg;
            this.btnYazilimMarka.Location = new System.Drawing.Point(0, 0);
            this.btnYazilimMarka.MaxItemId = 308;
            this.btnYazilimMarka.Name = "btnYazilimMarka";
            this.btnYazilimMarka.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage3,
            this.ribbonPage1,
            this.rpParametersGirisleri,
            this.rpAdminPanel,
            this.rpHelp});
            this.btnYazilimMarka.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1,
            this.repositoryItemFontEdit1,
            this.repositoryItemRangeTrackBar1,
            this.repositoryItemTrackBar1,
            this.repositoryItemRangeTrackBar2,
            this.repositoryItemPictureEdit1,
            this.repositoryItemPictureEdit2,
            this.repositoryItemPictureEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemTimeEdit1,
            this.repositoryItemZoomTrackBar2,
            this.repositoryItemFontEdit2});
            this.btnYazilimMarka.Size = new System.Drawing.Size(1338, 147);
            this.btnYazilimMarka.StatusBar = this.ribbonStatusBar1;
            this.btnYazilimMarka.Toolbar.ItemLinks.Add(this.barButtonItem10);
            this.btnYazilimMarka.Toolbar.ItemLinks.Add(this.barButtonItem1);
            this.btnYazilimMarka.ApplicationButtonDoubleClick += new System.EventHandler(this.rbMain_ApplicationButtonDoubleClick);
            // 
            // appM
            // 
            this.appM.Name = "appM";
            this.appM.Ribbon = this.btnYazilimMarka;
            // 
            // bbStockCodeGenerate
            // 
            this.bbStockCodeGenerate.Caption = "Stok Kod Tanımlama";
            this.bbStockCodeGenerate.Id = 4;
            this.bbStockCodeGenerate.LargeImageIndex = 13;
            this.bbStockCodeGenerate.Name = "bbStockCodeGenerate";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 5;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bbUsrViewName
            // 
            this.bbUsrViewName.Caption = "Kullanıcı Bilgi...";
            this.bbUsrViewName.Id = 6;
            this.bbUsrViewName.ImageIndex = 31;
            this.bbUsrViewName.Name = "bbUsrViewName";
            // 
            // bbUsrChgPass
            // 
            this.bbUsrChgPass.Caption = "şifremi Deðiştir...";
            this.bbUsrChgPass.Id = 7;
            this.bbUsrChgPass.ImageIndex = 30;
            this.bbUsrChgPass.Name = "bbUsrChgPass";
            this.bbUsrChgPass.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbUsrChgPass_ItemClick);
            // 
            // bbUsrDelete
            // 
            this.bbUsrDelete.Caption = "Kullanıcı Sil";
            this.bbUsrDelete.Id = 8;
            this.bbUsrDelete.LargeImageIndex = 11;
            this.bbUsrDelete.Name = "bbUsrDelete";
            this.bbUsrDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbUsrDelete_ItemClick);
            // 
            // bbUsrNew
            // 
            this.bbUsrNew.Caption = "Yeni Kullanıcı Ekle";
            this.bbUsrNew.Id = 9;
            this.bbUsrNew.LargeImageIndex = 10;
            this.bbUsrNew.Name = "bbUsrNew";
            this.bbUsrNew.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbUsrNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbUsrNew_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 10;
            this.barButtonItem5.ImageIndex = 32;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbUsrPermision
            // 
            this.bbUsrPermision.Caption = "Kullanıcı Yetkileri";
            this.bbUsrPermision.Id = 11;
            this.bbUsrPermision.LargeImageIndex = 12;
            this.bbUsrPermision.Name = "bbUsrPermision";
            this.bbUsrPermision.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbUsrPermision.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbUsrPermision_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItem1.Edit = this.repositoryItemProgressBar1;
            this.barEditItem1.Id = 14;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Edit = this.repositoryItemZoomTrackBar1;
            this.barEditItem2.Id = 15;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Middle = 5;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            this.repositoryItemZoomTrackBar1.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // bbDensity
            // 
            this.bbDensity.Caption = "Yoðunluk";
            this.bbDensity.Id = 29;
            this.bbDensity.ImageIndex = 1;
            this.bbDensity.Name = "bbDensity";
            // 
            // bbIth
            // 
            this.bbIth.Caption = "Ith";
            this.bbIth.Id = 34;
            this.bbIth.ImageIndex = 37;
            this.bbIth.Name = "bbIth";
            // 
            // bbPriMouldMeas
            // 
            this.bbPriMouldMeas.Caption = "Primer Kalıp Ölçüleri";
            this.bbPriMouldMeas.Id = 35;
            this.bbPriMouldMeas.ImageIndex = 38;
            this.bbPriMouldMeas.Name = "bbPriMouldMeas";
            // 
            // bbPriCondMeas
            // 
            this.bbPriCondMeas.Caption = "Primer ıletken Ölçüleri";
            this.bbPriCondMeas.Id = 36;
            this.bbPriCondMeas.ImageIndex = 39;
            this.bbPriCondMeas.Name = "bbPriCondMeas";
            // 
            // bbHelp
            // 
            this.bbHelp.Caption = "Yardım";
            this.bbHelp.Id = 37;
            this.bbHelp.LargeImageIndex = 10;
            this.bbHelp.Name = "bbHelp";
            this.bbHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbHelp_ItemClick);
            // 
            // bbVoltageLevelParams
            // 
            this.bbVoltageLevelParams.Caption = "Gerilim Seviyesi Ölçü";
            this.bbVoltageLevelParams.Id = 38;
            this.bbVoltageLevelParams.ImageIndex = 40;
            this.bbVoltageLevelParams.Name = "bbVoltageLevelParams";
            // 
            // bbAGAUrunAgaciOlustur
            // 
            this.bbAGAUrunAgaciOlustur.Caption = "CT Ürün Aðacı Resim Sabitleri";
            this.bbAGAUrunAgaciOlustur.Id = 62;
            this.bbAGAUrunAgaciOlustur.LargeImageIndex = 37;
            this.bbAGAUrunAgaciOlustur.Name = "bbAGAUrunAgaciOlustur";
            toolTipTitleItem1.Text = "Bilgi";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Akım Trafoları Ürün Aðacı Resim Sabitleri";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbAGAUrunAgaciOlustur.SuperTip = superToolTip1;
            // 
            // bbGerilimSargi
            // 
            this.bbGerilimSargi.Caption = "Gerilim Trafosu Sargı Fişi";
            this.bbGerilimSargi.Id = 70;
            this.bbGerilimSargi.LargeImageIndex = 44;
            this.bbGerilimSargi.Name = "bbGerilimSargi";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItem8";
            this.barButtonItem8.Id = 73;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "barButtonItem9";
            this.barButtonItem9.Id = 74;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // bbKutuEtiketAG
            // 
            this.bbKutuEtiketAG.Caption = "Kutu  Etiketi [AG]";
            this.bbKutuEtiketAG.Id = 87;
            this.bbKutuEtiketAG.LargeImageIndex = 56;
            this.bbKutuEtiketAG.Name = "bbKutuEtiketAG";
            // 
            // bbTrafoEtiket
            // 
            this.bbTrafoEtiket.Caption = "Akım Trafo Etiket";
            this.bbTrafoEtiket.Id = 88;
            this.bbTrafoEtiket.LargeImageIndex = 55;
            this.bbTrafoEtiket.Name = "bbTrafoEtiket";
            toolTipTitleItem2.Text = "Bilgi";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "AG Akım Trafo Etiketi";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbTrafoEtiket.SuperTip = superToolTip2;
            // 
            // bbSeriEslemeMuhur
            // 
            this.bbSeriEslemeMuhur.Caption = "Mühürlü Trafo Arşivi";
            this.bbSeriEslemeMuhur.Id = 103;
            this.bbSeriEslemeMuhur.LargeImageIndex = 63;
            this.bbSeriEslemeMuhur.Name = "bbSeriEslemeMuhur";
            // 
            // bbAkimTestSertifika
            // 
            this.bbAkimTestSertifika.Caption = "AG Akım Test  Sertifikası  ";
            this.bbAkimTestSertifika.Id = 104;
            this.bbAkimTestSertifika.LargeImageIndex = 64;
            this.bbAkimTestSertifika.Name = "bbAkimTestSertifika";
            // 
            // bbYmCards
            // 
            this.bbYmCards.Caption = "Gerilim Yarımamul Üretim Kartı";
            this.bbYmCards.Id = 112;
            this.bbYmCards.LargeImageIndex = 66;
            this.bbYmCards.Name = "bbYmCards";
            // 
            // btnOGAEtiket
            // 
            this.btnOGAEtiket.Caption = "Akım Trafo Etiket";
            this.btnOGAEtiket.Id = 119;
            this.btnOGAEtiket.LargeImageIndex = 68;
            this.btnOGAEtiket.Name = "btnOGAEtiket";
            // 
            // btnOGGEtiket
            // 
            this.btnOGGEtiket.Caption = "Gerilim Trafo Etiket";
            this.btnOGGEtiket.Id = 122;
            this.btnOGGEtiket.LargeImageIndex = 70;
            this.btnOGGEtiket.Name = "btnOGGEtiket";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Ring Core Trafosu Sargı Fişi";
            this.barButtonItem7.Id = 138;
            this.barButtonItem7.LargeImageIndex = 43;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // btnRCEtiket
            // 
            this.btnRCEtiket.Caption = "Ring Core Trafo Etiket";
            this.btnRCEtiket.Id = 141;
            this.btnRCEtiket.LargeImageIndex = 71;
            this.btnRCEtiket.Name = "btnRCEtiket";
            // 
            // bbSayimM10001
            // 
            this.bbSayimM10001.Caption = "SYM 01";
            this.bbSayimM10001.Id = 144;
            this.bbSayimM10001.LargeImageIndex = 72;
            this.bbSayimM10001.Name = "bbSayimM10001";
            toolTipTitleItem3.Text = "Bilgi";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Orta Gerilim Mamul Sayımı Giriş Ekranı.\r\nNot : Bu ekran SAP ZMM011 den beslenerek" +
    " ilgili seri girildiðinde, bilgilerini otomatik olarak getirir.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbSayimM10001.SuperTip = superToolTip3;
            // 
            // bbSayimMlz10001
            // 
            this.bbSayimMlz10001.Caption = "SYM 02";
            this.bbSayimMlz10001.Id = 145;
            this.bbSayimMlz10001.LargeImageIndex = 73;
            this.bbSayimMlz10001.Name = "bbSayimMlz10001";
            toolTipTitleItem4.Text = "Bilgi";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Orta Gerilim Malzeme Sayımı Giriş Ekranı.\r\nNot: ılgili girişler yapılmadan önce, " +
    "muhasebeden envanter listesi sisteme aktarılmış olmalıdır. (SAP deki, ZMM013 )";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbSayimMlz10001.SuperTip = superToolTip4;
            // 
            // bbSaySonuc
            // 
            this.bbSaySonuc.Caption = "Sayım Sonuçları";
            this.bbSaySonuc.Id = 160;
            this.bbSaySonuc.LargeImageIndex = 78;
            this.bbSaySonuc.Name = "bbSaySonuc";
            toolTipTitleItem5.Text = "Bilgi";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "SYM 01 ve SYM 02 nin alt alta birleştirilmiş halidir. (Union edilmiş hali.)";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbSaySonuc.SuperTip = superToolTip5;
            // 
            // bbDuranVarlikListesi
            // 
            this.bbDuranVarlikListesi.Caption = "Envanter Etiket";
            this.bbDuranVarlikListesi.Id = 170;
            this.bbDuranVarlikListesi.LargeImageIndex = 80;
            this.bbDuranVarlikListesi.Name = "bbDuranVarlikListesi";
            // 
            // bbTrafoEtiketOA
            // 
            this.bbTrafoEtiketOA.Caption = "Ölçü Alt. Etiket && Sertifika";
            this.bbTrafoEtiketOA.Id = 179;
            this.bbTrafoEtiketOA.LargeImageIndex = 85;
            this.bbTrafoEtiketOA.Name = "bbTrafoEtiketOA";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "Programdan Çıkış";
            this.barButtonItem12.Id = 185;
            this.barButtonItem12.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.barButtonItem12.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem12.LargeImageIndex = 1;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "barButtonItem10";
            this.barButtonItem10.Id = 189;
            this.barButtonItem10.ImageIndex = 13;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick_2);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 190;
            this.barButtonItem1.ImageIndex = 23;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick_1);
            // 
            // bbDepartmentList
            // 
            this.bbDepartmentList.Id = 251;
            this.bbDepartmentList.Name = "bbDepartmentList";
            // 
            // bbUrtTezgah
            // 
            this.bbUrtTezgah.Id = 252;
            this.bbUrtTezgah.Name = "bbUrtTezgah";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Id = 253;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // bbStoktanSatis
            // 
            this.bbStoktanSatis.Id = 254;
            this.bbStoktanSatis.Name = "bbStoktanSatis";
            // 
            // bbUretimdenSatis
            // 
            this.bbUretimdenSatis.Id = 255;
            this.bbUretimdenSatis.Name = "bbUretimdenSatis";
            // 
            // bbFasonMusteriden
            // 
            this.bbFasonMusteriden.Id = 256;
            this.bbFasonMusteriden.Name = "bbFasonMusteriden";
            // 
            // bbFasoncudan
            // 
            this.bbFasoncudan.Id = 257;
            this.bbFasoncudan.Name = "bbFasoncudan";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 258;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Tasarımı Kaydet";
            this.barButtonItem6.Id = 259;
            this.barButtonItem6.LargeImageIndex = 2;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // bmUlkeler
            // 
            this.bmUlkeler.Caption = "Ulkeler";
            this.bmUlkeler.Id = 261;
            this.bmUlkeler.ImageIndex = 24;
            this.bmUlkeler.LargeImageIndex = 112;
            this.bmUlkeler.Name = "bmUlkeler";
            this.bmUlkeler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUlkeler_ItemClick);
            // 
            // bmSehirler
            // 
            this.bmSehirler.Caption = "şehirler";
            this.bmSehirler.Id = 262;
            this.bmSehirler.ImageIndex = 59;
            this.bmSehirler.LargeImageIndex = 111;
            this.bmSehirler.Name = "bmSehirler";
            this.bmSehirler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmSehirler_ItemClick);
            // 
            // btnSektorler
            // 
            this.btnSektorler.Caption = "Sektörler";
            this.btnSektorler.Id = 263;
            this.btnSektorler.LargeImageIndex = 91;
            this.btnSektorler.Name = "btnSektorler";
            this.btnSektorler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSektorler_ItemClick);
            // 
            // btnRaporlar
            // 
            this.btnRaporlar.Caption = "Raporlar";
            this.btnRaporlar.Id = 264;
            this.btnRaporlar.LargeImageIndex = 78;
            this.btnRaporlar.Name = "btnRaporlar";
            this.btnRaporlar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRaporlar_ItemClick);
            // 
            // btnFirmalar
            // 
            this.btnFirmalar.Caption = "Aktif Firma Listesi";
            this.btnFirmalar.Id = 265;
            this.btnFirmalar.LargeImageIndex = 115;
            this.btnFirmalar.Name = "btnFirmalar";
            this.btnFirmalar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFirmalar_ItemClick);
            // 
            // bmIlceler
            // 
            this.bmIlceler.Caption = "ılçeler";
            this.bmIlceler.Id = 266;
            this.bmIlceler.LargeImageIndex = 114;
            this.bmIlceler.Name = "bmIlceler";
            this.bmIlceler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bmIlceler_ItemClick);
            // 
            // btnTtipi
            // 
            this.btnTtipi.Caption = "Teklif Konusu";
            this.btnTtipi.Id = 268;
            this.btnTtipi.ImageIndex = 60;
            this.btnTtipi.Name = "btnTtipi";
            this.btnTtipi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTtipi_ItemClick);
            // 
            // btnTHazirlayanlar
            // 
            this.btnTHazirlayanlar.Caption = "Teklif Hazırlayanlar";
            this.btnTHazirlayanlar.Id = 269;
            this.btnTHazirlayanlar.ImageIndex = 61;
            this.btnTHazirlayanlar.Name = "btnTHazirlayanlar";
            this.btnTHazirlayanlar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTHazirlayanlar_ItemClick);
            // 
            // btnTeklifSonuclari
            // 
            this.btnTeklifSonuclari.Caption = "Teklif Sonuçları";
            this.btnTeklifSonuclari.Id = 270;
            this.btnTeklifSonuclari.ImageIndex = 62;
            this.btnTeklifSonuclari.Name = "btnTeklifSonuclari";
            this.btnTeklifSonuclari.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTeklifSonuclari_ItemClick);
            // 
            // btnTeklifNedenleri
            // 
            this.btnTeklifNedenleri.Caption = "Teklif K/K Nedenleri";
            this.btnTeklifNedenleri.Id = 271;
            this.btnTeklifNedenleri.ImageIndex = 64;
            this.btnTeklifNedenleri.Name = "btnTeklifNedenleri";
            toolTipItem6.Text = "Teklif kazanma kaybetme nedenleri";
            superToolTip6.Items.Add(toolTipItem6);
            this.btnTeklifNedenleri.SuperTip = superToolTip6;
            this.btnTeklifNedenleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTeklifNedenleri_ItemClick);
            // 
            // btnOdemeTipleri
            // 
            this.btnOdemeTipleri.Caption = "Ödeme Tipleri";
            this.btnOdemeTipleri.Id = 272;
            this.btnOdemeTipleri.ImageIndex = 65;
            this.btnOdemeTipleri.Name = "btnOdemeTipleri";
            this.btnOdemeTipleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnOdemeTipleri_ItemClick);
            // 
            // btnFaturaTipleri
            // 
            this.btnFaturaTipleri.Caption = "Fatura Tipleri";
            this.btnFaturaTipleri.Id = 273;
            this.btnFaturaTipleri.ImageIndex = 66;
            this.btnFaturaTipleri.Name = "btnFaturaTipleri";
            this.btnFaturaTipleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFaturaTipleri_ItemClick);
            // 
            // btnGiderTipleri
            // 
            this.btnGiderTipleri.Caption = "Gider Tipleri";
            this.btnGiderTipleri.Id = 274;
            this.btnGiderTipleri.ImageIndex = 67;
            this.btnGiderTipleri.Name = "btnGiderTipleri";
            this.btnGiderTipleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGiderTipleri_ItemClick);
            // 
            // btnProjeTanimlari
            // 
            this.btnProjeTanimlari.Caption = "Proje Tanımları";
            this.btnProjeTanimlari.Id = 275;
            this.btnProjeTanimlari.ImageIndex = 68;
            this.btnProjeTanimlari.Name = "btnProjeTanimlari";
            this.btnProjeTanimlari.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnProjeTanimlari.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnProjeTanimlari_ItemClick);
            // 
            // btnDanismanlar
            // 
            this.btnDanismanlar.Caption = "Danışmanlar";
            this.btnDanismanlar.Id = 276;
            this.btnDanismanlar.ImageIndex = 55;
            this.btnDanismanlar.LargeImageIndex = 113;
            this.btnDanismanlar.Name = "btnDanismanlar";
            this.btnDanismanlar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDanismanlar_ItemClick);
            // 
            // btnStatu
            // 
            this.btnStatu.Caption = "Yetkili Statüleri";
            this.btnStatu.Id = 277;
            this.btnStatu.ImageIndex = 31;
            this.btnStatu.Name = "btnStatu";
            this.btnStatu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnStatu_ItemClick);
            // 
            // btnAnaGruplar
            // 
            this.btnAnaGruplar.Caption = "Hizmetler Ana Grupları";
            this.btnAnaGruplar.Id = 278;
            this.btnAnaGruplar.ImageIndex = 5;
            this.btnAnaGruplar.Name = "btnAnaGruplar";
            this.btnAnaGruplar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAnaGruplar_ItemClick);
            // 
            // btnAltGruplar
            // 
            this.btnAltGruplar.Caption = "Hizmetler Alt Grupları";
            this.btnAltGruplar.Id = 279;
            this.btnAltGruplar.ImageIndex = 4;
            this.btnAltGruplar.Name = "btnAltGruplar";
            this.btnAltGruplar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAltGruplar_ItemClick);
            // 
            // btnZiyaretNedenleri
            // 
            this.btnZiyaretNedenleri.Caption = "Ziyaret Nedenleri";
            this.btnZiyaretNedenleri.Id = 280;
            this.btnZiyaretNedenleri.ImageIndex = 0;
            this.btnZiyaretNedenleri.Name = "btnZiyaretNedenleri";
            this.btnZiyaretNedenleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnZiyaretNedenleri_ItemClick);
            // 
            // btnPromosyonTanim
            // 
            this.btnPromosyonTanim.Caption = "Promosyon Tanımları";
            this.btnPromosyonTanim.Id = 281;
            this.btnPromosyonTanim.ImageIndex = 52;
            this.btnPromosyonTanim.LargeImageIndex = 64;
            this.btnPromosyonTanim.Name = "btnPromosyonTanim";
            this.btnPromosyonTanim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPromosyonTanim_ItemClick);
            // 
            // btnSozlesmeIcerikTip
            // 
            this.btnSozlesmeIcerikTip.Caption = "Sözleşme ıçerik Tipleri";
            this.btnSozlesmeIcerikTip.Id = 282;
            this.btnSozlesmeIcerikTip.ImageIndex = 27;
            this.btnSozlesmeIcerikTip.Name = "btnSozlesmeIcerikTip";
            toolTipTitleItem6.Text = ".";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Sözleşme içerik tipleri";
            superToolTip7.Items.Add(toolTipTitleItem6);
            superToolTip7.Items.Add(toolTipItem7);
            this.btnSozlesmeIcerikTip.SuperTip = superToolTip7;
            this.btnSozlesmeIcerikTip.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSozlesmeIcerikTip_ItemClick);
            // 
            // btnProjeTanimlariDetay
            // 
            this.btnProjeTanimlariDetay.Caption = "Proje Detay Tanımları";
            this.btnProjeTanimlariDetay.Id = 283;
            this.btnProjeTanimlariDetay.ImageIndex = 69;
            this.btnProjeTanimlariDetay.Name = "btnProjeTanimlariDetay";
            this.btnProjeTanimlariDetay.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnProjeTanimlariDetay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnProjeTanimlariDetay_ItemClick);
            // 
            // btnMusteriListesi
            // 
            this.btnMusteriListesi.Caption = "Müşteri Listesi";
            this.btnMusteriListesi.Id = 284;
            this.btnMusteriListesi.LargeImageIndex = 113;
            this.btnMusteriListesi.Name = "btnMusteriListesi";
            this.btnMusteriListesi.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnMusteriListesi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMusteriListesi_ItemClick);
            // 
            // btnMusteriSeg
            // 
            this.btnMusteriSeg.Caption = "Müşteri Segmant";
            this.btnMusteriSeg.Id = 285;
            this.btnMusteriSeg.ImageIndex = 57;
            this.btnMusteriSeg.LargeImageIndex = 113;
            this.btnMusteriSeg.Name = "btnMusteriSeg";
            toolTipItem8.Text = "Müşteri Segmantasyonları";
            superToolTip8.Items.Add(toolTipItem8);
            this.btnMusteriSeg.SuperTip = superToolTip8;
            this.btnMusteriSeg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMusteriSeg_ItemClick);
            // 
            // btnKartVizitGiris
            // 
            this.btnKartVizitGiris.Caption = "Firma Kartı Listesi";
            this.btnKartVizitGiris.Id = 286;
            this.btnKartVizitGiris.LargeImageIndex = 116;
            this.btnKartVizitGiris.Name = "btnKartVizitGiris";
            this.btnKartVizitGiris.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKartVizitGiris_ItemClick);
            // 
            // btnNeredenUlasildi
            // 
            this.btnNeredenUlasildi.Caption = "Müşteriye Nereden Ulaşıldı";
            this.btnNeredenUlasildi.Id = 287;
            this.btnNeredenUlasildi.ImageIndex = 70;
            this.btnNeredenUlasildi.Name = "btnNeredenUlasildi";
            this.btnNeredenUlasildi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNeredenUlasildi_ItemClick);
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "Teklif";
            this.barLinkContainerItem1.Id = 288;
            this.barLinkContainerItem1.LargeImageIndex = 96;
            this.barLinkContainerItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTtipi),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTHazirlayanlar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTeklifSonuclari),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTeklifNedenleri)});
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barLinkContainerItem2
            // 
            this.barLinkContainerItem2.Caption = "Coðrafya";
            this.barLinkContainerItem2.Id = 289;
            this.barLinkContainerItem2.LargeImageIndex = 112;
            this.barLinkContainerItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmUlkeler),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmSehirler),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmIlceler)});
            this.barLinkContainerItem2.Name = "barLinkContainerItem2";
            // 
            // btnCiroBaslik
            // 
            this.btnCiroBaslik.Caption = "Ciro Başlıkları";
            this.btnCiroBaslik.Id = 290;
            this.btnCiroBaslik.ImageIndex = 72;
            this.btnCiroBaslik.LargeImageIndex = 76;
            this.btnCiroBaslik.Name = "btnCiroBaslik";
            this.btnCiroBaslik.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCiroBaslik_ItemClick);
            // 
            // btnSertifikaTanim
            // 
            this.btnSertifikaTanim.Caption = "Sertifaka Tanım";
            this.btnSertifikaTanim.Id = 291;
            this.btnSertifikaTanim.ImageIndex = 73;
            this.btnSertifikaTanim.LargeImageIndex = 36;
            this.btnSertifikaTanim.Name = "btnSertifikaTanim";
            this.btnSertifikaTanim.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSertifikaTanim_ItemClick);
            // 
            // btnYazMarka
            // 
            this.btnYazMarka.Caption = "Yazılım Marka";
            this.btnYazMarka.Id = 292;
            this.btnYazMarka.ImageIndex = 75;
            this.btnYazMarka.Name = "btnYazMarka";
            this.btnYazMarka.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazMarka_ItemClick);
            // 
            // btnYazG
            // 
            this.btnYazG.Caption = "Yazılım Grup";
            this.btnYazG.Id = 293;
            this.btnYazG.ImageIndex = 74;
            this.btnYazG.Name = "btnYazG";
            this.btnYazG.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazG_ItemClick);
            this.btnYazG.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYazG_ItemDoubleClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Döküman Ekle";
            this.barButtonItem11.Id = 294;
            this.barButtonItem11.LargeImageIndex = 117;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "Etiket Basımı";
            this.barButtonItem13.Id = 295;
            this.barButtonItem13.LargeImageIndex = 66;
            this.barButtonItem13.Name = "barButtonItem13";
            this.barButtonItem13.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem13_ItemClick);
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "barButtonGroup1";
            this.barButtonGroup1.Id = 296;
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // btnDosyaKlasörleri
            // 
            this.btnDosyaKlasörleri.Caption = "Dosya Klasörleri";
            this.btnDosyaKlasörleri.Id = 297;
            this.btnDosyaKlasörleri.ImageIndex = 7;
            this.btnDosyaKlasörleri.LargeImageIndex = 9;
            this.btnDosyaKlasörleri.Name = "btnDosyaKlasörleri";
            this.btnDosyaKlasörleri.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDosyaKlasörleri_ItemClick);
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Toplu Email Gönderimi";
            this.barButtonItem14.Id = 298;
            this.barButtonItem14.LargeImageIndex = 119;
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem14_ItemClick);
            // 
            // btnEmail
            // 
            this.btnEmail.Caption = "Email Havuzu";
            this.btnEmail.Id = 299;
            this.btnEmail.LargeImageIndex = 120;
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEmail_ItemClick);
            // 
            // barLinkContainerItem3
            // 
            this.barLinkContainerItem3.Caption = "Sektörler && Hizmetler";
            this.barLinkContainerItem3.Id = 300;
            this.barLinkContainerItem3.LargeImageIndex = 35;
            this.barLinkContainerItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAnaGruplar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAltGruplar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTaliGruplar),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSektorAna, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSektorAlt)});
            this.barLinkContainerItem3.Name = "barLinkContainerItem3";
            // 
            // btnTaliGruplar
            // 
            this.btnTaliGruplar.Caption = "Hizmetler Tali Grupları";
            this.btnTaliGruplar.Id = 303;
            this.btnTaliGruplar.Name = "btnTaliGruplar";
            this.btnTaliGruplar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTaliGruplar_ItemClick);
            // 
            // btnSektorAna
            // 
            this.btnSektorAna.Caption = "Sektörler Ana Grupları";
            this.btnSektorAna.Id = 301;
            this.btnSektorAna.ImageIndex = 60;
            this.btnSektorAna.Name = "btnSektorAna";
            this.btnSektorAna.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSektorAna_ItemClick);
            // 
            // btnSektorAlt
            // 
            this.btnSektorAlt.Caption = "Sektörler Alt Grupları";
            this.btnSektorAlt.Id = 302;
            this.btnSektorAlt.ImageIndex = 61;
            this.btnSektorAlt.Name = "btnSektorAlt";
            this.btnSektorAlt.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSektorAlt_ItemClick);
            // 
            // barLinkContainerItem4
            // 
            this.barLinkContainerItem4.Caption = "Genel Paremetreler";
            this.barLinkContainerItem4.Id = 304;
            this.barLinkContainerItem4.LargeImageIndex = 13;
            this.barLinkContainerItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDanismanlar, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnFaturaTipleri, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnGiderTipleri),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnOdemeTipleri),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNeredenUlasildi, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem16, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnProjeTanimlari, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnProjeTanimlariDetay),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSozlesmeIcerikTip, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazG),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYazMarka),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnStatu, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnZiyaretNedenleri, true)});
            this.barLinkContainerItem4.Name = "barLinkContainerItem4";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "Kategoriler";
            this.barButtonItem16.Id = 306;
            this.barButtonItem16.ImageIndex = 14;
            this.barButtonItem16.Name = "barButtonItem16";
            this.barButtonItem16.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem16_ItemClick);
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Çalışma Takvimi";
            this.barButtonItem15.Id = 305;
            this.barButtonItem15.LargeImageIndex = 123;
            this.barButtonItem15.Name = "barButtonItem15";
            this.barButtonItem15.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem15_ItemClick);
            // 
            // btnProjeYonetimi
            // 
            this.btnProjeYonetimi.Caption = "Proje Yönetim";
            this.btnProjeYonetimi.Id = 307;
            this.btnProjeYonetimi.LargeImageIndex = 10;
            this.btnProjeYonetimi.Name = "btnProjeYonetimi";
            this.btnProjeYonetimi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnProjeYonetimi_ItemClick);
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7,
            this.ribbonPageGroup8,
            this.ribbonPageGroup3});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Ana Giriş";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.btnFirmalar);
            this.ribbonPageGroup7.ItemLinks.Add(this.btnKartVizitGiris, true);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "I";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonGroup1);
            this.ribbonPageGroup8.ItemLinks.Add(this.btnProjeYonetimi, true);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem15, true);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem11, true);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem13, true);
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem14, true);
            this.ribbonPageGroup8.ItemLinks.Add(this.btnEmail, true);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "II";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.ribbonGalleryBarItem1);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnMusteriListesi);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Tasarım şablonu";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Raporlama";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRaporlar);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "DASHBOARD";
            // 
            // rpParametersGirisleri
            // 
            this.rpParametersGirisleri.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.rpParametersGirisleri.Name = "rpParametersGirisleri";
            this.rpParametersGirisleri.Text = "Parametre Girişleri";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barLinkContainerItem2, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.barLinkContainerItem1, true);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "I";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barLinkContainerItem4, true);
            this.ribbonPageGroup5.ItemLinks.Add(this.barLinkContainerItem3);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "II";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.btnCiroBaslik);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnMusteriSeg, true);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnSertifikaTanim, true);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnPromosyonTanim, true);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnDosyaKlasörleri, true);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Tipler && Tanımlar";
            // 
            // rpAdminPanel
            // 
            this.rpAdminPanel.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgUsers,
            this.rpgAdmin});
            this.rpAdminPanel.Name = "rpAdminPanel";
            this.rpAdminPanel.Text = "Kullanıcı Yetkileri";
            // 
            // rpgUsers
            // 
            this.rpgUsers.ItemLinks.Add(this.bbUsrViewName);
            this.rpgUsers.ItemLinks.Add(this.bbUsrChgPass);
            this.rpgUsers.Name = "rpgUsers";
            this.rpgUsers.Text = "KULLANICI";
            // 
            // rpgAdmin
            // 
            this.rpgAdmin.ItemLinks.Add(this.bbUsrNew);
            this.rpgAdmin.ItemLinks.Add(this.bbUsrDelete, true);
            this.rpgAdmin.ItemLinks.Add(this.bbUsrPermision, true);
            this.rpgAdmin.Name = "rpgAdmin";
            this.rpgAdmin.Text = "YÖNETıCı";
            // 
            // rpHelp
            // 
            this.rpHelp.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgHelp});
            this.rpHelp.Name = "rpHelp";
            this.rpHelp.Text = "Yardım";
            // 
            // rpgHelp
            // 
            this.rpgHelp.ItemLinks.Add(this.bbHelp);
            this.rpgHelp.ItemLinks.Add(this.barButtonItem3, true);
            this.rpgHelp.Name = "rpgHelp";
            this.rpgHelp.Text = "YARDIM                    ";
            // 
            // repositoryItemFontEdit1
            // 
            this.repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // repositoryItemRangeTrackBar1
            // 
            this.repositoryItemRangeTrackBar1.Name = "repositoryItemRangeTrackBar1";
            // 
            // repositoryItemTrackBar1
            // 
            this.repositoryItemTrackBar1.Name = "repositoryItemTrackBar1";
            // 
            // repositoryItemRangeTrackBar2
            // 
            this.repositoryItemRangeTrackBar2.Name = "repositoryItemRangeTrackBar2";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            // 
            // repositoryItemPictureEdit3
            // 
            this.repositoryItemPictureEdit3.Name = "repositoryItemPictureEdit3";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            this.repositoryItemTimeEdit1.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // repositoryItemZoomTrackBar2
            // 
            this.repositoryItemZoomTrackBar2.Middle = 5;
            this.repositoryItemZoomTrackBar2.Name = "repositoryItemZoomTrackBar2";
            this.repositoryItemZoomTrackBar2.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // repositoryItemFontEdit2
            // 
            this.repositoryItemFontEdit2.AutoHeight = false;
            this.repositoryItemFontEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemFontEdit2.Name = "repositoryItemFontEdit2";
            // 
            // OthImg
            // 
            this.OthImg.ImageSize = new System.Drawing.Size(15, 15);
            this.OthImg.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("OthImg.ImageStream")));
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Sargı Fişleri";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // bbEnvanterSoftwareList
            // 
            this.bbEnvanterSoftwareList.Id = -1;
            this.bbEnvanterSoftwareList.Name = "bbEnvanterSoftwareList";
            // 
            // SmallImg
            // 
            this.SmallImg.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImg.ImageStream")));
            this.SmallImg.Images.SetKeyName(0, "businessmen.png");
            this.SmallImg.Images.SetKeyName(1, "businessman_find.png");
            this.SmallImg.Images.SetKeyName(2, "sort_ascending.png");
            this.SmallImg.Images.SetKeyName(3, "sort_az_descending.png");
            // 
            // tileControl1
            // 
            this.tileControl1.AllowSelectedItem = true;
            this.tileControl1.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tileControl1.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileControl1.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.tileControl1.AppearanceItem.Normal.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.tileControl1.AppearanceItem.Normal.Options.UseFont = true;
            this.tileControl1.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.ItemCheckMode = DevExpress.XtraEditors.TileItemCheckMode.Multiple;
            this.tileControl1.ItemContentAnimation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollTop;
            this.tileControl1.ItemSize = 140;
            this.tileControl1.Location = new System.Drawing.Point(0, 147);
            this.tileControl1.MaxId = 49;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.RowCount = 3;
            this.tileControl1.SelectedItem = this.tileSalesYearly;
            this.tileControl1.Size = new System.Drawing.Size(1338, 585);
            this.tileControl1.TabIndex = 6;
            this.tileControl1.Visible = false;
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.tileSalesYearly);
            this.tileGroup2.Items.Add(this.tileCustomerAnalysis);
            this.tileGroup2.Items.Add(this.tileHedefSapma);
            this.tileGroup2.Items.Add(this.tileProductAnalysis);
            this.tileGroup2.Items.Add(this.tileCustomerGroup);
            this.tileGroup2.Items.Add(this.tileCountrySales);
            this.tileGroup2.Name = "tileGroup2";
            this.tileGroup2.Text = null;
            // 
            // tileSalesYearly
            // 
            this.tileSalesYearly.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileSalesYearly.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileSalesYearly.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileSalesYearly.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileSalesYearly.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileSalesYearly.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileSalesYearly.AppearanceItem.Normal.Options.UseFont = true;
            this.tileSalesYearly.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileSalesYearly.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileSalesYearly.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileSalesYearly.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileSalesYearly.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement1.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement1.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside;
            tileItemElement1.Text = "<size=28>Yıllık Satışlar</size>";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement2.Text = "<size=34></size>";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.tileSalesYearly.Elements.Add(tileItemElement1);
            this.tileSalesYearly.Elements.Add(tileItemElement2);
            this.tileSalesYearly.Id = 6;
            this.tileSalesYearly.IsLarge = true;
            this.tileSalesYearly.Name = "tileSalesYearly";
            this.tileSalesYearly.Padding = new System.Windows.Forms.Padding(10, 3, 10, 18);
            // 
            // tileCustomerAnalysis
            // 
            this.tileCustomerAnalysis.AppearanceItem.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileCustomerAnalysis.AppearanceItem.Hovered.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(200)))), ((int)(((byte)(26)))));
            this.tileCustomerAnalysis.AppearanceItem.Hovered.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileCustomerAnalysis.AppearanceItem.Hovered.Options.UseBackColor = true;
            this.tileCustomerAnalysis.AppearanceItem.Hovered.Options.UseBorderColor = true;
            this.tileCustomerAnalysis.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileCustomerAnalysis.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(200)))), ((int)(((byte)(26)))));
            this.tileCustomerAnalysis.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileCustomerAnalysis.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileCustomerAnalysis.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileCustomerAnalysis.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileCustomerAnalysis.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileCustomerAnalysis.AppearanceItem.Normal.Options.UseFont = true;
            this.tileCustomerAnalysis.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileCustomerAnalysis.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileCustomerAnalysis.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(200)))), ((int)(((byte)(26)))));
            this.tileCustomerAnalysis.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileCustomerAnalysis.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileCustomerAnalysis.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement3.Image = global::GABI_CRM.Properties.Resources.UserManagment;
            tileItemElement3.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement3.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement3.Text = "<size=14>Ülkeler  Bazında   <br>Durum</size>";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            this.tileCustomerAnalysis.Elements.Add(tileItemElement3);
            this.tileCustomerAnalysis.Id = 32;
            this.tileCustomerAnalysis.IsLarge = true;
            this.tileCustomerAnalysis.Name = "tileCustomerAnalysis";
            this.tileCustomerAnalysis.Padding = new System.Windows.Forms.Padding(10, 10, 10, 24);
            // 
            // tileHedefSapma
            // 
            this.tileHedefSapma.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileHedefSapma.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(90)))));
            this.tileHedefSapma.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileHedefSapma.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileHedefSapma.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileHedefSapma.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileHedefSapma.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileHedefSapma.AppearanceItem.Normal.Options.UseFont = true;
            this.tileHedefSapma.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileHedefSapma.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileHedefSapma.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(90)))));
            this.tileHedefSapma.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileHedefSapma.AppearanceItem.Selected.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileHedefSapma.AppearanceItem.Selected.ForeColor = System.Drawing.Color.White;
            this.tileHedefSapma.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileHedefSapma.AppearanceItem.Selected.Options.UseBorderColor = true;
            this.tileHedefSapma.AppearanceItem.Selected.Options.UseFont = true;
            this.tileHedefSapma.AppearanceItem.Selected.Options.UseForeColor = true;
            tileItemElement4.Image = global::GABI_CRM.Properties.Resources.Sign4;
            tileItemElement4.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement4.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement4.Text = "<size=14>Müşteri Analizi </size>";
            this.tileHedefSapma.Elements.Add(tileItemElement4);
            this.tileHedefSapma.Id = 41;
            this.tileHedefSapma.IsLarge = true;
            this.tileHedefSapma.Name = "tileHedefSapma";
            // 
            // tileProductAnalysis
            // 
            this.tileProductAnalysis.AppearanceItem.Hovered.ForeColor = System.Drawing.Color.White;
            this.tileProductAnalysis.AppearanceItem.Hovered.Options.UseForeColor = true;
            this.tileProductAnalysis.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileProductAnalysis.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tileProductAnalysis.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileProductAnalysis.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileProductAnalysis.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileProductAnalysis.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileProductAnalysis.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileProductAnalysis.AppearanceItem.Normal.Options.UseFont = true;
            this.tileProductAnalysis.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileProductAnalysis.AppearanceItem.Normal.Options.UseTextOptions = true;
            this.tileProductAnalysis.AppearanceItem.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tileProductAnalysis.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileProductAnalysis.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tileProductAnalysis.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileProductAnalysis.AppearanceItem.Selected.ForeColor = System.Drawing.Color.White;
            this.tileProductAnalysis.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileProductAnalysis.AppearanceItem.Selected.Options.UseBorderColor = true;
            this.tileProductAnalysis.AppearanceItem.Selected.Options.UseForeColor = true;
            tileItemElement5.Image = global::GABI_CRM.Properties.Resources.Rates;
            tileItemElement5.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement5.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement5.Text = "<size=16>Müşteri Grubu Bazında Yıllık Satış </size>";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopLeft;
            this.tileProductAnalysis.Elements.Add(tileItemElement5);
            this.tileProductAnalysis.Id = 0;
            this.tileProductAnalysis.IsLarge = true;
            this.tileProductAnalysis.Name = "tileProductAnalysis";
            // 
            // tileCustomerGroup
            // 
            this.tileCustomerGroup.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileCustomerGroup.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileCustomerGroup.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileCustomerGroup.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileCustomerGroup.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileCustomerGroup.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileCustomerGroup.AppearanceItem.Normal.Options.UseFont = true;
            this.tileCustomerGroup.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileCustomerGroup.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileCustomerGroup.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.tileCustomerGroup.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileCustomerGroup.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement6.Image = global::GABI_CRM.Properties.Resources.Statistics;
            tileItemElement6.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileItemElement6.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement6.Text = "<size=14>Bölgesel Bazlı Satış <size>";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.tileCustomerGroup.Elements.Add(tileItemElement6);
            this.tileCustomerGroup.Id = 33;
            this.tileCustomerGroup.IsLarge = true;
            this.tileCustomerGroup.Name = "tileCustomerGroup";
            // 
            // tileCountrySales
            // 
            this.tileCountrySales.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(111)))), ((int)(((byte)(40)))));
            this.tileCountrySales.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(111)))), ((int)(((byte)(40)))));
            this.tileCountrySales.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileCountrySales.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileCountrySales.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileCountrySales.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileCountrySales.AppearanceItem.Normal.Options.UseFont = true;
            this.tileCountrySales.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileCountrySales.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(111)))), ((int)(((byte)(40)))));
            this.tileCountrySales.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(111)))), ((int)(((byte)(40)))));
            this.tileCountrySales.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileCountrySales.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement7.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement7.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement7.Text = "<size=14>Sekteröl Analiz </size>";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.tileCountrySales.Elements.Add(tileItemElement7);
            this.tileCountrySales.Id = 26;
            this.tileCountrySales.IsLarge = true;
            this.tileCountrySales.Name = "tileCountrySales";
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.tileKarlilik);
            this.tileGroup3.Items.Add(this.tileInOut);
            this.tileGroup3.Items.Add(this.tileProductGroup);
            this.tileGroup3.Items.Add(this.tileCustomerTop);
            this.tileGroup3.Items.Add(this.tileOdemePerf);
            this.tileGroup3.Items.Add(this.tileGeneralReport);
            this.tileGroup3.Name = "tileGroup3";
            this.tileGroup3.Text = null;
            // 
            // tileKarlilik
            // 
            this.tileKarlilik.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(171)))), ((int)(((byte)(220)))));
            this.tileKarlilik.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(227)))));
            this.tileKarlilik.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(184)))), ((int)(((byte)(225)))));
            this.tileKarlilik.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileKarlilik.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileKarlilik.AppearanceItem.Normal.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.tileKarlilik.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileKarlilik.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileKarlilik.AppearanceItem.Normal.Options.UseFont = true;
            this.tileKarlilik.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileKarlilik.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(171)))), ((int)(((byte)(220)))));
            this.tileKarlilik.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(227)))));
            this.tileKarlilik.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(184)))), ((int)(((byte)(225)))));
            this.tileKarlilik.AppearanceItem.Selected.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileKarlilik.AppearanceItem.Selected.ForeColor = System.Drawing.Color.White;
            this.tileKarlilik.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileKarlilik.AppearanceItem.Selected.Options.UseBorderColor = true;
            this.tileKarlilik.AppearanceItem.Selected.Options.UseFont = true;
            this.tileKarlilik.AppearanceItem.Selected.Options.UseForeColor = true;
            tileItemElement8.Image = global::GABI_CRM.Properties.Resources.poep;
            tileItemElement8.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement8.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement8.Text = "<size=19>Satış <br>Mühendisleri</size>";
            this.tileKarlilik.Elements.Add(tileItemElement8);
            this.tileKarlilik.Id = 40;
            this.tileKarlilik.IsLarge = true;
            this.tileKarlilik.Name = "tileKarlilik";
            // 
            // tileInOut
            // 
            this.tileInOut.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(59)))));
            this.tileInOut.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(59)))));
            this.tileInOut.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(59)))));
            this.tileInOut.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileInOut.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileInOut.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileInOut.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileInOut.AppearanceItem.Normal.Options.UseFont = true;
            this.tileInOut.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileInOut.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(59)))));
            this.tileInOut.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(59)))));
            this.tileInOut.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(45)))), ((int)(((byte)(59)))));
            this.tileInOut.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileInOut.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement9.Image = global::GABI_CRM.Properties.Resources.OdemePer2;
            tileItemElement9.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement9.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement9.Text = "<size=20>Maliyet Analizleri</size>";
            this.tileInOut.Elements.Add(tileItemElement9);
            this.tileInOut.Id = 46;
            this.tileInOut.IsLarge = true;
            this.tileInOut.Name = "tileInOut";
            // 
            // tileProductGroup
            // 
            this.tileProductGroup.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(75)))), ((int)(((byte)(95)))));
            this.tileProductGroup.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(75)))), ((int)(((byte)(95)))));
            this.tileProductGroup.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(75)))), ((int)(((byte)(125)))));
            this.tileProductGroup.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileProductGroup.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileProductGroup.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileProductGroup.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileProductGroup.AppearanceItem.Normal.Options.UseFont = true;
            this.tileProductGroup.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileProductGroup.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(75)))), ((int)(((byte)(95)))));
            this.tileProductGroup.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(75)))), ((int)(((byte)(95)))));
            this.tileProductGroup.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(75)))), ((int)(((byte)(125)))));
            this.tileProductGroup.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileProductGroup.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement10.Image = global::GABI_CRM.Properties.Resources.Research;
            tileItemElement10.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileItemElement10.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement10.Text = "<size=17>Performans Analizleri</size>";
            this.tileProductGroup.Elements.Add(tileItemElement10);
            this.tileProductGroup.Id = 25;
            this.tileProductGroup.IsLarge = true;
            this.tileProductGroup.Name = "tileProductGroup";
            // 
            // tileCustomerTop
            // 
            this.tileCustomerTop.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileCustomerTop.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileCustomerTop.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tileCustomerTop.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileCustomerTop.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileCustomerTop.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileCustomerTop.AppearanceItem.Normal.Options.UseFont = true;
            this.tileCustomerTop.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileCustomerTop.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileCustomerTop.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(126)))), ((int)(((byte)(17)))));
            this.tileCustomerTop.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileCustomerTop.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement11.Image = global::GABI_CRM.Properties.Resources.dreamstime_3;
            tileItemElement11.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight;
            tileItemElement11.Text = "<size=19>Top 5+ Müşteri <br></size>";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            this.tileCustomerTop.Elements.Add(tileItemElement11);
            this.tileCustomerTop.Id = 12;
            this.tileCustomerTop.IsLarge = true;
            this.tileCustomerTop.Name = "tileCustomerTop";
            this.tileCustomerTop.Padding = new System.Windows.Forms.Padding(10);
            // 
            // tileOdemePerf
            // 
            this.tileOdemePerf.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileOdemePerf.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tileOdemePerf.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileOdemePerf.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileOdemePerf.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileOdemePerf.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileOdemePerf.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileOdemePerf.AppearanceItem.Normal.Options.UseFont = true;
            this.tileOdemePerf.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileOdemePerf.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(175)))), ((int)(((byte)(0)))));
            this.tileOdemePerf.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.tileOdemePerf.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(187)))), ((int)(((byte)(38)))));
            this.tileOdemePerf.AppearanceItem.Selected.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileOdemePerf.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileOdemePerf.AppearanceItem.Selected.Options.UseBorderColor = true;
            this.tileOdemePerf.AppearanceItem.Selected.Options.UseFont = true;
            tileItemElement12.Image = global::GABI_CRM.Properties.Resources.System;
            tileItemElement12.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement12.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement12.Text = "<size=20>Satış Verileri</size>";
            this.tileOdemePerf.Elements.Add(tileItemElement12);
            this.tileOdemePerf.Id = 47;
            this.tileOdemePerf.IsLarge = true;
            this.tileOdemePerf.Name = "tileOdemePerf";
            // 
            // tileGeneralReport
            // 
            this.tileGeneralReport.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(171)))), ((int)(((byte)(220)))));
            this.tileGeneralReport.AppearanceItem.Normal.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(227)))));
            this.tileGeneralReport.AppearanceItem.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(184)))), ((int)(((byte)(225)))));
            this.tileGeneralReport.AppearanceItem.Normal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tileGeneralReport.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileGeneralReport.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileGeneralReport.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileGeneralReport.AppearanceItem.Normal.Options.UseFont = true;
            this.tileGeneralReport.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileGeneralReport.AppearanceItem.Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(171)))), ((int)(((byte)(220)))));
            this.tileGeneralReport.AppearanceItem.Selected.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(227)))));
            this.tileGeneralReport.AppearanceItem.Selected.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(184)))), ((int)(((byte)(225)))));
            this.tileGeneralReport.AppearanceItem.Selected.Options.UseBackColor = true;
            this.tileGeneralReport.AppearanceItem.Selected.Options.UseBorderColor = true;
            tileItemElement13.Image = global::GABI_CRM.Properties.Resources.gabi;
            tileItemElement13.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement13.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Stretch;
            tileItemElement13.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.None;
            tileItemElement13.Text = "";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomLeft;
            this.tileGeneralReport.Elements.Add(tileItemElement13);
            this.tileGeneralReport.Id = 37;
            this.tileGeneralReport.IsLarge = true;
            this.tileGeneralReport.Name = "tileGeneralReport";
            this.tileGeneralReport.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileGeneralReport_ItemClick);
            // 
            // frmMain
            // 
            this.Appearance.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1338, 755);
            this.Controls.Add(this.tileControl1);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.btnYazilimMarka);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.btnYazilimMarka;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar1;
            this.Text = "GABI CRM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmImgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LarImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnYazilimMarka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRangeTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRangeTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemFontEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OthImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.PopupControlContainer popupControlContainer1;
        private System.ComponentModel.IContainer components;
        private DevExpress.Utils.ImageCollection LarImg;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.Utils.ImageCollection SmImgMain;
        private DevExpress.Utils.ImageCollection OthImg;

        private DevExpress.XtraBars.Ribbon.ApplicationMenu appM;
        private DevExpress.XtraBars.Ribbon.RibbonControl btnYazilimMarka;
        private DevExpress.XtraBars.BarButtonItem bbStockCodeGenerate;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        public DevExpress.XtraBars.BarButtonItem bbUsrViewName;
        public DevExpress.XtraBars.BarButtonItem bbUsrChgPass;
        public DevExpress.XtraBars.BarButtonItem bbUsrDelete;
        public DevExpress.XtraBars.BarButtonItem bbUsrNew;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpAdminPanel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgUsers;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAdmin;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraBars.BarButtonItem bbDensity;
        private DevExpress.XtraBars.BarButtonItem bbIth;
        private DevExpress.XtraBars.BarButtonItem bbPriMouldMeas;
        private DevExpress.XtraBars.BarButtonItem bbPriCondMeas;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpHelp;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgHelp;
        private DevExpress.XtraBars.BarButtonItem bbHelp;
        private DevExpress.XtraBars.BarButtonItem bbVoltageLevelParams;
        public DevExpress.LookAndFeel.DefaultLookAndFeel dLaf;
        private DevExpress.XtraBars.BarStaticItem bsiKullanicIsmi;
        private DevExpress.XtraBars.BarStaticItem bsiGirisTarihi;
        private DevExpress.XtraEditors.Repository.RepositoryItemRangeTrackBar repositoryItemRangeTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRangeTrackBar repositoryItemRangeTrackBar2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbAGAUrunAgaciOlustur;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.BarButtonItem bbGerilimSargi;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        public DevExpress.XtraBars.BarButtonItem bbUsrPermision;
        public DevExpress.XtraBars.Ribbon.RibbonPage rpParametersGirisleri;
        private DevExpress.XtraBars.BarButtonItem bbKutuEtiketAG;
        private DevExpress.XtraBars.BarButtonItem bbTrafoEtiket;
        private DevExpress.XtraBars.BarButtonItem bbAkimTestSertifika;
        public DevExpress.XtraBars.BarButtonItem bbSeriEslemeMuhur;
        private DevExpress.XtraBars.BarButtonItem bbYmCards;
        public DevExpress.XtraBars.BarButtonItem btnOGAEtiket;
        public DevExpress.XtraBars.BarButtonItem btnOGGEtiket;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem btnRCEtiket;
        public DevExpress.XtraBars.BarButtonItem bbSayimM10001;
        public DevExpress.XtraBars.BarButtonItem bbSayimMlz10001;
        public DevExpress.XtraBars.BarButtonItem bbSaySonuc;
        public DevExpress.XtraBars.BarButtonItem bbDuranVarlikListesi;
        private DevExpress.XtraBars.BarButtonItem bbTrafoEtiketOA;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit2;
        private DevExpress.XtraBars.BarButtonItem bbDepartmentList;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        public DevExpress.XtraBars.BarButtonItem bbEnvanterSoftwareList;
        public DevExpress.XtraBars.BarButtonItem bbUrtTezgah;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        public DevExpress.XtraBars.BarButtonItem bbStoktanSatis;
        public DevExpress.XtraBars.BarButtonItem bbUretimdenSatis;
        private DevExpress.XtraBars.BarButtonItem bbFasonMusteriden;
        private DevExpress.XtraBars.BarButtonItem bbFasoncudan;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem bmUlkeler;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem bmSehirler;
        private DevExpress.XtraBars.BarButtonItem btnSektorler;
        private DevExpress.XtraBars.BarButtonItem btnRaporlar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.Utils.ImageCollection SmallImg;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem tileSalesYearly;
        private DevExpress.XtraEditors.TileItem tileCustomerAnalysis;
        private DevExpress.XtraEditors.TileItem tileHedefSapma;
        private DevExpress.XtraEditors.TileItem tileProductAnalysis;
        private DevExpress.XtraEditors.TileItem tileCustomerGroup;
        private DevExpress.XtraEditors.TileItem tileCountrySales;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem tileKarlilik;
        private DevExpress.XtraEditors.TileItem tileInOut;
        private DevExpress.XtraEditors.TileItem tileProductGroup;
        private DevExpress.XtraEditors.TileItem tileCustomerTop;
        private DevExpress.XtraEditors.TileItem tileOdemePerf;
        public DevExpress.XtraEditors.TileItem tileGeneralReport;
        private DevExpress.XtraBars.BarButtonItem btnFirmalar;
        private DevExpress.XtraBars.BarButtonItem bmIlceler;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem btnTtipi;
        private DevExpress.XtraBars.BarButtonItem btnTHazirlayanlar;
        private DevExpress.XtraBars.BarButtonItem btnTeklifSonuclari;
        private DevExpress.XtraBars.BarButtonItem btnTeklifNedenleri;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem btnOdemeTipleri;
        private DevExpress.XtraBars.BarButtonItem btnFaturaTipleri;
        private DevExpress.XtraBars.BarButtonItem btnGiderTipleri;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem btnProjeTanimlari;
        private DevExpress.XtraBars.BarButtonItem btnDanismanlar;
        private DevExpress.XtraBars.BarButtonItem btnStatu;
        private DevExpress.XtraBars.BarButtonItem btnAnaGruplar;
        private DevExpress.XtraBars.BarButtonItem btnAltGruplar;
        private DevExpress.XtraBars.BarButtonItem btnZiyaretNedenleri;
        private DevExpress.XtraBars.BarButtonItem btnPromosyonTanim;
        private DevExpress.XtraBars.BarButtonItem btnSozlesmeIcerikTip;
        private DevExpress.XtraBars.BarButtonItem btnProjeTanimlariDetay;
        private DevExpress.XtraBars.BarButtonItem btnMusteriListesi;
        private DevExpress.XtraBars.BarButtonItem btnMusteriSeg;
        private DevExpress.XtraBars.BarButtonItem btnKartVizitGiris;
        private DevExpress.XtraBars.BarButtonItem btnNeredenUlasildi;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem2;
        private DevExpress.XtraBars.BarButtonItem btnCiroBaslik;
        private DevExpress.XtraBars.BarButtonItem btnSertifikaTanim;
        private DevExpress.XtraBars.BarButtonItem btnYazMarka;
        private DevExpress.XtraBars.BarButtonItem btnYazG;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        private DevExpress.XtraBars.BarButtonItem btnDosyaKlasörleri;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem btnEmail;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem3;
        private DevExpress.XtraBars.BarButtonItem btnSektorAna;
        private DevExpress.XtraBars.BarButtonItem btnSektorAlt;
        private DevExpress.XtraBars.BarButtonItem btnTaliGruplar;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarButtonItem btnProjeYonetimi;

    }
}
