﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;


using System.Diagnostics;
using DevExpress.XtraEditors;

using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Gallery;
using DevExpress.Utils.Drawing;
using DevExpress.Utils;
using System.Data.SqlClient;
using Global;
using DevExpress.XtraBars;

using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraReports.UI;
//using BTI.Properties;
using GABI_CRM.Properties;
using GABI_CRM;




namespace BTI {
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();

            InitSkinGallery();
        }

        void InitSkinGallery()
        {
            //SkinHelper.InitSkinGallery(rgbiSkins, true);
            //SkinHelper.InitSkinGallery(ribbonGalleryBarItem1, true);
           
          //  UserLookAndFeel.Default.SkinName =Settings.Default["ApplicationSkinName"].ToString();
            SkinHelper.InitSkinGallery(ribbonGalleryBarItem1);
           

        }


        private void GetSystemInfo()
        {
            bsiKullanicIsmi.Caption = string.Format("Kullanıcı ısmi :{0}", frmLogin.KullaniciAdi);
            if (string.IsNullOrEmpty(bsiGirisTarihi.Caption))
                bsiGirisTarihi.Caption = string.Format("Giriş Tarihi :{0}", DateTime.Now);

        }



        private void bbUsrPermision_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowPermision();
        }

        private void bbUsrChgPass_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowChangePassword();

        }



        private void bbUsrDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowDeleteUser();
        }

        private void bbUsrNew_ItemClick(object sender, ItemClickEventArgs e)
        {

            frmNewUser.KullaniciAdi = "";
            PubShowForm.ShowNewUser();
        }



  


        private void frmMain_Load(object sender, EventArgs e)
        {


          
            bool isExists = System.IO.Directory.Exists(PubVariable.fileNameFolder);

            if (!isExists)
                System.IO.Directory.CreateDirectory(PubVariable.fileNameFolder);

            GetSystemInfo();



            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;

                this.Text = string.Format("GABI CRM Versiyon:{0}", ad.CurrentVersion.ToString());

            }


        }



  
 

        private void rbMain_ApplicationButtonDoubleClick(object sender, EventArgs e)
        {
            Close();
        }

    
        private void barButtonItem10_ItemClick_2(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private void barButtonItem1_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }


        public class PersonInfo
        {
            private string _firstName;
        

            public PersonInfo(string firstName)
            {
                _firstName = firstName;
             
            }

            public override string ToString()
            {
                return _firstName;
            }
        }



 

        private Boolean KapatmaDurumu = false;
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (KapatmaDurumu == true)
                Application.Exit();
            else
            {
                if (MessageBox.Show("Programdan çıkış yapmak istediðinize emin misiniz?", Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    KapatmaDurumu = true;
                    Application.Exit();
                }
                else
                    e.Cancel = true;
            }

        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            Settings.Default["ApplicationSkinName"] = UserLookAndFeel.Default.SkinName;
            Settings.Default.Save();
            MessageBox.Show("Seçmiş olduðunuz tasarım kayıt edildi.");
        }

        private void btnUlkeler_ItemClick(object sender, ItemClickEventArgs e)
        {
            
            PubShowForm.ShowUlkeler();
        }

        private void btnStokKarti_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bmSehirler_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowSehirler();
        }

        private void btnSektorler_ItemClick(object sender, ItemClickEventArgs e)
        {
            //PubShowForm.ShowSektorler();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            
        }

        private void tileGeneralReport_ItemClick(object sender, TileItemEventArgs e)
        {
            tileControl1.Visible = false;
            btnYazilimMarka.Enabled = true;
          //  rbMain.Visible = true;
        }

        private void btnRaporlar_ItemClick(object sender, ItemClickEventArgs e)
        {
            
         //   rbMain.Visible = false;
            tileControl1.Visible = true;
            btnYazilimMarka.Enabled = false;
        }

        private void btnFirmalar_ItemClick(object sender, ItemClickEventArgs e)
        {
            Firmalar.Type = "1";
            Firmalar.FTITLE = "Aktif Firma Listesi";
            PubShowForm.ShowFirmalar();
        }

        private void bmIlceler_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowIlceler();
        }

        private void btnTtipi_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams("TTip", e.Item.Caption );
            
        }

        private void btnTHazirlayanlar_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams( "THazr",e.Item.Caption );
            //PubShowForm.ShowTekHazirlayanlar();
        }

        private void btnTeklifSonuclari_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams( "TSnc", e.Item.Caption );
            //PubShowForm.ShowTekSonucu();
        }

        private void btnTeklifNedenleri_ItemClick(object sender, ItemClickEventArgs e)
        {
            
         

            PubShowForm.ShowGenelParams( "TKnz", e.Item.Caption);
           // PubShowForm.ShowTekKazanmaKaybetmeNedenleri();
        }

        private void btnOdemeTipleri_ItemClick(object sender, ItemClickEventArgs e)
        {
          

            PubShowForm.ShowGenelParams("TFatOdeTip",e.Item.Caption );
        }

        private void btnFaturaTipleri_ItemClick(object sender, ItemClickEventArgs e)
        {
      
  
            PubShowForm.ShowGenelParams("TFatTip", e.Item.Caption );
            //PubShowForm.ShowFaturaTipleri();
        }

        private void btnGiderTipleri_ItemClick(object sender, ItemClickEventArgs e)
        {

  
            PubShowForm.ShowGenelParams("TFatGidTip", e.Item.Caption );
            
            
           // PubShowForm.ShowGiderTipleri();

        }

        private void btnProjeTanimlari_ItemClick(object sender, ItemClickEventArgs e)
        {

    
            PubShowForm.ShowGenelParams("Ptnm", e.Item.Caption );
           // PubShowForm.ShowProjeTanimlari();
        }

        private void btnDanismanlar_ItemClick(object sender, ItemClickEventArgs e)
        {


       

            PubShowForm.ShowGenelParams("Dnsm",e.Item.Caption );
          
        }

        private void btnStatu_ItemClick(object sender, ItemClickEventArgs e)
        {

          

          
            PubShowForm.ShowGenelParams("YSatutu",e.Item.Caption);

        }

        private void btnAnaGruplar_ItemClick(object sender, ItemClickEventArgs e)
        {

            frmCrmAnaGrup.GP_TYPE_ANA  = "Ang";
            frmCrmAnaGrup.FTITLE = e.Item.Caption;
            PubShowForm.ShowAnaGrup();
        }

        private void btnAltGruplar_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmCRMAltGrup.GP_TYPE_ANA = "Ang";
            frmCRMAltGrup.GP_TYPE_ALT = "AltG";
            frmCRMAltGrup.FTITLE = e.Item.Caption;
            PubShowForm.ShowAltGrup();
        }

        private void btnZiyaretNedenleri_ItemClick(object sender, ItemClickEventArgs e)
        {
    

            PubShowForm.ShowGenelParams("Zned", e.Item.Caption);


        //    PubShowForm.ShowZiyaretNedenleri();

        }

        private void btnPromosyonTanim_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowPromosyonTanim();
        }

        private void btnSozlesmeIcerikTip_ItemClick(object sender, ItemClickEventArgs e)
        {

          
            PubShowForm.ShowGenelParams("SzIc", e.Item.Caption);
          //  PubShowForm.ShowSozlesmeIcerikTipleri();
        }

        private void btnProjeTanimlariDetay_ItemClick(object sender, ItemClickEventArgs e)
        {

        
            PubShowForm.ShowGenelParams( "ProDetBas",  e.Item.Caption);
          //  PubShowForm.ShowProjeDetayBasliklari();
        }

        private void btnMusteriListesi_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowMusteriListesi();
        }

        private void btnMusteriSeg_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams("Mseg",e.Item.Caption ) ;
        }

        private void btnKartVizitGiris_ItemClick(object sender, ItemClickEventArgs e)
        {
            Firmalar.Type = "2";
            Firmalar.FTITLE ="Firma Kartı Listesi" ;
            PubShowForm.ShowFirmalar();
        }

        private void btnNeredenUlasildi_ItemClick(object sender, ItemClickEventArgs e)
        {
                  
          
            PubShowForm.ShowGenelParams("Nered",e.Item.Caption );
        }

        private void btnCiroBaslik_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams("CiroB",  e.Item.Caption );
        }

        private void btnSertifikaTanim_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams("SerT",e.Item.Caption );
        }

        private void btnYazG_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {

        }

        private void btnYazG_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams( "YazGrup","Yazılım Grupları");
        }

        private void btnYazMarka_ItemClick(object sender, ItemClickEventArgs e)
        {
           
            PubShowForm.ShowGenelParams( "YazMarka","Yazılım Markaları");
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show("Bu fonksiyonu aktif müşteri listesinden kullanabilirsiniz.");
            //PubShowForm.ShowMusteriListesi();
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show("Bu fonksiyonu aktif müşteri listesinden kullanabilirsiniz.");
            //PubShowForm.ShowMusteriListesi();
        }

        private void btnDosyaKlasörleri_ItemClick(object sender, ItemClickEventArgs e)
        {

            PubShowForm.ShowGenelParams( "DKlasor","Dosya Klasörleri" );
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show("Bu fonksiyonu aktif müşteri listesinden kullanabilirsiniz.");
            //PubShowForm.ShowMusteriListesi();
        }

        private void btnEmail_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowEmailPool();
        }

        private void btnSektorAna_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmCrmAnaGrup.GP_TYPE_ANA = "SektAna";
            frmCrmAnaGrup.FTITLE = e.Item.Caption;
            PubShowForm.ShowAnaGrup();
        }

        private void btnSektorAlt_ItemClick(object sender, ItemClickEventArgs e)
        {


            frmCRMAltGrup.GP_TYPE_ANA = "SektAna";
            frmCRMAltGrup.GP_TYPE_ALT = "NACE";// "SektAnaAlt1";
            frmCRMAltGrup.FTITLE = e.Item.Caption;
            PubShowForm.ShowAltGrup();


        }

        private void btnTaliGruplar_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmCRMAltGrup.GP_TYPE_ANA = "AltG";
            frmCRMAltGrup.GP_TYPE_ALT = "AltG1";
            frmCRMAltGrup.FTITLE = "Tali Gruplar";
            PubShowForm.ShowAltGrup();
        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowCalendar();
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            PubShowForm.ShowGenelParams("Ktg", e.Item.Caption);
        }

        private void btnProjeYonetimi_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmGantSchedule.ProjeNo = "1";
            PubShowForm.ShowProjeYonetimi();

        }

        private void bbHelp_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
      




 


    }
}
