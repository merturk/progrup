﻿namespace GABI_CRM
{
    partial class frmCRMAltGrupEkle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCRMAltGrupEkle));
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtTanim = new DevExpress.XtraEditors.TextEdit();
            this.repLupAnaGrup = new DevExpress.XtraEditors.LookUpEdit();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.barMan = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnKaydet = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.txtKod = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupAnaGrup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(21, 42);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(45, 13);
            this.labelControl7.TabIndex = 29;
            this.labelControl7.Text = "Ana Grup";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(21, 68);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(28, 13);
            this.labelControl8.TabIndex = 31;
            this.labelControl8.Text = "Tanım";
            // 
            // txtTanim
            // 
            this.txtTanim.Location = new System.Drawing.Point(78, 65);
            this.txtTanim.Name = "txtTanim";
            this.txtTanim.Properties.MaxLength = 150;
            this.txtTanim.Size = new System.Drawing.Size(214, 20);
            this.txtTanim.TabIndex = 122;
            // 
            // repLupAnaGrup
            // 
            this.repLupAnaGrup.EnterMoveNextControl = true;
            this.repLupAnaGrup.Location = new System.Drawing.Point(78, 39);
            this.repLupAnaGrup.Name = "repLupAnaGrup";
            this.repLupAnaGrup.Properties.AutoSearchColumnIndex = 1;
            this.repLupAnaGrup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLupAnaGrup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Kod", 5, "Kod"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Tanim", "Tanım")});
            this.repLupAnaGrup.Properties.DisplayMember = "Tanim";
            this.repLupAnaGrup.Properties.DropDownRows = 25;
            this.repLupAnaGrup.Properties.NullText = "";
            this.repLupAnaGrup.Properties.PopupWidth = 600;
            this.repLupAnaGrup.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.repLupAnaGrup.Properties.SortColumnIndex = 1;
            this.repLupAnaGrup.Properties.ValueMember = "Kod";
            this.repLupAnaGrup.Size = new System.Drawing.Size(214, 20);
            this.repLupAnaGrup.TabIndex = 92;
            this.repLupAnaGrup.ToolTip = "Kalıp Tipi";
            this.repLupAnaGrup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.repLupAnaGrup.EditValueChanged += new System.EventHandler(this.repLupSehir_EditValueChanged);
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            this.SmallImage.Images.SetKeyName(43, "attach.png");
            this.SmallImage.Images.SetKeyName(44, "paper_clip.png");
            this.SmallImage.Images.SetKeyName(45, "note2.png");
            this.SmallImage.Images.SetKeyName(46, "barcode_arrow_up.png");
            // 
            // barMan
            // 
            this.barMan.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barMan.DockControls.Add(this.barDockControlTop);
            this.barMan.DockControls.Add(this.barDockControlBottom);
            this.barMan.DockControls.Add(this.barDockControlLeft);
            this.barMan.DockControls.Add(this.barDockControlRight);
            this.barMan.Form = this;
            this.barMan.Images = this.SmallImage;
            this.barMan.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnKaydet,
            this.btnClose});
            this.barMan.MainMenu = this.bar2;
            this.barMan.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnKaydet),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnClose, true)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnKaydet
            // 
            this.btnKaydet.Caption = "Kaydet";
            this.btnKaydet.Id = 0;
            this.btnKaydet.ImageIndex = 10;
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKaydet_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "Kapat";
            this.btnClose.Id = 1;
            this.btnClose.ImageIndex = 22;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(303, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 119);
            this.barDockControlBottom.Size = new System.Drawing.Size(303, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 93);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(303, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 93);
            // 
            // txtKod
            // 
            this.txtKod.Location = new System.Drawing.Point(78, 90);
            this.txtKod.Name = "txtKod";
            this.txtKod.Properties.MaxLength = 10;
            this.txtKod.Size = new System.Drawing.Size(68, 20);
            this.txtKod.TabIndex = 128;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 93);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(18, 13);
            this.labelControl1.TabIndex = 127;
            this.labelControl1.Text = "Kod";
            // 
            // frmAltGrupEkle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(303, 119);
            this.Controls.Add(this.txtKod);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.repLupAnaGrup);
            this.Controls.Add(this.txtTanim);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAltGrupEkle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EkleMusteriTanim_FormClosed);
            this.Load += new System.EventHandler(this.EkleMusteriTanim_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtTanim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLupAnaGrup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKod.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtTanim;
        private DevExpress.XtraEditors.LookUpEdit repLupAnaGrup;
        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarManager barMan;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnKaydet;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraEditors.TextEdit txtKod;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}