﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;
using System.Data.SqlClient;

namespace GABI_CRM
{
    public partial class frmCRMAltGrupEkle : DevExpress.XtraEditors.XtraForm
    {
        public frmCRMAltGrupEkle()
        {
            InitializeComponent();
        }

     
        public static string FTITLE = "";
        public static string GelisKodu = "";
        public static string GP_TYPE = "";
        public static string GP_TYPE_ANA = "";

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void GetList()
        {
            this.Text = FTITLE;

            string ss = string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='{0}' AND GP_DURUM='A'", GP_TYPE_ANA);
            DABindings.GetXtraLookupBindNm(repLupAnaGrup, ss);
            Cursor.Current = Cursors.Default;
        

        
        
        }
        private void GetValues()
        {
            if ( string.IsNullOrEmpty( GelisKodu)) return;

            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format(@"
               SELECT  AnaGrup, GP_VIEW as Tanim,GP_VALUE_STR1 as KodAlt FROM  GEN_PARAMS as g
                LEFT JOIN 
                (SELECT  GP_VIEW as AnaGrup, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE ='{2}')AS SE ON SE.Kod = g.GP_VALUE_STR2
                 Where GP_TYPE='{1}' AND GP_ID = {0} AND GP_DURUM='A'
            ", GelisKodu, GP_TYPE,GP_TYPE_ANA));
            repLupAnaGrup.Text = dt.Rows[0]["AnaGrup"].ToString();
            txtTanim.Text = dt.Rows[0]["Tanim"].ToString();
            txtKod.Text = dt.Rows[0]["KodAlt"].ToString();

            if (!string.IsNullOrEmpty(GelisKodu))
            {
                txtKod.Properties.ReadOnly = true;

            }
        
        }
        private void ClearScreen()
        { 
           
            repLupAnaGrup.Text ="";
            txtTanim.Text ="";
            txtKod.Text = "";
          
        
        }
        private void Save()
        {
            try
            {

                   string AnaGrup, Kod,Tanim;
     
                    if  (string.IsNullOrEmpty(repLupAnaGrup.Text)) { MessageBox.Show("Ana grup tanımını boş geçemezsiniz."); return; } else AnaGrup = repLupAnaGrup.EditValue.ToString();
                    if  (string.IsNullOrEmpty(txtTanim.Text)) { MessageBox.Show("Tanımı boş geçemezsiniz."); return; } else Tanim = txtTanim.Text;
                    if  (string.IsNullOrEmpty(txtKod.Text)) { MessageBox.Show("Kod tanımını boş geçemezsiniz."); return; } else Kod = txtKod.Text;



                    AnaGrup = string.IsNullOrEmpty(repLupAnaGrup.Text) ? "" : repLupAnaGrup.EditValue.ToString();
                    Tanim = txtTanim.Text;
                    Kod = txtKod.Text;

                    if (string.IsNullOrEmpty(GelisKodu))
                    {
                        var dtKodKontrol = new DataTable();
                        dtKodKontrol = DbConnSql.SelectSQL(string.Format(@"SELECT A=1 FROM  GEN_PARAMS Where GP_TYPE='{0}' and GP_VALUE_STR1='{1}' ", GP_TYPE, Kod));
                        if (dtKodKontrol.Rows.Count > 0)
                        {
                            MessageBox.Show("Bu kod daha önceden kullanılmış, farklı bir kod deneyin.");
                            return;
                        }

                        SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(@" 
                                    INSERT INTO  dbo.GEN_PARAMS 
                                           ( GP_VIEW 
                                           , GP_VALUE_STR1
                                           , GP_VALUE_STR2
                                           , GP_TYPE 
                                           , GP_CREUSER
                                           , GP_CREDATE
                                            )
                                     VALUES
                                           ( @GP_VIEW 
                                           , @GP_VALUE_STR1 
                                           , @GP_VALUE_STR2
                                           , @GP_TYPE 
                                           , @GP_CREUSER 
                                           , getdate()
                                            )
                                ", conn);
                        cmd.Parameters.Add("@GP_VIEW", SqlDbType.NVarChar).Value = Tanim;
                        cmd.Parameters.Add("@GP_VALUE_STR1", SqlDbType.NVarChar).Value = Kod;
                        cmd.Parameters.Add("@GP_VALUE_STR2", SqlDbType.NVarChar).Value = AnaGrup;
                        cmd.Parameters.Add("@GP_TYPE", SqlDbType.NVarChar).Value = GP_TYPE;
                        cmd.Parameters.Add("@GP_CREUSER", SqlDbType.Int).Value = BTI.frmLogin.KullaniciNo;


                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Veriler kayıt edildi");
                        ClearScreen();
                    }
                    else
                    {
                        SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(@" 
                            UPDATE dbo.GEN_PARAMS 
                                   SET
                                      GP_VIEW       = @GP_VIEW
                                    , GP_VALUE_STR1 = @GP_VALUE_STR1
                                    , GP_TYPE       = @GP_TYPE
                                    , GP_VALUE_STR2 = @GP_VALUE_STR2
                                    , GP_MODUSER    = @GP_MODUSER
                                    , GP_MODDATE    = GETDATE()
                            WHERE  GP_ID=@GP_ID 
                                 
                        ", conn);
                        cmd.Parameters.Add("@GP_VIEW", SqlDbType.NVarChar).Value = Tanim;
                        cmd.Parameters.Add("@GP_VALUE_STR1", SqlDbType.NVarChar).Value = Kod;
                        cmd.Parameters.Add("@GP_VALUE_STR2", SqlDbType.NVarChar).Value = AnaGrup;
                        cmd.Parameters.Add("@GP_TYPE", SqlDbType.NVarChar).Value = GP_TYPE;
                        cmd.Parameters.Add("@GP_MODUSER", SqlDbType.Int).Value = BTI.frmLogin.KullaniciNo;
                        cmd.Parameters.Add("@GP_ID", SqlDbType.Int).Value = GelisKodu;

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Veriler güncellendi");
                        Close();
                    
                    }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
      
        
        }

    
        private void EkleMusteriTanim_Load(object sender, EventArgs e)
        {
           
            GetList();
            GetValues();
        }
        private void repLupUlke_EditValueChanged(object sender, EventArgs e)
        {
          
           
   
        }
        private void repLupSehir_EditValueChanged(object sender, EventArgs e)
        {
           


           
        }
        private void btnKaydet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
    
                Save();
          
        }
        private void EkleMusteriTanim_FormClosed(object sender, FormClosedEventArgs e)
        {
            GelisKodu = "";
        }

    }
}