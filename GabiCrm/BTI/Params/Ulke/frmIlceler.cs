﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using Global;


namespace GABI_CRM
{
    public partial class frmIlceler : DevExpress.XtraEditors.XtraForm
    {
        public frmIlceler()
        {
            InitializeComponent();
        }

        const string GP_TYPE = "ILCELER";
        const string FTITLE  = "İlçeler";

        SqlDataAdapter SqlDap;
        SqlCommandBuilder SqlScb;
        DataTable dtGenel;

        private void GetList()
        {
            this.Text = FTITLE;
            Cursor.Current = Cursors.WaitCursor;

            BTI.PubObjects.RowHandle(grdV);

            string Sql = string.Format(@"SELECT  *,CREUSER=dbo.GetUserName(GP_CREUSER),CREDATE=GP_CREDATE, MODUSER =dbo.GetUserName(GP_MODUSER), MODDATE= GP_MODDATE FROM  GEN_PARAMS Where GP_TYPE='{0}' AND  GP_DURUM='A'", GP_TYPE);
                SqlDap = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenel = new DataTable();
                SqlScb = new SqlCommandBuilder(SqlDap);
                SqlScb.DataAdapter.Fill(dtGenel);
            grd.DataSource = dtGenel;


            var dsIsyeri = new DataTable();
            dsIsyeri = DbConnSql.SelectSQL("SELECT  GP_VIEW as Tanim, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER' ");
            repositoryItemLookUpEdit3.DataSource = null;
            repositoryItemLookUpEdit3.DataSource = dsIsyeri;

            Cursor.Current = Cursors.Default;
            BTI.PubObjects.SetBookmark(grdV);


        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


            try
            {

                /*  textEdit1.Focus();          
                  SqlScb.DataAdapter.Update(dtGenel);          
                  MessageBox.Show("Veriler güncellendi.");
                  grdMusteri.MainView = grdVMusteriListesi;
                 **/
                BTI.PubObjects.RowHandle(grdV);
                string Kod = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();
                frmIlcelerEkle.FTITLE = string.Format("{0}>>Düzeltme :{1}", FTITLE, Kod);
                frmIlcelerEkle.GP_TYPE = GP_TYPE;
                frmIlcelerEkle.GelisKodu = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();

                var frm = new frmIlcelerEkle();
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                GetList();
                BTI.PubObjects.SetBookmark(grdV);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }

            /*
            //  string MusK = grdVMusteriListesi.GetFocusedRowCellDisplayText(cMusteriKodu).ToString();
            textEdit1.Focus();

            DataView _dv = new DataView(dtGenel);
            for (int i = 0; i < _dv.Count; i++)
            {
                string Ulke = _dv[i]["GP_VALUE_STR1"].ToString();
                if (string.IsNullOrEmpty(Ulke))
                {
                    MessageBox.Show("Ülke tanımını boş geçemezsiniz.");
                    return;
                }
            }

            
            SqlScb.DataAdapter.Update(dtGenel);
            MessageBox.Show("Veriler güncellendi.");
             */ 
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grd.ShowPrintPreview();
        }

        private void grdV_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {

             

            }

            if (e.KeyCode == Keys.Insert)
            {
                     
              
            }
          
        }

        private void frmUlkeler_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void btnSil_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            string ID = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();
            DbConnSql.SelectSQL(string.Format(@" 
                    UPDATE GEN_PARAMS 
                    SET  
                        GP_DURUM='P',
                        GP_MODUSER = {1},
                        GP_MODDATE =GETDATE()
                    WHERE GP_ID = {0}
                ", ID, BTI.frmLogin.KullaniciNo));
            GetList();


            
        }

        private void btnEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*
            textEdit1.Focus();
            grdV.AddNewRow();
            grdV.SetRowCellValue(grdV.FocusedRowHandle, grdV.Columns["GP_TYPE"], GP_TYPE);
            grdV.FocusedRowHandle = grdV.GetVisibleRowHandle(grdV.FocusedRowHandle);
           // grd.RefreshDataSource();
             */


            frmIlcelerEkle.FTITLE = FTITLE;
            frmIlcelerEkle.GP_TYPE = GP_TYPE;
            frmIlcelerEkle.GelisKodu = "";
            var frm = new frmIlcelerEkle();
            frm.WindowState = FormWindowState.Normal;
            frm.ShowDialog();
            GetList();

        }


    }
}