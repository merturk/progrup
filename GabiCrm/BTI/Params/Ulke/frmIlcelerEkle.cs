﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;
using System.Data.SqlClient;

namespace GABI_CRM
{
    public partial class frmIlcelerEkle : DevExpress.XtraEditors.XtraForm
    {
        public frmIlcelerEkle()
        {
            InitializeComponent();
        }

     
        public static string FTITLE = "";
        public static string GelisKodu = "";
        public static string GP_TYPE = ""; 
   

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void GetList()
        {
            this.Text = FTITLE;
            Cursor.Current = Cursors.WaitCursor;
            string sSehirler = string.Format("SELECT  GP_VIEW as Tanim, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER'  ");
            DABindings.GetXtraLookupBindNm(repLupSehir, sSehirler);
            Cursor.Current = Cursors.Default;
        
        
        
        }
        private void GetValues()
        {
            if ( string.IsNullOrEmpty( GelisKodu)) return;

            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format(@"
               SELECT  Sehir, GP_VIEW as Ilce FROM  GEN_PARAMS as g
                LEFT JOIN 
                (SELECT  GP_VIEW as Sehir, GP_VALUE_STR3 as Kod FROM  GEN_PARAMS Where GP_TYPE ='SEHIRLER')AS SE ON SE.Kod = g.GP_VALUE_STR1
                 Where GP_TYPE='{1}' AND GP_ID = {0} AND GP_DURUM='A'
            ", GelisKodu, GP_TYPE));
            repLupSehir.Text = dt.Rows[0]["Sehir"].ToString();
            txtIlce.Text = dt.Rows[0]["Ilce"].ToString();
          
        
        }
        private void ClearScreen()
        { 
           
            repLupSehir.Text ="";
            txtIlce.Text ="";
          
        
        }
        private void Save()
        {
            try
            {

                    string  Sehir, Ilce;
     
                    if  (string.IsNullOrEmpty(repLupSehir.Text)) { MessageBox.Show("Şehir tanımını boş geçemezsiniz."); return; } else Sehir = repLupSehir.EditValue.ToString();
                    if  (string.IsNullOrEmpty(txtIlce.Text)) { MessageBox.Show("İlçe tanımını boş geçemezsiniz."); return; } else Ilce = txtIlce.Text;


                    Sehir = string.IsNullOrEmpty(repLupSehir.Text) ? "" : repLupSehir.EditValue.ToString();
                    Ilce = txtIlce.Text;

                    if (string.IsNullOrEmpty(GelisKodu))
                    {
                        SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(@" 
                                    INSERT INTO  dbo.GEN_PARAMS 
                                           ( GP_VIEW 
                                           , GP_VALUE_STR1
                                           , GP_TYPE 
                                           , GP_CREUSER
                                           , GP_CREDATE
                                            )
                                     VALUES
                                           ( @GP_VIEW 
                                           , @GP_VALUE_STR1 
                                           , @GP_TYPE 
                                           , @GP_CREUSER 
                                           , getdate()
                                            )
                                ", conn);
                        cmd.Parameters.Add("@GP_VIEW", SqlDbType.NVarChar).Value = Ilce;
                        cmd.Parameters.Add("@GP_VALUE_STR1", SqlDbType.NVarChar).Value = Sehir;
                        cmd.Parameters.Add("@GP_TYPE", SqlDbType.NVarChar).Value = GP_TYPE;
                        cmd.Parameters.Add("@GP_CREUSER", SqlDbType.Int).Value = BTI.frmLogin.KullaniciNo;


                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Veriler kayıt edildi");
                        ClearScreen();
                    }
                    else
                    {
                        SqlConnection conn = new SqlConnection(DbConnSql.ConnStringSqlDb());
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(@" 
                            UPDATE dbo.GEN_PARAMS 
                                   SET
                                      GP_VIEW       = @GP_VIEW
                                    , GP_VALUE_STR1 = @GP_VALUE_STR1
                                    , GP_TYPE       = @GP_TYPE
                                    , GP_MODUSER    = @GP_MODUSER
                                    , GP_MODDATE    = GETDATE()
                            WHERE  GP_ID=@GP_ID 
                                 
                        ", conn);
                        cmd.Parameters.Add("@GP_VIEW", SqlDbType.NVarChar).Value = Ilce;
                        cmd.Parameters.Add("@GP_VALUE_STR1", SqlDbType.NVarChar).Value = Sehir;
                        cmd.Parameters.Add("@GP_TYPE", SqlDbType.NVarChar).Value = GP_TYPE;
                        cmd.Parameters.Add("@GP_MODUSER", SqlDbType.Int).Value = BTI.frmLogin.KullaniciNo;
                        cmd.Parameters.Add("@GP_ID", SqlDbType.Int).Value = GelisKodu;

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Veriler güncellendi");
                        Close();
                    
                    }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
      
        
        }

    
        private void EkleMusteriTanim_Load(object sender, EventArgs e)
        {
           
            GetList();
            GetValues();
        }
        private void repLupUlke_EditValueChanged(object sender, EventArgs e)
        {
          
           
   
        }
        private void repLupSehir_EditValueChanged(object sender, EventArgs e)
        {
           


           
        }
        private void btnKaydet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
    
                Save();
          
        }
        private void EkleMusteriTanim_FormClosed(object sender, FormClosedEventArgs e)
        {
            GelisKodu = "";
        }

    }
}