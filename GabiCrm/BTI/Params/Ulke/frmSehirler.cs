﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using Global;


namespace GABI_CRM
{
    public partial class frmSehirler : DevExpress.XtraEditors.XtraForm
    {
        public frmSehirler()
        {
            InitializeComponent();
        }

        const string GP_TYPE = "SEHIRLER";
        const string FTITLE  = "Şehirler";

        SqlDataAdapter SqlDap;
        SqlCommandBuilder SqlScb;
        DataTable dtGenel;

        private void GetList()
        {
            this.Text = FTITLE;
            Cursor.Current = Cursors.WaitCursor;

            string Sql = string.Format(@"SELECT  *,CREUSER=dbo.GetUserName(GP_CREUSER),CREDATE=GP_CREDATE, MODUSER =dbo.GetUserName(GP_MODUSER), MODDATE= GP_MODDATE FROM  GEN_PARAMS Where GP_TYPE='{0}' AND  GP_DURUM='A'", GP_TYPE);
                SqlDap = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenel = new DataTable();
                SqlScb = new SqlCommandBuilder(SqlDap);
                SqlScb.DataAdapter.Fill(dtGenel);
            grd.DataSource = dtGenel;


            var dsI = new DataTable();
            dsI = DbConnSql.SelectSQL("SELECT  GP_VIEW as Ulke, GP_VALUE_STR1 as UlkeKodu FROM  GEN_PARAMS Where GP_TYPE ='ULKELER' ");
            repositoryItemLookUpEdit2.DataSource = null;
            repositoryItemLookUpEdit2.DataSource = dsI;

            Cursor.Current = Cursors.Default;



        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {

                BTI.PubObjects.RowHandle(grdV);
                string Kod = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();
                frmSehirlerEkle.FTITLE = string.Format("{0}>>Düzeltme :{1}", FTITLE, Kod);
                frmSehirlerEkle.GP_TYPE = GP_TYPE;
                frmSehirlerEkle.GelisKodu = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();

                var frm = new frmSehirlerEkle();
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                GetList();
                BTI.PubObjects.SetBookmark(grdV);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }

        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grd.ShowPrintPreview();
        }

        private void grdV_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {

               

            }

            if (e.KeyCode == Keys.Insert)
            {
                     
             

            }
          
        }

        private void frmUlkeler_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void btnEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


            frmSehirlerEkle.FTITLE = FTITLE;
            frmSehirlerEkle.GP_TYPE = GP_TYPE;
            frmSehirlerEkle.GelisKodu = "";
            var frm = new frmSehirlerEkle();
            frm.WindowState = FormWindowState.Normal;
            frm.ShowDialog();
            GetList();
            /*
            textEdit1.Focus();
            grdV.AddNewRow();
            grdV.SetRowCellValue(grdV.FocusedRowHandle, grdV.Columns["GP_TYPE"], GP_TYPE);
            grdV.FocusedRowHandle = grdV.GetVisibleRowHandle(grdV.FocusedRowHandle);
             */ 
          //  grd.RefreshDataSource();
        }

        private void btnSil_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ID = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();
            DbConnSql.SelectSQL(string.Format(@" 
                    UPDATE GEN_PARAMS 
                    SET  
                        GP_DURUM='P',
                        GP_MODUSER = {1},
                        GP_MODDATE =GETDATE()
                    WHERE GP_ID = {0}
                ", ID, BTI.frmLogin.KullaniciNo));
            GetList();

        }


    }
}