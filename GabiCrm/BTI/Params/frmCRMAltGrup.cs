﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using Global;
using System.Collections;


namespace GABI_CRM
{
    public partial class frmCRMAltGrup : DevExpress.XtraEditors.XtraForm
    {
        public frmCRMAltGrup()
        {
            InitializeComponent();
        }

        public static string GP_TYPE_ANA = ""; //Ang
        public static string GP_TYPE_ALT = ""; //AltG
        public static string FTITLE =      ""; //Alt Gruplar 

       // const string GP_TYPE = "AltG";
      //const string FTITLE  = "Alt Gruplar";

        SqlDataAdapter SqlDap;
        SqlCommandBuilder SqlScb;
        DataTable dtGenel;

        private void GetList()
        {
            this.Text = FTITLE;
            Cursor.Current = Cursors.WaitCursor;

            
            var dsL = new DataTable();
            dsL = DbConnSql.SelectSQL(string.Format(@"SELECT   GP_VIEW As Tanim, GP_VALUE_STR1 as Kod FROM  GEN_PARAMS Where GP_TYPE='{0}'", GP_TYPE_ANA));
            repAnaGrup.DataSource = null;
            repAnaGrup.DataSource = dsL;

            string Sql = string.Format(@"
                        SELECT  
                                *,CREUSER=dbo.GetUserName(GP_CREUSER),CREDATE=GP_CREDATE, MODUSER =dbo.GetUserName(GP_MODUSER), MODDATE= GP_MODDATE
                        FROM  GEN_PARAMS Where GP_TYPE='{0}' and   GP_DURUM='A'
                        Order by GP_VALUE_STR2

                        ", GP_TYPE_ALT);
                SqlDap = new SqlDataAdapter(Sql, DbConnSql.ConnStringSqlDb());
                dtGenel = new DataTable();
                SqlScb = new SqlCommandBuilder(SqlDap);
                SqlScb.DataAdapter.Fill(dtGenel);
            grd.DataSource = dtGenel;


            Cursor.Current = Cursors.Default;

          
        }


    
       
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {

                BTI.PubObjects.RowHandle(grdV);
                string Kod = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();
                frmCRMAltGrupEkle.FTITLE = string.Format("{0}>>Düzeltme :{1}", FTITLE, Kod);
                frmCRMAltGrupEkle.GP_TYPE = GP_TYPE_ALT;
                frmCRMAltGrupEkle.GP_TYPE_ANA = GP_TYPE_ANA;
                frmCRMAltGrupEkle.GelisKodu = Kod;

                var frm = new frmCRMAltGrupEkle();
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                GetList();
                BTI.PubObjects.SetBookmark(grdV);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }

        }

     

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            grd.ShowPrintPreview();
        }

        private void grdV_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {

         

            }

            if (e.KeyCode == Keys.Insert)
            {
                     
          

            }
          
        }

        private void frmUlkeler_Load(object sender, EventArgs e)
        {
            GetList();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GetList();
        }

        private void btnEkle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


            frmCRMAltGrupEkle.FTITLE = FTITLE;
            frmCRMAltGrupEkle.GP_TYPE = GP_TYPE_ALT;
            frmCRMAltGrupEkle.GP_TYPE_ANA = GP_TYPE_ANA;
            frmCRMAltGrupEkle.GelisKodu = "";
            var frm = new frmCRMAltGrupEkle();
            frm.WindowState = FormWindowState.Normal;
            frm.ShowDialog();
            GetList(); 
     
            //grd.RefreshDataSource();
        }

        private void btnSil_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            string ID = grdV.GetFocusedRowCellDisplayText(cGP_ID).ToString();
            DbConnSql.SelectSQL(string.Format(@" 
                    UPDATE GEN_PARAMS 
                    SET  
                        GP_DURUM='P',
                        GP_MODUSER = {1},
                        GP_MODDATE =GETDATE()
                    WHERE GP_ID = {0}
                ", ID,BTI.frmLogin.KullaniciNo));
            GetList();

            //grdV.DeleteRow(grdV.FocusedRowHandle);
          //  grd.RefreshDataSource();
        }


      
        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
         
        }

        private void grd_Load(object sender, EventArgs e)
        {
       
        }


    }
}