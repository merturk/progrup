﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using GABI_CRM;




namespace BTI
{
    public partial class PubShowForm
    {
        #region Yetkilendirme Diyalog Ekranları
        public static void ShowNewUser()
        {
            var NewUser = new frmNewUser();
            NewUser.ShowDialog();
        }

        public static void ShowChangePassword()
        {
            var ChangePassword = new frmChangePassword();
            ChangePassword.ShowDialog();
        }

        public static void ShowDeleteUser()
        {
            var DeleteUser = new frmDeleteUser();
            DeleteUser.ShowDialog();
        }



        #endregion

        #region PERMISION
        public static frmPermision ChildPermision;
        public static void CloseChildPermision(object sender, EventArgs e)
        { ChildPermision = null; }
        public static void ShowPermision()
        {
            if (ChildPermision != null)
                //fChild.Text = Program.ProgramName + " ...";
                ChildPermision.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildPermision = new frmPermision(); //Yeni form oluştur
                ChildPermision.MdiParent = frmMain.ActiveForm;
                ChildPermision.Closed += CloseChildPermision;
                //fChild.Text = Program.ProgramName;
                ChildPermision.Show();
            }
            ChildPermision.Activate();
        }

        #endregion

        #region  Ulkeler
        public static frmUlkeler ChildUlkeler;
        public static void CloseChildUlkeler(object sender, EventArgs e) { ChildUlkeler = null; }
        public static void ShowUlkeler()
        {
            if (ChildUlkeler != null)
                ChildUlkeler.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildUlkeler = new frmUlkeler(); //Yeni form oluştur
                ChildUlkeler.MdiParent = frmMain.ActiveForm;
                ChildUlkeler.Closed += CloseChildUlkeler;
                ChildUlkeler.Show();
            }
            ChildUlkeler.Activate();
        }

        #endregion

        #region  Şehirler
        public static frmSehirler ChildSehirler;
        public static void CloseChildSehirler(object sender, EventArgs e) { ChildSehirler = null; }
        public static void ShowSehirler()
        {
            if (ChildSehirler != null)
                ChildSehirler.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildSehirler = new frmSehirler(); //Yeni form oluştur
                ChildSehirler.MdiParent = frmMain.ActiveForm;
                ChildSehirler.Closed += CloseChildSehirler;
                ChildSehirler.Show();
            }
            ChildSehirler.Activate();
        }

        #endregion


        #region  Sektörler
        public static frmSektorler ChildSektorler;
        public static void CloseChildSektorler(object sender, EventArgs e) { ChildSektorler = null; }
        public static void ShowSektorler()
        {
            if (ChildSektorler != null)
                ChildSektorler.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildSektorler = new frmSektorler(); //Yeni form oluştur
                ChildSektorler.MdiParent = frmMain.ActiveForm;
                ChildSektorler.Closed += CloseChildSektorler;
                ChildSektorler.Show();
            }
            ChildSektorler.Activate();
        }

        #endregion




        public static Firmalar ChildFirmalar;
        public static void CloseChildFirmalar(object sender, EventArgs e)
        { ChildFirmalar = null; }

        public static void ShowFirmalar()
        {


            if (ChildFirmalar != null)
            {
                //ChildSM.MdiParent = frmMain.ActiveForm;
                ChildFirmalar.Close();
                ChildFirmalar = new Firmalar();
                ChildFirmalar.MdiParent = frmMain.ActiveForm;
                ChildFirmalar.Closed += CloseChildFirmalar;
                ChildFirmalar.Show();
            }
            else
            {
                ChildFirmalar = new Firmalar();
                ChildFirmalar.MdiParent = frmMain.ActiveForm;
                ChildFirmalar.Closed += CloseChildFirmalar;
                ChildFirmalar.Show();
            }
            ChildFirmalar.Activate();


        }






        public static frmCalendar ChildCalendar;
        public static void CloseChildCalendar(object sender, EventArgs e)
        { ChildCalendar = null; }

        public static void ShowCalendar()
        {


            if (ChildCalendar != null)
            {
                //ChildSM.MdiParent = frmMain.ActiveForm;
                ChildCalendar.Close();
                ChildCalendar = new frmCalendar();
                ChildCalendar.MdiParent = frmMain.ActiveForm;
                ChildCalendar.Closed += CloseChildCalendar;
                ChildCalendar.Show();
            }
            else
            {
                ChildCalendar = new frmCalendar();
                ChildCalendar.MdiParent = frmMain.ActiveForm;
                ChildCalendar.Closed += CloseChildCalendar;
                ChildCalendar.Show();
            }
            ChildCalendar.Activate();


        }




        public static frmGantSchedule ChildfrmGantSchedule;
        public static void CloseChildfrmGantSchedule(object sender, EventArgs e)
        { ChildfrmGantSchedule = null; }

        public static void ShowProjeYonetimi()
        {


            if (ChildfrmGantSchedule != null)
            {
                //ChildSM.MdiParent = frmMain.ActiveForm;
                ChildfrmGantSchedule.Close();
                ChildfrmGantSchedule = new frmGantSchedule();
                ChildfrmGantSchedule.MdiParent = frmMain.ActiveForm;
                ChildfrmGantSchedule.Closed += CloseChildfrmGantSchedule;
                ChildfrmGantSchedule.Show();
            }
            else
            {
                ChildfrmGantSchedule = new frmGantSchedule();
                ChildfrmGantSchedule.MdiParent = frmMain.ActiveForm;
                ChildfrmGantSchedule.Closed += CloseChildfrmGantSchedule;
                ChildfrmGantSchedule.Show();
            }
            ChildfrmGantSchedule.Activate();


        }

        #region  Müşteri Listesi
        public static frmMusteriListesi ChildfrmMusteriListesi;
        public static void CloseChildfrmMusteriListesi(object sender, EventArgs e) { ChildfrmMusteriListesi = null; }
        public static void ShowMusteriListesi()
        {
            if (ChildfrmMusteriListesi != null)
                ChildfrmMusteriListesi.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmMusteriListesi = new frmMusteriListesi(); //Yeni form oluştur
                ChildfrmMusteriListesi.MdiParent = frmMain.ActiveForm;
                ChildfrmMusteriListesi.Closed += CloseChildfrmMusteriListesi;
                ChildfrmMusteriListesi.Show();
            }
            ChildfrmMusteriListesi.Activate();
        }

        #endregion

        #region  Email Havuzu
        public static EmailPool ChildEmailPool;
        public static void CloseChildEmailPool(object sender, EventArgs e) { ChildEmailPool = null; }
        public static void ShowEmailPool()
        {
            if (ChildEmailPool != null)
                ChildEmailPool.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildEmailPool = new EmailPool(); //Yeni form oluştur
                ChildEmailPool.MdiParent = frmMain.ActiveForm;
                ChildEmailPool.Closed += CloseChildEmailPool;
                ChildEmailPool.Show();
            }
            ChildEmailPool.Activate();
        }

        #endregion




        #region  İlçeler
        public static frmIlceler ChildIlceler;
        public static void CloseChildIlceler(object sender, EventArgs e) { ChildIlceler = null; }
        public static void ShowIlceler()
        {
            if (ChildIlceler != null)
                ChildIlceler.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildIlceler = new frmIlceler(); //Yeni form oluştur
                ChildIlceler.MdiParent = frmMain.ActiveForm;
                ChildIlceler.Closed += CloseChildIlceler;
                ChildIlceler.Show();
            }
            ChildIlceler.Activate();
        }

        #endregion
        /*
        #region  Teklif Tipleri
        public static frmTekTipleri ChildTekTipleri;
        public static void CloseChildTekTipleri(object sender, EventArgs e) { ChildTekTipleri = null; }
        public static void ShowTekTipleri()
        {
            if (ChildTekTipleri != null)
                ChildTekTipleri.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildTekTipleri = new frmTekTipleri(); //Yeni form oluştur
                ChildTekTipleri.MdiParent = frmMain.ActiveForm;
                ChildTekTipleri.Closed += CloseChildTekTipleri;
                ChildTekTipleri.Show();
            }
            ChildTekTipleri.Activate();
        }

        #endregion
        */




        public static frmGeneralParams ChildfrmTekTipleri;
        public static void CloseChildfrmTekTipleri(object sender, EventArgs e)
        { ChildfrmTekTipleri = null; }

        public static void ShowGenelParams(string GpType, string FormBaslik)
        {
            frmGeneralParams.GP_TYPE = GpType;  
            frmGeneralParams.FTITLE = FormBaslik;
            if (ChildfrmTekTipleri != null)
            {
                //ChildSM.MdiParent = frmMain.ActiveForm;
                ChildfrmTekTipleri.Close();
                ChildfrmTekTipleri = new frmGeneralParams();
                ChildfrmTekTipleri.MdiParent = frmMain.ActiveForm;
                ChildfrmTekTipleri.Closed += CloseChildfrmTekTipleri;
                ChildfrmTekTipleri.Show();
            }
            else
            {
                ChildfrmTekTipleri = new frmGeneralParams();
                ChildfrmTekTipleri.MdiParent = frmMain.ActiveForm;
                ChildfrmTekTipleri.Closed += CloseChildfrmTekTipleri;
                ChildfrmTekTipleri.Show();
            }
            ChildfrmTekTipleri.Activate();


        }


        #region  Teklif Hazırlayanlar Listesi
        public static frmTekHazirlayanlar ChildTekHazirlayanlar;
        public static void CloseChildTekHazirlayanlar(object sender, EventArgs e) { ChildTekHazirlayanlar = null; }
        public static void ShowTekHazirlayanlar()
        {
            if (ChildTekHazirlayanlar != null)
                ChildTekHazirlayanlar.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildTekHazirlayanlar = new frmTekHazirlayanlar(); //Yeni form oluştur
                ChildTekHazirlayanlar.MdiParent = frmMain.ActiveForm;
                ChildTekHazirlayanlar.Closed += CloseChildTekHazirlayanlar;
                ChildTekHazirlayanlar.Show();
            }
            ChildTekHazirlayanlar.Activate();
        }

        #endregion


        #region  Teklif Sonuçları Listesi
        public static frmTekSonucu ChildTekSonucu;
        public static void CloseChildTekSonucu(object sender, EventArgs e) { ChildTekSonucu = null; }
        public static void ShowTekSonucu()
        {
            if (ChildTekSonucu != null)
                ChildTekSonucu.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildTekSonucu = new frmTekSonucu(); //Yeni form oluştur
                ChildTekSonucu.MdiParent = frmMain.ActiveForm;
                ChildTekSonucu.Closed += CloseChildTekSonucu;
                ChildTekSonucu.Show();
            }
            ChildTekSonucu.Activate();
        }

        #endregion


        #region  Teklif Kazanma Kaybetme Nedenleri
        public static frmTekKazanmaKaybetmeNedenleri ChildTekKazanmaKaybetmeNedenleri;
        public static void CloseChildTekKazanmaKaybetmeNedenleri(object sender, EventArgs e) { ChildTekKazanmaKaybetmeNedenleri = null; }
        public static void ShowTekKazanmaKaybetmeNedenleri()
        {
            if (ChildTekKazanmaKaybetmeNedenleri != null)
                ChildTekKazanmaKaybetmeNedenleri.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildTekKazanmaKaybetmeNedenleri = new frmTekKazanmaKaybetmeNedenleri(); //Yeni form oluştur
                ChildTekKazanmaKaybetmeNedenleri.MdiParent = frmMain.ActiveForm;
                ChildTekKazanmaKaybetmeNedenleri.Closed += CloseChildTekKazanmaKaybetmeNedenleri;
                ChildTekKazanmaKaybetmeNedenleri.Show();
            }
            ChildTekKazanmaKaybetmeNedenleri.Activate();
        }

        #endregion



        #region  GiderTipleri
        public static frmGiderTipleri ChildfrmGiderTipleri;
        public static void CloseChildfrmGiderTipleri(object sender, EventArgs e) { ChildfrmGiderTipleri = null; }
        public static void ShowGiderTipleri()
        {
            if (ChildfrmGiderTipleri != null)
                ChildfrmGiderTipleri.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmGiderTipleri = new frmGiderTipleri(); //Yeni form oluştur
                ChildfrmGiderTipleri.MdiParent = frmMain.ActiveForm;
                ChildfrmGiderTipleri.Closed += CloseChildfrmGiderTipleri;
                ChildfrmGiderTipleri.Show();
            }
            ChildfrmGiderTipleri.Activate();
        }

        #endregion


        #region  ÖdemeTipleri
        public static frmOdemeTipleri ChildfrmOdemeTipleri;
        public static void CloseChildfrmOdemeTipleri(object sender, EventArgs e) { ChildfrmOdemeTipleri = null; }
        public static void ShowOdemeTipleri()
        {
            if (ChildfrmOdemeTipleri != null)
                ChildfrmOdemeTipleri.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmOdemeTipleri = new frmOdemeTipleri(); //Yeni form oluştur
                ChildfrmOdemeTipleri.MdiParent = frmMain.ActiveForm;
                ChildfrmOdemeTipleri.Closed += CloseChildfrmOdemeTipleri;
                ChildfrmOdemeTipleri.Show();
            }
            ChildfrmOdemeTipleri.Activate();
        }

        #endregion

        #region  Proje Tanımları
        public static frmProjeTanimlari ChildfrmProjeTanimlari;
        public static void CloseChildfrmProjeTanimlari(object sender, EventArgs e) { ChildfrmProjeTanimlari = null; }
        public static void ShowProjeTanimlari()
        {
            if (ChildfrmProjeTanimlari != null)
                ChildfrmProjeTanimlari.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmProjeTanimlari = new frmProjeTanimlari(); //Yeni form oluştur
                ChildfrmProjeTanimlari.MdiParent = frmMain.ActiveForm;
                ChildfrmProjeTanimlari.Closed += CloseChildfrmProjeTanimlari;
                ChildfrmProjeTanimlari.Show();
            }
            ChildfrmProjeTanimlari.Activate();
        }

        #endregion

     

        #region  Yetkili Statüleri
        public static frmYetkiliStatu ChildfrmYetkiliStatu;
        public static void CloseChildfrmYetkiliStatu(object sender, EventArgs e) { ChildfrmYetkiliStatu = null; }
        public static void ShowYetkiliStatu()
        {
            if (ChildfrmYetkiliStatu != null)
                ChildfrmYetkiliStatu.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmYetkiliStatu = new frmYetkiliStatu(); //Yeni form oluştur
                ChildfrmYetkiliStatu.MdiParent = frmMain.ActiveForm;
                ChildfrmYetkiliStatu.Closed += CloseChildfrmYetkiliStatu;
                ChildfrmYetkiliStatu.Show();
            }
            ChildfrmYetkiliStatu.Activate();
        }
        #endregion


        #region  Ana grup

        public static frmCrmAnaGrup ChildfrmAnaGrup;
        public static void CloseChildfrmAnaGrup(object sender, EventArgs e)
        { ChildfrmAnaGrup = null; }

        public static void ShowAnaGrup()
        {


            if (ChildfrmAnaGrup != null)
            {
                //ChildSM.MdiParent = frmMain.ActiveForm;
                ChildfrmAnaGrup.Close();
                ChildfrmAnaGrup = new frmCrmAnaGrup();
                ChildfrmAnaGrup.MdiParent = frmMain.ActiveForm;
                ChildfrmAnaGrup.Closed += CloseChildfrmAnaGrup;
                ChildfrmAnaGrup.Show();
            }
            else
            {
                ChildfrmAnaGrup = new frmCrmAnaGrup();
                ChildfrmAnaGrup.MdiParent = frmMain.ActiveForm;
                ChildfrmAnaGrup.Closed += CloseChildfrmAnaGrup;
                ChildfrmAnaGrup.Show();
            }
            ChildfrmAnaGrup.Activate();


        }
        #endregion





        #region  Alt grup


        public static frmCRMAltGrup ChildfrmAltGrup;
        public static void CloseChildfrmAltGrup(object sender, EventArgs e)
        { ChildfrmAltGrup = null; }

        public static void ShowAltGrup()
        {


            if (ChildfrmAltGrup != null)
            {
                //ChildSM.MdiParent = frmMain.ActiveForm;
                ChildfrmAltGrup.Close();
                ChildfrmAltGrup = new frmCRMAltGrup();
                ChildfrmAltGrup.MdiParent = frmMain.ActiveForm;
                ChildfrmAltGrup.Closed += CloseChildfrmAltGrup;
                ChildfrmAltGrup.Show();
            }
            else
            {
                ChildfrmAltGrup = new frmCRMAltGrup();
                ChildfrmAltGrup.MdiParent = frmMain.ActiveForm;
                ChildfrmAltGrup.Closed += CloseChildfrmAltGrup;
                ChildfrmAltGrup.Show();
            }
            ChildfrmAltGrup.Activate();

        }
        #endregion

        #region  Ziyaretleri
        public static frmZiyaretNedenleri ChildfrmZiyaretNedenleri;
        public static void CloseChildfrmZiyaretNedenleri(object sender, EventArgs e) { ChildfrmZiyaretNedenleri = null; }
        public static void ShowZiyaretNedenleri()
        {
            if (ChildfrmZiyaretNedenleri != null)
                ChildfrmZiyaretNedenleri.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmZiyaretNedenleri = new frmZiyaretNedenleri(); //Yeni form oluştur
                ChildfrmZiyaretNedenleri.MdiParent = frmMain.ActiveForm;
                ChildfrmZiyaretNedenleri.Closed += CloseChildfrmZiyaretNedenleri;
                ChildfrmZiyaretNedenleri.Show();
            }
            ChildfrmZiyaretNedenleri.Activate();
        }
        #endregion

          #region  Promosyon Tanımları
        public static frmPromosyonTanim ChildfrmPromosyonTanim;
        public static void CloseChildfrmPromosyonTanim(object sender, EventArgs e) { ChildfrmPromosyonTanim = null; }
        public static void ShowPromosyonTanim()
        {
            if (ChildfrmPromosyonTanim != null)
                ChildfrmPromosyonTanim.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmPromosyonTanim = new frmPromosyonTanim(); //Yeni form oluştur
                ChildfrmPromosyonTanim.MdiParent = frmMain.ActiveForm;
                ChildfrmPromosyonTanim.Closed += CloseChildfrmPromosyonTanim;
                ChildfrmPromosyonTanim.Show();
            }
            ChildfrmPromosyonTanim.Activate();
        }
        #endregion



        #region  Sözleşme içerik tipleri
        public static frmSozlesmeIcerikTipleri ChildfrmSozlesmeIcerikTipleri;
        public static void CloseChildfrmSozlesmeIcerikTipleri(object sender, EventArgs e) { ChildfrmSozlesmeIcerikTipleri = null; }
        public static void ShowSozlesmeIcerikTipleri()
        {
            if (ChildfrmSozlesmeIcerikTipleri != null)
                ChildfrmSozlesmeIcerikTipleri.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmSozlesmeIcerikTipleri = new frmSozlesmeIcerikTipleri(); //Yeni form oluştur
                ChildfrmSozlesmeIcerikTipleri.MdiParent = frmMain.ActiveForm;
                ChildfrmSozlesmeIcerikTipleri.Closed += CloseChildfrmSozlesmeIcerikTipleri;
                ChildfrmSozlesmeIcerikTipleri.Show();
            }
            ChildfrmSozlesmeIcerikTipleri.Activate();
        }
        #endregion


        #region  Proje Detay Tanımları
        public static frmProjeDetayBasliklari ChildfrmProjeDetayBasliklari;
        public static void CloseChildfrmProjeDetayBasliklari(object sender, EventArgs e) { ChildfrmProjeDetayBasliklari = null; }
        public static void ShowProjeDetayBasliklari()
        {
            if (ChildfrmProjeDetayBasliklari != null)
                ChildfrmProjeDetayBasliklari.MdiParent = frmMain.ActiveForm;
            else
            //form değişkeni bir nesneyi göstermiyorsa
            {
                ChildfrmProjeDetayBasliklari = new frmProjeDetayBasliklari(); //Yeni form oluştur
                ChildfrmProjeDetayBasliklari.MdiParent = frmMain.ActiveForm;
                ChildfrmProjeDetayBasliklari.Closed += CloseChildfrmProjeDetayBasliklari;
                ChildfrmProjeDetayBasliklari.Show();
            }
            ChildfrmProjeDetayBasliklari.Activate();
        }
        #endregion
          
    }




}
