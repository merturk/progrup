﻿using System;
using System.Windows.Forms;
using System.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System.Data.SqlClient;
using DevExpress.XtraPrinting;

using Global;

using System.Text.RegularExpressions;
using System.Collections;

using System.Net;
using System.Net.Mail;

using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using DevExpress.XtraRichEdit;
using DevExpress.Utils;
using DevExpress.Office.Services;
using System.Net.Mime;
using System.IO;
using DevExpress.XtraRichEdit.Export;
using System.Collections.Generic;
using System.Net;
using DevExpress.Office.Utils;



namespace BTI
{

    public partial class PubObjects
    {
        private const string STR_E = "E";
        public static string ComputerName = System.Windows.Forms.SystemInformation.ComputerName;
        public static string UserName = System.Windows.Forms.SystemInformation.UserName;


        public static int RowHandleNo, RowHandleNo2;
        public static string GetSelectedRows(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            string ret = "";        
            if (view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.RowSelect)
            {
                foreach (int i in view.GetSelectedRows())
                {
                    DataRow row = view.GetDataRow(i);
                    //if (ret != "") ret += "\r\n";
                    ret += string.Format("{0} ", i);
                }
            }
            return ret;
        }


        public class RichEditMailMessageExporter : IUriProvider
        {
            readonly RichEditControl control;
            readonly MailMessage message;
            List<AttachementInfo> attachments;
            int imageId;

            public RichEditMailMessageExporter(RichEditControl control, MailMessage message)
            {
                Guard.ArgumentNotNull(control, "control");
                Guard.ArgumentNotNull(message, "message");

                this.control = control;
                this.message = message;

            }

            public virtual void Export()
            {
                this.attachments = new List<AttachementInfo>();

                AlternateView htmlView = CreateHtmlView();
                message.AlternateViews.Add(htmlView);
                message.IsBodyHtml = true;
            }

            protected internal virtual AlternateView CreateHtmlView()
            {
                control.BeforeExport += OnBeforeExport;
                string htmlBody = control.Document.GetHtmlText(control.Document.Range, this);
                AlternateView view = AlternateView.CreateAlternateViewFromString(htmlBody, Encoding.UTF8, MediaTypeNames.Text.Html);
                control.BeforeExport -= OnBeforeExport;

                int count = attachments.Count;
                for (int i = 0; i < count; i++)
                {
                    AttachementInfo info = attachments[i];
                    LinkedResource resource = new LinkedResource(info.Stream, info.MimeType);
                    resource.ContentId = info.ContentId;
                    view.LinkedResources.Add(resource);
                }
                return view;
            }

            void OnBeforeExport(object sender, BeforeExportEventArgs e)
            {
                HtmlDocumentExporterOptions options = e.Options as HtmlDocumentExporterOptions;
                if (options != null)
                {
                    options.Encoding = Encoding.UTF8;
                }
            }


            #region IUriProvider Members

            public string CreateCssUri(string rootUri, string styleText, string relativeUri)
            {
                return String.Empty;
            }
            public string CreateImageUri(string rootUri, OfficeImage image, string relativeUri)
            {
                string imageName = String.Format("image{0}", imageId);
                imageId++;

                OfficeImageFormat imageFormat = GetActualImageFormat(image.RawFormat);
                Stream stream = new MemoryStream(image.GetImageBytes(imageFormat));
                string mediaContentType = OfficeImage.GetContentType(imageFormat);
                AttachementInfo info = new AttachementInfo(stream, mediaContentType, imageName);
                attachments.Add(info);

                return "cid:" + imageName;
            }

            OfficeImageFormat GetActualImageFormat(OfficeImageFormat _officeImageFormat)
            {
                if (_officeImageFormat == OfficeImageFormat.Exif ||
                    _officeImageFormat == OfficeImageFormat.MemoryBmp)
                    return OfficeImageFormat.Png;
                else
                    return _officeImageFormat;
            }
            #endregion
        }



        public class AttachementInfo
        {
            Stream stream;
            string mimeType;
            string contentId;

            public AttachementInfo(Stream stream, string mimeType, string contentId)
            {
                this.stream = stream;
                this.mimeType = mimeType;
                this.contentId = contentId;
            }

            public Stream Stream { get { return stream; } }
            public string MimeType { get { return mimeType; } }
            public string ContentId { get { return contentId; } }
        }

        public static void SendEmail(string Konu,DevExpress.XtraRichEdit.RichEditControl  Bodys,   string CariTanimi, string FileAttachPath, string MsgTo1, string MsgTo2, string MsgTo3, string Tarih, string MsgToBcc1)
        {

            
            /*
            //Email gönderimi yapılsınmı
            var dtActiveMi = new DataTable();
            dtActiveMi = DbConnSql.SelectSQL("Select * from GEN_PARAMS Where GP_TYPE='EMAIL_ACTIVE' AND GP_VALUE_STR1='true'");
            if (dtActiveMi.Rows.Count <= 0) return;
            */

            if (string.IsNullOrEmpty(MsgTo1) && string.IsNullOrEmpty(MsgToBcc1))
            {
                MessageBox.Show("Gönderim yapılacak Kime yada Gizli mail alanlarını doldurmanız gerekiyor.");
                return;
            }
            //Parametre kullanıcı bilgileri varmı
            string Sql= string.Format(@"Select Top 1  *  From usr_USERS Where USE_NO = {0} and USE_SMTP_CLIENT is not null", frmLogin.KullaniciNo);
            var dtKullanici = new DataTable();
            dtKullanici = DbConnSql.SelectSQL(Sql);
            if (dtKullanici.Rows.Count <= 0)
            {
                MessageBox.Show("Bu kullanıcıya ait email bilgileri tanımlanmamış.Kullanıcı bilgilerinden Email bilgilerini tanımlayınız.");
                return;
            }


            int KullaniciNo = 0;
         

            try
            {
                
                //UsrLoginden gelicek
                string Signuture = dtKullanici.Rows[0]["USE_SIGNUTURE"].ToString();
                string EmailUserName = dtKullanici.Rows[0]["USE_EMAIL_USER"].ToString();
                string EmailPassword = dtKullanici.Rows[0]["USE_EMAIL_PASSWORD"].ToString();
                string From = dtKullanici.Rows[0]["USE_EMAIL_USER"].ToString();
                string EmailSmtp = dtKullanici.Rows[0]["USE_SMTP_CLIENT"].ToString();
                int EmailPort = Convert.ToInt32(dtKullanici.Rows[0]["USE_SMTP_PORT"].ToString());
                KullaniciNo = Convert.ToInt32(dtKullanici.Rows[0]["USE_NO"].ToString());
                //*****************************
                
               // NetworkCredential cred = new NetworkCredential(EmailUserName, EmailPassword);
               // MailMessage Msg = new MailMessage();

                NetworkCredential cred = new NetworkCredential(EmailUserName, EmailPassword);                
                MailMessage Msg = new MailMessage();
                RichEditMailMessageExporter exporter = new RichEditMailMessageExporter(Bodys, Msg);
                exporter.Export();

    
                
                if (!string.IsNullOrEmpty(MsgTo1)) Msg.To.Add(MsgTo1);
                if (!string.IsNullOrEmpty(MsgTo2)) Msg.To.Add(MsgTo2);
                if (!string.IsNullOrEmpty(MsgTo3)) Msg.To.Add(MsgTo3);
                //Gizli gönderim
                if (!string.IsNullOrEmpty(MsgToBcc1)) Msg.Bcc.Add(MsgToBcc1);

         

                Msg.From = new MailAddress(From);
                if (!string.IsNullOrEmpty(FileAttachPath)) Msg.Attachments.Add(new Attachment(FileAttachPath));
                //***
                Msg.Subject = Konu;
                /*
                string sMessage = string.Format(@"{0} <br> ", Bodys);
                Msg.Body = sMessage;
                Msg.IsBodyHtml = true;
                */
                SmtpClient client = new SmtpClient(EmailSmtp, EmailPort);
                client.Credentials = cred;
                client.EnableSsl = false;
                client.Send(Msg);

                var DtIns = new DataTable();

                DtIns = DbConnSql.SelectSQL(string.Format(@"
                    INSERT EMAIL_HAVUZU (EH_GONDERME_TARIH, EH_CARI_KODU, EH_CARI_TANIM, EH_EVRAK_TIPI,EH_EVRAK_NO,EH_EMAIL,EH_ACIKLAMA,EH_GONDERIM_DURUM,EH_CREDATE,EH_CREUSER)
                    SELECT                  getdate(),          '{0}',       '{1}',          '{2}',         '{3}',      '{4}',  '{5}',      {6},                 getdate(),{7}

                    ", "", CariTanimi, "", "", MsgTo1, "Alıcılara email başarılı olarak gönderildi.", 1, KullaniciNo));

                MessageBox.Show("Email gönderimi gerçekleşmiştir.");

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

                var DtIns2 = new DataTable();
                DtIns2 = DbConnSql.SelectSQL(string.Format(@"
                    INSERT EMAIL_HAVUZU (EH_GONDERME_TARIH, EH_CARI_KODU, EH_CARI_TANIM, EH_EVRAK_TIPI,EH_EVRAK_NO,EH_EMAIL,EH_ACIKLAMA,EH_GONDERIM_DURUM,EH_CREDATE,EH_CREUSER)
                    SELECT                  getdate(),          '{0}',       '{1}',          '{2}',         '{3}',      '{4}',  '{5}',      {6},                 getdate(),{7}

                    ", "", CariTanimi, "", "", MsgTo1, ex.Message, 0, KullaniciNo));
                MessageBox.Show("Hata!!!.Email gönderiminde hata oluştu, hata nedenine Email havuzundan bakabilirsiniz.");

            }


        }






        public static string GetSelectedRows2(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            string ret = "";
            if (view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.RowSelect)
            {
                foreach (int i in view.GetSelectedRows())
                {
                    DataRow row = view.GetDataRow(i);
                    //if (ret != "") ret += "\r\n";
                    ret += string.Format("{0} ", i);
                }
            }
            return ret;
        }

        public static void RowHandle(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            if (view.RowCount > 0)
            {

                view.OptionsSelection.MultiSelect = false;
                RowHandleNo = Convert.ToInt32(GetSelectedRows(view));
            }
        }

        public static void RowHandle2(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            view.OptionsSelection.MultiSelect = false;
            RowHandleNo2 = Convert.ToInt32(GetSelectedRows2(view));


        }
        public static void SetBookmark(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            int rowHandle = RowHandleNo;
            if (rowHandle != GridControl.InvalidRowHandle)
            {
                view.FocusedColumn = view.Columns.ColumnByFieldName("gridColumn1");
                view.FocusedRowHandle = rowHandle;
                if (view.IsRowVisible(rowHandle) == RowVisibleState.Hidden)
                    view.MakeRowVisible(rowHandle, false);
                view.ShowEditor();
            }

            //grdVMusteriListesi.OptionsSelection.MultiSelect = false;
          // view.OptionsSelection.MultiSelect = true;

        }

        public static void SetBookmark2(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            int rowHandle = RowHandleNo2;
            if (rowHandle != GridControl.InvalidRowHandle)
            {
                view.FocusedColumn = view.Columns.ColumnByFieldName("gridColumn1");
                view.FocusedRowHandle = rowHandle;
                if (view.IsRowVisible(rowHandle) == RowVisibleState.Hidden)
                    view.MakeRowVisible(rowHandle, false);
                view.ShowEditor();
            }
        }

        public static double GetDovizSatisKur(int DovTip,string Tarih)
        {
            double Sonuc;
            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format("Select dbo.GetDovizSatisKur({0},'{1}')  as Kur", DovTip, Tarih));

            if (dt.Rows.Count<=0) 
                Sonuc=1;
            else


               Sonuc = Convert.ToDouble(dt.Rows[0]["Kur"].ToString());
            return Sonuc;




        }


        public static double GetDovizSatisKurChar(string DovTip, string Tarih)
        {
            double Sonuc;
            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format("Select dbo.GetDovizSatisKurChar('{0}','{1}')  as Kur", DovTip, Tarih));

            if (dt.Rows.Count <= 0)
                Sonuc = 1;
            else


                Sonuc = Convert.ToDouble(dt.Rows[0]["Kur"].ToString());
            return Sonuc;




        }

        public static bool IsNumeric(string text)
        {
            return Regex.IsMatch(text, "^\\d+$");
        }
                     

    }
  
}
