﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using DevExpress.XtraGrid;

namespace BTI
{
    public partial class PubVariable
    {

        public static string sqlParams1 =
          @"
                Select * 
                From pr_PARAMS 
                Where SCP_TYPE='{0}' AND SCP_VIEW='{1}'    
                Order by SCP_VALUE";

        public static string sqlParams =
                @"
                Select * 
                From pr_PARAMS 
                Where SCP_TYPE='{0}'    
                Order by SCP_VALUE";

        public static string RecStatu                   = "Save";

        public static bool   Error                      = false;
        public static bool   ErrorSAP_BOM_Export        = true;
        public static bool   ErrorSAP_Aktarilan_Kodlar  = true;

        public static string fileNameFolder = "c:\\Gabi_Layout";
        public static string fileNameSatisSiparis = "SSL8";
        public static string fileNameAcikSatisSiparisleri = "ASS3";

        public static string fileNameSatinalmaSiparis = "SAL2";
        public static string fileNameUretimEmirleriListesi = "UELX2";
        public static string fileNameYuklemeEmirleri = "YUE10";
        public static string fileNameSipStokKarsila = "SKK1";


        public static string fileStokHareketleri = "SH22";
        public static string fileFiiliStok1 = "FL1X6";
        public static string fileFiiliStok2 = "FL2Y4";
        public static string fileSayimListesi = "SYML1";


        public static string ComputerName = System.Windows.Forms.SystemInformation.ComputerName;
        public static string UserName = System.Windows.Forms.SystemInformation.UserName;
    //    public static string fileBarkodListesi= "FL2";



        public static void SaveGridLayout(DevExpress.XtraGrid.GridControl GridName, string FileName)
        {

            bool isExists = System.IO.File.Exists(string.Format(@"{0}\\{1}.xml", fileNameFolder,FileName));
            if (isExists)
            {
                GridName.ForceInitialize();             
                GridName.MainView.RestoreLayoutFromXml(string.Format(@"{0}\\{1}.xml", fileNameFolder,FileName));
            }
        }

        public static void SearchFilterColumn(DevExpress.XtraGrid.GridControl grd, DevExpress.XtraGrid.Views.Grid.GridView grdV, string SearchFilterName)
        {

            grd.ForceInitialize();
            grdV.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle;
            grdV.FocusedColumn = grdV.Columns[SearchFilterName];
            grdV.ShowEditor();
        
        }


        private static DateTime dt = DateTime.Now;  
        public static int Yil = dt.Year;





        



    }





}
