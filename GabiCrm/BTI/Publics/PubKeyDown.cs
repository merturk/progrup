﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using DevExpress.XtraEditors;


namespace BTI
{

    public partial class PubKeyDown
    {
        #region KeyDown Bildirimleri

        public delegate void EditorSelectAllProc(Control c);
        public static void EditorSelectAll(Control c)
        {
            ((TextBox)c.Controls[0]).SelectAll();
        }



        public static void nextTxt(KeyEventArgs e, TextBox adi)
        {
            if (e.KeyData == Keys.Enter)
            {
                adi.Focus();
            }
        }


        public static void nextTxtDevx(KeyEventArgs e, TextEdit adi)
        {
            if (e.KeyData == Keys.Enter)
            {
                adi.Focus();
            }
        }


        public static void nextImageCombo(KeyEventArgs e, ImageComboBoxEdit adi)
        {
            if (e.KeyData == Keys.Enter)
            {
                adi.Focus();
            }
        }

        public static void nextComboDevx(KeyEventArgs e, DevExpress.XtraEditors.ComboBoxEdit adi)
        {
            if (e.KeyData == Keys.Enter)
            {
                adi.Focus();
            }
        }

        public static void nextCombo(KeyEventArgs e, System.Windows.Forms.ComboBox adi)
        {
            if (e.KeyData == Keys.Enter)
            {
                adi.Focus();
            }
        }



        public static void nextBtn(KeyEventArgs e, Button adi)
        {
            if (e.KeyData == Keys.Enter)
            {
                // ActiveControl = adi;
                adi.Focus();
            }
        }

        public static void nextBtnDevx(KeyEventArgs e, DevExpress.XtraEditors.SimpleButton adi )
        {
            if (e.KeyData == Keys.Enter)
            {
                // ActiveControl = adi;
                adi.Focus();
            }
        }
        #endregion
    }
  
}
