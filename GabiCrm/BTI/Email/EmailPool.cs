﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Global;

namespace GABI_CRM
{
    public partial class EmailPool : DevExpress.XtraEditors.XtraForm
    {
        public EmailPool()
        {
            InitializeComponent();
        }
        private void GetList()
        {
            try
            {
                string Filter = radioGroup1.SelectedIndex == 0 ? "1" : "0";
                var dt = new DataTable();
                dt = DbConnSql.SelectSQL(string.Format(@"
                        SELECT 
                         [Gonderme Tarihi] = EH_GONDERME_TARIH,
                        -- [Cari Kodu] = EH_CARI_KODU,
                         [Cari Tanımı] = EH_CARI_TANIM,
                       --  [Evrak Tipi]= EH_EVRAK_TIPI,
                       --  [Evrak No]= EH_EVRAK_NO,
                         [Gönderilen Email]  = EH_EMAIL,
                         [Gonderim Notu] = EH_ACIKLAMA,
                         [Gönderim Sorumlusu] = (Select Top 1 USE_NAME From  usr_USERS Where USE_NO= EH_CREUSER)
                        FROM EMAIL_HAVUZU
                        Where 
                            (EH_GONDERIM_DURUM='{0}' )
                            and EH_GONDERME_TARIH between '{1}' and '{2}'
                ", Filter, dateIlkTarih.Text, dateSonTarih.Text));

                grdParams.DataSource = dt;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void OtomatikMailGonderimDurumu()
        {
            //Email gönderimi yapılsınmı
            var dtActiveMi = new DataTable();
            dtActiveMi = DbConnSql.SelectSQL("Select * from GEN_PARAMS Where GP_TYPE='EMAIL_ACTIVE' AND GP_VALUE_STR1='true'");
            if (dtActiveMi.Rows.Count > 0)
            {
                chkOto.Checked = true;
                chkOto.Text = "(Aktif ) Otomatik Mail Gönderimi ";
                chkOto.ForeColor = Color.Green;
            }
            else
            {
                chkOto.Checked = false;
                chkOto.Text = "(Pasif ) Otomatik Mail Gönderimi ";
                chkOto.ForeColor = Color.Red;
            }
        }

        private void btnVerileriGetir_Click(object sender, EventArgs e)
        {
            GetList();
        }

        private void EmailPool_Load(object sender, EventArgs e)
        {
            OtomatikMailGonderimDurumu();
            dateIlkTarih.DateTime = DateTime.Now.AddMonths(-2);
            dateSonTarih.DateTime = DateTime.Now.AddDays(1);
            GetList();
        }

        private void chkOto_CheckedChanged(object sender, EventArgs e)
        {
            if (BTI.frmLogin.KullaniciAdi != "ADMIN")
            {
                MessageBox.Show("Bu kısma yetkiniz bulunmamaktadır.");
                return;
            }

            string ChkState = chkOto.Checked ? "true" : "false";

            try
            {
                var dtActiveMi = new DataTable();
                dtActiveMi = DbConnSql.SelectSQL(string.Format(@"
                        Update GEN_PARAMS 
                         Set GP_VALUE_STR1='{0}'
                        Where GP_TYPE='EMAIL_ACTIVE' ", ChkState
                    ));
                OtomatikMailGonderimDurumu();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }


    }
}