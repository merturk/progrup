﻿namespace GABI_CRM
{
    partial class EmailPool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailPool));
            this.grdParams = new DevExpress.XtraGrid.GridControl();
            this.grdVParams = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkOto = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.dateSonTarih = new DevExpress.XtraEditors.DateEdit();
            this.dateIlkTarih = new DevExpress.XtraEditors.DateEdit();
            this.btnVerileriGetir = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSonTarih.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSonTarih.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateIlkTarih.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateIlkTarih.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // grdParams
            // 
            this.grdParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdParams.Location = new System.Drawing.Point(0, 61);
            this.grdParams.MainView = this.grdVParams;
            this.grdParams.Name = "grdParams";
            this.grdParams.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.grdParams.Size = new System.Drawing.Size(853, 444);
            this.grdParams.TabIndex = 15;
            this.grdParams.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVParams});
            // 
            // grdVParams
            // 
            this.grdVParams.GridControl = this.grdParams;
            this.grdVParams.HorzScrollStep = 30;
            this.grdVParams.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdVParams.Name = "grdVParams";
            this.grdVParams.OptionsBehavior.AllowIncrementalSearch = true;
            this.grdVParams.OptionsBehavior.Editable = false;
            this.grdVParams.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVParams.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVParams.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdVParams.OptionsSelection.EnableAppearanceHideSelection = false;
            this.grdVParams.OptionsView.ShowAutoFilterRow = true;
            this.grdVParams.OptionsView.ShowFooter = true;
            this.grdVParams.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Aktif", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pasif", 0, 1)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkOto);
            this.panelControl1.Controls.Add(this.radioGroup1);
            this.panelControl1.Controls.Add(this.dateSonTarih);
            this.panelControl1.Controls.Add(this.dateIlkTarih);
            this.panelControl1.Controls.Add(this.btnVerileriGetir);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.label8);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(853, 61);
            this.panelControl1.TabIndex = 16;
            // 
            // chkOto
            // 
            this.chkOto.Location = new System.Drawing.Point(669, 27);
            this.chkOto.Name = "chkOto";
            this.chkOto.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.chkOto.Properties.Appearance.Options.UseForeColor = true;
            this.chkOto.Properties.Caption = "Otomatik Mail Gönderimi Aktif";
            this.chkOto.Size = new System.Drawing.Size(190, 19);
            this.chkOto.TabIndex = 181;
            this.chkOto.CheckedChanged += new System.EventHandler(this.chkOto_CheckedChanged);
            // 
            // radioGroup1
            // 
            this.radioGroup1.EditValue = "false";
            this.radioGroup1.Location = new System.Drawing.Point(276, 24);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroup1.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroup1.Properties.Columns = 3;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("true", "Gönderilenler"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("false", "Gönderilemeyenler")});
            this.radioGroup1.Size = new System.Drawing.Size(252, 23);
            this.radioGroup1.TabIndex = 180;
            // 
            // dateSonTarih
            // 
            this.dateSonTarih.EditValue = null;
            this.dateSonTarih.Location = new System.Drawing.Point(75, 27);
            this.dateSonTarih.Name = "dateSonTarih";
            this.dateSonTarih.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateSonTarih.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateSonTarih.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.dateSonTarih.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.dateSonTarih.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateSonTarih.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateSonTarih.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateSonTarih.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateSonTarih.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateSonTarih.Size = new System.Drawing.Size(195, 20);
            this.dateSonTarih.TabIndex = 178;
            // 
            // dateIlkTarih
            // 
            this.dateIlkTarih.EditValue = null;
            this.dateIlkTarih.Location = new System.Drawing.Point(75, 5);
            this.dateIlkTarih.Name = "dateIlkTarih";
            this.dateIlkTarih.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateIlkTarih.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateIlkTarih.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.dateIlkTarih.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.dateIlkTarih.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateIlkTarih.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateIlkTarih.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateIlkTarih.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateIlkTarih.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateIlkTarih.Size = new System.Drawing.Size(195, 20);
            this.dateIlkTarih.TabIndex = 177;
            // 
            // btnVerileriGetir
            // 
            this.btnVerileriGetir.Location = new System.Drawing.Point(534, 25);
            this.btnVerileriGetir.Name = "btnVerileriGetir";
            this.btnVerileriGetir.Size = new System.Drawing.Size(103, 22);
            this.btnVerileriGetir.TabIndex = 176;
            this.btnVerileriGetir.Text = "Verileri Getir";
            this.btnVerileriGetir.Click += new System.EventHandler(this.btnVerileriGetir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 172;
            this.label1.Text = "Son Tarih";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(12, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 170;
            this.label8.Text = "İlk Tarih";
            // 
            // EmailPool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 505);
            this.Controls.Add(this.grdParams);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailPool";
            this.Text = "Email Havuzu";
            this.Load += new System.EventHandler(this.EmailPool_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkOto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSonTarih.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateSonTarih.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateIlkTarih.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateIlkTarih.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdParams;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVParams;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkOto;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.DateEdit dateSonTarih;
        private DevExpress.XtraEditors.DateEdit dateIlkTarih;
        private DevExpress.XtraEditors.SimpleButton btnVerileriGetir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
    }
}