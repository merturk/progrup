﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using System.Net.Mime;
using System.Net.Mail;
using System.IO;
using Global;

namespace GABI_CRM
{
    public partial class SendMail : DevExpress.XtraEditors.XtraForm
    {
        
        public SendMail()
        {
            InitializeComponent();
        }
        public static string SelectedEmailList = "", SelectedCariList="";

        List<AttachementInfo> attachments;
        protected internal virtual AlternateView CreateHtmlView()
        {

            string htmlBody = richBody.HtmlText;
            AlternateView view = AlternateView.CreateAlternateViewFromString(htmlBody, Encoding.UTF8, MediaTypeNames.Text.Html);


            int count = attachments.Count;
            for (int i = 0; i < count; i++)
            {
                AttachementInfo info = attachments[i];
                LinkedResource resource = new LinkedResource(info.Stream, info.MimeType);
                resource.ContentId = info.ContentId;
                view.LinkedResources.Add(resource);
            }
            return view;
        }

        public class AttachementInfo
        {
            Stream stream;
            string mimeType;
            string contentId;

            public AttachementInfo(Stream stream, string mimeType, string contentId)
            {
                this.stream = stream;
                this.mimeType = mimeType;
                this.contentId = contentId;
            }

            public Stream Stream { get { return stream; } }
            public string MimeType { get { return mimeType; } }
            public string ContentId { get { return contentId; } }
        }


        private void GetSignuture()
        {
            try
            {

          

            if (!string.IsNullOrEmpty(BTI.frmLogin.KullaniciAdi))
            {
                var DtKullaniciBilgileri = new DataTable();
                DtKullaniciBilgileri = DbConnSql.SelectSQL(string.Format(@"Select * from usr_USERS Where USE_NAME ='{0}'", BTI.frmLogin.KullaniciAdi));
                if (DtKullaniciBilgileri.Rows.Count <= 0)
                {
                    MessageBox.Show("Böyle bir kullanıcı bulunamadı", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                richBody.HtmlText = DtKullaniciBilgileri.Rows[0]["USE_SIGNUTURE"].ToString();


            }
            else
                MessageBox.Show("Bu kullanıcıya ait imza bulamadı.");
            }
            catch (Exception ex)
            {
               MessageBox.Show( ex.Message);
            }


        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnGonder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void btnnGonder_Click(object sender, EventArgs e)
        {
            //var a = new DevExpress.XtraRichEdit.RichEditControl();
            BTI.PubObjects.SendEmail(txtKonu.Text, richBody, SelectedCariList, txtAttachFile.Text, txtKime.Text, "", "", "", txtBcc.Text);            
        }

        private void changeTableCellsShadingItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void SendMail_Load(object sender, EventArgs e)
        {            
            txtBcc.Text = SelectedEmailList.Substring(0,SelectedEmailList.Length-1);          
            GetSignuture();
        }

        private void txtAttachFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
              OpenFileDialog openFileDialog1 = new OpenFileDialog();
              if (openFileDialog1.ShowDialog() == DialogResult.OK)
              {

                  if (openFileDialog1.FileName != "")
                  {
                      System.IO.FileInfo fileInfo = new System.IO.FileInfo(openFileDialog1.FileName);

                      txtAttachFile.Text = openFileDialog1.FileName.ToString();
                  }
              }

        }
    }
}