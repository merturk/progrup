﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;


using DevExpress.XtraScheduler;

using Global;
using System.Collections;

using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Calendar;
using System.Net;


namespace GABI_CRM
{
    public partial class frmCalendar : DevExpress.XtraEditors.XtraForm
    {
        //const string CalendarsUrl = "https://www.google.com/calendar/feeds/default/allcalendars/full";
        public frmCalendar()
        {
            InitializeComponent();

            this.entryList = new ArrayList();            
            

        }

        private ArrayList entryList;

        public TimelineView ActiveTimeLineBasedView
        {
            get
            {
                if (schedulerControl2.ActiveViewType == SchedulerViewType.Timeline)
                    return schedulerControl2.GanttView;
                return schedulerControl2.TimelineView;
            }
        }
        private void GetGantView()
        {


            schedulerStorage2.Appointments.DataSource = DbConnSql.SelectSQL("Select *  From dbo.Appointments");

            schedulerControl2.ActiveViewType = SchedulerViewType.Timeline;
            //schedulerControl2.ActiveViewType = SchedulerViewType.Month;
           // schedulerControl2.GroupType = SchedulerGroupType.Resource;
            schedulerControl2.GanttView.ShowResourceHeaders = false;
            

            schedulerControl2.Start = DateTime.Now.AddDays(-1);//string.IsNullOrEmpty(Tarih) ? DateTime.Now.AddDays(-5) : Convert.ToDateTime(Tarih).AddDays(-5);
          //  ActiveTimeLineBasedView.ResourcesPerPage = Convert.ToInt32(changeScaleWidthItem1.EditValue);
          //  repositoryItemSpinEdit1.MaxValue = 500;
            int scale = 200;
            changeScaleWidthItem1.EditValue = scale;

            ActiveTimeLineBasedView.ResourcesPerPage = scale;// Convert.ToInt32(changeScaleWidthItem1.EditValue);
  
        }
   
        private void frmCalendar_Load(object sender, EventArgs e)
        {
            GetGantView();
        }

        private void schedulerControl1_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            Appointment apt = e.Appointment;
            // Create a custom form.
         //   string SelectedSipNo = schedulerControl1.SelectedAppointments[0].Description.ToString();
       //     string SelectedSipNo = schedulerControl1.SelectedAppointments[0].ResourceId.ToString();
            

        //    MessageBox.Show(SelectedSipNo);
        }
        

        private void Go_Click(object sender, EventArgs e)
        {
           
        }


        private CalendarFeed RetrievingOwnGoogleCalendars()
        {
            // Create a CalenderService and authenticate
            CalendarService myService = new CalendarService("exampleCo-exampleApp-1");
            myService.setUserCredentials(UserName.Text, Password.Text);

            CalendarQuery query = new CalendarQuery();
            query.Uri = new Uri("http://www.google.com/calendar/feeds/default/owncalendars/full");
            CalendarFeed resultFeed = myService.Query(query);
            return resultFeed;
        }

        public static string userName = "";
        public static string passWord = "";
        public static CalendarService service = new CalendarService("My Google Calendar Service");
        private void RefreshFeed()
        {

            if (cmbGoogleCalendar.SelectedIndex >= 0)
            {
                this.CalendarURI.Text = "http://www.google.com/calendar/feeds/" + ((CalendarEntry)(cmbGoogleCalendar.SelectedItem)).SelfUri.ToString().Substring(((CalendarEntry)(cmbGoogleCalendar.SelectedItem)).SelfUri.ToString().LastIndexOf("/") + 1) + "/private/full";
            }
            string calendarURI = this.CalendarURI.Text;
            userName = this.UserName.Text;
            passWord = this.Password.Text;

            this.entryList = new ArrayList(50);
            ArrayList dates = new ArrayList(50);
            EventQuery query = new EventQuery();

            GDataGAuthRequestFactory requestFactory = (GDataGAuthRequestFactory)service.RequestFactory;
            IWebProxy iProxy = WebRequest.GetSystemWebProxy();
            WebProxy myProxy = new WebProxy();
            // potentially, setup credentials on the proxy here
            myProxy.Credentials = CredentialCache.DefaultCredentials;
            myProxy.UseDefaultCredentials = false;
            if (ProxyAddress.Text.Trim() != "" && ProxyPort.Text.Trim() != "")
            {
                myProxy.Address = new Uri("http://" + ProxyAddress.Text.Trim() + ":" + ProxyPort.Text.Trim());
            }

            if (userName != null && userName.Length > 0)
            {
                service.setUserCredentials(userName, passWord);
            }

            // only get event's for today - 1 month until today + 1 year

            query.Uri = new Uri(calendarURI);
            requestFactory.CreateRequest(GDataRequestType.Query, query.Uri);//  = myProxy;
            /*
            if (calendarControl.SelectionRange != null)
            {
                query.StartTime = calendarControl.SelectionRange.Start.AddDays(-1);
                query.EndTime = calendarControl.SelectionRange.End.AddDays(1);
            }
            else
            {
                query.StartTime = DateTime.Now.AddDays(-12);
                query.EndTime = DateTime.Now.AddMonths(0);
            }
            */

            query.StartTime = DateTime.Now.AddDays(-1200);
            query.EndTime = DateTime.Now.AddMonths(100);

            EventFeed calFeed = service.Query(query) as EventFeed;


            // now populate the calendar
            if (calFeed != null && calFeed.Entries.Count == 0)
            {
                MessageBox.Show("Seçili takvime ait herhangi bir bilgi bulunmadı...");

            }
            else
            {
                while (calFeed != null && calFeed.Entries.Count > 0)
                {
                    // look for the one with dinner time...
                    foreach (EventEntry entry in calFeed.Entries)
                    {
                        this.entryList.Add(entry);
                        if (entry.Times.Count > 0)
                        {
                            foreach (When w in entry.Times)
                            {
                                dates.Add(w.StartTime);
                            }
                        }
                    }
                    // just query the same query again.
                    if (calFeed.NextChunk != null)
                    {
                        query.Uri = new Uri(calFeed.NextChunk);
                        calFeed = service.Query(query) as EventFeed;
                    }
                    else
                        calFeed = null;

                }
                DateTime[] aDates = new DateTime[dates.Count];

                int i = 0;
                foreach (DateTime d in dates)
                {
                    aDates[i++] = d;
                }


                //this.calendarControl.BoldedDates = aDates;
                // this.calendarControl.SelectionRange = new SelectionRange(DateTime.Now, DateTime.Now);
                /*
                if (aDates.Length > 0)
                {
                    MessageBox.Show("Please select the Dates marked bold in the calendar to see events");
                }
                else
                {
                    MessageBox.Show("No Event found against selected dates rage and calendar");
                }
                */
            }
        }


        private void GetSelectedCalendarList()
        {
            try
            {

           
            this.DayEvents.Items.Clear();
            DbConnSql.SelectSQL(@"DELETE APPINTMENTS_TB ");

            ArrayList results = new ArrayList(5);
            foreach (EventEntry entry in this.entryList)
            {
                // let's find the entries for that date
                results.Add(entry);              
            }


            foreach (EventEntry entry in results)
            {
                ListViewItem item = new ListViewItem(entry.Title.Text);
                item.SubItems.Add(entry.Authors[0].Name);
                if (entry.Times.Count > 0)
                {
                    item.SubItems.Add(entry.Times[0].StartTime.TimeOfDay.ToString());
                    item.SubItems.Add(entry.Times[0].EndTime.TimeOfDay.ToString());
                    item.SubItems.Add(entry.EditUri.ToString());
                }

                this.DayEvents.Items.Add(item);


                DbConnSql.SelectSQL(string.Format(@"INSERT APPINTMENTS_TB (StartDate,EndDate,Subject,Description,Location,ResourceID,EditUri) Select '{0}','{1}','{2}','{3}' ,'{4}',1,'{5}'"
                    , entry.Times[0].StartTime.ToString("yyyy-MM-dd hh:mm")
                    , entry.Times[0].EndTime.ToString("yyyy-MM-dd hh:mm")
                    , entry.Title.Text
                    , entry.Content.Content
                    , entry.Locations[0].ValueString.ToString()
                    , entry.EditUri.ToString()
                        
                    ));
                //item.SubItems.Add(entry.EditUri.ToString()); 

                //  MessageBox.Show(entry.EditUri.ToString());
              
            }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            GetGantView();
        }

        private void calendarControl_DateSelected(object sender, DateRangeEventArgs e)
        {
         
        }

        private Boolean ValidateInput()
        {
            if (UserName.Text.Trim() == "" || Password.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter your Gmail account information");

                return false;
            }
            else if (chkIsPRoxyRequired.Checked == true)
            {
                if (ProxyAddress.Text.Trim() == "" || ProxyPort.Text.Trim() == "")
                {
                    MessageBox.Show("Please Enter Proxy Information");

                    return false;
                }
            }

            return true;
        }

        private void GetCalendarList()
        {

            try
            {
                if (ValidateInput() == true)
                {
                    cmbGoogleCalendar.Items.Clear();
                    CalendarFeed cal_Feed = RetrievingOwnGoogleCalendars();
                    foreach (CalendarEntry centry in cal_Feed.Entries)
                    {                        
                        cmbGoogleCalendar.Items.Add(centry);
                    }


                    cmbGoogleCalendar.DisplayMember = "Title";//title.text
                    cmbGoogleCalendar.ValueMember = "Title";
                    if (cmbGoogleCalendar.Items.Count > 0)
                    {
                        cmbGoogleCalendar.SelectedIndex = 0;
                    }
                }
                GetGantView();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message.ToString());
            }
        }

        
        private void btnRetrieveCalendars_Click(object sender, EventArgs e)
        {
            GetCalendarList();
        }

        private void chkIsPRoxyRequired_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsPRoxyRequired.Checked == true)
            {
                ProxyAddress.Enabled = true;
                ProxyPort.Enabled = true;
            }
            else
            {
                ProxyAddress.Enabled = false;
                ProxyPort.Enabled = false;
            }
        }

        private void cmbGoogleCalendar_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void cmbGoogleCalendar_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               

                    RefreshFeed();
                    GetSelectedCalendarList();
                    
                

            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message.ToString());
            }
        }

        private void frmCalendar_Load_1(object sender, EventArgs e)
        {
          GetGantView();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
       

        }

        private void btnYeni_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbGoogleCalendar.Text)) {  MessageBox.Show("Tekvim listesini çağırdıktan sonra yeniden deneyin.");       return;      }
            
            var frmForm = new frmAppointments();
            frmForm.WindowState = FormWindowState.Normal;
            frmForm.ShowDialog();
            GetCalendarList();
        }

        private void btnAc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbGoogleCalendar.Text)) { MessageBox.Show("Tekvim listesini çağırdıktan sonra yeniden deneyin."); return; }
            
            string SelectedId = schedulerControl2.SelectedAppointments[0].Id.ToString();
            frmAppointments.GelisId = SelectedId;
            var frmForm = new frmAppointments();
            frmForm.WindowState = FormWindowState.Normal;
            frmForm.ShowDialog();
            GetCalendarList();
        }

        private void schedulerControl2_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbGoogleCalendar.Text))
            {
                MessageBox.Show("Tekvim listesini çağırdıktan sonra yeniden deneyin.");

                Appointment apt = e.Appointment;
                var myForm = new Form1();//((SchedulerControl)sender, apt, openRecurrenceForm);
                myForm.WindowState = FormWindowState.Normal;

                try
                {
                    // Required for skins support.
                    //  myForm.LookAndFeel.ParentLookAndFeel = schedulerControl1.LookAndFeel;

                   // e.DialogResult = myForm.ShowDialog();
                    schedulerControl2.Refresh();
                    e.Handled = true;
                    GetCalendarList();
                }
                finally
                {
                    myForm.Dispose();
                }
            }

            else
            {
                Appointment apt = e.Appointment;
                // Create a custom form.
                string SelectedId = schedulerControl2.SelectedAppointments[0].Id.ToString();
                frmAppointments.GelisId = SelectedId;

                var myForm = new frmAppointments();//((SchedulerControl)sender, apt, openRecurrenceForm);
                myForm.WindowState = FormWindowState.Normal;

                try
                {
                    // Required for skins support.
                    //  myForm.LookAndFeel.ParentLookAndFeel = schedulerControl1.LookAndFeel;

                    e.DialogResult = myForm.ShowDialog();
                    schedulerControl2.Refresh();
                    e.Handled = true;
                    GetCalendarList();
                }
                finally
                {
                    myForm.Dispose();
                }
            }
        }

        


    }
}