﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Calendar;
using System.Net;
using Global;


namespace GABI_CRM
{
    public partial class frmAppointments : DevExpress.XtraEditors.XtraForm
    {
        public frmAppointments()
        {
            InitializeComponent();
        }
        public static string GelisId="",Title="",ThisFeedUri="";

        private void frmAppointments_FormClosed(object sender, FormClosedEventArgs e)
        {
            GelisId = "";
        }


        private void GetScreenValue()
        {
            if (string.IsNullOrEmpty(GelisId))
            {
                dateBaslama.DateTime = DateTime.Now;
                dateBitis.DateTime   = DateTime.Now;
                return;
            }
            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(string.Format(@" 
                 Select Top 1 UniqueId,StartDate,EndDate,Subject,Location,Description,EditUri from Appointments Where UniqueId={0}
            ", GelisId));
            

            txtKonu.Text        = dt.Rows[0]["Subject"].ToString();
            txtLokasyon.Text    = dt.Rows[0]["Location"].ToString();
            dateBaslama.DateTime    = Convert.ToDateTime( dt.Rows[0]["StartDate"].ToString());
            dateBitis.DateTime  =  Convert.ToDateTime( dt.Rows[0]["EndDate"].ToString());
            txtAciklama.Text    = dt.Rows[0]["Description"].ToString();
            ThisFeedUri         = dt.Rows[0]["EditUri"].ToString();
            this.Text = txtKonu.Text;
        
        }

        private void frmAppointments_Load(object sender, EventArgs e)
        {
            this.Text = Title;
            GetScreenValue();

        }

        CalendarService service = frmCalendar.service;


        /*
         Reminder reminder = new Reminder();
reminder.Minutes = 15;
reminder.Method = Reminder.ReminderMethod.alert;
eventTime.Reminders.Add( reminder );
         
         */

        private  void Save()
        {

            EventEntry entry = new EventEntry();
            entry.Title.Text = txtKonu.Text;
            entry.Content.Content = txtAciklama.Text;
            Where eventLocation = new Where();
            eventLocation.ValueString = txtLokasyon.Text;
            entry.Locations.Add(eventLocation);
            When eventTime = new When(dateBaslama.DateTime, dateBitis.DateTime);
            entry.Times.Add(eventTime);
            //Reminder
            Reminder reminder = new Reminder();
            reminder.Minutes = 15;
            reminder.Method = Reminder.ReminderMethod.alert;
            eventTime.Reminders.Add(reminder);
            //**************

            Uri postUri = new Uri("https://www.google.com/calendar/feeds/default/private/full");

            // Send the request and receive the response:
            AtomEntry insertedEntry = service.Insert(postUri, entry);
            MessageBox.Show("Veriler kayıt edildi.");
            Close();
        
        }

        private void Update()
        {
            Delete();
            Save();
           
        
        
        }

        private void Delete()
        {
        
            Uri postUri = new Uri(ThisFeedUri);
            EventQuery Query = new EventQuery(ThisFeedUri);
            Query.Uri = postUri;
            EventFeed calFeed = service.Query(Query);
            if (calFeed != null && calFeed.Entries.Count > 0)
            {
                foreach (EventEntry SearchedEntry in calFeed.Entries)
                {
                    SearchedEntry.Delete();
                    break;
                }

            }
        
        }

        private void btnKaydet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(GelisId))
                Save();
            else
                Update();


        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {


          
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
        }

    }
}