﻿namespace GABI_CRM
{
    partial class SendMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendMail));
            this.Img = new DevExpress.Utils.ImageCollection(this.components);
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtKime = new DevExpress.XtraEditors.TextEdit();
            this.txtBilgi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtKonu = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtAttachFile = new DevExpress.XtraEditors.ButtonEdit();
            this.txtBcc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnnGonder = new DevExpress.XtraEditors.SimpleButton();
            this.richBody = new DevExpress.XtraRichEdit.RichEditControl();
            this.richEditBarController1 = new DevExpress.XtraRichEdit.UI.RichEditBarController();
            this.ınsertPageBreakItem2 = new DevExpress.XtraRichEdit.UI.InsertPageBreakItem();
            ((System.ComponentModel.ISupportInitialize)(this.Img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBilgi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKonu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditBarController1)).BeginInit();
            this.SuspendLayout();
            // 
            // Img
            // 
            this.Img.ImageSize = new System.Drawing.Size(32, 32);
            this.Img.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("Img.ImageStream")));
            this.Img.Images.SetKeyName(0, "send.png");
            this.Img.Images.SetKeyName(1, "delete.png");
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(86, 9);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(22, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Kime";
            // 
            // txtKime
            // 
            this.txtKime.Location = new System.Drawing.Point(144, 5);
            this.txtKime.Name = "txtKime";
            this.txtKime.Size = new System.Drawing.Size(316, 20);
            this.txtKime.TabIndex = 5;
            // 
            // txtBilgi
            // 
            this.txtBilgi.Location = new System.Drawing.Point(490, 5);
            this.txtBilgi.Name = "txtBilgi";
            this.txtBilgi.Size = new System.Drawing.Size(316, 20);
            this.txtBilgi.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(466, 9);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(18, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Bilgi";
            // 
            // txtKonu
            // 
            this.txtKonu.Location = new System.Drawing.Point(144, 47);
            this.txtKonu.Name = "txtKonu";
            this.txtKonu.Size = new System.Drawing.Size(662, 20);
            this.txtKonu.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(86, 51);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Konu";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtAttachFile);
            this.panelControl1.Controls.Add(this.txtBcc);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.btnnGonder);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtKime);
            this.panelControl1.Controls.Add(this.txtKonu);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtBilgi);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(830, 95);
            this.panelControl1.TabIndex = 512;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(86, 72);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 13);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Dosya Ekle";
            // 
            // txtAttachFile
            // 
            this.txtAttachFile.Location = new System.Drawing.Point(144, 68);
            this.txtAttachFile.Name = "txtAttachFile";
            this.txtAttachFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtAttachFile.Properties.ReadOnly = true;
            this.txtAttachFile.Size = new System.Drawing.Size(316, 20);
            this.txtAttachFile.TabIndex = 13;
            this.txtAttachFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtAttachFile_ButtonClick);
            // 
            // txtBcc
            // 
            this.txtBcc.Location = new System.Drawing.Point(144, 26);
            this.txtBcc.Name = "txtBcc";
            this.txtBcc.Size = new System.Drawing.Size(662, 20);
            this.txtBcc.TabIndex = 12;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(86, 30);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(18, 13);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Gizli";
            // 
            // btnnGonder
            // 
            this.btnnGonder.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnnGonder.Appearance.Options.UseFont = true;
            this.btnnGonder.Appearance.Options.UseTextOptions = true;
            this.btnnGonder.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnnGonder.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.btnnGonder.ImageIndex = 0;
            this.btnnGonder.ImageList = this.Img;
            this.btnnGonder.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnnGonder.Location = new System.Drawing.Point(12, 8);
            this.btnnGonder.Name = "btnnGonder";
            this.btnnGonder.Size = new System.Drawing.Size(68, 58);
            this.btnnGonder.TabIndex = 10;
            this.btnnGonder.Text = "Gönder";
            this.btnnGonder.Click += new System.EventHandler(this.btnnGonder_Click);
            // 
            // richBody
            // 
            this.richBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richBody.Location = new System.Drawing.Point(0, 95);
            this.richBody.Name = "richBody";
            this.richBody.Options.Fields.UseCurrentCultureDateTimeFormat = false;
            this.richBody.Options.MailMerge.KeepLastParagraph = false;
            this.richBody.Size = new System.Drawing.Size(830, 476);
            this.richBody.TabIndex = 513;
            // 
            // richEditBarController1
            // 
            this.richEditBarController1.BarItems.Add(this.ınsertPageBreakItem2);
            this.richEditBarController1.Control = this.richBody;
            // 
            // ınsertPageBreakItem2
            // 
            this.ınsertPageBreakItem2.Id = -1;
            this.ınsertPageBreakItem2.Name = "ınsertPageBreakItem2";
            // 
            // SendMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 571);
            this.Controls.Add(this.richBody);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SendMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Toplu Mail Gönderimi";
            this.Load += new System.EventHandler(this.SendMail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBilgi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKonu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditBarController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection Img;
        private DevExpress.XtraEditors.TextEdit txtKonu;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtBilgi;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtKime;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnnGonder;
        private DevExpress.XtraRichEdit.RichEditControl richBody;
        private DevExpress.XtraRichEdit.UI.RichEditBarController richEditBarController1;
        private DevExpress.XtraEditors.TextEdit txtBcc;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ButtonEdit txtAttachFile;
        private DevExpress.XtraRichEdit.UI.InsertPageBreakItem ınsertPageBreakItem2;
    }
}