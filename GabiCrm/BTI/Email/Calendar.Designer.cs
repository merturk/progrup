﻿namespace GABI_CRM
{
    partial class frmCalendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalendar));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnRetrieveCalendars = new System.Windows.Forms.Button();
            this.cmbGoogleCalendar = new System.Windows.Forms.ComboBox();
            this.chkIsPRoxyRequired = new System.Windows.Forms.CheckBox();
            this.ProxyPort = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ProxyAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.TextBox();
            this.UserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CalendarURI = new System.Windows.Forms.TextBox();
            this.DayEvents = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.schedulerBarController1 = new DevExpress.XtraScheduler.UI.SchedulerBarController();
            this.btnOpen = new DevExpress.XtraScheduler.UI.OpenScheduleItem();
            this.saveScheduleItem1 = new DevExpress.XtraScheduler.UI.SaveScheduleItem();
            this.printPreviewItem1 = new DevExpress.XtraScheduler.UI.PrintPreviewItem();
            this.printItem1 = new DevExpress.XtraScheduler.UI.PrintItem();
            this.printPageSetupItem1 = new DevExpress.XtraScheduler.UI.PrintPageSetupItem();
            this.newAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewAppointmentItem();
            this.newRecurringAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem();
            this.navigateViewBackwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewBackwardItem();
            this.navigateViewForwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewForwardItem();
            this.gotoTodayItem1 = new DevExpress.XtraScheduler.UI.GotoTodayItem();
            this.viewZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewZoomInItem();
            this.viewZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewZoomOutItem();
            this.switchToDayViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToDayViewItem();
            this.switchToWorkWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem();
            this.switchToWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWeekViewItem();
            this.switchToMonthViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToMonthViewItem();
            this.switchToTimelineViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem();
            this.switchToGanttViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToGanttViewItem();
            this.groupByNoneItem1 = new DevExpress.XtraScheduler.UI.GroupByNoneItem();
            this.groupByDateItem1 = new DevExpress.XtraScheduler.UI.GroupByDateItem();
            this.groupByResourceItem1 = new DevExpress.XtraScheduler.UI.GroupByResourceItem();
            this.switchTimeScalesItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesItem();
            this.changeScaleWidthItem1 = new DevExpress.XtraScheduler.UI.ChangeScaleWidthItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.switchTimeScalesCaptionItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem();
            this.switchCompressWeekendItem1 = new DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem();
            this.switchShowWorkTimeOnlyItem1 = new DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem();
            this.switchCellsAutoHeightItem1 = new DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem();
            this.changeSnapToCellsUIItem1 = new DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem();
            this.editAppointmentQueryItem1 = new DevExpress.XtraScheduler.UI.EditAppointmentQueryItem();
            this.editOccurrenceUICommandItem1 = new DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem();
            this.editSeriesUICommandItem1 = new DevExpress.XtraScheduler.UI.EditSeriesUICommandItem();
            this.deleteAppointmentsItem1 = new DevExpress.XtraScheduler.UI.DeleteAppointmentsItem();
            this.deleteOccurrenceItem1 = new DevExpress.XtraScheduler.UI.DeleteOccurrenceItem();
            this.deleteSeriesItem1 = new DevExpress.XtraScheduler.UI.DeleteSeriesItem();
            this.splitAppointmentItem1 = new DevExpress.XtraScheduler.UI.SplitAppointmentItem();
            this.changeAppointmentStatusItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem();
            this.changeAppointmentLabelItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem();
            this.toggleRecurrenceItem1 = new DevExpress.XtraScheduler.UI.ToggleRecurrenceItem();
            this.changeAppointmentReminderItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.schedulerControl2 = new DevExpress.XtraScheduler.SchedulerControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.commonBar1 = new DevExpress.XtraScheduler.UI.CommonBar();
            this.printBar1 = new DevExpress.XtraScheduler.UI.PrintBar();
            this.appointmentBar1 = new DevExpress.XtraScheduler.UI.AppointmentBar();
            this.navigatorBar1 = new DevExpress.XtraScheduler.UI.NavigatorBar();
            this.arrangeBar1 = new DevExpress.XtraScheduler.UI.ArrangeBar();
            this.groupByBar1 = new DevExpress.XtraScheduler.UI.GroupByBar();
            this.timeScaleBar1 = new DevExpress.XtraScheduler.UI.TimeScaleBar();
            this.layoutBar1 = new DevExpress.XtraScheduler.UI.LayoutBar();
            this.actionsBar1 = new DevExpress.XtraScheduler.UI.ActionsBar();
            this.optionsBar1 = new DevExpress.XtraScheduler.UI.OptionsBar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnYeni = new DevExpress.XtraBars.BarButtonItem();
            this.btnAc = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.schedulerStorage2 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnRetrieveCalendars);
            this.panelControl1.Controls.Add(this.cmbGoogleCalendar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1198, 54);
            this.panelControl1.TabIndex = 3;
            // 
            // btnRetrieveCalendars
            // 
            this.btnRetrieveCalendars.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRetrieveCalendars.Location = new System.Drawing.Point(293, 15);
            this.btnRetrieveCalendars.Name = "btnRetrieveCalendars";
            this.btnRetrieveCalendars.Size = new System.Drawing.Size(118, 24);
            this.btnRetrieveCalendars.TabIndex = 32;
            this.btnRetrieveCalendars.Text = "Takvim Listesi";
            this.btnRetrieveCalendars.Click += new System.EventHandler(this.btnRetrieveCalendars_Click);
            // 
            // cmbGoogleCalendar
            // 
            this.cmbGoogleCalendar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoogleCalendar.FormattingEnabled = true;
            this.cmbGoogleCalendar.Location = new System.Drawing.Point(5, 18);
            this.cmbGoogleCalendar.Name = "cmbGoogleCalendar";
            this.cmbGoogleCalendar.Size = new System.Drawing.Size(282, 21);
            this.cmbGoogleCalendar.TabIndex = 26;
            this.cmbGoogleCalendar.SelectedIndexChanged += new System.EventHandler(this.cmbGoogleCalendar_SelectedIndexChanged);
            this.cmbGoogleCalendar.SelectedValueChanged += new System.EventHandler(this.cmbGoogleCalendar_SelectedValueChanged);
            // 
            // chkIsPRoxyRequired
            // 
            this.chkIsPRoxyRequired.AutoSize = true;
            this.chkIsPRoxyRequired.Location = new System.Drawing.Point(112, 92);
            this.chkIsPRoxyRequired.Name = "chkIsPRoxyRequired";
            this.chkIsPRoxyRequired.Size = new System.Drawing.Size(73, 17);
            this.chkIsPRoxyRequired.TabIndex = 33;
            this.chkIsPRoxyRequired.Text = "Set Proxy";
            this.chkIsPRoxyRequired.UseVisualStyleBackColor = true;
            this.chkIsPRoxyRequired.CheckedChanged += new System.EventHandler(this.chkIsPRoxyRequired_CheckedChanged);
            // 
            // ProxyPort
            // 
            this.ProxyPort.Location = new System.Drawing.Point(112, 151);
            this.ProxyPort.Name = "ProxyPort";
            this.ProxyPort.Size = new System.Drawing.Size(344, 21);
            this.ProxyPort.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(14, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 22);
            this.label6.TabIndex = 30;
            this.label6.Text = "Proxy Port:";
            // 
            // ProxyAddress
            // 
            this.ProxyAddress.Location = new System.Drawing.Point(112, 125);
            this.ProxyAddress.Name = "ProxyAddress";
            this.ProxyAddress.Size = new System.Drawing.Size(344, 21);
            this.ProxyAddress.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(14, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 24);
            this.label5.TabIndex = 28;
            this.label5.Text = "Proxy IpAddress:";
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(112, 70);
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(344, 21);
            this.Password.TabIndex = 25;
            this.Password.Text = "123456__";
            // 
            // UserName
            // 
            this.UserName.Location = new System.Drawing.Point(112, 44);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(344, 21);
            this.UserName.TabIndex = 24;
            this.UserName.Text = "progruptakvim@gmail.com";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 23;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 24);
            this.label2.TabIndex = 22;
            this.label2.Text = "User:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "URL:";
            // 
            // CalendarURI
            // 
            this.CalendarURI.Enabled = false;
            this.CalendarURI.Location = new System.Drawing.Point(112, 14);
            this.CalendarURI.Name = "CalendarURI";
            this.CalendarURI.Size = new System.Drawing.Size(344, 21);
            this.CalendarURI.TabIndex = 20;
            this.CalendarURI.Text = "http://www.google.com/calendar/feeds/default/private/full";
            // 
            // DayEvents
            // 
            this.DayEvents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.DayEvents.FullRowSelect = true;
            this.DayEvents.GridLines = true;
            this.DayEvents.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.DayEvents.LabelWrap = false;
            this.DayEvents.Location = new System.Drawing.Point(112, 178);
            this.DayEvents.Name = "DayEvents";
            this.DayEvents.Size = new System.Drawing.Size(447, 224);
            this.DayEvents.TabIndex = 10;
            this.DayEvents.UseCompatibleStateImageBehavior = false;
            this.DayEvents.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Event";
            this.columnHeader1.Width = 184;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Author";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Start";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "End";
            // 
            // schedulerBarController1
            // 
            this.schedulerBarController1.BarItems.Add(this.btnOpen);
            this.schedulerBarController1.BarItems.Add(this.saveScheduleItem1);
            this.schedulerBarController1.BarItems.Add(this.printPreviewItem1);
            this.schedulerBarController1.BarItems.Add(this.printItem1);
            this.schedulerBarController1.BarItems.Add(this.printPageSetupItem1);
            this.schedulerBarController1.BarItems.Add(this.newAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.newRecurringAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewBackwardItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewForwardItem1);
            this.schedulerBarController1.BarItems.Add(this.gotoTodayItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomInItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomOutItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToDayViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWorkWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToMonthViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToTimelineViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToGanttViewItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByNoneItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByDateItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByResourceItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesItem1);
            this.schedulerBarController1.BarItems.Add(this.changeScaleWidthItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesCaptionItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCompressWeekendItem1);
            this.schedulerBarController1.BarItems.Add(this.switchShowWorkTimeOnlyItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCellsAutoHeightItem1);
            this.schedulerBarController1.BarItems.Add(this.changeSnapToCellsUIItem1);
            this.schedulerBarController1.BarItems.Add(this.editAppointmentQueryItem1);
            this.schedulerBarController1.BarItems.Add(this.editOccurrenceUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.editSeriesUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteAppointmentsItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteOccurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteSeriesItem1);
            this.schedulerBarController1.BarItems.Add(this.splitAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentStatusItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentLabelItem1);
            this.schedulerBarController1.BarItems.Add(this.toggleRecurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentReminderItem1);
            this.schedulerBarController1.Control = this.schedulerControl2;
            // 
            // btnOpen
            // 
            this.btnOpen.Caption = "Aç";
            this.btnOpen.Id = 0;
            this.btnOpen.Name = "btnOpen";
            // 
            // saveScheduleItem1
            // 
            this.saveScheduleItem1.Id = 1;
            this.saveScheduleItem1.Name = "saveScheduleItem1";
            // 
            // printPreviewItem1
            // 
            this.printPreviewItem1.Id = 2;
            this.printPreviewItem1.Name = "printPreviewItem1";
            toolTipTitleItem1.Text = "Bilgi";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Önizleme";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.printPreviewItem1.SuperTip = superToolTip1;
            // 
            // printItem1
            // 
            this.printItem1.Id = 3;
            this.printItem1.Name = "printItem1";
            toolTipTitleItem2.Text = "Bilgi";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Yazdır";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.printItem1.SuperTip = superToolTip2;
            // 
            // printPageSetupItem1
            // 
            this.printPageSetupItem1.Id = 4;
            this.printPageSetupItem1.Name = "printPageSetupItem1";
            toolTipTitleItem3.Text = "Bilgi";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Yazdırma ayarları";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.printPageSetupItem1.SuperTip = superToolTip3;
            // 
            // newAppointmentItem1
            // 
            this.newAppointmentItem1.Id = 5;
            this.newAppointmentItem1.Name = "newAppointmentItem1";
            // 
            // newRecurringAppointmentItem1
            // 
            this.newRecurringAppointmentItem1.Id = 6;
            this.newRecurringAppointmentItem1.Name = "newRecurringAppointmentItem1";
            // 
            // navigateViewBackwardItem1
            // 
            this.navigateViewBackwardItem1.Id = 7;
            this.navigateViewBackwardItem1.Name = "navigateViewBackwardItem1";
            toolTipTitleItem4.Text = "Bilgi";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Geri";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.navigateViewBackwardItem1.SuperTip = superToolTip4;
            // 
            // navigateViewForwardItem1
            // 
            this.navigateViewForwardItem1.Id = 8;
            this.navigateViewForwardItem1.Name = "navigateViewForwardItem1";
            toolTipTitleItem5.Text = "Bilgi";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "İleri";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.navigateViewForwardItem1.SuperTip = superToolTip5;
            // 
            // gotoTodayItem1
            // 
            this.gotoTodayItem1.Id = 9;
            this.gotoTodayItem1.Name = "gotoTodayItem1";
            toolTipTitleItem6.Text = "Bilgi";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Bügüne git";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.gotoTodayItem1.SuperTip = superToolTip6;
            // 
            // viewZoomInItem1
            // 
            this.viewZoomInItem1.Id = 10;
            this.viewZoomInItem1.Name = "viewZoomInItem1";
            toolTipTitleItem7.Text = "Bilgi";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Büyült";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.viewZoomInItem1.SuperTip = superToolTip7;
            // 
            // viewZoomOutItem1
            // 
            this.viewZoomOutItem1.Id = 11;
            this.viewZoomOutItem1.Name = "viewZoomOutItem1";
            toolTipTitleItem8.Text = "Bilgi";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Küçült";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.viewZoomOutItem1.SuperTip = superToolTip8;
            // 
            // switchToDayViewItem1
            // 
            this.switchToDayViewItem1.Id = 12;
            this.switchToDayViewItem1.Name = "switchToDayViewItem1";
            toolTipTitleItem9.Text = "Bilgi";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Günlük  Görünüm";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.switchToDayViewItem1.SuperTip = superToolTip9;
            // 
            // switchToWorkWeekViewItem1
            // 
            this.switchToWorkWeekViewItem1.Id = 13;
            this.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1";
            toolTipTitleItem10.Text = "Bilgi";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "5 Günlük Görünüm";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.switchToWorkWeekViewItem1.SuperTip = superToolTip10;
            // 
            // switchToWeekViewItem1
            // 
            this.switchToWeekViewItem1.Id = 14;
            this.switchToWeekViewItem1.Name = "switchToWeekViewItem1";
            toolTipTitleItem11.Text = "Bilgi";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "7 Günlük Görünüm";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.switchToWeekViewItem1.SuperTip = superToolTip11;
            // 
            // switchToMonthViewItem1
            // 
            this.switchToMonthViewItem1.Id = 15;
            this.switchToMonthViewItem1.Name = "switchToMonthViewItem1";
            toolTipTitleItem12.Text = "Bilgi";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Aylık Görünüm";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.switchToMonthViewItem1.SuperTip = superToolTip12;
            // 
            // switchToTimelineViewItem1
            // 
            this.switchToTimelineViewItem1.Id = 16;
            this.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1";
            toolTipTitleItem13.Text = "Bilgi";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Saatsel Gösterim Görünümü";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.switchToTimelineViewItem1.SuperTip = superToolTip13;
            // 
            // switchToGanttViewItem1
            // 
            this.switchToGanttViewItem1.Id = 17;
            this.switchToGanttViewItem1.Name = "switchToGanttViewItem1";
            toolTipTitleItem14.Text = "Bilgi";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Gant Görünümü";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.switchToGanttViewItem1.SuperTip = superToolTip14;
            // 
            // groupByNoneItem1
            // 
            this.groupByNoneItem1.Id = 18;
            this.groupByNoneItem1.Name = "groupByNoneItem1";
            // 
            // groupByDateItem1
            // 
            this.groupByDateItem1.Id = 19;
            this.groupByDateItem1.Name = "groupByDateItem1";
            // 
            // groupByResourceItem1
            // 
            this.groupByResourceItem1.Id = 20;
            this.groupByResourceItem1.Name = "groupByResourceItem1";
            // 
            // switchTimeScalesItem1
            // 
            this.switchTimeScalesItem1.Id = 21;
            this.switchTimeScalesItem1.Name = "switchTimeScalesItem1";
            toolTipTitleItem15.Text = "Bilgi";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Saatsel görünüm değişikliği";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.switchTimeScalesItem1.SuperTip = superToolTip15;
            // 
            // changeScaleWidthItem1
            // 
            this.changeScaleWidthItem1.Edit = this.repositoryItemSpinEdit1;
            this.changeScaleWidthItem1.EditValue = 180;
            this.changeScaleWidthItem1.Id = 22;
            this.changeScaleWidthItem1.Name = "changeScaleWidthItem1";
            toolTipTitleItem16.Text = "Bilgi";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Ölçekleme aralık genişliği";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.changeScaleWidthItem1.SuperTip = superToolTip16;
            this.changeScaleWidthItem1.UseCommandCaption = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // switchTimeScalesCaptionItem1
            // 
            this.switchTimeScalesCaptionItem1.Caption = "Ölçeklendirme Başlıkları";
            this.switchTimeScalesCaptionItem1.Id = 23;
            this.switchTimeScalesCaptionItem1.Name = "switchTimeScalesCaptionItem1";
            this.switchTimeScalesCaptionItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // switchCompressWeekendItem1
            // 
            this.switchCompressWeekendItem1.Id = 24;
            this.switchCompressWeekendItem1.Name = "switchCompressWeekendItem1";
            toolTipTitleItem17.Text = "Bilgi";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Sıkıştırılmış görünüm";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            this.switchCompressWeekendItem1.SuperTip = superToolTip17;
            // 
            // switchShowWorkTimeOnlyItem1
            // 
            this.switchShowWorkTimeOnlyItem1.Id = 25;
            this.switchShowWorkTimeOnlyItem1.Name = "switchShowWorkTimeOnlyItem1";
            toolTipTitleItem18.Text = "Bilgi";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Saatsel gösterim";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.switchShowWorkTimeOnlyItem1.SuperTip = superToolTip18;
            // 
            // switchCellsAutoHeightItem1
            // 
            this.switchCellsAutoHeightItem1.Id = 26;
            this.switchCellsAutoHeightItem1.Name = "switchCellsAutoHeightItem1";
            toolTipTitleItem19.Text = "Bilgi";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Hücresel görünüm";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            this.switchCellsAutoHeightItem1.SuperTip = superToolTip19;
            // 
            // changeSnapToCellsUIItem1
            // 
            this.changeSnapToCellsUIItem1.Id = 27;
            this.changeSnapToCellsUIItem1.Name = "changeSnapToCellsUIItem1";
            toolTipTitleItem20.Text = "Bilgi";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Hücresel görünüm";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            this.changeSnapToCellsUIItem1.SuperTip = superToolTip20;
            // 
            // editAppointmentQueryItem1
            // 
            this.editAppointmentQueryItem1.Caption = "&Aç";
            this.editAppointmentQueryItem1.Id = 28;
            this.editAppointmentQueryItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.editOccurrenceUICommandItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.editSeriesUICommandItem1)});
            this.editAppointmentQueryItem1.Name = "editAppointmentQueryItem1";
            this.editAppointmentQueryItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // editOccurrenceUICommandItem1
            // 
            this.editOccurrenceUICommandItem1.Id = 29;
            this.editOccurrenceUICommandItem1.Name = "editOccurrenceUICommandItem1";
            // 
            // editSeriesUICommandItem1
            // 
            this.editSeriesUICommandItem1.Id = 30;
            this.editSeriesUICommandItem1.Name = "editSeriesUICommandItem1";
            // 
            // deleteAppointmentsItem1
            // 
            this.deleteAppointmentsItem1.Caption = "&Sil";
            this.deleteAppointmentsItem1.Id = 31;
            this.deleteAppointmentsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteOccurrenceItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteSeriesItem1)});
            this.deleteAppointmentsItem1.Name = "deleteAppointmentsItem1";
            this.deleteAppointmentsItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // deleteOccurrenceItem1
            // 
            this.deleteOccurrenceItem1.Id = 32;
            this.deleteOccurrenceItem1.Name = "deleteOccurrenceItem1";
            // 
            // deleteSeriesItem1
            // 
            this.deleteSeriesItem1.Id = 33;
            this.deleteSeriesItem1.Name = "deleteSeriesItem1";
            // 
            // splitAppointmentItem1
            // 
            this.splitAppointmentItem1.Id = 34;
            this.splitAppointmentItem1.Name = "splitAppointmentItem1";
            // 
            // changeAppointmentStatusItem1
            // 
            this.changeAppointmentStatusItem1.Id = 35;
            this.changeAppointmentStatusItem1.Name = "changeAppointmentStatusItem1";
            // 
            // changeAppointmentLabelItem1
            // 
            this.changeAppointmentLabelItem1.Id = 36;
            this.changeAppointmentLabelItem1.Name = "changeAppointmentLabelItem1";
            // 
            // toggleRecurrenceItem1
            // 
            this.toggleRecurrenceItem1.Id = 37;
            this.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1";
            // 
            // changeAppointmentReminderItem1
            // 
            this.changeAppointmentReminderItem1.Edit = this.repositoryItemDuration1;
            this.changeAppointmentReminderItem1.Id = 38;
            this.changeAppointmentReminderItem1.Name = "changeAppointmentReminderItem1";
            this.changeAppointmentReminderItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // schedulerControl2
            // 
            this.schedulerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControl2.Location = new System.Drawing.Point(0, 54);
            this.schedulerControl2.LookAndFeel.SkinName = "Office 2010 Blue";
            this.schedulerControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.schedulerControl2.MenuManager = this.barManager1;
            this.schedulerControl2.Name = "schedulerControl2";
            this.schedulerControl2.OptionsBehavior.RecurrentAppointmentDeleteAction = DevExpress.XtraScheduler.RecurrentAppointmentAction.Cancel;
            this.schedulerControl2.OptionsBehavior.RecurrentAppointmentEditAction = DevExpress.XtraScheduler.RecurrentAppointmentAction.Cancel;
            this.schedulerControl2.OptionsBehavior.ShowRemindersForm = false;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.schedulerControl2.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsCustomization.AllowDisplayAppointmentDependencyForm = DevExpress.XtraScheduler.AllowDisplayAppointmentDependencyForm.Never;
            this.schedulerControl2.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Never;
            this.schedulerControl2.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl2.OptionsView.EnableAnimation = false;
            this.schedulerControl2.Size = new System.Drawing.Size(1198, 376);
            this.schedulerControl2.Start = new System.DateTime(2015, 11, 8, 0, 0, 0, 0);
            this.schedulerControl2.Storage = this.schedulerStorage2;
            this.schedulerControl2.TabIndex = 5;
            this.schedulerControl2.Text = "schedulerControl2";
            this.schedulerControl2.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schedulerControl2.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this.schedulerControl2.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControl2_EditAppointmentFormShowing);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.commonBar1,
            this.printBar1,
            this.appointmentBar1,
            this.navigatorBar1,
            this.arrangeBar1,
            this.groupByBar1,
            this.timeScaleBar1,
            this.layoutBar1,
            this.actionsBar1,
            this.optionsBar1,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.SmallImage;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnOpen,
            this.saveScheduleItem1,
            this.printPreviewItem1,
            this.printItem1,
            this.printPageSetupItem1,
            this.newAppointmentItem1,
            this.newRecurringAppointmentItem1,
            this.navigateViewBackwardItem1,
            this.navigateViewForwardItem1,
            this.gotoTodayItem1,
            this.viewZoomInItem1,
            this.viewZoomOutItem1,
            this.switchToDayViewItem1,
            this.switchToWorkWeekViewItem1,
            this.switchToWeekViewItem1,
            this.switchToMonthViewItem1,
            this.switchToTimelineViewItem1,
            this.switchToGanttViewItem1,
            this.groupByNoneItem1,
            this.groupByDateItem1,
            this.groupByResourceItem1,
            this.switchTimeScalesItem1,
            this.changeScaleWidthItem1,
            this.switchTimeScalesCaptionItem1,
            this.switchCompressWeekendItem1,
            this.switchShowWorkTimeOnlyItem1,
            this.switchCellsAutoHeightItem1,
            this.changeSnapToCellsUIItem1,
            this.editAppointmentQueryItem1,
            this.editOccurrenceUICommandItem1,
            this.editSeriesUICommandItem1,
            this.deleteAppointmentsItem1,
            this.deleteOccurrenceItem1,
            this.deleteSeriesItem1,
            this.splitAppointmentItem1,
            this.changeAppointmentStatusItem1,
            this.changeAppointmentLabelItem1,
            this.toggleRecurrenceItem1,
            this.changeAppointmentReminderItem1,
            this.btnAc,
            this.btnYeni});
            this.barManager1.MaxItemId = 43;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // commonBar1
            // 
            this.commonBar1.Control = this.schedulerControl2;
            this.commonBar1.DockCol = 2;
            this.commonBar1.DockRow = 1;
            this.commonBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.commonBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnOpen),
            new DevExpress.XtraBars.LinkPersistInfo(this.saveScheduleItem1)});
            this.commonBar1.Offset = 315;
            this.commonBar1.Visible = false;
            // 
            // printBar1
            // 
            this.printBar1.Control = this.schedulerControl2;
            this.printBar1.DockCol = 1;
            this.printBar1.DockRow = 0;
            this.printBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.printBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPageSetupItem1)});
            // 
            // appointmentBar1
            // 
            this.appointmentBar1.Control = this.schedulerControl2;
            this.appointmentBar1.DockCol = 4;
            this.appointmentBar1.DockRow = 1;
            this.appointmentBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.appointmentBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.newAppointmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.newRecurringAppointmentItem1)});
            this.appointmentBar1.Offset = 731;
            this.appointmentBar1.Visible = false;
            // 
            // navigatorBar1
            // 
            this.navigatorBar1.Control = this.schedulerControl2;
            this.navigatorBar1.DockCol = 3;
            this.navigatorBar1.DockRow = 0;
            this.navigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.navigatorBar1.FloatLocation = new System.Drawing.Point(394, 200);
            this.navigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.gotoTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomOutItem1)});
            this.navigatorBar1.Offset = 423;
            // 
            // arrangeBar1
            // 
            this.arrangeBar1.Control = this.schedulerControl2;
            this.arrangeBar1.DockCol = 2;
            this.arrangeBar1.DockRow = 0;
            this.arrangeBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.arrangeBar1.FloatLocation = new System.Drawing.Point(228, 195);
            this.arrangeBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToDayViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWorkWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToMonthViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToTimelineViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToGanttViewItem1)});
            this.arrangeBar1.Offset = 122;
            // 
            // groupByBar1
            // 
            this.groupByBar1.Control = this.schedulerControl2;
            this.groupByBar1.DockCol = 1;
            this.groupByBar1.DockRow = 1;
            this.groupByBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.groupByBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByNoneItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByDateItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByResourceItem1)});
            this.groupByBar1.Offset = 179;
            this.groupByBar1.Visible = false;
            // 
            // timeScaleBar1
            // 
            this.timeScaleBar1.Control = this.schedulerControl2;
            this.timeScaleBar1.DockCol = 5;
            this.timeScaleBar1.DockRow = 0;
            this.timeScaleBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.timeScaleBar1.FloatLocation = new System.Drawing.Point(889, 137);
            this.timeScaleBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeScaleWidthItem1)});
            this.timeScaleBar1.Offset = 727;
            // 
            // layoutBar1
            // 
            this.layoutBar1.Control = this.schedulerControl2;
            this.layoutBar1.DockCol = 4;
            this.layoutBar1.DockRow = 0;
            this.layoutBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.layoutBar1.FloatLocation = new System.Drawing.Point(793, 197);
            this.layoutBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchCompressWeekendItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchShowWorkTimeOnlyItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchCellsAutoHeightItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeSnapToCellsUIItem1)});
            this.layoutBar1.Offset = 596;
            // 
            // actionsBar1
            // 
            this.actionsBar1.Control = this.schedulerControl2;
            this.actionsBar1.DockCol = 0;
            this.actionsBar1.DockRow = 1;
            this.actionsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.actionsBar1.FloatLocation = new System.Drawing.Point(160, 175);
            this.actionsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.editAppointmentQueryItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteAppointmentsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.splitAppointmentItem1)});
            this.actionsBar1.Offset = 12;
            this.actionsBar1.Visible = false;
            // 
            // optionsBar1
            // 
            this.optionsBar1.Control = this.schedulerControl2;
            this.optionsBar1.DockCol = 3;
            this.optionsBar1.DockRow = 1;
            this.optionsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.optionsBar1.FloatLocation = new System.Drawing.Point(1066, 191);
            this.optionsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.changeAppointmentStatusItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeAppointmentLabelItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.toggleRecurrenceItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeAppointmentReminderItem1)});
            this.optionsBar1.Offset = 422;
            this.optionsBar1.Visible = false;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 12";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(357, 195);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnYeni),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAc)});
            this.bar1.Text = "Custom 12";
            // 
            // btnYeni
            // 
            this.btnYeni.Caption = "Yeni";
            this.btnYeni.Id = 42;
            this.btnYeni.ImageIndex = 6;
            this.btnYeni.Name = "btnYeni";
            toolTipTitleItem21.Text = "Bilgi";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Yeni takvim kaydı oluşturma";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            this.btnYeni.SuperTip = superToolTip21;
            this.btnYeni.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnYeni_ItemClick);
            // 
            // btnAc
            // 
            this.btnAc.Caption = "Düzelt";
            this.btnAc.Id = 41;
            this.btnAc.ImageIndex = 7;
            this.btnAc.Name = "btnAc";
            toolTipTitleItem22.Text = "Bilgi";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Takvim kaydını düzeltme.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            this.btnAc.SuperTip = superToolTip22;
            this.btnAc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAc_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1203, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 506);
            this.barDockControlBottom.Size = new System.Drawing.Size(1203, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 455);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1203, 51);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 455);
            // 
            // SmallImage
            // 
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(29, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(30, "PassChg.png");
            this.SmallImage.Images.SetKeyName(31, "userNamePass.png");
            this.SmallImage.Images.SetKeyName(32, "cancel.png");
            this.SmallImage.Images.SetKeyName(33, "StockCode.png");
            this.SmallImage.Images.SetKeyName(34, "copy_v2.png");
            this.SmallImage.Images.SetKeyName(35, "SapMini1.jpg");
            this.SmallImage.Images.SetKeyName(36, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(37, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(38, "alceIco3.png");
            this.SmallImage.Images.SetKeyName(39, "Ok.png");
            this.SmallImage.Images.SetKeyName(40, "nook.png");
            this.SmallImage.Images.SetKeyName(41, "table_replace.png");
            this.SmallImage.Images.SetKeyName(42, "+.png");
            // 
            // schedulerStorage2
            // 
            this.schedulerStorage2.AppointmentDependencies.Mappings.DependentId = "DependentId";
            this.schedulerStorage2.AppointmentDependencies.Mappings.ParentId = "ParentId";
            this.schedulerStorage2.AppointmentDependencies.Mappings.Type = "Type";
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.SystemColors.Window, "None", "&None"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(190))))), "Important", "&Important"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(213)))), ((int)(((byte)(255))))), "Business", "&Business"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(244)))), ((int)(((byte)(156))))), "Personal", "&Personal"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(228)))), ((int)(((byte)(199))))), "Vacation", "&Vacation"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(206)))), ((int)(((byte)(147))))), "Must Attend", "Must &Attend"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(244)))), ((int)(((byte)(255))))), "Travel Required", "&Travel Required"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(219)))), ((int)(((byte)(152))))), "Needs Preparation", "&Needs Preparation"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(207)))), ((int)(((byte)(233))))), "Birthday", "&Birthday"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(233)))), ((int)(((byte)(223))))), "Anniversary", "&Anniversary"));
            this.schedulerStorage2.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.Black, "Phone Call", "Phone &Call"));
            this.schedulerStorage2.Appointments.Mappings.AllDay = "AllDay";
            this.schedulerStorage2.Appointments.Mappings.AppointmentId = "UniqueId";
            this.schedulerStorage2.Appointments.Mappings.Description = "Description";
            this.schedulerStorage2.Appointments.Mappings.End = "EndDate";
            this.schedulerStorage2.Appointments.Mappings.Label = "Label";
            this.schedulerStorage2.Appointments.Mappings.Location = "Location";
            this.schedulerStorage2.Appointments.Mappings.PercentComplete = "PercentComplete";
            this.schedulerStorage2.Appointments.Mappings.RecurrenceInfo = "RecurrenceInfo";
            this.schedulerStorage2.Appointments.Mappings.ReminderInfo = "ReminderInfo";
            this.schedulerStorage2.Appointments.Mappings.ResourceId = "ResourceId";
            this.schedulerStorage2.Appointments.Mappings.Start = "StartDate";
            this.schedulerStorage2.Appointments.Mappings.Status = "Status";
            this.schedulerStorage2.Appointments.Mappings.Subject = "Subject";
            this.schedulerStorage2.Appointments.Mappings.Type = "Type";
            this.schedulerStorage2.Resources.CustomFieldMappings.Add(new DevExpress.XtraScheduler.ResourceCustomFieldMapping("IdSort", "IdSort"));
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 51);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1203, 455);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.schedulerControl2);
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1198, 430);
            this.xtraTabPage1.Text = "Takvim Listesi";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.chkIsPRoxyRequired);
            this.xtraTabPage2.Controls.Add(this.CalendarURI);
            this.xtraTabPage2.Controls.Add(this.label1);
            this.xtraTabPage2.Controls.Add(this.DayEvents);
            this.xtraTabPage2.Controls.Add(this.ProxyPort);
            this.xtraTabPage2.Controls.Add(this.label2);
            this.xtraTabPage2.Controls.Add(this.label6);
            this.xtraTabPage2.Controls.Add(this.label3);
            this.xtraTabPage2.Controls.Add(this.ProxyAddress);
            this.xtraTabPage2.Controls.Add(this.UserName);
            this.xtraTabPage2.Controls.Add(this.label5);
            this.xtraTabPage2.Controls.Add(this.Password);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1197, 427);
            this.xtraTabPage2.Text = "Bağlantı Ayarları";
            // 
            // frmCalendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 506);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCalendar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Takvim";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCalendar_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraScheduler.UI.SchedulerBarController schedulerBarController1;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl2;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage2;
        private System.Windows.Forms.ListView DayEvents;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.CheckBox chkIsPRoxyRequired;
        private System.Windows.Forms.Button btnRetrieveCalendars;
        private System.Windows.Forms.TextBox ProxyPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ProxyAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbGoogleCalendar;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CalendarURI;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraScheduler.UI.OpenScheduleItem btnOpen;
        private DevExpress.XtraScheduler.UI.SaveScheduleItem saveScheduleItem1;
        private DevExpress.XtraScheduler.UI.PrintPreviewItem printPreviewItem1;
        private DevExpress.XtraScheduler.UI.PrintItem printItem1;
        private DevExpress.XtraScheduler.UI.PrintPageSetupItem printPageSetupItem1;
        private DevExpress.XtraScheduler.UI.NewAppointmentItem newAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem newRecurringAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewBackwardItem navigateViewBackwardItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewForwardItem navigateViewForwardItem1;
        private DevExpress.XtraScheduler.UI.GotoTodayItem gotoTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomInItem viewZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomOutItem viewZoomOutItem1;
        private DevExpress.XtraScheduler.UI.SwitchToDayViewItem switchToDayViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem switchToWorkWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWeekViewItem switchToWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToMonthViewItem switchToMonthViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem switchToTimelineViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToGanttViewItem switchToGanttViewItem1;
        private DevExpress.XtraScheduler.UI.GroupByNoneItem groupByNoneItem1;
        private DevExpress.XtraScheduler.UI.GroupByDateItem groupByDateItem1;
        private DevExpress.XtraScheduler.UI.GroupByResourceItem groupByResourceItem1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesItem switchTimeScalesItem1;
        private DevExpress.XtraScheduler.UI.ChangeScaleWidthItem changeScaleWidthItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem switchTimeScalesCaptionItem1;
        private DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem switchCompressWeekendItem1;
        private DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem switchShowWorkTimeOnlyItem1;
        private DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem switchCellsAutoHeightItem1;
        private DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem changeSnapToCellsUIItem1;
        private DevExpress.XtraScheduler.UI.EditAppointmentQueryItem editAppointmentQueryItem1;
        private DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem editOccurrenceUICommandItem1;
        private DevExpress.XtraScheduler.UI.EditSeriesUICommandItem editSeriesUICommandItem1;
        private DevExpress.XtraScheduler.UI.DeleteAppointmentsItem deleteAppointmentsItem1;
        private DevExpress.XtraScheduler.UI.DeleteOccurrenceItem deleteOccurrenceItem1;
        private DevExpress.XtraScheduler.UI.DeleteSeriesItem deleteSeriesItem1;
        private DevExpress.XtraScheduler.UI.SplitAppointmentItem splitAppointmentItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem changeAppointmentStatusItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem changeAppointmentLabelItem1;
        private DevExpress.XtraScheduler.UI.ToggleRecurrenceItem toggleRecurrenceItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem changeAppointmentReminderItem1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraScheduler.UI.CommonBar commonBar1;
        private DevExpress.XtraScheduler.UI.PrintBar printBar1;
        private DevExpress.XtraScheduler.UI.AppointmentBar appointmentBar1;
        private DevExpress.XtraScheduler.UI.NavigatorBar navigatorBar1;
        private DevExpress.XtraScheduler.UI.ArrangeBar arrangeBar1;
        private DevExpress.XtraScheduler.UI.GroupByBar groupByBar1;
        private DevExpress.XtraScheduler.UI.TimeScaleBar timeScaleBar1;
        private DevExpress.XtraScheduler.UI.LayoutBar layoutBar1;
        private DevExpress.XtraScheduler.UI.ActionsBar actionsBar1;
        private DevExpress.XtraScheduler.UI.OptionsBar optionsBar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnAc;
        private DevExpress.XtraBars.BarButtonItem btnYeni;
        private DevExpress.Utils.ImageCollection SmallImage;
    }
}