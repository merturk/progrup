﻿namespace GABI_CRM
{
    partial class RepMusteriEtiket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.pnl1 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl1Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl1Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl1Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl1Lbl4 = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 340F;
            this.Detail.MultiColumn.ColumnSpacing = 30F;
            this.Detail.MultiColumn.ColumnWidth = 638F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // pnl1
            // 
            this.pnl1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl1.CanGrow = false;
            this.pnl1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl1Lbl3,
            this.pnl1Lbl1,
            this.pnl1Lbl2,
            this.pnl1Lbl4});
            this.pnl1.Dpi = 254F;
            this.pnl1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.pnl1.Name = "pnl1";
            this.pnl1.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl1.StylePriority.UseBorders = false;
            // 
            // pnl1Lbl3
            // 
            this.pnl1Lbl3.Dpi = 254F;
            this.pnl1Lbl3.Font = new System.Drawing.Font("Arial", 7F);
            this.pnl1Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(4.624944F, 92.46613F);
            this.pnl1Lbl3.Name = "pnl1Lbl3";
            this.pnl1Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl1Lbl3.SizeF = new System.Drawing.SizeF(630.7499F, 30F);
            this.pnl1Lbl3.StylePriority.UseFont = false;
            this.pnl1Lbl3.StylePriority.UseTextAlignment = false;
            this.pnl1Lbl3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl1Lbl1
            // 
            this.pnl1Lbl1.Dpi = 254F;
            this.pnl1Lbl1.Font = new System.Drawing.Font("Arial", 7F);
            this.pnl1Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(4.624944F, 28.56369F);
            this.pnl1Lbl1.Name = "pnl1Lbl1";
            this.pnl1Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl1Lbl1.SizeF = new System.Drawing.SizeF(630.7501F, 30F);
            this.pnl1Lbl1.StylePriority.UseFont = false;
            this.pnl1Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl1Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl1Lbl2
            // 
            this.pnl1Lbl2.Dpi = 254F;
            this.pnl1Lbl2.Font = new System.Drawing.Font("Arial", 7F);
            this.pnl1Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(4.624944F, 59.51612F);
            this.pnl1Lbl2.Name = "pnl1Lbl2";
            this.pnl1Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl1Lbl2.SizeF = new System.Drawing.SizeF(630.7499F, 30F);
            this.pnl1Lbl2.StylePriority.UseFont = false;
            this.pnl1Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl1Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl1Lbl4
            // 
            this.pnl1Lbl4.Dpi = 254F;
            this.pnl1Lbl4.Font = new System.Drawing.Font("Arial", 7F);
            this.pnl1Lbl4.LocationFloat = new DevExpress.Utils.PointFloat(4.624944F, 124.436F);
            this.pnl1Lbl4.Name = "pnl1Lbl4";
            this.pnl1Lbl4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl1Lbl4.SizeF = new System.Drawing.SizeF(630.7499F, 30F);
            this.pnl1Lbl4.StylePriority.UseFont = false;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 119F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 130F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // RepMusteriEtiket
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(61, 61, 119, 130);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 24;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 31.75F;
            this.Version = "13.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RepMusteriEtiket_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRPanel pnl1;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl4;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl3;
    }
}
