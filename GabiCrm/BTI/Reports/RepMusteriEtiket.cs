﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Global;

namespace GABI_CRM
{
    public partial class RepMusteriEtiket : DevExpress.XtraReports.UI.XtraReport
    {
        public RepMusteriEtiket()
        {
            InitializeComponent();
        }

        public static string EmpId;

        private void GetList()
        {
            string sql = (string.Format(@"                       
						Select *,'SAYIN, '+Yetkili as FYetkili From dbo.MusteriListesi Where EmpID in ({0}) "
                 , EmpId));
            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(sql);
            if (dt.Rows.Count <= 0) return;
            DataSource = dt;

            pnl1Lbl1.DataBindings.Add("Text", DataSource, "FYetkili");
            pnl1Lbl2.DataBindings.Add("Text", DataSource, "Adi");
            pnl1Lbl3.DataBindings.Add("Text", DataSource, "Gorevi");
            pnl1Lbl4.DataBindings.Add("Text", DataSource, "Adres2");
        
        }

        private void RepMusteriEtiket_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            GetList();
        }


    }
}
