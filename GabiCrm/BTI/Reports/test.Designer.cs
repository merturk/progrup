﻿namespace GABI_CRM
{
    partial class test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl1 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrPanel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl3 = new DevExpress.XtraReports.UI.XRPanel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.pnl6 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl5 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl4 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl9 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl8 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl7 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl10 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl11 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl12 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl15 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl14 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl13 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl16 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl17 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl18 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl24 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl23 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl22 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl19 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl20 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl21 = new DevExpress.XtraReports.UI.XRPanel();
            this.pnl1Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl1Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl1Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl2Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl2Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl2Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl3Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl3Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl3Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl4Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl4Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl4Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl5Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl5Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl5Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl6Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl6Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl6Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl7Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl7Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl7Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl8Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl8Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl8Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl9Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl9Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl9Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl10Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl10Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl10Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl11Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl11Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl11Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl12Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl12Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl12Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl13Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl13Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl13Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl14Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl14Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl14Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl15Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl15Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl15Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl16Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl16Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl16Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl17Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl17Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl17Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl18Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl18Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl18Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl19Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl19Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl19Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl20Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl20Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl20Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl21Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl21Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl21Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl22Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl22Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl22Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl23Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl23Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl23Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl24Lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl24Lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pnl24Lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl15,
            this.pnl20,
            this.pnl19,
            this.pnl22,
            this.pnl23,
            this.pnl24,
            this.pnl18,
            this.pnl17,
            this.pnl16,
            this.pnl13,
            this.pnl14,
            this.pnl21,
            this.pnl9,
            this.pnl8,
            this.pnl7,
            this.pnl10,
            this.pnl11,
            this.pnl12,
            this.pnl6,
            this.pnl5,
            this.pnl4,
            this.xrPanel2,
            this.pnl1,
            this.pnl2,
            this.xrPanel4,
            this.pnl3});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 2725.125F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel2
            // 
            this.xrPanel2.Dpi = 254F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(640.75F, 0F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(30F, 340F);
            this.xrPanel2.Visible = false;
            // 
            // pnl1
            // 
            this.pnl1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl1Lbl3,
            this.pnl1Lbl2,
            this.pnl1Lbl1});
            this.pnl1.Dpi = 254F;
            this.pnl1.LocationFloat = new DevExpress.Utils.PointFloat(1F, 0F);
            this.pnl1.Name = "pnl1";
            this.pnl1.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl1.StylePriority.UseBorders = false;
            // 
            // pnl2
            // 
            this.pnl2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl2Lbl1,
            this.pnl2Lbl2,
            this.pnl2Lbl3});
            this.pnl2.Dpi = 254F;
            this.pnl2.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 0F);
            this.pnl2.Name = "pnl2";
            this.pnl2.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl2.StylePriority.UseBorders = false;
            // 
            // xrPanel4
            // 
            this.xrPanel4.Dpi = 254F;
            this.xrPanel4.LocationFloat = new DevExpress.Utils.PointFloat(1310.625F, 1.614889E-05F);
            this.xrPanel4.Name = "xrPanel4";
            this.xrPanel4.SizeF = new System.Drawing.SizeF(30F, 340F);
            this.xrPanel4.Visible = false;
            // 
            // pnl3
            // 
            this.pnl3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl3Lbl1,
            this.pnl3Lbl2,
            this.pnl3Lbl3});
            this.pnl3.Dpi = 254F;
            this.pnl3.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 0F);
            this.pnl3.Name = "pnl3";
            this.pnl3.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl3.StylePriority.UseBorders = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 119F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 130F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // pnl6
            // 
            this.pnl6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl6Lbl1,
            this.pnl6Lbl2,
            this.pnl6Lbl3});
            this.pnl6.Dpi = 254F;
            this.pnl6.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 338.5104F);
            this.pnl6.Name = "pnl6";
            this.pnl6.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl6.StylePriority.UseBorders = false;
            // 
            // pnl5
            // 
            this.pnl5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl5Lbl1,
            this.pnl5Lbl2,
            this.pnl5Lbl3});
            this.pnl5.Dpi = 254F;
            this.pnl5.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 338.5104F);
            this.pnl5.Name = "pnl5";
            this.pnl5.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl5.StylePriority.UseBorders = false;
            // 
            // pnl4
            // 
            this.pnl4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl4Lbl1,
            this.pnl4Lbl2,
            this.pnl4Lbl3});
            this.pnl4.Dpi = 254F;
            this.pnl4.LocationFloat = new DevExpress.Utils.PointFloat(1F, 338.5104F);
            this.pnl4.Name = "pnl4";
            this.pnl4.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl4.StylePriority.UseBorders = false;
            // 
            // pnl9
            // 
            this.pnl9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl9Lbl1,
            this.pnl9Lbl2,
            this.pnl9Lbl3});
            this.pnl9.Dpi = 254F;
            this.pnl9.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 677.6946F);
            this.pnl9.Name = "pnl9";
            this.pnl9.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl9.StylePriority.UseBorders = false;
            // 
            // pnl8
            // 
            this.pnl8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl8Lbl1,
            this.pnl8Lbl2,
            this.pnl8Lbl3});
            this.pnl8.Dpi = 254F;
            this.pnl8.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 677.6946F);
            this.pnl8.Name = "pnl8";
            this.pnl8.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl8.StylePriority.UseBorders = false;
            // 
            // pnl7
            // 
            this.pnl7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl7Lbl1,
            this.pnl7Lbl2,
            this.pnl7Lbl3});
            this.pnl7.Dpi = 254F;
            this.pnl7.LocationFloat = new DevExpress.Utils.PointFloat(1F, 677.6946F);
            this.pnl7.Name = "pnl7";
            this.pnl7.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl7.StylePriority.UseBorders = false;
            // 
            // pnl10
            // 
            this.pnl10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl10Lbl1,
            this.pnl10Lbl2,
            this.pnl10Lbl3});
            this.pnl10.Dpi = 254F;
            this.pnl10.LocationFloat = new DevExpress.Utils.PointFloat(1F, 1020.205F);
            this.pnl10.Name = "pnl10";
            this.pnl10.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl10.StylePriority.UseBorders = false;
            // 
            // pnl11
            // 
            this.pnl11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl11Lbl1,
            this.pnl11Lbl2,
            this.pnl11Lbl3});
            this.pnl11.Dpi = 254F;
            this.pnl11.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 1020.205F);
            this.pnl11.Name = "pnl11";
            this.pnl11.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl11.StylePriority.UseBorders = false;
            // 
            // pnl12
            // 
            this.pnl12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl12Lbl1,
            this.pnl12Lbl2,
            this.pnl12Lbl3});
            this.pnl12.Dpi = 254F;
            this.pnl12.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 1020.205F);
            this.pnl12.Name = "pnl12";
            this.pnl12.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl12.StylePriority.UseBorders = false;
            // 
            // pnl15
            // 
            this.pnl15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl15Lbl1,
            this.pnl15Lbl2,
            this.pnl15Lbl3});
            this.pnl15.Dpi = 254F;
            this.pnl15.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 1359.92F);
            this.pnl15.Name = "pnl15";
            this.pnl15.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl15.StylePriority.UseBorders = false;
            // 
            // pnl14
            // 
            this.pnl14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl14Lbl1,
            this.pnl14Lbl2,
            this.pnl14Lbl3});
            this.pnl14.Dpi = 254F;
            this.pnl14.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 1359.92F);
            this.pnl14.Name = "pnl14";
            this.pnl14.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl14.StylePriority.UseBorders = false;
            // 
            // pnl13
            // 
            this.pnl13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl13Lbl1,
            this.pnl13Lbl2,
            this.pnl13Lbl3});
            this.pnl13.Dpi = 254F;
            this.pnl13.LocationFloat = new DevExpress.Utils.PointFloat(1F, 1359.92F);
            this.pnl13.Name = "pnl13";
            this.pnl13.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl13.StylePriority.UseBorders = false;
            // 
            // pnl16
            // 
            this.pnl16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl16Lbl1,
            this.pnl16Lbl2,
            this.pnl16Lbl3});
            this.pnl16.Dpi = 254F;
            this.pnl16.LocationFloat = new DevExpress.Utils.PointFloat(1F, 1701.431F);
            this.pnl16.Name = "pnl16";
            this.pnl16.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl16.StylePriority.UseBorders = false;
            // 
            // pnl17
            // 
            this.pnl17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl17Lbl1,
            this.pnl17Lbl2,
            this.pnl17Lbl3});
            this.pnl17.Dpi = 254F;
            this.pnl17.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 1701.431F);
            this.pnl17.Name = "pnl17";
            this.pnl17.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl17.StylePriority.UseBorders = false;
            // 
            // pnl18
            // 
            this.pnl18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl18Lbl1,
            this.pnl18Lbl2,
            this.pnl18Lbl3});
            this.pnl18.Dpi = 254F;
            this.pnl18.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 1701.431F);
            this.pnl18.Name = "pnl18";
            this.pnl18.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl18.StylePriority.UseBorders = false;
            // 
            // pnl24
            // 
            this.pnl24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl24Lbl1,
            this.pnl24Lbl2,
            this.pnl24Lbl3});
            this.pnl24.Dpi = 254F;
            this.pnl24.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 2378.125F);
            this.pnl24.Name = "pnl24";
            this.pnl24.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl24.StylePriority.UseBorders = false;
            // 
            // pnl23
            // 
            this.pnl23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl23Lbl1,
            this.pnl23Lbl2,
            this.pnl23Lbl3});
            this.pnl23.Dpi = 254F;
            this.pnl23.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 2378.125F);
            this.pnl23.Name = "pnl23";
            this.pnl23.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl23.StylePriority.UseBorders = false;
            // 
            // pnl22
            // 
            this.pnl22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl22Lbl1,
            this.pnl22Lbl2,
            this.pnl22Lbl3});
            this.pnl22.Dpi = 254F;
            this.pnl22.LocationFloat = new DevExpress.Utils.PointFloat(1F, 2378.125F);
            this.pnl22.Name = "pnl22";
            this.pnl22.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl22.StylePriority.UseBorders = false;
            // 
            // pnl19
            // 
            this.pnl19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl19Lbl1,
            this.pnl19Lbl2,
            this.pnl19Lbl3});
            this.pnl19.Dpi = 254F;
            this.pnl19.LocationFloat = new DevExpress.Utils.PointFloat(1F, 2041.615F);
            this.pnl19.Name = "pnl19";
            this.pnl19.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl19.StylePriority.UseBorders = false;
            // 
            // pnl20
            // 
            this.pnl20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl20Lbl1,
            this.pnl20Lbl2,
            this.pnl20Lbl3});
            this.pnl20.Dpi = 254F;
            this.pnl20.LocationFloat = new DevExpress.Utils.PointFloat(669.75F, 2041.615F);
            this.pnl20.Name = "pnl20";
            this.pnl20.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl20.StylePriority.UseBorders = false;
            // 
            // pnl21
            // 
            this.pnl21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pnl21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pnl21Lbl1,
            this.pnl21Lbl2,
            this.pnl21Lbl3});
            this.pnl21.Dpi = 254F;
            this.pnl21.LocationFloat = new DevExpress.Utils.PointFloat(1338.625F, 2041.615F);
            this.pnl21.Name = "pnl21";
            this.pnl21.SizeF = new System.Drawing.SizeF(640F, 340F);
            this.pnl21.StylePriority.UseBorders = false;
            // 
            // pnl1Lbl1
            // 
            this.pnl1Lbl1.Dpi = 254F;
            this.pnl1Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl1Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(8.999928F, 40.99999F);
            this.pnl1Lbl1.Name = "pnl1Lbl1";
            this.pnl1Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.pnl1Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl1Lbl1.StylePriority.UseFont = false;
            this.pnl1Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl1Lbl1.Text = " ";
            this.pnl1Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl1Lbl2
            // 
            this.pnl1Lbl2.Dpi = 254F;
            this.pnl1Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl1Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(8.999928F, 139.2887F);
            this.pnl1Lbl2.Name = "pnl1Lbl2";
            this.pnl1Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl1Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl1Lbl2.StylePriority.UseFont = false;
            this.pnl1Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl1Lbl2.Text = " ";
            this.pnl1Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl1Lbl3
            // 
            this.pnl1Lbl3.Dpi = 254F;
            this.pnl1Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl1Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(8.999928F, 209.3148F);
            this.pnl1Lbl3.Name = "pnl1Lbl3";
            this.pnl1Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl1Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl1Lbl3.StylePriority.UseFont = false;
            this.pnl1Lbl3.Text = " ";
            // 
            // pnl2Lbl1
            // 
            this.pnl2Lbl1.Dpi = 254F;
            this.pnl2Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl2Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl2Lbl1.Name = "pnl2Lbl1";
            this.pnl2Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl2Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl2Lbl1.StylePriority.UseFont = false;
            this.pnl2Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl2Lbl1.Text = " ";
            this.pnl2Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl2Lbl2
            // 
            this.pnl2Lbl2.Dpi = 254F;
            this.pnl2Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl2Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl2Lbl2.Name = "pnl2Lbl2";
            this.pnl2Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl2Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl2Lbl2.StylePriority.UseFont = false;
            this.pnl2Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl2Lbl2.Text = " ";
            this.pnl2Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl2Lbl3
            // 
            this.pnl2Lbl3.Dpi = 254F;
            this.pnl2Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl2Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl2Lbl3.Name = "pnl2Lbl3";
            this.pnl2Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl2Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl2Lbl3.StylePriority.UseFont = false;
            this.pnl2Lbl3.Text = " ";
            // 
            // pnl3Lbl1
            // 
            this.pnl3Lbl1.Dpi = 254F;
            this.pnl3Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl3Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl3Lbl1.Name = "pnl3Lbl1";
            this.pnl3Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl3Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl3Lbl1.StylePriority.UseFont = false;
            this.pnl3Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl3Lbl1.Text = " ";
            this.pnl3Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl3Lbl2
            // 
            this.pnl3Lbl2.Dpi = 254F;
            this.pnl3Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl3Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl3Lbl2.Name = "pnl3Lbl2";
            this.pnl3Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl3Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl3Lbl2.StylePriority.UseFont = false;
            this.pnl3Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl3Lbl2.Text = " ";
            this.pnl3Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl3Lbl3
            // 
            this.pnl3Lbl3.Dpi = 254F;
            this.pnl3Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl3Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl3Lbl3.Name = "pnl3Lbl3";
            this.pnl3Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl3Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl3Lbl3.StylePriority.UseFont = false;
            this.pnl3Lbl3.Text = " ";
            // 
            // pnl4Lbl1
            // 
            this.pnl4Lbl1.Dpi = 254F;
            this.pnl4Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl4Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl4Lbl1.Name = "pnl4Lbl1";
            this.pnl4Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl4Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl4Lbl1.StylePriority.UseFont = false;
            this.pnl4Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl4Lbl1.Text = " ";
            this.pnl4Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl4Lbl2
            // 
            this.pnl4Lbl2.Dpi = 254F;
            this.pnl4Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl4Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl4Lbl2.Name = "pnl4Lbl2";
            this.pnl4Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl4Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl4Lbl2.StylePriority.UseFont = false;
            this.pnl4Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl4Lbl2.Text = " ";
            this.pnl4Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl4Lbl3
            // 
            this.pnl4Lbl3.Dpi = 254F;
            this.pnl4Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl4Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl4Lbl3.Name = "pnl4Lbl3";
            this.pnl4Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl4Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl4Lbl3.StylePriority.UseFont = false;
            this.pnl4Lbl3.Text = " ";
            // 
            // pnl5Lbl1
            // 
            this.pnl5Lbl1.Dpi = 254F;
            this.pnl5Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl5Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl5Lbl1.Name = "pnl5Lbl1";
            this.pnl5Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl5Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl5Lbl1.StylePriority.UseFont = false;
            this.pnl5Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl5Lbl1.Text = " ";
            this.pnl5Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl5Lbl2
            // 
            this.pnl5Lbl2.Dpi = 254F;
            this.pnl5Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl5Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl5Lbl2.Name = "pnl5Lbl2";
            this.pnl5Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl5Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl5Lbl2.StylePriority.UseFont = false;
            this.pnl5Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl5Lbl2.Text = " ";
            this.pnl5Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl5Lbl3
            // 
            this.pnl5Lbl3.Dpi = 254F;
            this.pnl5Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl5Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl5Lbl3.Name = "pnl5Lbl3";
            this.pnl5Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl5Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl5Lbl3.StylePriority.UseFont = false;
            this.pnl5Lbl3.Text = " ";
            // 
            // pnl6Lbl1
            // 
            this.pnl6Lbl1.Dpi = 254F;
            this.pnl6Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl6Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl6Lbl1.Name = "pnl6Lbl1";
            this.pnl6Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl6Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl6Lbl1.StylePriority.UseFont = false;
            this.pnl6Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl6Lbl1.Text = " ";
            this.pnl6Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl6Lbl2
            // 
            this.pnl6Lbl2.Dpi = 254F;
            this.pnl6Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl6Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl6Lbl2.Name = "pnl6Lbl2";
            this.pnl6Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl6Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl6Lbl2.StylePriority.UseFont = false;
            this.pnl6Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl6Lbl2.Text = " ";
            this.pnl6Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl6Lbl3
            // 
            this.pnl6Lbl3.Dpi = 254F;
            this.pnl6Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl6Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl6Lbl3.Name = "pnl6Lbl3";
            this.pnl6Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl6Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl6Lbl3.StylePriority.UseFont = false;
            this.pnl6Lbl3.Text = " ";
            // 
            // pnl7Lbl1
            // 
            this.pnl7Lbl1.Dpi = 254F;
            this.pnl7Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl7Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl7Lbl1.Name = "pnl7Lbl1";
            this.pnl7Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl7Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl7Lbl1.StylePriority.UseFont = false;
            this.pnl7Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl7Lbl1.Text = " ";
            this.pnl7Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl7Lbl2
            // 
            this.pnl7Lbl2.Dpi = 254F;
            this.pnl7Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl7Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl7Lbl2.Name = "pnl7Lbl2";
            this.pnl7Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl7Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl7Lbl2.StylePriority.UseFont = false;
            this.pnl7Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl7Lbl2.Text = " ";
            this.pnl7Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl7Lbl3
            // 
            this.pnl7Lbl3.Dpi = 254F;
            this.pnl7Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl7Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl7Lbl3.Name = "pnl7Lbl3";
            this.pnl7Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl7Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl7Lbl3.StylePriority.UseFont = false;
            this.pnl7Lbl3.Text = " ";
            // 
            // pnl8Lbl1
            // 
            this.pnl8Lbl1.Dpi = 254F;
            this.pnl8Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl8Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl8Lbl1.Name = "pnl8Lbl1";
            this.pnl8Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl8Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl8Lbl1.StylePriority.UseFont = false;
            this.pnl8Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl8Lbl1.Text = " ";
            this.pnl8Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl8Lbl2
            // 
            this.pnl8Lbl2.Dpi = 254F;
            this.pnl8Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl8Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl8Lbl2.Name = "pnl8Lbl2";
            this.pnl8Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl8Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl8Lbl2.StylePriority.UseFont = false;
            this.pnl8Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl8Lbl2.Text = " ";
            this.pnl8Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl8Lbl3
            // 
            this.pnl8Lbl3.Dpi = 254F;
            this.pnl8Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl8Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl8Lbl3.Name = "pnl8Lbl3";
            this.pnl8Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl8Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl8Lbl3.StylePriority.UseFont = false;
            this.pnl8Lbl3.Text = " ";
            // 
            // pnl9Lbl1
            // 
            this.pnl9Lbl1.Dpi = 254F;
            this.pnl9Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl9Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl9Lbl1.Name = "pnl9Lbl1";
            this.pnl9Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl9Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl9Lbl1.StylePriority.UseFont = false;
            this.pnl9Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl9Lbl1.Text = " ";
            this.pnl9Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl9Lbl2
            // 
            this.pnl9Lbl2.Dpi = 254F;
            this.pnl9Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl9Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl9Lbl2.Name = "pnl9Lbl2";
            this.pnl9Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl9Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl9Lbl2.StylePriority.UseFont = false;
            this.pnl9Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl9Lbl2.Text = " ";
            this.pnl9Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl9Lbl3
            // 
            this.pnl9Lbl3.Dpi = 254F;
            this.pnl9Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl9Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl9Lbl3.Name = "pnl9Lbl3";
            this.pnl9Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl9Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl9Lbl3.StylePriority.UseFont = false;
            this.pnl9Lbl3.Text = " ";
            // 
            // pnl10Lbl1
            // 
            this.pnl10Lbl1.Dpi = 254F;
            this.pnl10Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl10Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl10Lbl1.Name = "pnl10Lbl1";
            this.pnl10Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl10Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl10Lbl1.StylePriority.UseFont = false;
            this.pnl10Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl10Lbl1.Text = " ";
            this.pnl10Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl10Lbl2
            // 
            this.pnl10Lbl2.Dpi = 254F;
            this.pnl10Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl10Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl10Lbl2.Name = "pnl10Lbl2";
            this.pnl10Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl10Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl10Lbl2.StylePriority.UseFont = false;
            this.pnl10Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl10Lbl2.Text = " ";
            this.pnl10Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl10Lbl3
            // 
            this.pnl10Lbl3.Dpi = 254F;
            this.pnl10Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl10Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl10Lbl3.Name = "pnl10Lbl3";
            this.pnl10Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl10Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl10Lbl3.StylePriority.UseFont = false;
            this.pnl10Lbl3.Text = " ";
            // 
            // pnl11Lbl1
            // 
            this.pnl11Lbl1.Dpi = 254F;
            this.pnl11Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl11Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl11Lbl1.Name = "pnl11Lbl1";
            this.pnl11Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl11Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl11Lbl1.StylePriority.UseFont = false;
            this.pnl11Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl11Lbl1.Text = " ";
            this.pnl11Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl11Lbl2
            // 
            this.pnl11Lbl2.Dpi = 254F;
            this.pnl11Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl11Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl11Lbl2.Name = "pnl11Lbl2";
            this.pnl11Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl11Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl11Lbl2.StylePriority.UseFont = false;
            this.pnl11Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl11Lbl2.Text = " ";
            this.pnl11Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl11Lbl3
            // 
            this.pnl11Lbl3.Dpi = 254F;
            this.pnl11Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl11Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl11Lbl3.Name = "pnl11Lbl3";
            this.pnl11Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl11Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl11Lbl3.StylePriority.UseFont = false;
            this.pnl11Lbl3.Text = " ";
            // 
            // pnl12Lbl1
            // 
            this.pnl12Lbl1.Dpi = 254F;
            this.pnl12Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl12Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl12Lbl1.Name = "pnl12Lbl1";
            this.pnl12Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl12Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl12Lbl1.StylePriority.UseFont = false;
            this.pnl12Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl12Lbl1.Text = " ";
            this.pnl12Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl12Lbl2
            // 
            this.pnl12Lbl2.Dpi = 254F;
            this.pnl12Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl12Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl12Lbl2.Name = "pnl12Lbl2";
            this.pnl12Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl12Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl12Lbl2.StylePriority.UseFont = false;
            this.pnl12Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl12Lbl2.Text = " ";
            this.pnl12Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl12Lbl3
            // 
            this.pnl12Lbl3.Dpi = 254F;
            this.pnl12Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl12Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl12Lbl3.Name = "pnl12Lbl3";
            this.pnl12Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl12Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl12Lbl3.StylePriority.UseFont = false;
            this.pnl12Lbl3.Text = " ";
            // 
            // pnl13Lbl1
            // 
            this.pnl13Lbl1.Dpi = 254F;
            this.pnl13Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl13Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl13Lbl1.Name = "pnl13Lbl1";
            this.pnl13Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl13Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl13Lbl1.StylePriority.UseFont = false;
            this.pnl13Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl13Lbl1.Text = " ";
            this.pnl13Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl13Lbl2
            // 
            this.pnl13Lbl2.Dpi = 254F;
            this.pnl13Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl13Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl13Lbl2.Name = "pnl13Lbl2";
            this.pnl13Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl13Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl13Lbl2.StylePriority.UseFont = false;
            this.pnl13Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl13Lbl2.Text = " ";
            this.pnl13Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl13Lbl3
            // 
            this.pnl13Lbl3.Dpi = 254F;
            this.pnl13Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl13Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl13Lbl3.Name = "pnl13Lbl3";
            this.pnl13Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl13Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl13Lbl3.StylePriority.UseFont = false;
            this.pnl13Lbl3.Text = " ";
            // 
            // pnl14Lbl1
            // 
            this.pnl14Lbl1.Dpi = 254F;
            this.pnl14Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl14Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl14Lbl1.Name = "pnl14Lbl1";
            this.pnl14Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl14Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl14Lbl1.StylePriority.UseFont = false;
            this.pnl14Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl14Lbl1.Text = " ";
            this.pnl14Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl14Lbl2
            // 
            this.pnl14Lbl2.Dpi = 254F;
            this.pnl14Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl14Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl14Lbl2.Name = "pnl14Lbl2";
            this.pnl14Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl14Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl14Lbl2.StylePriority.UseFont = false;
            this.pnl14Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl14Lbl2.Text = " ";
            this.pnl14Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl14Lbl3
            // 
            this.pnl14Lbl3.Dpi = 254F;
            this.pnl14Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl14Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl14Lbl3.Name = "pnl14Lbl3";
            this.pnl14Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl14Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl14Lbl3.StylePriority.UseFont = false;
            this.pnl14Lbl3.Text = " ";
            // 
            // pnl15Lbl1
            // 
            this.pnl15Lbl1.Dpi = 254F;
            this.pnl15Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl15Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl15Lbl1.Name = "pnl15Lbl1";
            this.pnl15Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl15Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl15Lbl1.StylePriority.UseFont = false;
            this.pnl15Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl15Lbl1.Text = " ";
            this.pnl15Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl15Lbl2
            // 
            this.pnl15Lbl2.Dpi = 254F;
            this.pnl15Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl15Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl15Lbl2.Name = "pnl15Lbl2";
            this.pnl15Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl15Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl15Lbl2.StylePriority.UseFont = false;
            this.pnl15Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl15Lbl2.Text = " ";
            this.pnl15Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl15Lbl3
            // 
            this.pnl15Lbl3.Dpi = 254F;
            this.pnl15Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl15Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl15Lbl3.Name = "pnl15Lbl3";
            this.pnl15Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl15Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl15Lbl3.StylePriority.UseFont = false;
            this.pnl15Lbl3.Text = " ";
            // 
            // pnl16Lbl1
            // 
            this.pnl16Lbl1.Dpi = 254F;
            this.pnl16Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl16Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl16Lbl1.Name = "pnl16Lbl1";
            this.pnl16Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl16Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl16Lbl1.StylePriority.UseFont = false;
            this.pnl16Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl16Lbl1.Text = " ";
            this.pnl16Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl16Lbl2
            // 
            this.pnl16Lbl2.Dpi = 254F;
            this.pnl16Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl16Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl16Lbl2.Name = "pnl16Lbl2";
            this.pnl16Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl16Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl16Lbl2.StylePriority.UseFont = false;
            this.pnl16Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl16Lbl2.Text = " ";
            this.pnl16Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl16Lbl3
            // 
            this.pnl16Lbl3.Dpi = 254F;
            this.pnl16Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl16Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl16Lbl3.Name = "pnl16Lbl3";
            this.pnl16Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl16Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl16Lbl3.StylePriority.UseFont = false;
            this.pnl16Lbl3.Text = " ";
            // 
            // pnl17Lbl1
            // 
            this.pnl17Lbl1.Dpi = 254F;
            this.pnl17Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl17Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl17Lbl1.Name = "pnl17Lbl1";
            this.pnl17Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl17Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl17Lbl1.StylePriority.UseFont = false;
            this.pnl17Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl17Lbl1.Text = " ";
            this.pnl17Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl17Lbl2
            // 
            this.pnl17Lbl2.Dpi = 254F;
            this.pnl17Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl17Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl17Lbl2.Name = "pnl17Lbl2";
            this.pnl17Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl17Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl17Lbl2.StylePriority.UseFont = false;
            this.pnl17Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl17Lbl2.Text = " ";
            this.pnl17Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl17Lbl3
            // 
            this.pnl17Lbl3.Dpi = 254F;
            this.pnl17Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl17Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl17Lbl3.Name = "pnl17Lbl3";
            this.pnl17Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl17Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl17Lbl3.StylePriority.UseFont = false;
            this.pnl17Lbl3.Text = " ";
            // 
            // pnl18Lbl1
            // 
            this.pnl18Lbl1.Dpi = 254F;
            this.pnl18Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl18Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl18Lbl1.Name = "pnl18Lbl1";
            this.pnl18Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl18Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl18Lbl1.StylePriority.UseFont = false;
            this.pnl18Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl18Lbl1.Text = " ";
            this.pnl18Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl18Lbl2
            // 
            this.pnl18Lbl2.Dpi = 254F;
            this.pnl18Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl18Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl18Lbl2.Name = "pnl18Lbl2";
            this.pnl18Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl18Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl18Lbl2.StylePriority.UseFont = false;
            this.pnl18Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl18Lbl2.Text = " ";
            this.pnl18Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl18Lbl3
            // 
            this.pnl18Lbl3.Dpi = 254F;
            this.pnl18Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl18Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl18Lbl3.Name = "pnl18Lbl3";
            this.pnl18Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl18Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl18Lbl3.StylePriority.UseFont = false;
            this.pnl18Lbl3.Text = " ";
            // 
            // pnl19Lbl1
            // 
            this.pnl19Lbl1.Dpi = 254F;
            this.pnl19Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl19Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl19Lbl1.Name = "pnl19Lbl1";
            this.pnl19Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl19Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl19Lbl1.StylePriority.UseFont = false;
            this.pnl19Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl19Lbl1.Text = " ";
            this.pnl19Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl19Lbl2
            // 
            this.pnl19Lbl2.Dpi = 254F;
            this.pnl19Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl19Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl19Lbl2.Name = "pnl19Lbl2";
            this.pnl19Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl19Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl19Lbl2.StylePriority.UseFont = false;
            this.pnl19Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl19Lbl2.Text = " ";
            this.pnl19Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl19Lbl3
            // 
            this.pnl19Lbl3.Dpi = 254F;
            this.pnl19Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl19Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl19Lbl3.Name = "pnl19Lbl3";
            this.pnl19Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl19Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl19Lbl3.StylePriority.UseFont = false;
            this.pnl19Lbl3.Text = " ";
            // 
            // pnl20Lbl1
            // 
            this.pnl20Lbl1.Dpi = 254F;
            this.pnl20Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl20Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl20Lbl1.Name = "pnl20Lbl1";
            this.pnl20Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl20Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl20Lbl1.StylePriority.UseFont = false;
            this.pnl20Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl20Lbl1.Text = " ";
            this.pnl20Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl20Lbl2
            // 
            this.pnl20Lbl2.Dpi = 254F;
            this.pnl20Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl20Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl20Lbl2.Name = "pnl20Lbl2";
            this.pnl20Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl20Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl20Lbl2.StylePriority.UseFont = false;
            this.pnl20Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl20Lbl2.Text = " ";
            this.pnl20Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl20Lbl3
            // 
            this.pnl20Lbl3.Dpi = 254F;
            this.pnl20Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl20Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl20Lbl3.Name = "pnl20Lbl3";
            this.pnl20Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl20Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl20Lbl3.StylePriority.UseFont = false;
            this.pnl20Lbl3.Text = " ";
            // 
            // pnl21Lbl1
            // 
            this.pnl21Lbl1.Dpi = 254F;
            this.pnl21Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl21Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl21Lbl1.Name = "pnl21Lbl1";
            this.pnl21Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl21Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl21Lbl1.StylePriority.UseFont = false;
            this.pnl21Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl21Lbl1.Text = " ";
            this.pnl21Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl21Lbl2
            // 
            this.pnl21Lbl2.Dpi = 254F;
            this.pnl21Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl21Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl21Lbl2.Name = "pnl21Lbl2";
            this.pnl21Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl21Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl21Lbl2.StylePriority.UseFont = false;
            this.pnl21Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl21Lbl2.Text = " ";
            this.pnl21Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl21Lbl3
            // 
            this.pnl21Lbl3.Dpi = 254F;
            this.pnl21Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl21Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl21Lbl3.Name = "pnl21Lbl3";
            this.pnl21Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl21Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl21Lbl3.StylePriority.UseFont = false;
            this.pnl21Lbl3.Text = " ";
            // 
            // pnl22Lbl1
            // 
            this.pnl22Lbl1.Dpi = 254F;
            this.pnl22Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl22Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl22Lbl1.Name = "pnl22Lbl1";
            this.pnl22Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl22Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl22Lbl1.StylePriority.UseFont = false;
            this.pnl22Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl22Lbl1.Text = " ";
            this.pnl22Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl22Lbl2
            // 
            this.pnl22Lbl2.Dpi = 254F;
            this.pnl22Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl22Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl22Lbl2.Name = "pnl22Lbl2";
            this.pnl22Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl22Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl22Lbl2.StylePriority.UseFont = false;
            this.pnl22Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl22Lbl2.Text = " ";
            this.pnl22Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl22Lbl3
            // 
            this.pnl22Lbl3.Dpi = 254F;
            this.pnl22Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl22Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl22Lbl3.Name = "pnl22Lbl3";
            this.pnl22Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl22Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl22Lbl3.StylePriority.UseFont = false;
            this.pnl22Lbl3.Text = " ";
            // 
            // pnl23Lbl1
            // 
            this.pnl23Lbl1.Dpi = 254F;
            this.pnl23Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl23Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl23Lbl1.Name = "pnl23Lbl1";
            this.pnl23Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl23Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl23Lbl1.StylePriority.UseFont = false;
            this.pnl23Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl23Lbl1.Text = " ";
            this.pnl23Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl23Lbl2
            // 
            this.pnl23Lbl2.Dpi = 254F;
            this.pnl23Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl23Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl23Lbl2.Name = "pnl23Lbl2";
            this.pnl23Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl23Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl23Lbl2.StylePriority.UseFont = false;
            this.pnl23Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl23Lbl2.Text = " ";
            this.pnl23Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl23Lbl3
            // 
            this.pnl23Lbl3.Dpi = 254F;
            this.pnl23Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl23Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl23Lbl3.Name = "pnl23Lbl3";
            this.pnl23Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl23Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl23Lbl3.StylePriority.UseFont = false;
            this.pnl23Lbl3.Text = " ";
            // 
            // pnl24Lbl1
            // 
            this.pnl24Lbl1.Dpi = 254F;
            this.pnl24Lbl1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl24Lbl1.LocationFloat = new DevExpress.Utils.PointFloat(9F, 41F);
            this.pnl24Lbl1.Name = "pnl24Lbl1";
            this.pnl24Lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl24Lbl1.SizeF = new System.Drawing.SizeF(630.75F, 86.34824F);
            this.pnl24Lbl1.StylePriority.UseFont = false;
            this.pnl24Lbl1.StylePriority.UseTextAlignment = false;
            this.pnl24Lbl1.Text = " ";
            this.pnl24Lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl24Lbl2
            // 
            this.pnl24Lbl2.Dpi = 254F;
            this.pnl24Lbl2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl24Lbl2.LocationFloat = new DevExpress.Utils.PointFloat(9F, 139.29F);
            this.pnl24Lbl2.Name = "pnl24Lbl2";
            this.pnl24Lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl24Lbl2.SizeF = new System.Drawing.SizeF(630.75F, 50.26627F);
            this.pnl24Lbl2.StylePriority.UseFont = false;
            this.pnl24Lbl2.StylePriority.UseTextAlignment = false;
            this.pnl24Lbl2.Text = " ";
            this.pnl24Lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // pnl24Lbl3
            // 
            this.pnl24Lbl3.Dpi = 254F;
            this.pnl24Lbl3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.pnl24Lbl3.LocationFloat = new DevExpress.Utils.PointFloat(9F, 209.31F);
            this.pnl24Lbl3.Name = "pnl24Lbl3";
            this.pnl24Lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.pnl24Lbl3.SizeF = new System.Drawing.SizeF(630.75F, 67.9744F);
            this.pnl24Lbl3.StylePriority.UseFont = false;
            this.pnl24Lbl3.Text = " ";
            // 
            // RepMusteriEtiket
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(61, 61, 119, 130);
            this.PageHeight = 2969;
            this.PageWidth = 2101;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 31.75F;
            this.Version = "12.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRPanel pnl1;
        private DevExpress.XtraReports.UI.XRPanel pnl2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel4;
        private DevExpress.XtraReports.UI.XRPanel pnl3;
        private DevExpress.XtraReports.UI.XRPanel pnl15;
        private DevExpress.XtraReports.UI.XRPanel pnl20;
        private DevExpress.XtraReports.UI.XRPanel pnl19;
        private DevExpress.XtraReports.UI.XRPanel pnl22;
        private DevExpress.XtraReports.UI.XRPanel pnl23;
        private DevExpress.XtraReports.UI.XRPanel pnl24;
        private DevExpress.XtraReports.UI.XRPanel pnl18;
        private DevExpress.XtraReports.UI.XRPanel pnl17;
        private DevExpress.XtraReports.UI.XRPanel pnl16;
        private DevExpress.XtraReports.UI.XRPanel pnl13;
        private DevExpress.XtraReports.UI.XRPanel pnl14;
        private DevExpress.XtraReports.UI.XRPanel pnl21;
        private DevExpress.XtraReports.UI.XRPanel pnl9;
        private DevExpress.XtraReports.UI.XRPanel pnl8;
        private DevExpress.XtraReports.UI.XRPanel pnl7;
        private DevExpress.XtraReports.UI.XRPanel pnl10;
        private DevExpress.XtraReports.UI.XRPanel pnl11;
        private DevExpress.XtraReports.UI.XRPanel pnl12;
        private DevExpress.XtraReports.UI.XRPanel pnl6;
        private DevExpress.XtraReports.UI.XRPanel pnl5;
        private DevExpress.XtraReports.UI.XRPanel pnl4;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl1Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl2Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl2Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl2Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl3Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl3Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl3Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl6Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl6Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl6Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl5Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl5Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl5Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl4Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl4Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl4Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl15Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl15Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl15Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl20Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl20Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl20Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl19Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl19Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl19Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl22Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl22Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl22Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl23Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl23Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl23Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl24Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl24Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl24Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl18Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl18Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl18Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl17Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl17Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl17Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl16Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl16Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl16Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl13Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl13Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl13Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl14Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl14Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl14Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl21Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl21Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl21Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl9Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl9Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl9Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl8Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl8Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl8Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl7Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl7Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl7Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl10Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl10Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl10Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl11Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl11Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl11Lbl3;
        private DevExpress.XtraReports.UI.XRLabel pnl12Lbl1;
        private DevExpress.XtraReports.UI.XRLabel pnl12Lbl2;
        private DevExpress.XtraReports.UI.XRLabel pnl12Lbl3;
    }
}
