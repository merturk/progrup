﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Global;

using System.Net.Mime;
using System.Net.Mail;
using System.IO;


namespace BTI
{
    public partial class frmNewUser : DevExpress.XtraEditors.XtraForm
    {
        public frmNewUser()
        {
            InitializeComponent();
        }

        public static string KullaniciAdi;
        List<AttachementInfo> attachments;
        protected internal virtual AlternateView CreateHtmlView()
        {

            string htmlBody = richEditControl1.HtmlText;
            AlternateView view = AlternateView.CreateAlternateViewFromString(htmlBody, Encoding.UTF8, MediaTypeNames.Text.Html);


            int count = attachments.Count;
            for (int i = 0; i < count; i++)
            {
                AttachementInfo info = attachments[i];
                LinkedResource resource = new LinkedResource(info.Stream, info.MimeType);
                resource.ContentId = info.ContentId;
                view.LinkedResources.Add(resource);
            }
            return view;
        }

        public class AttachementInfo
        {
            Stream stream;
            string mimeType;
            string contentId;

            public AttachementInfo(Stream stream, string mimeType, string contentId)
            {
                this.stream = stream;
                this.mimeType = mimeType;
                this.contentId = contentId;
            }

            public Stream Stream { get { return stream; } }
            public string MimeType { get { return mimeType; } }
            public string ContentId { get { return contentId; } }
        }

        private void GetList()
        {
            DABindings.GetXtraLookupBindNm(lUpDepartment, @"
                    SELECT     GP_ID, GP_VIEW, GP_VALUE,GP_VALUE_STR1 FROM GEN_PARAMS Where GP_TYPE='DEPARTMENT'");
            /*
            DABindings.GetXtraLookupBindNm(lupPlasiyerKodu, string.Format(@"
                   Select 
                     PLASIYER_KODU,
                     PLASIYER_ACIKLAMA=[dbo].[ReplaceCharacter](PLASIYER_ACIKLAMA)
						                    
                    From {0}.dbo.TBLCARIPLASIYER
            ", frmLogin.EntegrasyonDB));
             */ 

            if  (!string.IsNullOrEmpty( KullaniciAdi ) )
            {
                var DtKullaniciBilgileri = new DataTable();
                DtKullaniciBilgileri = DbConnSql.SelectSQL(string.Format( @"Select * from usr_USERS Where USE_NAME ='{0}'",KullaniciAdi));
                if (DtKullaniciBilgileri.Rows.Count <= 0)
                {
                    MessageBox.Show("Böyle bir kullanıcı bulunamadı", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                txtkullaniciAdi.Text        = DtKullaniciBilgileri.Rows[0]["USE_NAME"].ToString();
                txtSifre.Text               = DtKullaniciBilgileri.Rows[0]["USE_PASSWORD"].ToString();
                lUpDepartment.EditValue     = string.IsNullOrEmpty( DtKullaniciBilgileri.Rows[0]["USE_DEPARTMENT"].ToString())?"" : DtKullaniciBilgileri.Rows[0]["USE_DEPARTMENT"].ToString();
                lupPlasiyerKodu.EditValue   = string.IsNullOrEmpty(DtKullaniciBilgileri.Rows[0]["USE_PLASIYER_KODU"].ToString()) ? "-1": DtKullaniciBilgileri.Rows[0]["USE_PLASIYER_KODU"].ToString();
                txtEmail.Text               = DtKullaniciBilgileri.Rows[0]["USE_PLASIYER_EMAIL"].ToString();
                txtGsm1.Text                = DtKullaniciBilgileri.Rows[0]["USE_PLASIYER_GSM1"].ToString();
                txtGsm2.Text                = DtKullaniciBilgileri.Rows[0]["USE_PLASIYER_GSM2"].ToString();
                richEditControl1.HtmlText   = DtKullaniciBilgileri.Rows[0]["USE_SIGNUTURE"].ToString();
                txtEmailUserName.Text       = DtKullaniciBilgileri.Rows[0]["USE_EMAIL_USER"].ToString();
                txtEmailPassword.Text       = DtKullaniciBilgileri.Rows[0]["USE_EMAIL_PASSWORD"].ToString();
                txtEmailSmtp.Text           = DtKullaniciBilgileri.Rows[0]["USE_SMTP_CLIENT"].ToString();
                txtEmailSmtpPort.Text       = DtKullaniciBilgileri.Rows[0]["USE_SMTP_PORT"].ToString();
                txtkullaniciAdi.Enabled     = false;
            
            }
            else
                txtkullaniciAdi.Enabled     = true;
        
        }
        private void AddNewUser()
        {
           
        }


        private void UpdUser()
        {
         
        }
        private void btnAddUser_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(KullaniciAdi))
             //Kayıt ekleme
            {
                if (String.IsNullOrEmpty(txtkullaniciAdi.Text))
                {
                    MessageBox.Show("Lütfen Bir Kullanıcı İsmi Giriniz!", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrEmpty(lUpDepartment.Text))
                {
                    MessageBox.Show("Departman hanesini boş geçemezsiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (string.IsNullOrEmpty(txtEmail.Text))
                {
                    MessageBox.Show("Email  hanesini boş geçemezsiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                if (string.IsNullOrEmpty(txtGsm1.Text))
                {
                    MessageBox.Show("Gsm1  hanesini boş geçemezsiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (MessageBox.Show("Yeni Kullanıcı Kaydını Onaylıyormusunuz?", Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                { return; }
                try
                {

                    Global.DbConnSql.SelectSQL(string.Format("usr_NEWUSER_INS'{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7}", txtkullaniciAdi.Text, txtSifre.Text, lUpDepartment.EditValue, lupPlasiyerKodu.EditValue, txtEmail.Text, txtGsm1.Text, txtGsm2.Text, frmLogin.KullaniciNo));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
                MessageBox.Show("Yeni Kullanıcı Başarıyla Eklendi", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            
            
            }
            else
            {
               //Güncelleme
                if (String.IsNullOrEmpty(txtkullaniciAdi.Text))
                {
                    MessageBox.Show("Lütfen Bir Kullanıcı İsmi Giriniz!", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrEmpty(lUpDepartment.Text))
                {
                    MessageBox.Show("Departman hanesini boş geçemezsiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (string.IsNullOrEmpty(txtEmail.Text))
                {
                    MessageBox.Show("Email  hanesini boş geçemezsiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                if (string.IsNullOrEmpty(txtGsm1.Text))
                {
                    MessageBox.Show("Gsm1  hanesini boş geçemezsiniz.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (MessageBox.Show("Kullanıcı verilerini güncellemek istediğinizden emin misiniz?", Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                { return; }
                try
                {

                    Global.DbConnSql.SelectSQL(string.Format(@"
                    UPDATE usr_USERS
                        SET 
                            USE_PASSWORD        = '{1}',
                            USE_DEPARTMENT      = '{2}',
                            USE_PLASIYER_KODU   = '{3}',
                            USE_PLASIYER_EMAIL  = '{4}',
                            USE_PLASIYER_GSM1   = '{5}',
                            USE_PLASIYER_GSM2   = '{6}',
                            USE_SIGNUTURE       = '{7}',                      
                            USE_EMAIL_USER      = '{8}',        
                            USE_EMAIL_PASSWORD  = '{9}',        
                            USE_SMTP_CLIENT     = '{10}',        
                            USE_SMTP_PORT       =  {11}        
                     Where USE_NAME ='{0}'
                    
                ", txtkullaniciAdi.Text, txtSifre.Text, lUpDepartment.EditValue, lupPlasiyerKodu.EditValue, txtEmail.Text, txtGsm1.Text, txtGsm2.Text, richEditControl1.HtmlText, txtEmailUserName.Text, txtEmailPassword.Text, txtEmailSmtp.Text, txtEmailSmtpPort.Text));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
                MessageBox.Show("Kullanıcı Bilgileri güncellendi", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);



            };
            Close();
        
        }

        private void frmNewUser_Load(object sender, EventArgs e)
        {
            try
            {
                GetList();
            }
            catch (Exception EX)
            {
                
                MessageBox.Show(EX.Message);
            }
            
        }
    }
}
