﻿namespace BTI
{
    partial class frmDeleteUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUserDelete = new System.Windows.Forms.Button();
            this.lblUsers = new System.Windows.Forms.Label();
            this.grdUsers = new DevExpress.XtraGrid.GridControl();
            this.grdVUsers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdUseNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd_USENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUserDelete
            // 
            this.btnUserDelete.Location = new System.Drawing.Point(15, 257);
            this.btnUserDelete.Name = "btnUserDelete";
            this.btnUserDelete.Size = new System.Drawing.Size(202, 31);
            this.btnUserDelete.TabIndex = 4;
            this.btnUserDelete.Text = "DELETE";
            this.btnUserDelete.UseVisualStyleBackColor = true;
            this.btnUserDelete.Click += new System.EventHandler(this.btnUserDelete_Click);
            // 
            // lblUsers
            // 
            this.lblUsers.AutoSize = true;
            this.lblUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUsers.Location = new System.Drawing.Point(12, 9);
            this.lblUsers.Name = "lblUsers";
            this.lblUsers.Size = new System.Drawing.Size(44, 16);
            this.lblUsers.TabIndex = 3;
            this.lblUsers.Text = "Users";
            // 
            // grdUsers
            // 
            this.grdUsers.Location = new System.Drawing.Point(15, 28);
            this.grdUsers.MainView = this.grdVUsers;
            this.grdUsers.Name = "grdUsers";
            this.grdUsers.Size = new System.Drawing.Size(202, 223);
            this.grdUsers.TabIndex = 5;
            this.grdUsers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVUsers});
            // 
            // grdVUsers
            // 
            this.grdVUsers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grdUseNo,
            this.grd_USENAME});
            this.grdVUsers.GridControl = this.grdUsers;
            this.grdVUsers.Name = "grdVUsers";
            this.grdVUsers.OptionsBehavior.AllowIncrementalSearch = true;
            this.grdVUsers.OptionsBehavior.Editable = false;
            this.grdVUsers.OptionsSelection.EnableAppearanceHideSelection = false;
            this.grdVUsers.OptionsView.ShowGroupPanel = false;
            this.grdVUsers.OptionsView.ShowIndicator = false;
            // 
            // grdUseNo
            // 
            this.grdUseNo.Caption = "USER_NO";
            this.grdUseNo.FieldName = "USE_NO";
            this.grdUseNo.Name = "grdUseNo";
            // 
            // grd_USENAME
            // 
            this.grd_USENAME.Caption = "Kullanıcı";
            this.grd_USENAME.FieldName = "USE_NAME";
            this.grd_USENAME.Name = "grd_USENAME";
            this.grd_USENAME.Visible = true;
            this.grd_USENAME.VisibleIndex = 0;
            // 
            // frmDeleteUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 312);
            this.Controls.Add(this.grdUsers);
            this.Controls.Add(this.btnUserDelete);
            this.Controls.Add(this.lblUsers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Black";
            this.Name = "frmDeleteUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Delete User";
            this.Load += new System.EventHandler(this.frmDeleteUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVUsers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUserDelete;
        private System.Windows.Forms.Label lblUsers;
        private DevExpress.XtraGrid.GridControl grdUsers;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVUsers;
        private DevExpress.XtraGrid.Columns.GridColumn grdUseNo;
        private DevExpress.XtraGrid.Columns.GridColumn grd_USENAME;
    }
}