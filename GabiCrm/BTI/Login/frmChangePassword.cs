﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Global;

namespace BTI
{
    public partial class frmChangePassword : DevExpress.XtraEditors.XtraForm
    {
        public frmChangePassword()
        {
            InitializeComponent();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(txtOldPass.Text) || String.IsNullOrEmpty(txtNewPass.Text))
            {
                MessageBox.Show("Lütfen Eski ve Yeni Şifrenizi Giriniz", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtOldPass.Text != frmLogin.KullaniciSifre)
            {
                MessageBox.Show("Eski Şifrenizi Lütfen Doğru Giriniz", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (MessageBox.Show("Şifre Değişikliğini Onaylıyor musunuz?", Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            { return; }

            try
            {
                DbConnSql.SelectSQL(string.Format("usr_PASSWORD_UPD '{0}','{1}',{2}", frmLogin.KullaniciAdi, txtNewPass.Text, frmLogin.KullaniciNo));

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); return; }

            MessageBox.Show("Şifre Başarıyla Değiştirildi", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
    }
}
