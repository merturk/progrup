﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Global;

namespace BTI
{
    public partial class frmPermision : DevExpress.XtraEditors.XtraForm
    {
        public frmPermision()
        {
            InitializeComponent();
        }
        private static int SatirSayisi=100;
        private static CheckBox[] cbDizi = new CheckBox[SatirSayisi];
        public  string KullaniciAdi;
        public  int KullaniciNo;
       // private int cbDiziRows;

        private void frmPermision_Load(object sender, EventArgs e)
        {
         //   cbDiziRows = 501;
            cbDizi[1]    = chkUsrInfo;
            cbDizi[2]    = chkChgPassword;
            cbDizi[3]    = chkNewUser;
            cbDizi[4]    = chkDeleteUser;
            cbDizi[5]    = chkKullaniciYetkileri;
          //  cbDizi[6]    = chkOyakRenaultFatAktarim;


            for (int i =6; i < cbDizi.Length; i++)
                cbDizi[i] = chkBos;     
            DABindings.GetXtraGridBind(grdPermision, @"
                    SELECT * FROM usr_USERS 
                    LEFT JOIN(
                            Select GP_VIEW,GP_VALUE_STR1 from GEN_PARAMS 
		                    Where GP_TYPE='DEPARTMENT') AS PSC ON PSC.GP_VALUE_STR1=USE_DEPARTMENT
                    Where USE_STAT is null Order by USE_NAME
                ");
     
        }  
        private void SavePermision()
        {

            string Kullanici = grdVPermision.GetFocusedRowCellDisplayText(grdColUSE_NO).ToString();
            if (string.IsNullOrEmpty(Kullanici))
                return;
            if (MessageBox.Show("Yetkileri Değiştirmek İstediğinize Eminmisiniz?", Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                return; 
            try
            {
                dockYetki.Show();
                String[] ISACCESS = new String[SatirSayisi];
                for (int i = 1; i < ISACCESS.Length; i++)
                {
                    ISACCESS[i] = Convert.ToString(cbDizi[i].Checked); 
                    DbConnSql.SelectSQL(string.Format("usr_PER_UPD {0},'{1}',{2}", Kullanici, ISACCESS[i], i));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            dockYetki.Hide();
            MessageBox.Show("Yetkiler Başarıyla Değiştirildi.", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        private void grdVPermision_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
  
            string Kullanici = grdVPermision.GetFocusedRowCellDisplayText(grdColUSE_NO).ToString();
            if (string.IsNullOrEmpty(Kullanici))
                return;
            var ds2 = new DataTable();
            ds2 = DbConnSql.SelectSQL(string.Format("SELECT  PER_AUTH_NO, PER_ISACCESS FROM  usr_PERMISION WHERE PER_USE_NO ={0} Order by PER_AUTH_NO ", Kullanici));


            if ((ds2.Rows.Count == 0))
            {
                MessageBox.Show("Yetkiler Yüklenemiyor.", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            for (int i = 0; i < cbDizi.Length-1; ++i)
            {
                int yetkiIndis              = Convert.ToInt32((int)ds2.Rows[i]["PER_AUTH_NO"]);
                cbDizi[yetkiIndis].Checked  = Convert.ToBoolean((string)ds2.Rows[i]["PER_ISACCESS"]);

            }

        }
        private void btnnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SavePermision();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            frmNewUser.KullaniciAdi = "";
            string SeciliKullanaci = grdVPermision.GetFocusedRowCellValue(grdColUSE_NAME).ToString();

            frmNewUser.KullaniciAdi = SeciliKullanaci;
            PubShowForm.ShowNewUser();
        }

   


      
    }
}
