﻿namespace BTI
{
    partial class frmNewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.lUpDepartment = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtkullaniciAdi = new DevExpress.XtraEditors.TextEdit();
            this.txtSifre = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtGsm1 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txtGsm2 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEmailSmtpPort = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEmailSmtp = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.txtEmailPassword = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEmailUserName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            this.lupPlasiyerKodu = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lUpDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkullaniciAdi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSifre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGsm1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGsm2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailSmtpPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailSmtp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lupPlasiyerKodu.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Kullanıcı Adı ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Kullanıcı Şifresi";
            // 
            // btnAddUser
            // 
            this.btnAddUser.Location = new System.Drawing.Point(654, 438);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(127, 29);
            this.btnAddUser.TabIndex = 500;
            this.btnAddUser.Text = "Kaydet";
            this.btnAddUser.UseVisualStyleBackColor = true;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // lUpDepartment
            // 
            this.lUpDepartment.EnterMoveNextControl = true;
            this.lUpDepartment.Location = new System.Drawing.Point(102, 56);
            this.lUpDepartment.Name = "lUpDepartment";
            this.lUpDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lUpDepartment.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GP_VIEW", 15, "Tanım"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GP_VALUE_STR1", "Deger")});
            this.lUpDepartment.Properties.DisplayMember = "GP_VIEW";
            this.lUpDepartment.Properties.DropDownRows = 20;
            this.lUpDepartment.Properties.NullText = "";
            this.lUpDepartment.Properties.PopupWidth = 350;
            this.lUpDepartment.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.lUpDepartment.Properties.SortColumnIndex = 1;
            this.lUpDepartment.Properties.ValueMember = "GP_VALUE_STR1";
            this.lUpDepartment.Size = new System.Drawing.Size(201, 20);
            this.lUpDepartment.TabIndex = 20;
            this.lUpDepartment.ToolTip = "Gerilim seviyeleri";
            this.lUpDepartment.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Departman ";
            // 
            // txtkullaniciAdi
            // 
            this.txtkullaniciAdi.EnterMoveNextControl = true;
            this.txtkullaniciAdi.Location = new System.Drawing.Point(102, 14);
            this.txtkullaniciAdi.Name = "txtkullaniciAdi";
            this.txtkullaniciAdi.Size = new System.Drawing.Size(201, 20);
            this.txtkullaniciAdi.TabIndex = 0;
            // 
            // txtSifre
            // 
            this.txtSifre.EnterMoveNextControl = true;
            this.txtSifre.Location = new System.Drawing.Point(102, 35);
            this.txtSifre.Name = "txtSifre";
            this.txtSifre.Properties.PasswordChar = '*';
            this.txtSifre.Size = new System.Drawing.Size(201, 20);
            this.txtSifre.TabIndex = 10;
            // 
            // txtEmail
            // 
            this.txtEmail.EnterMoveNextControl = true;
            this.txtEmail.Location = new System.Drawing.Point(102, 98);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(201, 20);
            this.txtEmail.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(21, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 195;
            this.label4.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(21, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 197;
            this.label5.Text = "Gsm 1";
            // 
            // txtGsm1
            // 
            this.txtGsm1.EnterMoveNextControl = true;
            this.txtGsm1.Location = new System.Drawing.Point(102, 119);
            this.txtGsm1.Name = "txtGsm1";
            this.txtGsm1.Properties.Mask.EditMask = "(\\d?\\d?\\d?\\d?)\\d\\d\\d-\\d\\d-\\d\\d";
            this.txtGsm1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtGsm1.Size = new System.Drawing.Size(201, 20);
            this.txtGsm1.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(21, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 199;
            this.label6.Text = "Gsm 2";
            // 
            // txtGsm2
            // 
            this.txtGsm2.EnterMoveNextControl = true;
            this.txtGsm2.Location = new System.Drawing.Point(102, 140);
            this.txtGsm2.Name = "txtGsm2";
            this.txtGsm2.Properties.Mask.EditMask = "(\\d?\\d?\\d?\\d?)\\d\\d\\d-\\d\\d-\\d\\d";
            this.txtGsm2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtGsm2.Size = new System.Drawing.Size(201, 20);
            this.txtGsm2.TabIndex = 60;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.label13);
            this.groupControl1.Controls.Add(this.txtEmailSmtpPort);
            this.groupControl1.Controls.Add(this.label12);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.txtEmailSmtp);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.txtEmailPassword);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.txtEmailUserName);
            this.groupControl1.Location = new System.Drawing.Point(304, 13);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(477, 147);
            this.groupControl1.TabIndex = 504;
            this.groupControl1.Text = "Email Bilgileri";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label14.ForeColor = System.Drawing.Color.Gray;
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(314, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 206;
            this.label14.Text = "Örn : 587";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(9, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 205;
            this.label13.Text = "Smtp Port";
            // 
            // txtEmailSmtpPort
            // 
            this.txtEmailSmtpPort.EnterMoveNextControl = true;
            this.txtEmailSmtpPort.Location = new System.Drawing.Point(107, 85);
            this.txtEmailSmtpPort.Name = "txtEmailSmtpPort";
            this.txtEmailSmtpPort.Properties.Mask.EditMask = "n0";
            this.txtEmailSmtpPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtEmailSmtpPort.Size = new System.Drawing.Size(201, 20);
            this.txtEmailSmtpPort.TabIndex = 204;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label12.ForeColor = System.Drawing.Color.Gray;
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(314, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 13);
            this.label12.TabIndex = 203;
            this.label12.Text = "Örn : mail.gabipro.com";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(9, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 202;
            this.label11.Text = "Smtp Client";
            // 
            // txtEmailSmtp
            // 
            this.txtEmailSmtp.EnterMoveNextControl = true;
            this.txtEmailSmtp.Location = new System.Drawing.Point(107, 64);
            this.txtEmailSmtp.Name = "txtEmailSmtp";
            this.txtEmailSmtp.Size = new System.Drawing.Size(201, 20);
            this.txtEmailSmtp.TabIndex = 201;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(9, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 200;
            this.label10.Text = "Email Şifre";
            // 
            // txtEmailPassword
            // 
            this.txtEmailPassword.EnterMoveNextControl = true;
            this.txtEmailPassword.Location = new System.Drawing.Point(107, 43);
            this.txtEmailPassword.Name = "txtEmailPassword";
            this.txtEmailPassword.Properties.PasswordChar = '*';
            this.txtEmailPassword.Size = new System.Drawing.Size(201, 20);
            this.txtEmailPassword.TabIndex = 199;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(314, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(158, 13);
            this.label9.TabIndex = 198;
            this.label9.Text = "Örn : murat.erturk@gabipro.com";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(9, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 197;
            this.label8.Text = "Email Kullanici Adı";
            // 
            // txtEmailUserName
            // 
            this.txtEmailUserName.EnterMoveNextControl = true;
            this.txtEmailUserName.Location = new System.Drawing.Point(107, 22);
            this.txtEmailUserName.Name = "txtEmailUserName";
            this.txtEmailUserName.Size = new System.Drawing.Size(201, 20);
            this.txtEmailUserName.TabIndex = 196;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(21, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 505;
            this.label7.Text = "Email İmza";
            // 
            // richEditControl1
            // 
            this.richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControl1.Location = new System.Drawing.Point(102, 162);
            this.richEditControl1.Name = "richEditControl1";
            this.richEditControl1.Options.DocumentSaveOptions.DefaultFormat = DevExpress.XtraRichEdit.DocumentFormat.Html;
            this.richEditControl1.Options.MailMerge.KeepLastParagraph = true;
            this.richEditControl1.Options.MailMerge.ViewMergedData = true;
            this.richEditControl1.Size = new System.Drawing.Size(679, 270);
            this.richEditControl1.TabIndex = 506;
            // 
            // lupPlasiyerKodu
            // 
            this.lupPlasiyerKodu.EnterMoveNextControl = true;
            this.lupPlasiyerKodu.Location = new System.Drawing.Point(102, 77);
            this.lupPlasiyerKodu.Name = "lupPlasiyerKodu";
            this.lupPlasiyerKodu.Properties.Appearance.Options.UseFont = true;
            this.lupPlasiyerKodu.Properties.AutoSearchColumnIndex = 1;
            this.lupPlasiyerKodu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lupPlasiyerKodu.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLASIYER_KODU", 5, "Kodu"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PLASIYER_ACIKLAMA", "Aciklama")});
            this.lupPlasiyerKodu.Properties.DisplayMember = "PLASIYER_KODU";
            this.lupPlasiyerKodu.Properties.DropDownRows = 15;
            this.lupPlasiyerKodu.Properties.NullText = "";
            this.lupPlasiyerKodu.Properties.PopupWidth = 350;
            this.lupPlasiyerKodu.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.lupPlasiyerKodu.Properties.SortColumnIndex = 1;
            this.lupPlasiyerKodu.Properties.ValueMember = "PLASIYER_KODU";
            this.lupPlasiyerKodu.Size = new System.Drawing.Size(201, 20);
            this.lupPlasiyerKodu.TabIndex = 30;
            this.lupPlasiyerKodu.ToolTip = "Depo Yeri";
            this.lupPlasiyerKodu.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label15.Location = new System.Drawing.Point(21, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 192;
            this.label15.Text = "Plasiyer";
            // 
            // frmNewUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 474);
            this.Controls.Add(this.richEditControl1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.txtGsm2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtGsm1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lupPlasiyerKodu);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtSifre);
            this.Controls.Add(this.txtkullaniciAdi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lUpDepartment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmNewUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Yeni Kullanıcı Ekleme Ve Düzenleme";
            this.Load += new System.EventHandler(this.frmNewUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lUpDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkullaniciAdi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSifre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGsm1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGsm2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailSmtpPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailSmtp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lupPlasiyerKodu.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddUser;
        private DevExpress.XtraEditors.LookUpEdit lUpDepartment;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtkullaniciAdi;
        private DevExpress.XtraEditors.TextEdit txtSifre;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txtGsm1;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txtGsm2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit txtEmailSmtpPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit txtEmailSmtp;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit txtEmailPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtEmailUserName;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraRichEdit.RichEditControl richEditControl1;
        private DevExpress.XtraEditors.LookUpEdit lupPlasiyerKodu;
        private System.Windows.Forms.Label label15;
    }
}