﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Global;


namespace BTI
{
    public partial class frmDeleteUser : DevExpress.XtraEditors.XtraForm
    {
        public frmDeleteUser()
        {
            InitializeComponent();
        }

        private void btnUserDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Silme işlemini onaylıyor musunuz?", Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                MessageBox.Show("Silme işlemi iptal edildi", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                string KullaniciNo = grdVUsers.GetFocusedRowCellValue(grdUseNo).ToString();
                if (String.IsNullOrEmpty(KullaniciNo))
                {
                    MessageBox.Show("Lütfen önce listeden silinecek kişiyi seçin", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }

                DbConnSql.SelectSQL(string.Format("dbo.usr_DELETE {0}", KullaniciNo));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            MessageBox.Show("İlgili kayıt başarıyla silindi", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            GetList();
       //     Close();


        }
        private void GetList()
        {
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL("SELECT * FROM usr_USERS Where USE_STAT is null ");
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show("Kullanıcılar Yüklenemiyor.", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            grdUsers.DataSource = ds;
        
        }

        private void frmDeleteUser_Load(object sender, EventArgs e)
        {
            GetList();
        }
    }
}
