﻿namespace BTI
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lUpUserName = new DevExpress.XtraEditors.LookUpEdit();
            this.txt_sifre = new DevExpress.XtraEditors.TextEdit();
            this.btnLogin = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmbEntegrasyonDB = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.dLaf = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            ((System.ComponentModel.ISupportInitialize)(this.lUpUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sifre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEntegrasyonDB.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPassword
            // 
            resources.ApplyResources(this.lblPassword, "lblPassword");
            this.lblPassword.ForeColor = System.Drawing.Color.Black;
            this.lblPassword.Name = "lblPassword";
            // 
            // lblUsername
            // 
            resources.ApplyResources(this.lblUsername, "lblUsername");
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            this.lblUsername.ForeColor = System.Drawing.Color.Black;
            this.lblUsername.Name = "lblUsername";
            // 
            // lUpUserName
            // 
            this.lUpUserName.EnterMoveNextControl = true;
            resources.ApplyResources(this.lUpUserName, "lUpUserName");
            this.lUpUserName.Name = "lUpUserName";
            this.lUpUserName.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("lUpUserName.Properties.Appearance.BackColor")));
            this.lUpUserName.Properties.Appearance.Options.UseBackColor = true;
            this.lUpUserName.Properties.AutoSearchColumnIndex = 2;
            this.lUpUserName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("lUpUserName.Properties.Buttons"))))});
            this.lUpUserName.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("lUpUserName.Properties.Columns"), resources.GetString("lUpUserName.Properties.Columns1"))});
            this.lUpUserName.Properties.DisplayMember = "USE_NAME";
            this.lUpUserName.Properties.DropDownRows = 25;
            this.lUpUserName.Properties.NullText = resources.GetString("lUpUserName.Properties.NullText");
            this.lUpUserName.Properties.PopupWidth = 150;
            this.lUpUserName.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.lUpUserName.Properties.SortColumnIndex = 1;
            this.lUpUserName.Properties.ValueMember = "USE_NAME";
            // 
            // txt_sifre
            // 
            this.txt_sifre.EnterMoveNextControl = true;
            resources.ApplyResources(this.txt_sifre, "txt_sifre");
            this.txt_sifre.Name = "txt_sifre";
            this.txt_sifre.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txt_sifre.Properties.Appearance.BackColor")));
            this.txt_sifre.Properties.Appearance.Options.UseBackColor = true;
            this.txt_sifre.Properties.PasswordChar = '*';
            // 
            // btnLogin
            // 
            resources.ApplyResources(this.btnLogin, "btnLogin");
            this.btnLogin.LookAndFeel.SkinName = "Springtime";
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.cmbEntegrasyonDB);
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.txt_sifre);
            this.panelControl1.Controls.Add(this.lblUsername);
            this.panelControl1.Controls.Add(this.lblPassword);
            this.panelControl1.Controls.Add(this.btnLogin);
            this.panelControl1.Controls.Add(this.lUpUserName);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.LookAndFeel.SkinName = "iMaginary";
            this.panelControl1.Name = "panelControl1";
            // 
            // cmbEntegrasyonDB
            // 
            resources.ApplyResources(this.cmbEntegrasyonDB, "cmbEntegrasyonDB");
            this.cmbEntegrasyonDB.EnterMoveNextControl = true;
            this.cmbEntegrasyonDB.Name = "cmbEntegrasyonDB";
            this.cmbEntegrasyonDB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbEntegrasyonDB.Properties.Buttons"))))});
            this.cmbEntegrasyonDB.Properties.Items.AddRange(new object[] {
            resources.GetString("cmbEntegrasyonDB.Properties.Items"),
            resources.GetString("cmbEntegrasyonDB.Properties.Items1"),
            resources.GetString("cmbEntegrasyonDB.Properties.Items2")});
            this.cmbEntegrasyonDB.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            // 
            // dLaf
            // 
            this.dLaf.LookAndFeel.SkinName = "Office 2007 Blue";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            resources.ApplyResources(this.ribbonPageGroup1, "ribbonPageGroup1");
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            resources.ApplyResources(this.ribbonPage1, "ribbonPage1");
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            resources.ApplyResources(this.ribbonPageGroup2, "ribbonPageGroup2");
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2});
            this.ribbonPage2.Name = "ribbonPage2";
            resources.ApplyResources(this.ribbonPage2, "ribbonPage2");
            // 
            // frmLogin
            // 
            this.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("frmLogin.Appearance.BackColor")));
            this.Appearance.Options.UseBackColor = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.LookAndFeel.SkinName = "Office 2007 Green";
            this.Name = "frmLogin";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lUpUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sifre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEntegrasyonDB.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblUsername;
        private DevExpress.XtraEditors.SimpleButton btnLogin;
        private DevExpress.XtraEditors.LookUpEdit lUpUserName;
        private DevExpress.XtraEditors.TextEdit txt_sifre;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        public DevExpress.LookAndFeel.DefaultLookAndFeel dLaf;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEntegrasyonDB;
        private System.Windows.Forms.Label label1;
    }
}