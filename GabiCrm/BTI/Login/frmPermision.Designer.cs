﻿namespace BTI
{
    partial class frmPermision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPermision));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tpGenel = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.chkBos = new System.Windows.Forms.CheckBox();
            this.chkKullaniciYetkileri = new System.Windows.Forms.CheckBox();
            this.chkDeleteUser = new System.Windows.Forms.CheckBox();
            this.chkNewUser = new System.Windows.Forms.CheckBox();
            this.chkChgPassword = new System.Windows.Forms.CheckBox();
            this.chkUsrInfo = new System.Windows.Forms.CheckBox();
            this.tpStok = new DevExpress.XtraTab.XtraTabPage();
            this.grdPermision = new DevExpress.XtraGrid.GridControl();
            this.grdVPermision = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdColUSE_NO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdColUSE_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdColUSE_PASSWORD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdColUSE_DEPARTMENT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnnSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockYetki = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.SmallImage = new DevExpress.Utils.ImageCollection(this.components);
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tpGenel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPermision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVPermision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockYetki.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(414, 24);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tpGenel;
            this.xtraTabControl1.Size = new System.Drawing.Size(702, 549);
            this.xtraTabControl1.TabIndex = 11;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpGenel,
            this.tpStok});
            // 
            // tpGenel
            // 
            this.tpGenel.Controls.Add(this.labelControl13);
            this.tpGenel.Controls.Add(this.labelControl14);
            this.tpGenel.Controls.Add(this.chkBos);
            this.tpGenel.Controls.Add(this.chkKullaniciYetkileri);
            this.tpGenel.Controls.Add(this.chkDeleteUser);
            this.tpGenel.Controls.Add(this.chkNewUser);
            this.tpGenel.Controls.Add(this.chkChgPassword);
            this.tpGenel.Controls.Add(this.chkUsrInfo);
            this.tpGenel.Name = "tpGenel";
            this.tpGenel.Size = new System.Drawing.Size(697, 524);
            this.tpGenel.Text = "Genel";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelControl13.Location = new System.Drawing.Point(10, 97);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(45, 13);
            this.labelControl13.TabIndex = 21;
            this.labelControl13.Text = "Yönetici";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelControl14.Location = new System.Drawing.Point(10, 17);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 13);
            this.labelControl14.TabIndex = 20;
            this.labelControl14.Text = "Standart Kullanıcı";
            // 
            // chkBos
            // 
            this.chkBos.AutoSize = true;
            this.chkBos.Location = new System.Drawing.Point(8, 396);
            this.chkBos.Name = "chkBos";
            this.chkBos.Size = new System.Drawing.Size(43, 17);
            this.chkBos.TabIndex = 9;
            this.chkBos.Text = "Boş";
            this.chkBos.UseVisualStyleBackColor = true;
            this.chkBos.Visible = false;
            // 
            // chkKullaniciYetkileri
            // 
            this.chkKullaniciYetkileri.AutoSize = true;
            this.chkKullaniciYetkileri.ForeColor = System.Drawing.Color.Red;
            this.chkKullaniciYetkileri.Location = new System.Drawing.Point(17, 156);
            this.chkKullaniciYetkileri.Name = "chkKullaniciYetkileri";
            this.chkKullaniciYetkileri.Size = new System.Drawing.Size(103, 17);
            this.chkKullaniciYetkileri.TabIndex = 8;
            this.chkKullaniciYetkileri.Text = "Kullanıcı Yetkileri";
            this.chkKullaniciYetkileri.UseVisualStyleBackColor = true;
            // 
            // chkDeleteUser
            // 
            this.chkDeleteUser.AutoSize = true;
            this.chkDeleteUser.ForeColor = System.Drawing.Color.Red;
            this.chkDeleteUser.Location = new System.Drawing.Point(17, 136);
            this.chkDeleteUser.Name = "chkDeleteUser";
            this.chkDeleteUser.Size = new System.Drawing.Size(90, 17);
            this.chkDeleteUser.TabIndex = 7;
            this.chkDeleteUser.Text = "Kullanıcı Silme";
            this.chkDeleteUser.UseVisualStyleBackColor = true;
            // 
            // chkNewUser
            // 
            this.chkNewUser.AutoSize = true;
            this.chkNewUser.ForeColor = System.Drawing.Color.Red;
            this.chkNewUser.Location = new System.Drawing.Point(17, 116);
            this.chkNewUser.Name = "chkNewUser";
            this.chkNewUser.Size = new System.Drawing.Size(122, 17);
            this.chkNewUser.TabIndex = 6;
            this.chkNewUser.Text = "Yeni Kullanıcı Ekleme";
            this.chkNewUser.UseVisualStyleBackColor = true;
            // 
            // chkChgPassword
            // 
            this.chkChgPassword.AutoSize = true;
            this.chkChgPassword.Location = new System.Drawing.Point(17, 58);
            this.chkChgPassword.Name = "chkChgPassword";
            this.chkChgPassword.Size = new System.Drawing.Size(101, 17);
            this.chkChgPassword.TabIndex = 5;
            this.chkChgPassword.Text = "Şifre Değiştirme";
            this.chkChgPassword.UseVisualStyleBackColor = true;
            // 
            // chkUsrInfo
            // 
            this.chkUsrInfo.AutoSize = true;
            this.chkUsrInfo.Location = new System.Drawing.Point(17, 37);
            this.chkUsrInfo.Name = "chkUsrInfo";
            this.chkUsrInfo.Size = new System.Drawing.Size(98, 17);
            this.chkUsrInfo.TabIndex = 4;
            this.chkUsrInfo.Text = "Kullanıcı Bilgileri";
            this.chkUsrInfo.UseVisualStyleBackColor = true;
            // 
            // tpStok
            // 
            this.tpStok.Name = "tpStok";
            this.tpStok.Size = new System.Drawing.Size(697, 524);
            this.tpStok.Text = "Ana Giriş";
            // 
            // grdPermision
            // 
            this.grdPermision.Dock = System.Windows.Forms.DockStyle.Left;
            this.grdPermision.Location = new System.Drawing.Point(0, 24);
            this.grdPermision.MainView = this.grdVPermision;
            this.grdPermision.Name = "grdPermision";
            this.grdPermision.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.grdPermision.Size = new System.Drawing.Size(414, 549);
            this.grdPermision.TabIndex = 12;
            this.grdPermision.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdVPermision});
            // 
            // grdVPermision
            // 
            this.grdVPermision.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grdColUSE_NO,
            this.grdColUSE_NAME,
            this.grdColUSE_PASSWORD,
            this.grdColUSE_DEPARTMENT});
            this.grdVPermision.GridControl = this.grdPermision;
            this.grdVPermision.HorzScrollStep = 30;
            this.grdVPermision.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdVPermision.Name = "grdVPermision";
            this.grdVPermision.OptionsBehavior.Editable = false;
            this.grdVPermision.OptionsNavigation.AutoFocusNewRow = true;
            this.grdVPermision.OptionsNavigation.AutoMoveRowFocus = false;
            this.grdVPermision.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdVPermision.OptionsSelection.EnableAppearanceHideSelection = false;
            this.grdVPermision.OptionsView.ColumnAutoWidth = false;
            this.grdVPermision.OptionsView.ShowAutoFilterRow = true;
            this.grdVPermision.OptionsView.ShowGroupPanel = false;
            this.grdVPermision.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grdVPermision_FocusedRowChanged);
            // 
            // grdColUSE_NO
            // 
            this.grdColUSE_NO.Caption = "No";
            this.grdColUSE_NO.FieldName = "USE_NO";
            this.grdColUSE_NO.Name = "grdColUSE_NO";
            this.grdColUSE_NO.Visible = true;
            this.grdColUSE_NO.VisibleIndex = 0;
            this.grdColUSE_NO.Width = 31;
            // 
            // grdColUSE_NAME
            // 
            this.grdColUSE_NAME.Caption = "Adı";
            this.grdColUSE_NAME.FieldName = "USE_NAME";
            this.grdColUSE_NAME.Name = "grdColUSE_NAME";
            this.grdColUSE_NAME.Visible = true;
            this.grdColUSE_NAME.VisibleIndex = 1;
            this.grdColUSE_NAME.Width = 151;
            // 
            // grdColUSE_PASSWORD
            // 
            this.grdColUSE_PASSWORD.Caption = "Şifre";
            this.grdColUSE_PASSWORD.FieldName = "USE_PASSWORD";
            this.grdColUSE_PASSWORD.Name = "grdColUSE_PASSWORD";
            this.grdColUSE_PASSWORD.Visible = true;
            this.grdColUSE_PASSWORD.VisibleIndex = 3;
            this.grdColUSE_PASSWORD.Width = 110;
            // 
            // grdColUSE_DEPARTMENT
            // 
            this.grdColUSE_DEPARTMENT.Caption = "Departman";
            this.grdColUSE_DEPARTMENT.FieldName = "SCP_VIEW";
            this.grdColUSE_DEPARTMENT.Name = "grdColUSE_DEPARTMENT";
            this.grdColUSE_DEPARTMENT.Visible = true;
            this.grdColUSE_DEPARTMENT.VisibleIndex = 2;
            this.grdColUSE_DEPARTMENT.Width = 182;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Aktif", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Pasif", 0, 1)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.Images = this.SmallImage;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnnSave,
            this.barEditItem1,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 4;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemProgressBar2});
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnnSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1, true)});
            this.bar1.Text = "Tools";
            // 
            // btnnSave
            // 
            this.btnnSave.Caption = "Kaydet";
            this.btnnSave.Id = 0;
            this.btnnSave.ImageIndex = 10;
            this.btnnSave.Name = "btnnSave";
            this.btnnSave.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnnSave_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Kullanici Bilgileri";
            this.barButtonItem1.Id = 3;
            this.barButtonItem1.ImageIndex = 14;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1116, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 573);
            this.barDockControlBottom.Size = new System.Drawing.Size(1116, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 549);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1116, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 549);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockYetki});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockYetki
            // 
            this.dockYetki.Controls.Add(this.dockPanel1_Container);
            this.dockYetki.Dock = DevExpress.XtraBars.Docking.DockingStyle.Top;
            this.dockYetki.ID = new System.Guid("484cf9ee-ea76-4fd2-9064-c8eda42413be");
            this.dockYetki.Location = new System.Drawing.Point(0, 24);
            this.dockYetki.Name = "dockYetki";
            this.dockYetki.OriginalSize = new System.Drawing.Size(0, 0);
            this.dockYetki.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Top;
            this.dockYetki.SavedIndex = 0;
            this.dockYetki.Size = new System.Drawing.Size(933, 91);
            this.dockYetki.Text = "Kullanıcı Yetkileri";
            this.dockYetki.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.marqueeProgressBarControl1);
            this.dockPanel1_Container.Controls.Add(this.labelControl11);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 24);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(927, 64);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(50, 34);
            this.marqueeProgressBarControl1.MenuManager = this.barManager1;
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(372, 18);
            this.marqueeProgressBarControl1.TabIndex = 1;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelControl11.Location = new System.Drawing.Point(16, 8);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(411, 19);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "Lütfen bekleyin...Kullanıcı Yetkileri Güncelleniyor. ";
            // 
            // SmallImage
            // 
            this.SmallImage.ImageSize = new System.Drawing.Size(15, 15);
            this.SmallImage.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("SmallImage.ImageStream")));
            this.SmallImage.Images.SetKeyName(14, "businessman_find.png");
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Prog";
            this.barEditItem1.Edit = this.repositoryItemProgressBar1;
            this.barEditItem1.Id = 1;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemProgressBar2
            // 
            this.repositoryItemProgressBar2.Maximum = 500;
            this.repositoryItemProgressBar2.Name = "repositoryItemProgressBar2";
            this.repositoryItemProgressBar2.Step = 5;
            // 
            // frmPermision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 573);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.grdPermision);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPermision";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Yetkilendirme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPermision_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tpGenel.ResumeLayout(false);
            this.tpGenel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPermision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdVPermision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockYetki.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.dockPanel1_Container.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmallImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tpGenel;
        private System.Windows.Forms.CheckBox chkNewUser;
        private System.Windows.Forms.CheckBox chkChgPassword;
        private DevExpress.XtraTab.XtraTabPage tpStok;
        private System.Windows.Forms.CheckBox chkDeleteUser;
        private System.Windows.Forms.CheckBox chkKullaniciYetkileri;
        private System.Windows.Forms.CheckBox chkUsrInfo;
        private DevExpress.XtraGrid.GridControl grdPermision;
        private DevExpress.XtraGrid.Views.Grid.GridView grdVPermision;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn grdColUSE_NO;
        private DevExpress.XtraGrid.Columns.GridColumn grdColUSE_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn grdColUSE_PASSWORD;
        private System.Windows.Forms.CheckBox chkBos;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem btnnSave;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection SmallImage;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar2;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockYetki;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraGrid.Columns.GridColumn grdColUSE_DEPARTMENT;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
    }
}