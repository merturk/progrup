﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.XtraBars.Helpers;


using Global;
using GABI_CRM.Properties;
//using BTI.Properties;

namespace BTI
{
   
   
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        public frmLogin()
        {
            InitializeComponent();
            InitSkinGallery();
        }

        void InitSkinGallery()
        {

           // UserLookAndFeel.Default.SkinName =  Settings.Default["ApplicationSkinName"].ToString();
        }
            

        


        public static string KullaniciSifre, KullaniciAdi,PlasiyerKodu,Gsm1,Gsm2,Email, UretimBirimi,Departman,EntegrasyonDB;
        public static int KullaniciNo,KullaniciDocumentStat;
        public static string[] YetkiDizisi = new string[501];

        #region YetkileriniYukle
        public static bool YetkileriniYukle(double KullaniciNo)
        {

            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(string.Format("SELECT PER_AUTH_NO, PER_ISACCESS FROM usr_PERMISION Where PER_USE_NO={0} order by PER_AUTH_NO ", KullaniciNo));
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show("Yetkiler Yüklenemiyor.", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            for (int i = 0; i < ds.Rows.Count; ++i)
            {
                int yetkiIndis = Convert.ToInt32((int)ds.Rows[i][0]);
                YetkiDizisi[yetkiIndis] = (string)ds.Rows[i][1];
            }

            return true;
        }

        #endregion
        #region ObjectsPermisions
        private void ObjectsPermisions()
        {
            var MainForm = new frmMain();
          
            MainForm.bbUsrViewName.Enabled                  = Convert.ToBoolean((string)YetkiDizisi[1]);
            MainForm.bbUsrChgPass.Enabled                   = Convert.ToBoolean((string)YetkiDizisi[2]);
            MainForm.bbUsrNew.Enabled                       = Convert.ToBoolean((string)YetkiDizisi[3]);
            MainForm.bbUsrDelete.Enabled                    = Convert.ToBoolean((string)YetkiDizisi[4]);
            MainForm.bbUsrPermision.Enabled                 = Convert.ToBoolean((string)YetkiDizisi[5]);
            //MainForm.btnStokKarti.Enabled                   = Convert.ToBoolean((string)YetkiDizisi[6]);
         
      
            MainForm.Show();         

        }

        #endregion
        #region LoadingSecurity
        private void LoadingSecurity()
        {

            var rd = DbConnSql.ReaderRowsReturn(String.Format("SELECT  USE_NO,USE_PASSWORD,USE_NAME,USE_DEPARTMENT,USE_PLASIYER_KODU= isnull(USE_PLASIYER_KODU,''),USE_PLASIYER_EMAIL=isnull(USE_PLASIYER_EMAIL,''),USE_PLASIYER_GSM1=isnull(USE_PLASIYER_GSM1,''),USE_PLASIYER_GSM2= isnull(USE_PLASIYER_GSM2,'')  FROM usr_USERS WHERE USE_NAME='{0}' AND USE_STAT is null ", lUpUserName.Text));
                     
            if (rd.HasRows)
            {
                KullaniciNo     = (int)rd[0];
                KullaniciSifre  = (string)rd[1];
                KullaniciAdi    = (string)rd[2];
                Departman       = (string)rd[3];
                PlasiyerKodu    = (string)rd[4];
                Email           = (string)rd[5];
                Gsm1            = (string)rd[6];
                Gsm2            = (string)rd[7];

                UretimBirimi    ="10001";
                KullaniciDocumentStat =1;
                EntegrasyonDB   = cmbEntegrasyonDB.Text;
              

                if (KullaniciSifre != txt_sifre.Text)
                {
                    MessageBox.Show("Geçersiz Şifre", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ///label4.Text = string.Format("Kullanıcı Adı :{0}", KullaniciNo);
                if (YetkileriniYukle(KullaniciNo) != true) { return; }
                ObjectsPermisions();
                Hide();
            }
            else
            {
                MessageBox.Show("Geçersiz Kullanıcı.", Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }
        #endregion


        private void SystemInfo()
        {


            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(string.Format("SALES_LOG_INSERT '{0}','{1}'", PubVariable.UserName, PubVariable.ComputerName));


        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            lUpUserName.Focus();
            //Kullanıcı Listesi
            DABindings.GetXtraLookupBind(lUpUserName, "SELECT  USE_NAME FROM usr_USERS  Where USE_STAT is null Order By USE_NAME", "Kullanıcı listesi yüklenemedi");

          //  SystemInfo();

   
              
        }

        private void txt_sifre_KeyDown(object sender, KeyEventArgs e)
        {
            PubKeyDown.nextBtnDevx(e, btnLogin);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            if (DateTime.Now.Year > 2019)
            {
                MessageBox.Show("Programın 2019 yılı için ilgili bakım işlemlerinin yapılması gerekmektedir.Lütfen program satıcınız ile irtibata geçin.muraterturk@hotmail.com yada Tel : 0545 853 63 21");
                return;
            }

            if (string.IsNullOrEmpty(cmbEntegrasyonDB.Text))
            {
                MessageBox.Show("Entegrasyon database ini boş geçemezsiniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            LoadingSecurity(); 

   

        }
    
    }

     


}
