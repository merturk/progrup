﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
//using DevExpressCs;

namespace Global
{
    public class DbConnSql
    {


         //**********************************************************************************


        public static string ServerMurat            = @"MURATERTRK7FCB\SQLEXPRESS2017";
        public static string ServerMusteri          =  @"192.168.16.2"; // 192.168.0.3

        //----------------------------------------------------------------------------------
        public static string DatabaseNameMusteri    = "GABI_CRM";
        public static string DatabaseNameMurat      = "GABI_CRM";


        public static string UserMurat              = "sa";
        public static string PasswordMurat          = "ber75*45*hal";

        public static string UserMusteri            = "gabi";
        public static string PasswordMusteri        = "ber75*45*hal";

        //----------------------------------------------------------------------------------
        /* 
 public static string DataSource             = ServerMusteri;
 public static string DatabaseName           = DatabaseNameMusteri;
 public static string UserName               = UserMusteri;
 public static string Password               = PasswordMusteri;
   //*/     


            
public static string DataSource             =  ServerMurat;
public static string DatabaseName           =  DatabaseNameMurat;
public static string UserName               =  UserMurat;
public static string Password               =  PasswordMurat;
//*/

        public static string ConnString()
        {         

            return             
            string.Format(@"
                    server={0};
                    Persist Security Info=True;                   
                    User ID={1};  
                    Password = {2};                                
                    Initial Catalog={3}", DataSource, UserName, Password, DatabaseName);


            /*
            return string.Format(@"
                    Provider = SQLOLEDB;
                    Data Source = {0};
                    Persist Security Info=True;
                    User ID={1}; 
                    Password = {2}; 
                    Initial Catalog={3}", DataSource, UserName, Password, DatabaseName);
             */ 
        }

        public static string ConnStringSqlDb()
        {
            return string.Format(@"
                    server={0};
                    Persist Security Info=True;
                    User ID={1};  
                    Password = {2}; 
                    Initial Catalog={3}", DataSource, UserName, Password, DatabaseName);
        }


        public static void ExcProcedure(string SQL)
        {
            using (OleDbConnection conn = new OleDbConnection(ConnString()))
            {

                conn.Open();
                var cmd = new OleDbCommand(SQL) { Connection = conn, CommandType = CommandType.StoredProcedure };
                cmd.ExecuteScalar();
                conn.Close();

            }

        }


        public static void ExcProcedureParams(string SQL, OleDbCommand cmd)
        {
            using (OleDbConnection conn = new OleDbConnection(ConnString()))
            {

                conn.Open();
                new OleDbCommand(SQL) { Connection = conn, CommandType = CommandType.StoredProcedure };
                cmd.ExecuteScalar();
                conn.Close();

            }

        }



        public static void ExcProcedureNonQuery(string SQL, OleDbCommand cmdName)
        {
            using (OleDbConnection conn = new OleDbConnection(ConnString()))
            {

                conn.Open();
                cmdName = new OleDbCommand(SQL) { Connection = conn, CommandType = CommandType.StoredProcedure };
                cmdName.ExecuteReader();
                conn.Close();

            }

        }


        /*
        public static OleDbDataReader ReaderRowsReturn(string SQL)
        {
            var conn = new OleDbConnection(ConnString());
            conn.Open();
            using (OleDbCommand cmd = new OleDbCommand(SQL) { Connection = conn })
            {

                OleDbDataReader reader = cmd.ExecuteReader();
                reader.Read();
                return reader;

            }
            //conn.Close();
        }

        */
        public static SqlDataReader ReaderRowsReturn(string SQL)
        {
            var conn = new SqlConnection(ConnString());
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL) { Connection = conn })
            {

                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                return reader;

            }
            //conn.Close();
        }



        /*
        public static DataTable SelectSQL(string SQL)
        {
            var conn = new OleDbConnection(ConnString());
            conn.Open();
            OleDbCommand cmd = new OleDbCommand(SQL);
            cmd.Connection = conn;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }
        */

        public static DataTable SelectSQL(string SQL)
        //{
        {
            var conn = new SqlConnection(ConnString());        
            conn.Open();
            SqlCommand cmd = new SqlCommand(SQL);
            cmd.Connection = conn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;            
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }


        public static DataTable SelectSQLTrans(string SQL)
        {
            using (var da = new SqlDataAdapter(SQL, ConnStringSqlDb()))
            {
                var table = new DataTable();
                da.Fill(table);
                return table;
            }
        }

        public static void CmdDelete(string ID, string procedure)
        {

            if (MessageBox.Show(GlobProgram.MesDelRecordBefore, GlobProgram.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                MessageBox.Show(GlobProgram.MesDelRecordCancel, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {

                if (String.IsNullOrEmpty(ID))
                {

                    MessageBox.Show(GlobProgram.MesDelRecordNullId, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;

                }
                DbConnSql.ExcProcedure(string.Format(procedure, ID));


                // DbConnSql.ExcProcedure(string.Format("dbo.usr_DELETE ({0})", ID));

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return;

            }
            MessageBox.Show(GlobProgram.MesDelRecordAfter, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }



        public static void CmdDeleteNew(string Sql)
        {

            if (MessageBox.Show(GlobProgram.MesDelRecordBefore, GlobProgram.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {

                MessageBox.Show(GlobProgram.MesDelRecordCancel, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

            }
            try
            {

                DbConnSql.SelectSQL(string.Format(Sql));
                // DbConnSql.ExcProcedure(string.Format("dbo.usr_DELETE ({0})", ID));

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return;

            }
            MessageBox.Show(GlobProgram.MesDelRecordAfter, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }



    }
}
