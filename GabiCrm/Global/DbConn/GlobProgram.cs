﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;


namespace Global
{
    class GlobProgram
    {



        #region Program Başlığı

        public static string ProgramName = "GABI-PRO";
        #endregion
        #region Hazır Mesajlar
        public static string MesNewRecord = "Yeni Kayıt Eklendi";

        public static string MesUpdRecord = "Kayıt güncellendi";


        public static string MesDelRecordBefore = "Seçili kayıdın silinme işlemini onaylıyor musunuz?";
        public static string MesDelRecordCancel = "Silme işlemi iptal edildi";
        public static string MesDelRecordNullId = "Lütfen önce listeden silinecek kişiyi seçin";
        public static string MesDelRecordAfter = "Seçili kayıt başarıyla silindi";
        public static string MesDelQM = "Seçili kaydın kalite verileri silinecektir.";
        public static string MesDelQMRecordAfter = "Seçili kayıdın kalite verileri başarıyla silindi";


        public static string MesListRecordBefore = "Yüklenecek veri bulunamadı.";



        #endregion
        #region Functions

        public static string IsAssignedCalculator(string TalepNo, string SiraNo)
        {
            string Hesapci = null;

            var dt = new DataTable();
            dt = DbConnSql.SelectSQL(
                 string.Format
                 (
                  @"Select Hesapci from VW_KOTA where SistemTalepNo={0} and SistemTalepSiraNo={1}", TalepNo, SiraNo)
                 );
            Hesapci = dt.Rows[0]["Hesapci"].ToString();
            Hesapci = Hesapci.Length > 1 ? Hesapci : null;


            return (Hesapci);
        }


        public static string RemoveDot(string metin)
        {

            string v = metin.Replace(".", "");
            return (v);
        }

        public static bool FindInString(string metin, string isaret)
        {
            //Ters mantıkta çalışıyor: varsa false döndürür
            string s = metin;
            bool breturn;

            breturn = s.IndexOf(isaret) != -1 ? false : true;
            return breturn;
        }

        public static string Replacer(string metin)
        {

            string v = !string.IsNullOrEmpty(metin) ? metin.Replace(",", ".") : null;
            return (v);
        }
        public static string ReplacerZero2(string metin)
        {

            string v = metin.Replace(",00", "");
            return (v);
        }
        public static string ReplacerZero1(string metin)
        {

            string v = metin.Replace(",0", "");
            return (v);
        }
        public static string ReplacerInternalForDouble(string metin)
        {

            string v = metin.Replace(".", ",");
            return (v);
        }
        public static Double Frac(Double Sayi)
        {

            if ((Double.IsNaN(Sayi)) || (Double.IsNegativeInfinity(Sayi)) || (Double.IsPositiveInfinity(Sayi)))
                return 0;

            //Verilen ondalıklı sayının ondalık kısmını alır
            Decimal SayiAl = Convert.ToDecimal(Sayi);
            Sayi = Sayi - Convert.ToDouble(Decimal.Truncate(SayiAl));
            return Convert.ToDouble(Sayi);





        }
        public static SqlDecimal DblToSqlDecimal(Double Input1)
        {
            //string str_date = "1990/12/03";
            //SqlDateTime dt = new SqlDateTime(DateTime.Parse(str_date)); 
            SqlDecimal dt = new SqlDecimal(Input1);
            return dt;
        }
        public static int FindThenComma(string Value)
        {

            Double Control = Convert.ToDouble(Value);
            if ((Double.IsNaN(Control)) || (Double.IsNegativeInfinity(Control)) || (Double.IsPositiveInfinity(Control)))
                return 0;

            string Ilk = Value;
            if (!string.IsNullOrEmpty(Value))
            {
                //Verilen Double değerin virgülden sonraki ilk hanesini bulur
                Value = ReplacerInternalForDouble(Value);
                Double Calc1 = Convert.ToDouble(Value);
                Calc1 = Frac(Calc1);

                if (Calc1 < 0)
                {
                    if (string.Format("{0}", Calc1).Length >= 3)
                        Value = string.Format("{0}", Calc1).Substring(3, 1);
                    else Value = Ilk;
                }
                else
                {
                    if (string.Format("{0}", Calc1).Length >= 3)
                        Value = string.Format("{0}", Calc1).Substring(2, 1);
                    else Value = Ilk;

                }

                return Convert.ToInt32(Value);
            }
            else
                return 0;
        }
        public static void CleanArray(string[] Dizi)
        {
            for (int i = 0; i < Dizi.Length; i++)
                Dizi[i] = "0";
        }
        public static void CleanArray(double[] Dizi)
        {
            for (int i = 0; i < Dizi.Length; i++)
                Dizi[i] = 0;
        }

        #endregion
        #region Paints
        public static void Paint(DevExpress.XtraEditors.TextEdit txtName, bool Durum)
        {
            if (Durum)
            {
                txtName.BackColor = Color.Red;
                txtName.ForeColor = Color.White;
            }
            else
            {
                txtName.BackColor = Color.FromName("227; 239; 255");
                txtName.ForeColor = Color.Black;
            }


        }
        public static void Paint2(DevExpress.XtraEditors.TextEdit txtName, bool Durum)
        {
            if (Durum)
            {
                txtName.BackColor = Color.Red;
                txtName.ForeColor = Color.White;
            }
            else
            {
                txtName.BackColor = Color.Empty;
                txtName.ForeColor = Color.Black;
            }


        }
        public static void Paint3(DevExpress.XtraEditors.TextEdit txtName, bool Durum)
        {
            if (Durum)
            {
                txtName.BackColor = Color.Red;
                txtName.ForeColor = Color.White;
            }
            else
            {
                txtName.BackColor = Color.White;
                txtName.ForeColor = Color.Black;

            }


        }



        public static int RowHandleNo;
        public static string GetSelectedRows(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            string ret = "";
            if (view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.RowSelect)
            {
                foreach (int i in view.GetSelectedRows())
                {
                    DataRow row = view.GetDataRow(i);
                    //if (ret != "") ret += "\r\n";
                    ret += string.Format("{0} ", i);
                }
            }
            return ret;
        }

        public static void RowHandle(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            view.OptionsSelection.MultiSelect = false;
            RowHandleNo = Convert.ToInt32(GetSelectedRows(view));
        }

        public static void SetBookmark(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            int rowHandle = RowHandleNo;
            if (rowHandle != GridControl.InvalidRowHandle)
            {
                view.FocusedColumn = view.Columns.ColumnByFieldName("gridColumn1");
                view.FocusedRowHandle = rowHandle;
                if (view.IsRowVisible(rowHandle) == RowVisibleState.Hidden)
                    view.MakeRowVisible(rowHandle, false);
                view.ShowEditor();
            }
        }


        #endregion
    }
}
