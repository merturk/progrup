﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
//using DevExpressCs;

namespace Global
{
    public class DbConnSql
    {


        public static string ServerEntes            = "192.168.1.5";//"IFSSERVER";//
        public static string DatabaseNameEntes      = "GABI";
        //**********************************************************************************
        public static string ServerBulent           = @"BULENTERTURK\SQLEXPRESS"; // @"MERTURK\MERTURK";//@".\SQLEXPRESS"; //"MERTURK"; //    
        public static string ServerMurat            =  @"MERTURK\MERTURK";
        //--------------------------------------------------------
        public static string ServerKapital          =  @"SRVTES01";
        public static string DatabaseNameKapital    = "GABI_FACTORING";
        public static string ServerCoskun           =  @"SQLCOSKUN";
        public static string DatabaseNameCoskun     = "GABI-PRO";
        public static string DatabaseNameMurat      = "GABI-PRO";


        public static string UserMurat              = "gabi";
        public static string PasswordMurat          = "ber75*45*hal";

        public static string UserCoskun             = "sa";
        public static string PasswordCoskun         = "coskundna";

        //----------------------------------------------------------
        /*
        public static string DataSource             =  ServerCoskun;
        public static string DatabaseName           =  DatabaseNameCoskun;
        public static string UserName               =  UserCoskun;
        public static string Password               =  PasswordCoskun;

            */
        public static string DataSource             =  ServerMurat;
        public static string DatabaseName           =  DatabaseNameMurat;
        public static string UserName               =  UserMurat;
        public static string Password               =  PasswordMurat;
                      

        public static string UserNameREP            = "RAPOR";
        public static string PasswordREP            = "RAPOR1";

  

        public static string ConnString()
        {

            return string.Format(@"
                    server={0};
                    Persist Security Info=True;
                    User ID={1};  
                    Password = {2}; 
                    Initial Catalog={3}", DataSource, UserName, Password, DatabaseName);

            /*
            return string.Format(@"
                    Provider = SQLOLEDB;
                    Data Source = {0};
                    Persist Security Info=True;
                    User ID={1}; 
                    Password = {2}; 
                    Initial Catalog={3}", DataSource, UserName, Password, DatabaseName);
             */ 
        }

        public static string ConnStringSqlDb()
        {

            return string.Format(@"
                    server={0};
                    Persist Security Info=True;
                    User ID={1};  
                    Password = {2}; 
                    Initial Catalog={3}", DataSource, UserName, Password, DatabaseName);

        }



        public static void ExcProcedure(string SQL)
        {
            using (OleDbConnection conn = new OleDbConnection(ConnString()))
            {

                conn.Open();
                var cmd = new OleDbCommand(SQL) { Connection = conn, CommandType = CommandType.StoredProcedure };
                cmd.ExecuteScalar();
                conn.Close();

            }

        }


        public static void ExcProcedureParams(string SQL, OleDbCommand cmd)
        {
            using (OleDbConnection conn = new OleDbConnection(ConnString()))
            {

                conn.Open();
                new OleDbCommand(SQL) { Connection = conn, CommandType = CommandType.StoredProcedure };
                cmd.ExecuteScalar();
                conn.Close();

            }

        }



        public static void ExcProcedureNonQuery(string SQL, OleDbCommand cmdName)
        {
            using (OleDbConnection conn = new OleDbConnection(ConnString()))
            {

                conn.Open();
                cmdName = new OleDbCommand(SQL) { Connection = conn, CommandType = CommandType.StoredProcedure };
                cmdName.ExecuteReader();
                conn.Close();

            }

        }


        /*
        public static OleDbDataReader ReaderRowsReturn(string SQL)
        {
            var conn = new OleDbConnection(ConnString());
            conn.Open();
            using (OleDbCommand cmd = new OleDbCommand(SQL) { Connection = conn })
            {

                OleDbDataReader reader = cmd.ExecuteReader();
                reader.Read();
                return reader;

            }
            //conn.Close();
        }

        */
        public static SqlDataReader ReaderRowsReturn(string SQL)
        {
            var conn = new SqlConnection(ConnString());
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL) { Connection = conn })
            {

                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                return reader;

            }
            //conn.Close();
        }



        /*
        public static DataTable SelectSQL(string SQL)
        {
            var conn = new OleDbConnection(ConnString());
            conn.Open();
            OleDbCommand cmd = new OleDbCommand(SQL);
            cmd.Connection = conn;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }
        */

        public static DataTable SelectSQL(string SQL)
        {
            
            var conn = new SqlConnection(ConnString());
            conn.Open();
            SqlCommand cmd = new SqlCommand(SQL);
            cmd.Connection = conn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        public static void CmdDelete(string ID, string procedure)
        {

            if (MessageBox.Show(Glob.MesDelRecordBefore, Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {

                MessageBox.Show(Glob.MesDelRecordCancel, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

            }
            try
            {

                if (String.IsNullOrEmpty(ID))
                {

                    MessageBox.Show(Glob.MesDelRecordNullId, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;

                }
                DbConnSql.ExcProcedure(string.Format(procedure, ID));


                // DbConnSql.ExcProcedure(string.Format("dbo.usr_DELETE ({0})", ID));

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return;

            }
            MessageBox.Show(Glob.MesDelRecordAfter, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }



        public static void CmdDeleteNew(string Sql)
        {

            if (MessageBox.Show(Glob.MesDelRecordBefore, Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {

                MessageBox.Show(Glob.MesDelRecordCancel, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

            }
            try
            {

            
                DbConnSql.SelectSQL(string.Format(Sql));


                // DbConnSql.ExcProcedure(string.Format("dbo.usr_DELETE ({0})", ID));

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return;

            }
            MessageBox.Show(Glob.MesDelRecordAfter, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }



    }
}
