﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Global;
using DevExpress.XtraGrid;

namespace Global
{
    public static class DACmd
    {

        #region SİLME İŞLEMLERİ

        public static void CmdDelete(string procedure,string ID,string DbName)
        {

            if (MessageBox.Show(Glob.MesDelRecordBefore, Glob.ProgramName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                MessageBox.Show(Glob.MesDelRecordCancel, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {

                if (String.IsNullOrEmpty(ID))
                {
                    MessageBox.Show(Glob.MesDelRecordNullId, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
                DbConnSql.SelectSQL(string.Format(procedure, ID, DbName));


                // DbConnSql.ExcProcedure(string.Format("dbo.usr_DELETE ({0})", ID));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            MessageBox.Show(Glob.MesDelRecordAfter, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        
        
        #endregion

    }
}
