﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using Global;
using DevExpress.XtraGrid;




namespace Global
{
    public class DABindings
    {
        
        //PARAMETRE EKRANLARI
        public static string[] RdValuesGeneralParams = new string[50];
        public static string[] RdValuesGeneralParamsAGA = new string[50];
        public static string[] RdValuesGeneralParamsTOR = new string[50];
        public static string[] RdValuesGeneralParamsOGG = new string[50];

        public static string[] RdValuesDensity = new string[20];
        public static string[] RdValuesVoltageLevel = new string[20];

        //OG AKIM HESAP
        public static string[] RdValuesOCCVoltageLevel = new string[20];
        public static string[] RdValuesOCCDensity = new string[20];
        public static string[] RdOccValuesGeneralParams = new string[20];
        //OG AKIM HESAP SEKONDER
        public static string[] RdValuesOCCWireDia   = new string[20]; /*Tel Çapları*/
        public static string[] RdValuesOCCGaus      = new string[20]; /*Gaus değerleri*/
        //MIX için Gaus
        public static string[] RdValuesMixGaus      = new string[5];
        //Sekonder Kalıpları
        public static string[] RdValuesSecMouldMeas = new string[6];

        //AUTO HESAPLAMA ALANLARI 
        public static string[] RdValuesAutoCopperCord = new string[5];
        public static string[] RdValuesAutoOGAMouldName = new string[1];

        //RAPORLAMA İÇİN
        public static string[] RdValuesCalcPlanning = new string[5];
        //OGG İÇİN EMAYE TABLOSUNDA VERİ GELİŞİ
        public static string[] RdValuesEmaye = new string[1];


        

        public static void GetXtraGridBind(GridControl GridName, string Sql)
        {
            //xtra grid e data bind etmek için kullanılır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show(Glob.MesListRecordBefore, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridName.DataSource = ds;
        }

        public static void GetXtraGridBindNoDate(GridControl GridName, string Sql)
        {
            //xtra grid e boş data döndürür
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);            
            GridName.DataSource = ds;
        }

        public static void GetTxtValueTop1Bind(string Sql, string[] Dizi, string MesageDataEmpty)
        {
            //Tek satırlık veri dödürmek için kullanılır, diziye atılan değerler kolon bazındadır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show(MesageDataEmpty, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            for (int i = 0; i < ds.Columns.Count; i++)
            { 
                Dizi[i] = Convert.ToString(ds.Rows[0][i]); 
            }       
        }


        public static void GetTxtValueTop1BindNm(string Sql, string[] Dizi)
        {
            //Tek satırlık veri döndürmek için kullanılır, diziye atılan değerler kolon bazındadır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
              //  MessageBox.Show(MesageDataEmpty, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }         

            for (int i = 0; i < ds.Columns.Count; i++)
            {
                Dizi[i] = Convert.ToString(ds.Rows[0][i]);
            }
        }

        public static void GetTxtValueTop1BindNmRows(string Sql, string[] Dizi)
        {
            //Tek satırlık veri döndürmek için kullanılır, diziye atılan değerler satır bazındadır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
                //  MessageBox.Show(MesageDataEmpty, GlobProgram.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                //Dizimiz 1 den başlasın
                Dizi[i+1] = Convert.ToString(ds.Rows[i][0]);
            }
        }


        public static void GetComboBind(System.Windows.Forms.ComboBox ComboBoxName, string Sql,string Member )
        {
            //Combo Box a veri bind etmek için kullanılır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show(Glob.MesListRecordBefore, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ComboBoxName.DataSource = ds;
            ComboBoxName.DisplayMember = Member;        
        }

        public static void GetXtraComboBind(DevExpress.XtraEditors.ComboBoxEdit ComboBoxName, string Sql)
        {
            //Xtra Comboya veri bind etmek için kullanılır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show(Glob.MesListRecordBefore, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                ComboBoxName.Properties.Items.Add(ds.Rows[i][0]);
              
            }   
        }


        public static void GetXtraLookupBind(DevExpress.XtraEditors.LookUpEdit LookUpName, string Sql, string MesageDataEmpty)
        {
            //Extra Loook up veri bind etmek için kullanılır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
                MessageBox.Show(MesageDataEmpty, Glob.ProgramName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //Kayıt yoksa listeyi boşaltması için
                LookUpName.Properties.DataSource = ds;
                return;
            }

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                LookUpName.Properties.DataSource = ds;
                
                //comboBoxEditSample.SelectedIndex = 0;
                //txtSelectedIndexValue.Text = string.Format("{0} / {1}", comboBoxEditSample.SelectedIndex, comboBoxEditSample.EditValue);
            }
        }
        public static void GetXtraLookupBindNm(DevExpress.XtraEditors.LookUpEdit LookUpName, string Sql)
        {
            //Extra Loook up veri bind etmek için kullanılır
            var ds = new DataTable();
            ds = DbConnSql.SelectSQL(Sql);
            if ((ds.Rows.Count == 0))
            {
              
                //Kayıt yoksa listeyi boşaltması için
                LookUpName.Properties.DataSource = ds;
                return;
            }

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                LookUpName.Properties.DataSource = ds;

                
            }
        }
     


    }
}
